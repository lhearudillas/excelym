/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 * @NAmdConfig  /SuiteScripts/excelym/config/configuration.json
 */
define([ 'N/search', 'N/error', 'EYM/preferences' ],

function( search, error, prex ) {
   
    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {string} scriptContext.type - Trigger type
     * @param {Form} scriptContext.form - Current form
     * @Since 2015.2
     */
    function beforeLoad(scriptContext) {

    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function beforeSubmit(scriptContext) 
    {
    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function afterSubmit(scriptContext) 
    {
    	// Assign scriptContext to a shorter variable.
    	var cntx 	= scriptContext
    	var typ 	= cntx.UserEventType;
    	
    	// Run the code for EDIT operations.
    	if( cntx.type === typ.XEDIT ) {
    		
    		// Declare context constants.
    		const BMU_BQIFLD_STR = 'custrecord_eym_eur_h_bqi';
    		
    		// Declare context variables.
    		var rec, projLookup, projValue, projText = null;
    		var errorArray = [];
    		
        	// Get the old and the new record.
        	var oRec = cntx.oldRecord;
        	var nRec = cntx.newRecord;
        	
        	// Get the values of the BQI field.
        	var oBqi = oRec.getValue( {fieldId: BMU_BQIFLD_STR} );
        	var nBqi = nRec.getValue( {fieldId: BMU_BQIFLD_STR} ); 
        	
    		// Load the record for editing. Context records cannot be edited.
        	try{
        		rec = record.load( {type: 'customrecord_eym_eur_header', id: nRec.id} );
        	} catch (e) {
        		log.debug({
        			title: 'Error loading record',
        			details: e.message
        		});
        	}
        	
        	// If the record was loaded, continue the script.
        	if( rec ){
        		
        		// Get the project field, the subdepartment, and status.
        		var proj = rec.getValue( {fieldId: 'custrecord_eym_deur_h_proj'} );
        		var subd = rec.getValue( {fieldId: 'custrecord_eym_deur_h_ed_subdept'} );
        		var stat = rec.getValue( {fieldId: 'custrecord_eym_eur_h_status'} );
        		
        		// Search for the project in the new bqi record.
        		try{
	        		projLookup = search.lookupFields({
	        		    type: 'customrecord_eym_boq',
	        		    id: nBqi,
	        		    columns: ['custrecord_eym_boq_projid']
	        		});
        		} catch( e ) {
        			log.debug({
        				title: 'Error in searching the BQI.',
        				details: e.message	
        			})
        		}
        		
        		// Check if the search was performed.
        		if( projLookup && Object.keys(projLookup).length > 0) {
        			
        			// Get the value
        			try {
        				projValue 	= projLookup.custrecord_eym_boq_projid[0].value;
        				projText 	= projLookup.custrecord_eym_boq_projid[0].text;
        			} catch (e) {
        				log.debug({
        					title: 'Project cannot be retrieved from the search',
        					details: e.message
        				})
        			}
        			
        			// Compare the project value from the search and from the EUR.
        			if( proj != projValue ) {
        				
        				// Undo the bqi value.
        				undoBqi({
        					record: rec,
        					oldValue: oBqi,
        					fieldId: BMU_BQIFLD_STR
        				});
        				
        				// Push an error to the error array.
        				errorArray.push( 'BQI' + nBqi + ' does not belong to Project ' + projText );
        			}
        		} else {
    				// Undo the bqi value.
    				undoBqi({
    					record: rec,
    					oldValue: oBqi,
    					fieldId: BMU_BQIFLD_STR
    				});
    				
    				// Push an error to the error array.
    				errorArray.push( 'BQI' + nBqi + ' does not belong to Project ' + projText );   			
        		}
        		
        		// Check if there are errors in the error array.
        		if( errorArray.length > 0 ){
        			var errorString = 'Errors: ';
        			errorArray.forEach(function(error){
        				errorString += error + ', ';
        			});
        			
        			// Create an error.
        			var bqiError = error.create({
        				name: 'BQI_ERROR',
        				message: errorString,
        				notifyOff: true
        			});
        			
        			// Throw the error to stop execution.
        			throw bqiError;
        		}
        		
        	} else {
        		log.debug({
        			title: 'No record available',
        			details: 'No record was loaded'
        		});
        	}
    	}  
    }
    
    function undoBqi( options )
    {
    	// Assign the record to a variable.
    	var rec = options.record;
    	
    	// Set the bqi to the old value.
    	rec.setValue({
    		fieldId: options.fieldId,
    		value: options.oldValue
    	});
    	
    	// Save the record.
    	return rec.save({});
    }

    return {
        afterSubmit: afterSubmit
    };
    
});
