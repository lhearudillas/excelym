/* XML Manipulation */
function addNodeFrPath(xmlDoc, xmlPath, nodeName) 
{		
	var newNode = null;
	
	var parentNode = nlapiSelectNode(xmlDoc, xmlPath);
	if (parentNode != null) {
		newNode = addNodeFrNode(xmlDoc, parentNode, nodeName);
	}
	
	return newNode;  
}

function addNodeFrNode(xmlDoc, parentNode, nodeName) 
{	
	var newNode = null;
	
	if (parentNode != null) {
		newNode = xmlDoc.createElement(nodeName);
		parentNode.appendChild(newNode);			
	}
	
	return newNode;
}

function addTextNodeFrPath(xmlDoc, xmlPath, nodeName, nodeValue) 
{
	var newNode = null;	
	
	if (xmlDoc != null) {
		var parentNode = nlapiSelectNode(xmlDoc, xmlPath);
		if (parentNode != null) {
			newNode = addTextNodeFrNode(xmlDoc, parentNode, nodeName, nodeValue);
		}
	}
	
	return newNode;
}

function addTextNodeFrNode(xmlDoc, parentNode, nodeName, nodeValue) 
{
	var newNode = null;	

	if (xmlDoc != null && parentNode != null && !IsNullOrEmpty(nodeName) && !IsNullOrEmpty(nodeValue)) {
		newNode = xmlDoc.createElement(nodeName);
		newNode.appendChild(xmlDoc.createTextNode(nodeValue));
		parentNode.appendChild(newNode);
	}
	
	return newNode;
}

function setValueFrPath(node, xmlPath, nodeValue) 
{
	var textNode = nlapiSelectNode(node, xmlPath);
	setValueFrNode(textNode, nodeValue);
}

function setValueFrNode(node, nodeValue) 
{	
	if (node != null)
		node.firstChild.nodeValue = nodeValue;
}

function getValueFrPath(node, xmlPath) 
{
	var	textNode = nlapiSelectNode(node, xmlPath);		
	return getValueFrNode(textNode);
}

function getValueFrNode(node) 
{
	var	value = '';
	
	if (node != null) {
		if (node.firstChild != null)
			value = node.firstChild.nodeValue;
	}
	
	return value;	
}

function getNode(xmlDoc, xmlPath) 
{
	return nlapiSelectNode(xmlDoc, xmlPath);	
}

function getNodes(xmlDoc, xmlPath) 
{
	return nlapiSelectNodes(xmlDoc, xmlPath);	
}

function removeNode(xmlDoc, xmlPath) 
{
	var delNode = nlapiSelectNode(xmlDoc, xmlPath);
	delNode.parentNode.removeChild(delNode);
} 

function setAttriValue(node, tag, value)
{
	if(node != null)
		node.setAttribute(tag, value);
}

function getAttriValue(node, tag) 
{
	var value = '';
	if(node != null)
		value = node.getAttribute(tag);
		
	return value;
}

function generateResponse(xmlDocRes, rootName) 
{
	var response = addNodeFrPath(xmlDocRes, rootName, 'response');					
	addTextNodeFrNode(xmlDocRes, response, 'errorcode', '1');
	addTextNodeFrNode(xmlDocRes, response, 'errormessage', 'SUCCESS');
	stacktrace = addNodeFrNode(xmlDocRes, response, 'stacktrace');	
	addTextNodeFrNode(xmlDocRes, stacktrace, 'failed', '0');	
	addTextNodeFrNode(xmlDocRes, stacktrace, 'succeeded', '0');
	
	return response;
}

function setError(xmlDocRes, rootName, errCd, errMsg) 
{
	var response = getNode(xmlDocRes, rootName + '/response');
	if (response == null) {
		response = generateResponse(xmlDocRes, rootName);
	}
	
	setValueFrPath(response, 'errorcode', errCd);
	setValueFrPath(response, 'errormessage', errMsg);
}

function setFailures(xmlDocRes, rootName, schema, id, errMsg) 
{
	var response = getNode(xmlDocRes, rootName + '/response');
	if (response == null) {
		response = generateResponse(xmlDocRes, rootName);
	}
	
	var failuresNode = getNode(response, 'stacktrace/failures');
	if (failuresNode == null) {
		failuresNode = addNodeFrPath(xmlDocRes, rootName + '/response/stacktrace', 'failures');		
	}
		
	var failureNode = addTextNodeFrNode(xmlDocRes, failuresNode, 'failure', errMsg);
	setAttriValue(failureNode, "schema", schema);
	setAttriValue(failureNode, "id", id);
	
	var failedCnt = parseInt(getValueFrPath(response,'stacktrace/failed'));
	failedCnt += 1;
	setValueFrPath(response,'stacktrace/failed', failedCnt);
}

function setSucceeded(xmlDocRes, rootName) 
{
	var response = getNode(xmlDocRes, rootName + '/response');
	if (response == null) {
		response = generateResponse(xmlDocRes, rootName);
	}
		
	var successCnt = parseInt(getValueFrPath(response,'stacktrace/succeeded'));
	successCnt += 1;
	setValueFrPath(response,'stacktrace/succeeded', successCnt);
}

