/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       13 Mar 2018     EYM-003
 *
 */

/**
 *
 * Excelym object helper
 *
 */
 
 
 function isNullOrEmpty(value) {
	var nullOrEmpty = true;
	
	if (value != undefined && value != null) {
		if (typeof (value) == 'string') {
			if (value.length > 0)
				nullOrEmpty = false;
		}else {
			nullOrEmpty = false;
		}
	}		
	
	return nullOrEmpty;
}

function isIndexOfArray(arr, value)
{
	var indexOfArray = false;
	
	if(!isNullOrEmpty(arr) && Array.isArray(arr)) {
		if(arr.indexOf(value) >= 0) {
			indexOfArray = true;
		}
	}
	
	return indexOfArray;
}

function toCurrency(amount)
{
	if (!amount) {
		amount = "0.00";
	}
	
	amount = parseFloat(Math.round(amount * 100) / 100).toFixed(2);
	
	return amount.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
}

function getCurrentDate() 
{
	var today = new Date();
	var day = today.getDate();
	var month = today.getMonth() + 1; 
	var year = today.getFullYear();
	
	if(day < 10){
	    day = '0' + day;
	} 
	if(month < 10){
	    month = '0' + month;
	}
	
	today = month + '/' + day + '/' + year;
	
	return today;
}


