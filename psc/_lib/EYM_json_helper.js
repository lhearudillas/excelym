function jsonResponse(errCd, errMsg, total, data)
{	
	var ret = {		
		"success":errCd == 0 ? true : false,
		"error_code":errCd,
		"error_message":errMsg,
		"total":total,
		"data":data 
	}
	
	return ret;
}

function jsonResponse2(errCd, errMsg, total, lastpage, totalrows, data)
{	
	var ret = {		
		"success":errCd == 0 ? true : false,
		"error_code":errCd,
		"error_message":errMsg,
		"total":total,
		"lastpage":lastpage,
		"totalrows":totalrows,
		"data":data 
	}
	
	return ret;
}

function jsonToString(obj)
{
	var s = '';
	
	try {
		
		s = JSON.stringify(obj);
	
	}catch(ex) {}
	
	return s;
}

function jsonToObject(str) 
{
	var o = null;
	
	try {
		
		var tmp = JSON.parse(str);
		if(typeof(tmp) == 'string') {
			o = JSON.parse(tmp)
		}else {
			o = tmp;
		}
		
	}catch(ex){}
	
	return o;
}