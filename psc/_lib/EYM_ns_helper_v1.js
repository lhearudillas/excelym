function getNSRecord(recordNm, fieldNm, fieldVal, operator) 
{
	var filters = new Array();				
	filters.push(new nlobjSearchFilter(fieldNm, null, operator, fieldVal));
	
	var rec = null;
	var res =  nlapiSearchRecord(recordNm, null, filters);
	if (res != null && res.length > 0) {
		var recordId = res[0].getId();
		var recordType = res[0].getRecordType();
		rec = nlapiLoadRecord(recordType, recordId, {recordmode: 'dynamic'});
	}
	
	return rec;
}

function getNSRecordId(recordNm, fieldNm, fieldVal, operator) 
{
	var recordId = null
	var filters = new Array();				
	filters.push(new nlobjSearchFilter(fieldNm, null, operator, fieldVal));
	
	var rec = null;
	var res =  nlapiSearchRecord(recordNm, null, filters);
	if (res != null && res.length > 0) {
		recordId = res[0].getId();		
	}
	
	return recordId;
}

function getNSFieldValue(recordNm, fieldNm, fieldVal, operator, column)
{
	var fieldValue = null
	
	var columns = new Array();
	columns.push(new nlobjSearchColumn(column));
	
	var filters = new Array();				
	filters.push(new nlobjSearchFilter(fieldNm, null, operator, fieldVal));
	
	var res =  nlapiSearchRecord(recordNm, null, filters, columns);
	if (res != null) {
		fieldValue = res[0].getValue(column);		
	}
	
	return fieldValue;
}

function getNSRecordType(recType, recId) 
{
	return nlapiLookupField(recType, recId, 'recordtype');
}

function createNSRecord(recType, recFields)
{
	var rec = nlapiCreateRecord(recType);

	for (var oname in recFields) {
		rec.setFieldValue(oname, recFields[oname]);				
	}
	
	return nlapiSubmitRecord(rec);
}

function updateNSRecord(recType, recId, recFields)
{
	var fields = new Array();
	var values = new Array();

	for (var oname in recFields) {
		fields.push(oname);
		values.push(recFields[oname]);		
	}
	
	if(fields.length > 0)
		nlapiSubmitField(recType, recId, fields, values);
	
	return recId; 
}

function closeTransaction(recordType, recordId, bodyFields, error) 
{	
	try {
		var trxRec = nlapiLoadRecord(recordType, recordId); 
		
		for (var oname in bodyFields) {    						
			trxRec.setFieldValue(oname, bodyFields[oname]);					
		}
		
		if(trxRec != null){
			var lineCtr = trxRec.getLineItemCount('item');		
			for (var i = 1; i <= lineCtr; i++)
			{
				trxRec.selectLineItem('item', i);
				trxRec.setCurrentLineItemValue('item', 'isclosed', 'T');
				trxRec.commitLineItem('item');
			}
			
			nlapiSubmitRecord(trxRec, true, true);			
		}else {
			error.Message = 'Record not found.';
			nlapiLogExecution('DEBUG', 'NETSUITE', error.Message);	
		}
	}catch(e) {
		if (e instanceof nlobjError) {
          	error.Code = e.getCode();
			error.Message = e.getDetails()
			nlapiLogExecution('DEBUG', 'NETSUITE', error.Message);				
		}else {						
			error.Message = 'UNEXPECTED ERROR:' + e.toString();
			nlapiLogExecution('DEBUG', 'NETSUITE', error.Message);			
		}
	}	
}

function getMappingJSONFiles(fileId){
	if(fileId){
		var psFile = nlapiLoadFile(fileId);
		if(psFile){
			var prop = psFile.getValue(); 
			var jsonVal = JSON.parse(prop);
			return jsonVal;	
		}
	}
	return null;
}

function getFileId(filename){
	var fileId = null;
	if(filename){
		var filter = new Array();
		filter.push(new nlobjSearchFilter('name', 'file', 'is', filename));
		var column = new nlobjSearchColumn('internalid', 'file');
		var searchResult = nlapiSearchRecord('folder', null , filter , column);
		if(searchResult){
		fileId = searchResult[0].getValue('internalid','file');
		}
	}
	
	return fileId;
}

function isoToDate(isoDate){
	if(isoDate){
		nlapiLogExecution('DEBUG','ISO DATE',isoDate);
		var stringDate = isoDate.slice(0,10);
		var a = stringDate.split(' '); // break date from time
		var d = a[0].split('-'); // break date into year, month day
		var retDate = new Date(d[0], d[1]-1, d[2]);
		return nlapiDateToString(retDate);
	}
	return null;
}


function toISODate(tranDate){
	if(tranDate){
		
		var d = nlapiStringToDate(tranDate);
		var isoDate = d.toISOString();
		
		return isoDate;
		
	}
  
	return null;
}

function getInventoryItems(itemIds, columnFields)
{		
	var filters = new Array();	
	filters[0] = new nlobjSearchFilter('formulatext', null, 'is', '1');
	filters[0].setFormula("CASE WHEN {internalid} in(" +  itemIds.join(",") + ") THEN '1' ELSE '0' END");
	filters[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F')
	
	var columns = new Array();
	columns.push(new nlobjSearchColumn('internalid'));
	columns.push(new nlobjSearchColumn('displayname'));
	for(var i = 0; i < columnFields.length; i++) {
		columns.push(new nlobjSearchColumn(columnFields[i]));
	}
	
	return nlapiSearchRecord('item', null, filters, columns);
}

function getInventoryItemsByUpcs(upcCodes, columnFields)
{	
	var quotedAndCommaSeparated = "'" + upcCodes.join("','") + "'";
	
	var filterItems = new Array();	
	filterItems[0] = new nlobjSearchFilter('formulatext', null, 'is', '1');
	filterItems[0].setFormula("CASE WHEN {upccode} in(" + quotedAndCommaSeparated + ") THEN '1' ELSE '0' END");
	filterItems[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F')
	
	var columns = new Array();
	columns.push(new nlobjSearchColumn('internalid'));
	columns.push(new nlobjSearchColumn('displayname'));
	if(columnFields != null) {
		for(var i = 0; i < columnFields.length; i++) {
			columns.push(new nlobjSearchColumn(columnFields[i]));
		}
	}
	
	return nlapiSearchRecord('item', null, filterItems, columns);
}

function getInventoryItem(itemRess, itemId)
{		
	var itemRes = null;
	if(itemRess != null) {
		for(var i = 0; i < itemRess.length; i++) {
			itemRes = itemRess[i];
			if(itemRes.getValue('internalid') == itemId) {
				break;
			}
		}
	}
	
	return itemRes;
}

function getInventoryItemByPriceLevel(itemRess, itemId, priceLevel)
{		
	var itemRes = null;
	if(itemRess != null) {
		for(var i = 0; i < itemRess.length; i++) {
			itemRes = itemRess[i];
			if(itemRes.getValue('internalid') == itemId && itemRes.getValue('pricelevel', 'pricing') == priceLevel) {
				break;
			}
		}
	}
	
	return itemRes;
}

function getFileBody(filename){
	
	if(filename){
		
		var fileId = null;
		
		var filter = new Array();
			filter.push(new nlobjSearchFilter('name', 'file', 'is', filename));
		var column = new nlobjSearchColumn('internalid', 'file');
		
		var searchResult = nlapiSearchRecord('folder', null , filter , column);
		if(searchResult){
			fileId = searchResult[0].getValue('internalid','file');
		}
		
		if(fileId){
			getMappingJSONFiles(fileId);
		}
		
	}
	
}

function isProductionEnvironment(){
	
	var context = nlapiGetContext();
	if (context.getEnvironment() == 'PRODUCTION') {	
		return true;
	}
	return false;
}

function replaceNull(value) 
{
	return value == null ? '' : nlapiEscapeXML(value);
}

function getNSCountryCode(filter)
{
	var countryCode = null;
	
	if(filter == 'AD' || filter == 'Andorra')
		countryCode = 'AD';
	if(filter == 'AE' || filter == 'United Arab Emirates')
		countryCode = 'AE';
	if(filter == 'AF' || filter == 'Afghanistan')
		countryCode = 'AF';
	if(filter == 'AG' || filter == 'Antigua and Barbuda')
		countryCode = 'AG';
	if(filter == 'AI' || filter == 'Anguilla')
		countryCode = 'AI';
	if(filter == 'AL' || filter == 'Albania')
		countryCode = 'AL';
	if(filter == 'AM' || filter == 'Armenia')
		countryCode = 'AM';
	if(filter == 'AO' || filter == 'Angola')
		countryCode = 'AO';
	if(filter == 'AQ' || filter == 'Antarctica')
		countryCode = 'AQ';
	if(filter == 'AR' || filter == 'Argentina')
		countryCode = 'AR';
	if(filter == 'AS' || filter == 'American Samoa')
		countryCode = 'AS';
	if(filter == 'AT' || filter == 'Austria')
		countryCode = 'AT';
	if(filter == 'AU' || filter == 'Australia')
		countryCode = 'AU';
	if(filter == 'AW' || filter == 'Aruba')
		countryCode = 'AW';
	if(filter == 'AX' || filter == 'Aland Islands')
		countryCode = 'AX';
	if(filter == 'AZ' || filter == 'Azerbaijan')
		countryCode = 'AZ';
	if(filter == 'BA' || filter == 'Bosnia and Herzegovina')
		countryCode = 'BA';
	if(filter == 'BB' || filter == 'Barbados')
		countryCode = 'BB';
	if(filter == 'BD' || filter == 'Bangladesh')
		countryCode = 'BD';
	if(filter == 'BE' || filter == 'Belgium')
		countryCode = 'BE';
	if(filter == 'BF' || filter == 'Burkina Faso')
		countryCode = 'BF';
	if(filter == 'BG' || filter == 'Bulgaria')
		countryCode = 'BG';
	if(filter == 'BH' || filter == 'Bahrain')
		countryCode = 'BH';
	if(filter == 'BI' || filter == 'Burundi')
		countryCode = 'BI';
	if(filter == 'BJ' || filter == 'Benin')
		countryCode = 'BJ';
	if(filter == 'BL' || filter == 'Saint Barthélemy')
		countryCode = 'BL';
	if(filter == 'BM' || filter == 'Bermuda')
		countryCode = 'BM';
	if(filter == 'BN' || filter == 'Brunei Darrussalam')
		countryCode = 'BN';
	if(filter == 'BO' || filter == 'Bolivia')
		countryCode = 'BO';
	if(filter == 'BQ' || filter == 'Bonaire, Saint Eustatius, and Saba')
		countryCode = 'BQ';
	if(filter == 'BR' || filter == 'Brazil')
		countryCode = 'BR';
	if(filter == 'BS' || filter == 'Bahamas')
		countryCode = 'BS';
	if(filter == 'BT' || filter == 'Bhutan')
		countryCode = 'BT';
	if(filter == 'BV' || filter == 'Bouvet Island')
		countryCode = 'BV';
	if(filter == 'BW' || filter == 'Botswana')
		countryCode = 'BW';
	if(filter == 'BY' || filter == 'Belarus')
		countryCode = 'BY';
	if(filter == 'BZ' || filter == 'Belize')
		countryCode = 'BZ';
	if(filter == 'CA' || filter == 'Canada')
		countryCode = 'CA';
	if(filter == 'CC' || filter == 'Cocos (Keeling) Islands')
		countryCode = 'CC';
	if(filter == 'CD' || filter == 'Congo, Democratic People\'s Republic')
		countryCode = 'CD';
	if(filter == 'CF' || filter == 'Central African Republic')
		countryCode = 'CF';
	if(filter == 'CG' || filter == 'Congo, Republic of')
		countryCode = 'CG';
	if(filter == 'CH' || filter == 'Switzerland')
		countryCode = 'CH';
	if(filter == 'CI' || filter == 'Cote d\'Ivoire')
		countryCode = 'CI';
	if(filter == 'CK' || filter == 'Cook Islands')
		countryCode = 'CK';
	if(filter == 'CL' || filter == 'Chile')
		countryCode = 'CL';
	if(filter == 'CM' || filter == 'Cameroon')
		countryCode = 'CM';
	if(filter == 'CN' || filter == 'China')
		countryCode = 'CN';
	if(filter == 'CO' || filter == 'Colombia')
		countryCode = 'CO';
	if(filter == 'CR' || filter == 'Costa Rica')
		countryCode = 'CR';
	if(filter == 'CU' || filter == 'Cuba')
		countryCode = 'CU';
	if(filter == 'CV' || filter == 'Cape Verde')
		countryCode = 'CV';
	if(filter == 'CW' || filter == 'Curacao')
		countryCode = 'CW';
	if(filter == 'CX' || filter == 'Christmas Island')
		countryCode = 'CX';
	if(filter == 'CY' || filter == 'Cyprus')
		countryCode = 'CY';
	if(filter == 'CZ' || filter == 'Czech Republic')
		countryCode = 'CZ';
	if(filter == 'DE' || filter == 'Germany')
		countryCode = 'DE';
	if(filter == 'DJ' || filter == 'Djibouti')
		countryCode = 'DJ';
	if(filter == 'DK' || filter == 'Denmark')
		countryCode = 'DK';
	if(filter == 'DM' || filter == 'Dominica')
		countryCode = 'DM';
	if(filter == 'DO' || filter == 'Dominican Republic')
		countryCode = 'DO';
	if(filter == 'DZ' || filter == 'Algeria')
		countryCode = 'DZ';
	if(filter == 'EA' || filter == 'Ceuta and Melilla')
		countryCode = 'EA';
	if(filter == 'EC' || filter == 'Ecuador')
		countryCode = 'EC';
	if(filter == 'EE' || filter == 'Estonia')
		countryCode = 'EE';
	if(filter == 'EG' || filter == 'Egypt')
		countryCode = 'EG';
	if(filter == 'EH' || filter == 'Western Sahara')
		countryCode = 'EH';
	if(filter == 'ER' || filter == 'Eritrea')
		countryCode = 'ER';
	if(filter == 'ES' || filter == 'Spain')
		countryCode = 'ES';
	if(filter == 'ET' || filter == 'Ethiopia')
		countryCode = 'ET';
	if(filter == 'FI' || filter == 'Finland')
		countryCode = 'FI';
	if(filter == 'FJ' || filter == 'Fiji')
		countryCode = 'FJ';
	if(filter == 'FK' || filter == 'Falkland Islands')
		countryCode = 'FK';
	if(filter == 'FM' || filter == 'Micronesia, Federal State of')
		countryCode = 'FM';
	if(filter == 'FO' || filter == 'Faroe Islands')
		countryCode = 'FO';
	if(filter == 'FR' || filter == 'France')
		countryCode = 'FR';
	if(filter == 'GA' || filter == 'Gabon')
		countryCode = 'GA';
	if(filter == 'GB' || filter == 'United Kingdom')
		countryCode = 'GB';
	if(filter == 'GD' || filter == 'Grenada')
		countryCode = 'GD';
	if(filter == 'GE' || filter == 'Georgia')
		countryCode = 'GE';
	if(filter == 'GF' || filter == 'French Guiana')
		countryCode = 'GF';
	if(filter == 'GG' || filter == 'Guernsey')
		countryCode = 'GG';
	if(filter == 'GH' || filter == 'Ghana')
		countryCode = 'GH';
	if(filter == 'GI' || filter == 'Gibraltar')
		countryCode = 'GI';
	if(filter == 'GL' || filter == 'Greenland')
		countryCode = 'GL';
	if(filter == 'GM' || filter == 'Gambia')
		countryCode = 'GM';
	if(filter == 'GN' || filter == 'Guinea')
		countryCode = 'GN';
	if(filter == 'GP' || filter == 'Guadeloupe')
		countryCode = 'GP';
	if(filter == 'GQ' || filter == 'Equatorial Guinea')
		countryCode = 'GQ';
	if(filter == 'GR' || filter == 'Greece')
		countryCode = 'GR';
	if(filter == 'GS' || filter == 'South Georgia')
		countryCode = 'GS';
	if(filter == 'GT' || filter == 'Guatemala')
		countryCode = 'GT';
	if(filter == 'GU' || filter == 'Guam')
		countryCode = 'GU';
	if(filter == 'GW' || filter == 'Guinea-Bissau')
		countryCode = 'GW';
	if(filter == 'GY' || filter == 'Guyana')
		countryCode = 'GY';
	if(filter == 'HK' || filter == 'Hong Kong')
		countryCode = 'HK';
	if(filter == 'HM' || filter == 'Heard and McDonald Islands')
		countryCode = 'HM';
	if(filter == 'HN' || filter == 'Honduras')
		countryCode = 'HN';
	if(filter == 'HR' || filter == 'Croatia/Hrvatska')
		countryCode = 'HR';
	if(filter == 'HT' || filter == 'Haiti')
		countryCode = 'HT';
	if(filter == 'HU' || filter == 'Hungary')
		countryCode = 'HU';
	if(filter == 'IC' || filter == 'Canary Islands')
		countryCode = 'IC';
	if(filter == 'ID' || filter == 'Indonesia')
		countryCode = 'IC';
	if(filter == 'IE' || filter == 'Ireland')
		countryCode = 'IE';
	if(filter == 'IL' || filter == 'Israel')
		countryCode = 'IL';
	if(filter == 'IM' || filter == 'Isle of Man')
		countryCode = 'IM';
	if(filter == 'IN' || filter == 'India')
		countryCode = 'IN';
	if(filter == 'IO' || filter == 'British Indian Ocean Territory')
		countryCode = 'IO';
	if(filter == 'IQ' || filter == 'Iraq')
		countryCode = 'IQ';
	if(filter == 'IR' || filter == 'Iran (Islamic Republic of)')
		countryCode = 'IR';
	if(filter == 'IS' || filter == 'Iceland')
		countryCode = 'IS';
	if(filter == 'IT' || filter == 'Italy')
		countryCode = 'IT';
	if(filter == 'JE' || filter == 'Jersey')
		countryCode = 'JE';
	if(filter == 'JM' || filter == 'Jamaica')
		countryCode = 'JM';
	if(filter == 'JO' || filter == 'Jordan')
		countryCode = 'JO';
	if(filter == 'JP' || filter == 'Japan')
		countryCode = 'JP';
	if(filter == 'KE' || filter == 'Kenya')
		countryCode = 'KE';
	if(filter == 'KG' || filter == 'Kyrgyzstan')
		countryCode = 'KG';
	if(filter == 'KH' || filter == 'Cambodia')
		countryCode = 'KH';
	if(filter == 'KI' || filter == 'Kiribati')
		countryCode = 'KI';
	if(filter == 'KM' || filter == 'Comoros')
		countryCode = 'KM';
	if(filter == 'KN' || filter == 'Saint Kitts and Nevis')
		countryCode = 'KN';
	if(filter == 'KP' || filter == 'Korea, Democratic People\'s Republic')
		countryCode = 'KP';
	if(filter == 'KR' || filter == 'Korea, Republic of')
		countryCode = 'KR';
	if(filter == 'KW' || filter == 'Kuwait')
		countryCode = 'KW';
	if(filter == 'KY' || filter == 'Cayman Islands')
		countryCode = 'KY';
	if(filter == 'KZ' || filter == 'Kazakhstan')
		countryCode = 'KZ';
	if(filter == 'LA' || filter == 'Lao People\'s Democratic Republic')
		countryCode = 'LA';
	if(filter == 'LB' || filter == 'Lebanon')
		countryCode = 'LB';
	if(filter == 'LC' || filter == 'Saint Lucia')
		countryCode = 'LC';
	if(filter == 'LI' || filter == 'Liechtenstein')
		countryCode = 'LI';
	if(filter == 'LK' || filter == 'Sri Lanka')
		countryCode = 'LK';
	if(filter == 'LR' || filter == 'Liberia')
		countryCode = 'LR';
	if(filter == 'LS' || filter == 'Lesotho')
		countryCode = 'LS';
	if(filter == 'LT' || filter == 'Lithuania')
		countryCode = 'LT';
	if(filter == 'LU' || filter == 'Luxembourg')
		countryCode = 'LU';
	if(filter == 'LV' || filter == 'Latvia')
		countryCode = 'LV';
	if(filter == 'LY' || filter == 'Libya')
		countryCode = 'LY';
	if(filter == 'MA' || filter == 'Morocco')
		countryCode = 'MA';
	if(filter == 'MC' || filter == 'Monaco')
		countryCode = 'MC';
	if(filter == 'MD' || filter == 'Moldova, Republic of')
		countryCode = 'MD';
	if(filter == 'ME' || filter == 'Montenegro')
		countryCode = 'ME';
	if(filter == 'MF' || filter == 'Saint Martin')
		countryCode = 'MF';
	if(filter == 'MG' || filter == 'Madagascar')
		countryCode = 'MG';
	if(filter == 'MH' || filter == 'Marshall Islands')
		countryCode = 'MH';
	if(filter == 'MK' || filter == 'Macedonia')
		countryCode = 'MK';
	if(filter == 'ML' || filter == 'Mali')
		countryCode = 'ML';
	if(filter == 'MM' || filter == 'Myanmar')
		countryCode = 'MM';
	if(filter == 'MN' || filter == 'Mongolia')
		countryCode = 'MN';
	if(filter == 'MO' || filter == 'Macau')
		countryCode = 'MO';
	if(filter == 'MP' || filter == 'Northern Mariana Islands')
		countryCode = 'MP';
	if(filter == 'MQ' || filter == 'Martinique')
		countryCode = 'MQ';
	if(filter == 'MR' || filter == 'Mauritania')
		countryCode = 'MR';
	if(filter == 'MS' || filter == 'Montserrat')
		countryCode = 'MS';
	if(filter == 'MT' || filter == 'Malta')
		countryCode = 'MT';
	if(filter == 'MU' || filter == 'Mauritius')
		countryCode = 'MU';
	if(filter == 'MV' || filter == 'Maldives')
		countryCode = 'MV';
	if(filter == 'MW' || filter == 'Malawi')
		countryCode = 'MW';
	if(filter == 'MX' || filter == 'Mexico')
		countryCode = 'MX';
	if(filter == 'MY' || filter == 'Malaysia')
		countryCode = 'MY';
	if(filter == 'MZ' || filter == 'Mozambique')
		countryCode = 'MZ';
	if(filter == 'NA' || filter == 'Namibia')
		countryCode = 'NA';
	if(filter == 'NC' || filter == 'New Caledonia')
		countryCode = 'NC';
	if(filter == 'NE' || filter == 'Niger')
		countryCode = 'NE';
	if(filter == 'NF' || filter == 'Norfolk Island')
		countryCode = 'NF';
	if(filter == 'NG' || filter == 'Nigeria')
		countryCode = 'NG';
	if(filter == 'NI' || filter == 'Nicaragua')
		countryCode = 'NI';
	if(filter == 'NL' || filter == 'Netherlands')
		countryCode = 'NL';
	if(filter == 'NO' || filter == 'Norway')
		countryCode = 'NO';
	if(filter == 'NP' || filter == 'Nepal')
		countryCode = 'NP';
	if(filter == 'NR' || filter == 'Nauru')
		countryCode = 'NR';
	if(filter == 'NU' || filter == 'Niue')
		countryCode = 'NU';
	if(filter == 'NZ' || filter == 'New Zealand')
		countryCode = 'NZ';
	if(filter == 'OM' || filter == 'Oman')
		countryCode = 'OM';
	if(filter == 'PA' || filter == 'Panama')
		countryCode = 'PA';
	if(filter == 'PE' || filter == 'Peru')
		countryCode = 'PE';
	if(filter == 'PF' || filter == 'French Polynesia')
		countryCode = 'PF';
	if(filter == 'PG' || filter == 'Papua New Guinea')
		countryCode = 'PG';
	if(filter == 'PH' || filter == 'Philippines')
		countryCode = 'PH';
	if(filter == 'PK' || filter == 'Pakistan')
		countryCode = 'PK';
	if(filter == 'PL' || filter == 'Poland')
		countryCode = 'PL';
	if(filter == 'PM' || filter == 'St. Pierre and Miquelon')
		countryCode = 'PM';
	if(filter == 'PN' || filter == 'Pitcairn Island')
		countryCode = 'PN';
	if(filter == 'PR' || filter == 'Puerto Rico')
		countryCode = 'PR';
	if(filter == 'PS' || filter == 'State of Palestine')
		countryCode = 'PS';
	if(filter == 'PT' || filter == 'Portugal')
		countryCode = 'PT';
	if(filter == 'PW' || filter == 'Palau')
		countryCode = 'PW';
	if(filter == 'PY' || filter == 'Paraguay')
		countryCode = 'PY';
	if(filter == 'QA' || filter == 'Qatar')
		countryCode = 'QA';
	if(filter == 'RE' || filter == 'Reunion Island')
		countryCode = 'RE';
	if(filter == 'RO' || filter == 'Romania')
		countryCode = 'RO';
	if(filter == 'RS' || filter == 'Serbia')
		countryCode = 'RS';
	if(filter == 'RU' || filter == 'Russian Federation')
		countryCode = 'RU';
	if(filter == 'RW' || filter == 'Rwanda')
		countryCode = 'RW';
	if(filter == 'SA' || filter == 'Saudi Arabia')
		countryCode = 'SA';
	if(filter == 'SB' || filter == 'Solomon Islands')
		countryCode = 'SB';
	if(filter == 'SC' || filter == 'Seychelles')
		countryCode = 'SC';
	if(filter == 'SD' || filter == 'Sudan')
		countryCode = 'SD';
	if(filter == 'SE' || filter == 'Sweden')
		countryCode = 'SE';
	if(filter == 'SG' || filter == 'Singapore')
		countryCode = 'SG';
	if(filter == 'SH' || filter == 'Saint Helena')
		countryCode = 'SH';
	if(filter == 'SI' || filter == 'Slovenia')
		countryCode = 'SI';
	if(filter == 'SJ' || filter == 'Svalbard and Jan Mayen Islands')
		countryCode = 'SJ';
	if(filter == 'SK' || filter == 'Slovak Republic')
		countryCode = 'SK';
	if(filter == 'SL' || filter == 'Sierra Leone')
		countryCode = 'SL';
	if(filter == 'SM' || filter == 'San Marino')
		countryCode = 'SM';
	if(filter == 'SN' || filter == 'Senegal')
		countryCode = 'SN';
	if(filter == 'SO' || filter == 'Somalia')
		countryCode = 'SO';
	if(filter == 'SR' || filter == 'Suriname')
		countryCode = 'SR';
	if(filter == 'SS' || filter == 'South Sudan')
		countryCode = 'SS';
	if(filter == 'ST' || filter == 'Sao Tome and Principe')
		countryCode = 'ST';
	if(filter == 'SV' || filter == 'El Salvador')
		countryCode = 'SV';
	if(filter == 'SX' || filter == 'Sint Maarten')
		countryCode = 'SX';
	if(filter == 'SY' || filter == 'Syrian Arab Republic')
		countryCode = 'SY';
	if(filter == 'SZ' || filter == 'Swaziland')
		countryCode = 'SZ';
	if(filter == 'TC' || filter == 'Turks and Caicos Islands')
		countryCode = 'TC';
	if(filter == 'TD' || filter == 'Chad')
		countryCode = 'TD';
	if(filter == 'TF' || filter == 'French Southern Territories')
		countryCode = 'TF';
	if(filter == 'TG' || filter == 'Togo')
		countryCode = 'TG';
	if(filter == 'TH' || filter == 'Thailand')
		countryCode = 'TH';
	if(filter == 'TJ' || filter == 'Tajikistan')
		countryCode = 'TJ';
	if(filter == 'TK' || filter == 'Tokelau')
		countryCode = 'TK';
	if(filter == 'TM' || filter == 'Turkmenistan')
		countryCode = 'TM';
	if(filter == 'TN' || filter == 'Tunisia')
		countryCode = 'TN';
	if(filter == 'TO' || filter == 'Tonga')
		countryCode = 'TO';
	if(filter == 'TP' || filter == 'East Timor')
		countryCode = 'TP';
	if(filter == 'TR' || filter == 'Turkey')
		countryCode = 'TR';
	if(filter == 'TT' || filter == 'Trinidad and Tobago')
		countryCode = 'TT';
	if(filter == 'TV' || filter == 'Tuvalu')
		countryCode = 'TV';
	if(filter == 'TW' || filter == 'Taiwan')
		countryCode = 'TW';
	if(filter == 'TZ' || filter == 'Tanzania')
		countryCode = 'TZ';
	if(filter == 'UA' || filter == 'Ukraine')
		countryCode = 'UA';
	if(filter == 'UG' || filter == 'Uganda')
		countryCode = 'UG';
	if(filter == 'UM' || filter == 'US Minor Outlying Islands')
		countryCode = 'UM';
	if(filter == 'US' || filter == 'United States')
		countryCode = 'US';
	if(filter == 'UY' || filter == 'Uruguay')
		countryCode = 'UY';
	if(filter == 'UZ' || filter == 'Uzbekistan')
		countryCode = 'UZ';
	if(filter == 'VA' || filter == 'Holy See (City Vatican State)')
		countryCode = 'VA';
	if(filter == 'VC' || filter == 'Saint Vincent and the Grenadines')
		countryCode = 'VC';
	if(filter == 'VE' || filter == 'Venezuela')
		countryCode = 'VE';
	if(filter == 'VG' || filter == 'Virgin Islands (British)')
		countryCode = 'VG';
	if(filter == 'VI' || filter == 'Virgin Islands (USA)')
		countryCode = 'VI';
	if(filter == 'VN' || filter == 'Vietnam')
		countryCode = 'VN';
	if(filter == 'VU' || filter == 'Vanuatu')
		countryCode = 'VU';
	if(filter == 'WF' || filter == 'Wallis and Futuna Islands')
		countryCode = 'WF';
	if(filter == 'WS' || filter == 'Samoa')
		countryCode = 'WS';
	if(filter == 'XK' || filter == 'Kosovo')
		countryCode = 'XK';
	if(filter == 'YE' || filter == 'Yemen')
		countryCode = 'YE';
	if(filter == 'YT' || filter == 'Mayotte')
		countryCode = 'YT';
	if(filter == 'ZA' || filter == 'South Africa')
		countryCode = 'ZA';
	if(filter == 'ZM' || filter == 'Zambia')
		countryCode = 'ZM';
	if(filter == 'ZW' || filter == 'Zimbabwe')
		countryCode = 'ZW';
	
	return countryCode;
}
