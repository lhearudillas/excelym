/**
 * Constants used for "WITHIN BQI" validation.
 *
 * Version    Date            Author           Remarks
 * 1.01       16 Mar 2018     Roy Selim
 * Last Edit: 3/19/2018
 */

// Common validation constants
const COM_AMT_DDCTD    = 'amount_deducted';
const COM_BGT_GET_TYPE = 'value';
const COM_BQI_BUD_SS   = 'customsearch_script_eym_bqi_budget';
const COM_BQI_VALUE    = 'custcol_eym_pur_bqi';
const COM_CUST_FORM    = 'customform';
const COM_EST_BGT      = 'estimated_budget';
const COM_FORM_NUM     = 'formulanumeric';
const COM_ITEM_NAME	   = 'item';
const COM_NAME_VALUE   = 'name';
const COM_ON_BGT_FLD   = 'custcol_eym_bqi_budget_col';
const COM_PRPSD_AMT    = 'proposed_amount';
const COM_SAVE_SEARCH  = 'customsearch_eym_usage_per_bqi_2';
const COM_STAT_FIELD   = 'custbody_eym_pur_budget_stat';
const COM_SUMM_GRP	   = 'group';
const COM_SUMM_SUM	   = 'sum';
const COM_USG_GET_TYPE = 'text';

// Purchase Requisition validation constants
const PRV_CUST_FORM    = 'customform';
const PRV_EST_AMOUNT   = 'estimatedamount';
const PRV_LOG_ERROR_1  = 'File: EYM_ue_purchase_requisition_validation.js, function: userEventAfterSubmit, comment line: \/\/';
const PRV_PR		   = 'purchaserequisition'

// Purchase Order validation constants
const POV_GROSS_AMOUNT = 'grossamt';
const POV_LOG_ERROR_1  = 'File: EYM_ue_purchase_order_validation.js, function: userEventAfterSubmit, comment line: \/\/';
const POV_PO   		   = 'purchaseorder';
const POV_PROJECT_CODE = 'custbody_eym_pr_projcode';

// Sales Order validation constants.
const SOV_LOG_ERROR_1  = 'File: EYM_ue_purchase_requisition_validation.js, function: userEventAfterSubmit, comment line: \/\/';
const SOV_ON_BGT_FLD   = 'custcol_eym_so_on_budget';
const SOV_RATE_REQ 	   = 'custcol_eym_item_amount_srst';
const SOV_SO		   = 'salesorder';