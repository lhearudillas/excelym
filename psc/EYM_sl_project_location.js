/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       12 Mar 2018     eym
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response){
	
	if (request.getMethod() == 'GET') {
		doGet(request, response);
	}else if(request.getMethod() == 'POST') {
		doPost(request, response);
	}

}

function doGet(request, response){
	
	var projectVal = request.getParameter('projectid');
	var projectIncharge = request.getParameter('incharge');
	
	var column = new Array();
	column.push(new nlobjSearchColumn('entityid'));
	var projects = nlapiSearchRecord('job',null,null,column);
	
	//var mainCss = 'https://system.netsuite.com/core/media/media.nl?id=626&c=4317943_SB1&h=80e5044fe17a116c6e29&_xt=.css';
	 
	var epRess = getPreferences();
	var mainCss = getPreference(epRess, 'main-css');
	 
	var disabled = 'disabled';
	
	var html = '<html>\n';
	html += '<head>\n';
	html += '<link rel="stylesheet" href="'+mainCss+'" type="text/css" media="screen" />\n';
	html += '<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" type="text/css" media="screen" />\n';
	html += '<title>PSC BOQ Template</title>\n';
	html += '</head>\n';
	html += '<body>\n';
	//Save Button
	html += '<form name="frmBoq" id="frmBoq" method="POST" action="">\n';
	html += '<header class="actions stick-to-top">\n';
	html += '	<div class="card">\n';
	  html += '		<div class="card-body"><div class="row"><div class="col">\n';
	  html += '    <input type="submit" class="btn btn-primary" value="Save" name="btnSave" id="btnSave">\n';
	html += '<main class="main-content">\n';
	html += ' <div class="container-fluid">\n';
	html += '	 <div class="row">\n';
	html += '		<div class="col">\n';
	html += '		<div class="card mb-3">\n';
	html += '		<div class="card-header"><h3 class="float-left mt-2 mb-1">Details</h3></div>\n';
	html += '		<div class="card-body"><div class="row">\n';
	html += '			<div class="col-md-4">\n';
	html += '				<dl class="row">\n';
	html += '					<dt class="col-md-6">Project</dt>\n';
	html += '					<dd class="col-md-6"><select name="custpage_cl_project" id="custpage_cl_project" '+disabled+'>\n';
	html += '						<option value=""></option>\n';
	  if(projects){
		  for(var b=0;b<projects.length;b++){
		      var selected = "";
			  if(projectVal ==projects[b].getId()){
		    	  	selected = "selected";
		      }
			  html += '<option value="'+projects[b].getId()+'" '+selected+'>'+projects[b].getValue('entityid')+'</option>\n';
		  }
	  }
	  html += '         				</select></dd>\n';
	  html += '					<dt class="col-md-6">Location</dt>\n';
	  html += '					<dd class="col-md-6"><input type="text" name="custpage_cl_location" id="custpage_cl_location" required></dt>\n';
	  html += '				</dl>\n';
	  html += '			</div>\n';
	  html += '		</div></div>\n';
	  html += '		</div>\n';
	  html += '		</div>\n';
	  html += '	 </div>\n';
	
	html += '</form>\n';
	html += '</body>\n';
	
	response.write(html);
	
}

function doPost(request, response){
	
	var project = request.getParameter('projectid');
	var location = request.getParameter('custpage_cl_location');
	var inCharge = request.getParameter('incharge');
	nlapiLogExecution('DEBUG','VAL',project+'-'+location);
	if(location && project){
		var mainLoc = createLocation(location,null,project,2,inCharge);
		if(mainLoc){
			nlapiSubmitField('job',project,'custentity_eym_proj_loc_whouse',mainLoc);
			var mainBin = createBin(IDS_MAIN_PREFFIX+'-'+project,mainLoc);
			var childLoc = createLocation(IDS_EXS_PREFFIX+'-'+location,mainLoc,project,3,inCharge);
			if(childLoc){
				var childBin = createBin(IDS_EXS_PREFFIX+'-'+project,childLoc);
			}
		}
	}
	
	response.write('<script type="text/javascript">window.opener.location.reload();window.close();</script>');
	response.setContentType('HTMLDOC');
	
}

function createLocation(location,parent,project,locType,inCharge){
	
	var id = createNSRecord('location', {
		name: location,
		parent:parent,
		custrecord_eym_cust_locationtype: locType, //Project
		custrecord_eym_projectcode: project,
		custrecord_eym_loc_pic:inCharge
	});
	
	return id
	
}

function createBin(binName,location){
	
	var id = createNSRecord('bin', {
		location: location,
		binnumber: binName,
	});
	
	return id
	
}
