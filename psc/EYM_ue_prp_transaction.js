function asPrpTransaction(type)
{
	if(type == 'create' || type == 'edit') {
		var recType = nlapiGetRecordType();
		var recId = nlapiGetRecordId();
		
		var trxRec = nlapiLoadRecord(recType, recId);
		var itemCnt = trxRec.getLineItemCount('item');
		for(var i = 1; i <= itemCnt; i++) {
			var prpId = trxRec.getLineItemValue('item', 'custcol_eym_sales_prpid', i);
			if(!isNullOrEmpty(prpId)) {
				var totalQty = getItemReceivedTotalQuantity(prpId);
				var qty = nlapiLookupField('customrecord_eym_procplan_item', prpId, 'custrecord_eym_procplan_qty');
				if(isNullOrEmpty(qty)) qty = 0;
				
				var fields = new Array();
				var values = new Array();
				
				fields.push('custrecord_eym_received_qty');
				values.push(totalQty);
				
				if(parseInt(totalQty) >= parseInt(qty)) {
					fields.push('custrecord_eym_procplan_status');
					values.push(2);
				}else {
					fields.push('custrecord_eym_procplan_status');
					values.push(1);	
				}
				
				nlapiSubmitField('customrecord_eym_procplan_item', prpId, fields, values);
			}
		}
	}
}