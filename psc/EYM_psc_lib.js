/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       13 Mar 2018     EYM-003
 *
 */

/**
 *
 * Common library for Primary Structure Corporation
 *
 */

function getPreferences()
{
	var columns = new Array();
	columns.push(new nlobjSearchColumn('name'));
	columns.push(new nlobjSearchColumn('custrecord_eym_pref_value'));
	return nlapiSearchRecord('customrecord_eym_preferences', null, null, columns);	
}

function getPreference(epRess, name, isArray, separator)
{
	var value = "";
	
	if(epRess == undefined)
		epRess = null;
	
	if(isArray == undefined)
		isArray = false;
	
	if(separator == undefined)
		separator = ',';
	
	if(epRess != null){
		for(var i = 0; i < epRess.length; i++) {	
			if(epRess[i].getValue('name') == name){
				value = epRess[i].getValue('custrecord_eym_pref_value');
				break;
			}	
		}
	}
	
	if(isArray == true) {
		if(isNullOrEmpty(value)) {
			value = null;
		}else {
			value = value.split(separator);
		}
	}
	
	return value;
}

function isProjectHasLocation(projId)
{
	var hasLocation = false;
	var locId = getNSRecordId('location', 'custrecord_eym_projectcode', projId, 'is');
	if(locId != null) {
		hasLocation = true;
	}
	
	return hasLocation;
}

function pinCodePopup(scriptId, deploymentId, params)
{
	var url = nlapiResolveURL('SUITELET', scriptId, deploymentId);
	var w = 500;
	var h = 350;
	
	if (!isNullOrEmpty(params)) {
		url += '&';
		url += params;
	}
	
	windowOpener(url, w, h);
}

function windowOpener(url, w, h)
{
	newWindow = window.open(url, "PopUp", "resizable=1,width=" + w + ",height=" + h + ",left=" + (window.screen.width - w) / 2 + ",top=" + (window.screen.height - h) / 2 + ",scrollbars=yes");
	newWindow.focus();
	
	return newWindow.name;
}

function pinCodeUI(mainCss, hiddenFields) 
{
	var html = '<html>\n';
	html += '<head>\n';
	html += '<link rel="stylesheet" href="' + mainCss + '" type="text/css" media="screen" />\n';
	html += '<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" type="text/css" media="screen" />\n';
	html += '<title>Enter PIN</title>\n';
	html += '</head>\n';
	html += '<body>\n';
	html += '<form name="pincode" id="pincode" method="POST" action="">\n';
	html += '<main class="main-content">\n';	
	html += ' <div class="container-fluid">\n';
	html += '	 <div class="row">\n';
	html += '		<div class="col">\n';
	html += '		<div class="card mb-3">\n';
	html += '		<div class="card-header"><h3 class="float-left mt-2 mb-1">Enter PIN</h3></div>\n';
	html += '		<div class="card-body">\n';
	html += '		  <div class="row">\n';
	html += '            <div class="col-12">';
	html += '               <div class="row align-items-center">';
	html += '	               <div class="col-8">';
	html += '                      <input type="password" class="form-control" name="custpage_employee_pin" id="custpage_employee_pin" style="width:100%;" required="">';
	
	for (var fieldName in hiddenFields) {		
		html += '                      <input type="hidden" name="' + fieldName + '" id="' + fieldName + '" value="' + hiddenFields[fieldName] + '">';
	}
	
	html += '                  </div>';
	html += '                  <div class="col-4">';
	html += '                      <input type="submit" class="btn btn-primary btn-block" value="Ok" name="submitPin" id="submitPin">';
	html += '                  </div>';
	html += '               </div>';
	html += '            </div>';
	html += '		  </div>\n';
	html += '		</div>\n';
	html += '		</div>\n';
	html += '		</div>\n';
	html += '		</div>\n';
	html += '	</div>\n';
	html += ' </div>\n';
	html += '</main>\n';	
	html += '</form>\n';
	html += '</body>\n';
	html += '</html>\n';
	
	return html;
}

function numberToEnglish(n)
{
    var string = n.toString(), units, tens, scales, start, end, chunks, chunksLen, chunk, ints, i, word, words, and = '';

    /* Is number zero? */
    if(parseInt(string) === 0) {
        return 'Zero';
    }

    /* Array of units as words */
    units = [ '', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen' ];

    /* Array of tens as words */
    tens = [ '', '', 'Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety' ];

    /* Array of scales as words */
    scales = [ '', 'Thousand', 'Million', 'Billion', 'Trillion', 'Quadrillion', 'quintillion', 'sextillion', 'septillion', 'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quatttuor-decillion', 'quindecillion', 'sexdecillion', 'septen-decillion', 'octodecillion', 'novemdecillion', 'vigintillion', 'centillion' ];

    /* Split user arguemnt into 3 digit chunks from right to left */
    start = string.length;
    chunks = [];
    
    while(start > 0) {
        end = start;
        chunks.push( string.slice((start = Math.max(0, start - 3)), end));
    }
    /* Check if function has enough scale words to be able to stringify the user argument */
    chunksLen = chunks.length;
    if(chunksLen > scales.length) {
        return '';
    }

    /* Stringify each integer in each chunk */
    words = [];
    for(i = 0; i < chunksLen; i++) {

        chunk = parseInt(Number(chunks[i]));

        if(chunk) {

            /* Split chunk into array of individual integers */
            ints = chunks[i].split('').reverse().map(parseFloat);

            /* If tens integer is 1, i.e. 10, then add 10 to units integer */
            if(ints[1] === 1) {
                ints[0] += 10;
            }

            /* Add scale word if chunk is not zero and array item exists */
            if((word = scales[i])) {
                words.push( word );
            }

            /* Add unit word if array item exists */
            if((word = units[ints[0]])) {
                words.push( word );
            }

            /* Add tens word if array item exists */
            if((word = tens[ints[1]])) {
                words.push( word );
            }

            /* Add 'and' string after units or tens integer if: */
            if(ints[0] || ints[1]) {

                /* Chunk has a hundreds integer or chunk is the first of multiple chunks */
                if(ints[2] || ! i && chunksLen) {
                    words.push(and);
                }
            }
            /* Add hundreds word if array item exists */
            if( (word = units[ ints[2]])) {
                words.push(word + ' Hundred');
            }
        }
    }

    return capitalizedFirstChar(words.reverse().join(' '));
}

function capitalizedFirstChar(string)
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}