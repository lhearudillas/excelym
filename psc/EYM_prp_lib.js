var IDR_CLOSED = 2;

function blProcurementPlan(type,form)
{
	var prpStatus = nlapiGetFieldValue('custrecord_eym_procplan_status');
	
	if(type == 'edit'){
		//added by ivanne (PNID-311 for acceptance critiria #5
		if(prpStatus == IDR_CLOSED){
			throw nlapiCreateError('99999', 'Cannot Edit. Procurement Plan is already closed.');
		}
	}
	
	if(type == 'view' || type == 'edit') {
		form.addTab('custpage_tab_records', 'Related Transactions');
		form.addSubTab('custpage_sub_tab_records',  '', 'custpage_tab_records');
		var records = form.addSubList('custpage_records_list', 'inlineeditor', 'Related Transactions', 'custpage_sub_tab_records');
		
		records.addField('custpage_record_type','text','Type');
		records.addField('custpage_record_id','select','ID','transaction');
		records.addField('custpage_record_qty','text','Quantity');
		
		var prpId = nlapiGetRecordId();
		if(prpId)
		{
			var receivedItems = getItemReceivedTransactions(prpId);
			if(receivedItems){
				for(var i = 0; i < receivedItems.length; i++){
					
					var lineNumber = i+1;
					var recType = receivedItems[i].type;
					var id = receivedItems[i].id;
					var qty = receivedItems[i].quantity;
					
					records.setLineItemValue('custpage_record_type',lineNumber,recType);
					records.setLineItemValue('custpage_record_id',lineNumber,id);
					records.setLineItemValue('custpage_record_qty',lineNumber,qty);
					
					//added by ivanne (PNID-311 for acceptance critiria #6
					if(type == 'edit' && qty > 0){
						throw nlapiCreateError('99999', 'Cannot Edit. Procurement Plan has a record created.');
					}
					
				}
			}
		}
	}
	
	
}

function asProcurementPlan(type)
{
	nlapiLogExecution('DEBUG', '[EYM-PpP] Type', type);
	if(type == 'edit' || type == 'xedit') {
		var recType = nlapiGetRecordType();
		var recId = nlapiGetRecordId();
		
		var prpRec = nlapiLoadRecord(recType, recId);
		var qty = prpRec.getFieldValue('custrecord_eym_procplan_qty');
		var qtyR = prpRec.getFieldValue('custrecord_eym_received_qty');
		if(parseInt(qtyR) >= parseInt(qty)) {
			nlapiSubmitField(recType, recId, 'custrecord_eym_procplan_status', 2);
		}else {
			nlapiSubmitField(recType, recId, 'custrecord_eym_procplan_status', 1);
		}
	}
}

function getItemReceivedTotalQuantity(prpId)
{
	var qty = 0;
	
	if(!isNullOrEmpty(prpId)) {
		var filters = new Array();				
		filters.push(new nlobjSearchFilter('custcol_eym_sales_prpid', null, 'is', prpId));
		
		var items =  nlapiSearchRecord(null, 'customsearch_eym_prp_fulfilled_items', filters);
		if(items != null) {
			for(var i = 0; i < items.length; i++) {
				qty = parseInt(qty) + parseInt(items[i].getValue('quantity'));
			}
		}
	}
	
	return qty;
}

function getItemReceivedTransactions(prpId)
{
	var itemTrx = null;
	
	if(!isNullOrEmpty(prpId)) {
		var filters = new Array();				
		filters.push(new nlobjSearchFilter('custcol_eym_sales_prpid', null, 'is', prpId));
		
		var items =  nlapiSearchRecord(null, 'customsearch_eym_prp_fulfilled_items', filters);
		if(items != null) {
			itemTrx = new Array();
			for(var i = 0; i < items.length; i++) {
				itemTrx.push({
					"id":items[i].getId(),
					"type":items[i].getText('type'),
					"quantity":items[i].getValue('quantity')
				});
			}
		}
	}
	
	return itemTrx;
}