/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       13 Mar 2018     EYM-003
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
 
function ueBeforeLoad(type, form, request)
{
	var context = nlapiGetContext();
	var recType = nlapiGetRecordType();
	var recId = nlapiGetRecordId();
	
	var role = context.getRole().toString();
	var projStatus = nlapiGetFieldValue('entitystatus');
	var projInCharge = nlapiGetFieldValue('custentity_eym_proj_in_charge');
	if(isNullOrEmpty(projInCharge)) projInCharge = '';
	
	form.setScript('customscript_cs_eym_project');
	form.removeButton('createtemplate');
	
	if(type == 'view' || type == 'edit') {
		var epRess = getPreferences();
		var roles = getPreference(epRess, 'create-boq-allowed-roles', true);
		
		if(isIndexOfArray(roles, role) && projStatus == IDR_PROJECT_STATUS_AWARDED) {
			if(isProjectHasLocation(recId)) {
				var jobRec = nlapiLoadRecord(recType, recId);
				var fileSearchId = getNSRecordId('file', 'name', 'boq_template_' + recId, 'is');
				var fileSearchFreeFormId = getNSRecordId('file', 'name', 'boq_freeform_template_' + recId, 'is');
				
				if(fileSearchId == null && fileSearchFreeFormId == null) {
					var projCharter = jobRec.getFieldValue('custentity_eym_proj_charter');
					if(isNullOrEmpty(projCharter)) projCharter = '';
                  	var script = "createBOQ(" + recId + ", '" + projCharter + "');";
					form.addButton('custpage_createboq_btn', 'Create BOQ', "createBOQ(" + recId + ", '" + projCharter + "', '" + projInCharge + "');");
                  	var script = "createFreeFormBOQ(" + recId + ", '" + projCharter + "');"
					form.addButton('custpage_createfreeform_btn', 'Create Free-form BOQ', "createFreeFormBOQ(" + recId + ", '" + projCharter + "', '" + projInCharge + "');");
				}else if(fileSearchId != null) {
					form.addButton('custpage_manageboq_btn', 'Manage BOQ', "manageBOQ(" + recId + ", '" + projInCharge + "');");
				}else if(fileSearchFreeFormId != null) {
					form.addButton('custpage_manageboq_btn', 'Manage BOQ', "manageFreeFormBOQ(" + recId + ", '" + projInCharge + "');");
				}
			}else {
				form.addButton('custpage_project_loc_btn', 'Create Project Location', "createProjectLocation(" + recId + ", '" + projInCharge + "');");
			}
		}
	}
	
	try{

		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_eym_boq_projid', null, 'is', recId);	
		
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('name');
 		columns[1] = new nlobjSearchColumn('custrecord_eym_proj_subsystem');
 		columns[2] = new nlobjSearchColumn('custrecord_eym_boq_categories');
 		columns[3] = new nlobjSearchColumn('custrecord_eym_act_desc');
 		columns[4] = new nlobjSearchColumn('custrecord_eym_boq_base_cost');
 		columns[5] = new nlobjSearchColumn('custrecord_eym_boq_class');
 		columns[6] = new nlobjSearchColumn('custrecord_eym_changeorder_no');


		var searchRes = nlapiSearchRecord('customrecord_eym_boq',null,filters,columns);
	
		boqSubtab(searchRes, type);
		
		var searchLocValue = searchLoc(recId);
		locationSubtab(searchLocValue, type);
	}
	catch(ex){
		nlapiLogExecution('DEBUG', 'Exception', ex.message == '' ? ex : ex.message);
	}
}


function boqSubtab(searchRes, type)
{
	if (type == 'create' || type == 'edit' || type == 'view'){
		var boqTab = form.addTab('custpage_boq_tab', 'Bill of Quantity');
	    var boqSubTab = form.addSubTab('custpage_boq_subtab', 'Bill of Quantity','custpage_boq_tab');
		var boqSubList = form.addSubList('custpage_boq_sublist', 'list', 'Bill of Quantity', 'custpage_boq_subtab');

		boqSubList.addField('custpage_boq_projid', 'text', 'ID').setDisplayType('inline'); 
		//boqSubList.addField('custpage_boq_projsubsystem', 'select', 'Project Subsystem', 'customlist_eym_proj_subsystem').setDisplayType('inline');
		boqSubList.addField('custpage_boq_projsubsystem', 'text', 'Project Subsystem').setDisplayType('inline');
		//boqSubList.addField('custpage_boq_actgroup', 'select', 'Activity Group', 'customrecord_eym_boq_activity').setDisplayType('inline');
		boqSubList.addField('custpage_boq_actgroup', 'text', 'Activity Group').setDisplayType('inline');
		boqSubList.addField('custpage_boq_description', 'text', 'Description').setDisplayType('inline'); 
		boqSubList.addField('custpage_boq_totalcost', 'currency', 'Total Cost').setDisplayType('inline'); 
		boqSubList.addField('custpage_boq_costtype', 'select', 'Cost Type', 'classification').setDisplayType('inline'); 
		boqSubList.addField('custpage_boq_pconum', 'select', 'Change Order No.', 'customrecord_eym_change_order').setDisplayType('inline'); 


		if(searchRes){		
			for(var i = 0; i < searchRes.length; i++) {
				
				var outputResultBOQ =  searchRes[i];
				//nlapiLogExecution('DEBUG','outputResultBOQ', outputResultBOQ);

				var listBOQ = outputResultBOQ.getId();
					    	
				var projSubsystem = outputResultBOQ.getValue('custrecord_eym_proj_subsystem');
				var actGroup = outputResultBOQ.getValue('custrecord_eym_boq_categories');
				var desc = outputResultBOQ.getValue('custrecord_eym_act_desc');
				var baseCost = outputResultBOQ.getValue('custrecord_eym_boq_base_cost');
				nlapiLogExecution('DEBUG', 'base cost', baseCost);
				var actClass = outputResultBOQ.getValue('custrecord_eym_boq_class');
				var coNum = outputResultBOQ.getValue('custrecord_eym_changeorder_no');
				
				var boqLink = nlapiResolveURL('RECORD', 'customrecord_eym_boq', listBOQ);
		    	
				boqSubList.setLineItemValue('custpage_boq_projid', i + 1, "<a href='"+boqLink+"'>"+ outputResultBOQ.getValue('name')  +"</a>");
				boqSubList.setLineItemValue('custpage_boq_projsubsystem', i + 1, projSubsystem);
				boqSubList.setLineItemValue('custpage_boq_actgroup', i + 1, actGroup);
				boqSubList.setLineItemValue('custpage_boq_description', i + 1, desc);
				boqSubList.setLineItemValue('custpage_boq_totalcost', i + 1, baseCost);
				boqSubList.setLineItemValue('custpage_boq_costtype', i + 1, actClass);
				boqSubList.setLineItemValue('custpage_boq_pconum', i + 1, coNum);

			}
		}
	}
}

function searchLoc(projectId)
{
	var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_eym_projectcode', null, 'is', projectId);	
	
	var columns = new Array();
		columns[0] = new nlobjSearchColumn('name');

	var searchLocRes = nlapiSearchRecord('location',null,filters,columns);
	
	return searchLocRes;
}

function locationSubtab(searchLocRes, type)
{
	if (type == 'create' || type == 'edit' || type == 'view'){
		var locationTab = form.addTab('custpage_loc_tab', 'Location');
	    var locationSubTab = form.addSubTab('custpage_loc_subtab', 'Location','custpage_loc_tab');
		var locationSubList = form.addSubList('custpage_loc_sublist', 'list', 'Location', 'custpage_loc_subtab');

		locationSubList.addField('custpage_loc_name', 'text', 'Location').setDisplayType('inline'); 

		if(searchLocRes){		
			for(var i = 0; i < searchLocRes.length; i++) {
				
				var outputResultLoc =  searchLocRes[i];

				var listLoc = outputResultLoc.getId();
					    					
				var locLink = nlapiResolveURL('RECORD', 'location', listLoc);
		    	
				locationSubList.setLineItemValue('custpage_loc_name', i + 1, "<a href='"+locLink+"'>"+ outputResultLoc.getValue('name')  +"</a>");

			}
		}
	}
}

