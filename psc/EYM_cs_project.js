/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       15 Mar 2018     EYM-013
 *
 */

/**
 *
 * Client script for Project 
 * 
 */
 
function createBOQ(projId, projCharter, projInCharge)
{
	if(isNullOrEmpty(projCharter)) { 
		alert('Please upload a Project Charter before creating a BOQ.'); 
	}else if(isNullOrEmpty(projInCharge)) { 
		alert('Please input a Project In Charge before creating a BOQ.'); 
	}else {
		var url = nlapiResolveURL('SUITELET', 'customscript_sl_eym_boq_template', 'customdeploy_sl_eym_boq_template') + '&projectid=' + projId;
		window.location.replace(url); 
	}
}

function createFreeFormBOQ(projId, projCharter, projInCharge)
{
	if(isNullOrEmpty(projCharter)) { 
		alert('Please upload a Project Charter before creating a BOQ.');
	}else if(isNullOrEmpty(projInCharge)) { 
		alert('Please input a Project In Charge before creating a BOQ.'); 
	}else {
		var url = nlapiResolveURL('SUITELET', 'customscript_sl_eym_boq_free_template', 'customdeploy_sl_eym_boq_free_template') + '&projectid=' + projId;
		window.location.replace(url); 
	}
}

function manageBOQ(projId, projInCharge)
{
	if(isNullOrEmpty(projInCharge)) { 
		alert('Please input a Project In Charge before creating a BOQ.'); 
	}else {
		var url = nlapiResolveURL('SUITELET', 'customscript_sl_eym_boq_template', 'customdeploy_sl_eym_boq_template') + '&projectid=' + projId;
		window.location.replace(url);
	}
}

function manageFreeFormBOQ(projId, projInCharge)
{
	if(isNullOrEmpty(projInCharge)) { 
		alert('Please input a Project In Charge before creating a BOQ.'); 
	}else {
		var url = nlapiResolveURL('SUITELET', 'customscript_sl_eym_boq_free_template', 'customdeploy_sl_eym_boq_free_template') + '&projectid=' + projId;
		window.location.replace(url);
	}
}

function createProjectLocation(projId,inCharge)
{
	showPopup('customscript_sl_eym_project_location', 'customdeploy_sl_eym_project_location', 'projectid=' + projId + '&incharge=' +inCharge, 800, 400);
}

function showPopup(scriptId, deploymentId, args, w, h)
{
	var url = nlapiResolveURL('SUITELET', scriptId, deploymentId);
	if(!isNullOrEmpty(args)) {
		url += '&';
		url += args;
	}
	
	windowOpener(url, w, h);
}

function windowOpener(url, a, b)
{
	newWindow = window.open(url, "PopUp", "resizable=1,width=" + a + ",height=" + b + ",left=" + (window.screen.width - a) / 2 + ",top=" + (window.screen.height - b) / 2 + ",scrollbars=yes");
	newWindow.focus();
	return newWindow.name
};

// Project Events Here
function onFieldChanged(sublistId, fieldId, lineNo) {
	if (fieldId == 'parent') {
		var entityId = nlapiGetFieldValue('parent');
		if(!isNullOrEmpty(entityId)) {
			var recType = nlapiLookupField('customer', entityId, 'recordtype');
			if(isNullOrEmpty(recType)) {
				var customerId = nlapiLookupField('job', entityId, 'customer');
				if(!isNullOrEmpty(customerId)) {
					nlapiSetFieldValue('parent', customerId);
				}
			}
		}
	}
}



