/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       11 Jun 2018     EYM-013
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType 
 * 
 * @param {String} type Access mode: create, copy, edit
 * @returns {Void}
 */
var IDS_QUOTE_ITEM = 'item';
var IDS_QUOTE_EQUIP_ASSET = 'custcol_eym_fuel_so_equip_asset';

function validateEquipAsset(type)
{	
		var itemVal = nlapiGetCurrentLineItemValue('item', IDS_QUOTE_ITEM);
		var equipAssetVal = nlapiGetCurrentLineItemValue('item', IDS_QUOTE_EQUIP_ASSET);

		if ((itemVal == 3394 || itemVal == 3395) && isNullOrEmpty(equipAssetVal)) {
			alert('Please select an Equipment Asset No. for your equipment rental items.');
			return false;	
		}

	return true;
}