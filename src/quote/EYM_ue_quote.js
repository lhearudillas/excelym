/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       11 Jun 2018     EYM-013
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */

var IDS_SIGNED_QUOTATION = 'custbody_eym_ed_quote_signed_doc';
var IDS_QUOTE_STATUS = 'status';
var IDS_CUSTOM_FORM = 'customform';

function userEventBeforeLoad(type, form, request){
		
	if (type == 'view' || type == 'edit') {
		
		var recId = nlapiGetRecordId();
		var recType = nlapiGetRecordType();
		var rec = nlapiLoadRecord(recType, recId);
		var custForm = rec.getFieldValue(IDS_CUSTOM_FORM);
		var signedQuote = rec.getFieldValue(IDS_SIGNED_QUOTATION); 
		var quoteStat = rec.getFieldValue(IDS_QUOTE_STATUS);

		if (custForm == 125) {
			
			if (isNullOrEmpty(signedQuote) || quoteStat == IDS_QUOTE_PROCESSED) {	
				form.removeButton('createsalesord');
			} 
		}
		
	}
}
