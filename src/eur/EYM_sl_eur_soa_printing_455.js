/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define([ 'N/runtime', 'N/file', 'N/xml', 'N/record', 'N/search' ],

function( runtime, file, xml, record, search ) {

    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context)
  	{
      	// Initialize scope variables.
      	var templateFile	= null;
      	var assetObj		= null;
      	var assetName		= null;
      	var errorsArray 	= [];
      	var customerName	= null;

      	// Declare scope constants.
      	const ESP_RECORD_TYP	= 'customrecord_eym_eur_header';
      	const ESP_ASSET_FIELD	= 'custrecord_eym_deur_h_equip_no';
      	const ESP_EQPMNT_REC	= 'customrecord_eym_psc__equip_asset';
      	const ESP_EUR_DATE_FLD	= 'custrecord_eym_deur_h_date';
      	const ESP_EQPMNT_NAME	= 'name';
        const ESP_EUR_CUST_FLD	= 'custrecord_eym_deur_h_customer';
      	const ESP_EUR_PREP_BY	= 'custrecord_eym_deur_h_prep_by';

      	// Get the project Id parameter.
      	var id = context.request.parameters.recordId;

       	// Load the EUR record.
      	var EURRecord = record.load( {type: ESP_RECORD_TYP, id: id} );

      	// Get record field values.
      	var assetId 	= EURRecord.getValue( {fieldId: ESP_ASSET_FIELD} );
      	var EURId 		= EURRecord.getValue( {fieldId: ESP_EQPMNT_NAME} );
      	var EURDate		= EURRecord.getValue( {fieldId: ESP_EUR_DATE_FLD} );
      	var customerId	= EURRecord.getValue( {fieldId: ESP_EUR_CUST_FLD} );
      	var preparedBy	= EURRecord.getValue( {fieldId: ESP_EUR_PREP_BY} );

      	// Get the asset object.
      	if( assetId ) {
            assetObj = search.lookupFields({
                type	: ESP_EQPMNT_REC,
                id		: assetId,
                columns	: ESP_EQPMNT_NAME
            });

          	// Get the asset name.
          	assetName = assetObj.name;
        }

      	// Get the script object.
      	var script = runtime.getCurrentScript();

      	// Get the template parameters.
      	var templatetId = script.getParameter( {name: 'custscript_print_eur_soa'} );
      	var department	= script.getParameter( {name: 'custscript_dept_name'} );
      	var address		= script.getParameter( {name: 'custscript_dept_address'} );
      	var logoUrl		= script.getParameter( {name: 'custscript_logo_url'} );
      	var refPrepend	= script.getParameter( {name: 'custscript_ref_prepend'} );
      	var deptManager	= script.getParameter( {name: 'custscript_dept_manager'} );

      	// Get the customer record.
      	if( customerId ) {
          	var customerRec = record.load({
              	type	: record.Type.CUSTOMER,
              	id		: customerId
            });

          	// If the record was successfully loaded, get the name.
          	if( customerRec ) {
              	customerName = customerRec.getValue( {fieldId: 'entityid'} );
            }
        }

      	// Load the template file. Use try/catch to avoid script stopping.
      	try{
            var templateFile = file.load( {id: templatetId} );

			// Get the contents of the file.
          	var xmlString = templateFile.getContents();

          	// Compose the logo.
          	var logo = '<img src="' + xml.escape( logoUrl ) + '" width="50%" height="40%"/>';

            // Replace the placeholders with values.
            xmlString = replacePlaceHolders( xmlString, {
             	logo		: logo 			? logo			: '<br />',
              	equipment	: assetName 	? assetName 	: '<br />',
              	department	: department	? department	: '<br />',
              	address		: address		? address		: '<br />',
              	prepend		: refPrepend	? refPrepend	: '',
              	ref			: EURId			? EURId			: '',
              	date		: new Date().toDateString(),
              	customer	: customerName	? customerName	: '<br />',
              	prepared	: preparedBy	? preparedBy	: '<br />',
              	manager1	: deptManager	? deptManager	: '<br />'
            });

          	// Render the file into the browser.
          	context.response.renderPdf( {xmlString: xmlString} );

        } catch(e){
			errorsArray.push( e.message );
        }
    }

  	function replacePlaceHolders( str, obj )
	{
        // Loop thru all the object properties.
        for( var n in obj ) {
          	eval( 'str = str.replace( /{' + n + '}/g, obj[n] )' );
        }

        // Return the value.
        return str;
	}

    return {
        onRequest: onRequest
    };

});
