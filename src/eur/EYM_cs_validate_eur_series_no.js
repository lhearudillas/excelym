/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(['N/search', 'N/ui/dialog', 'N/log'],

function(search, dialog, log) 
{
    function showErrorMessage()
    {
    	var alertMessage = {
	        title: "ERROR!",
	        message: "This EUR Series No. has already been used. Please check the DEUR form and input the correct value."
	     };
    	
    	dialog.alert(alertMessage);
    }
    
    /**
     * Function to be executed when field is changed.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     * @param {string} scriptContext.fieldId - Field name
     * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
     * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
     *
     * @since 2015.2
     */
    function fieldChanged(scriptContext) 
    {
    	try {
    		if (scriptContext.fieldId == 'custrecord_eym_deur_eur_no') {
        		var eurSeriesNo = scriptContext.currentRecord.getValue({
            		fieldId: 'custrecord_eym_deur_eur_no'
            	});
        		
        		if (eurSeriesNo) {
                    var recordFilter = [
                        search.createFilter({
                            name: 'custrecord_eym_deur_eur_no',
                            operator: search.Operator.EQUALTO,
                            values: eurSeriesNo
                        })
                    ];

        			var recordSearch = search.create({
        				type: 'customrecord_eym_eur_header',
        				title: 'EUR Search',
        				filters: recordFilter
        			});
        			
        			recordSearch = recordSearch.run().getRange({
                        start: 0, 
                        end: 1
                    });

                    if (recordSearch.length > 0) {
                        showErrorMessage();
                    }
        		}
        	}
    	} catch (e) {
    		log.error({
    			title: e.name,
                details: e.message
            }); 
    	}
    }
    
    /**
     * Validation function to be executed when record is saved.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @returns {boolean} Return true if record is valid
     *
     * @since 2015.2
     */
    function saveRecord(scriptContext) 
    {
    	try {
    		var eurSeriesNo = scriptContext.currentRecord.getValue({
        		fieldId: 'custrecord_eym_deur_eur_no'
        	});
            	
            if (eurSeriesNo) {
        		var recordFilter = [
                    search.createFilter({
                        name: 'custrecord_eym_deur_eur_no',
                        operator: search.Operator.EQUALTO,
                        values: eurSeriesNo
                    })
                ];
        		
        		var recordSearch = search.create({
                    type: 'customrecord_eym_eur_header',
                    title: 'EUR Search',
                    filters: recordFilter
                });
                
                recordSearch = recordSearch.run().getRange({
                    start: 0, 
                    end: 1
                });

                if (recordSearch.length > 0) {
                    showErrorMessage();
                    return false;
                }
            }

            return true;
    	} catch (e) {
    		log.error({
    			title: e.name,
                details: e.message
            }); 
    	}
    }

    return {
        fieldChanged: fieldChanged,
        saveRecord: saveRecord
    };
    
});
