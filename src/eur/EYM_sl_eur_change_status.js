/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       12 Jun 2018     Roy Selim
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response){

	// Run the script for GET request.
	if(request.getMethod() == 'GET') {

		// Declare scope variables.
		var projectNum 			= null;
		var EURItems			= null;

		// Declare scope constants.
		const EUR_RECORD_STR	= 'customrecord_eym_eur_header';
		const EUR_RECID_STR		= 'recordId';
		const EUR_STATUS_STR	= 'custrecord_eym_eur_h_status';
		const EUR_POSTED_NUM	= 3;
		const EUR_RECSTR_STR	= 'RECORD';
		const EUR_PROJFLD_STR	= 'custrecord_eym_deur_h_proj';
		const EUR_SUBDEPT_STR	= 'custrecord_eym_deur_h_ed_subdept';
		const EUR_LOGISTC_NUM	= 1;
		const EUR_DUER_NUM_FLD	= 'custrecord_eym_deur_num';
		const EUR_EUR_REC_STR	= 'customrecord_eym_psc_eur';
      	const EUR_SOA_FIELD		= 'custrecord_eym_eur_h_soa';

		// Get querystring from the request object.
		var recordIdStr = request.getParameter( EUR_RECID_STR );

		// Check if there is an id retrieved.
		if( recordIdStr ) {

			// try to load the record.
			var tryEURRecordObj = tryLoadRecord( EUR_RECORD_STR, recordIdStr );

			// If there is a record loaded.
			if( !tryEURRecordObj.error && tryEURRecordObj.value ) {

				// Assign to a variable 
				var EURRecordObj = tryEURRecordObj.value

				// Post EUR to project ledger or equipment ledger.
				projectNum = EURRecordObj.getFieldValue( EUR_PROJFLD_STR );

				// Get the sub department value.
				var subDeptNum = EURRecordObj.getFieldValue( EUR_SUBDEPT_STR );

				// Post to project ledger. Start by getting all the usage record.
				var filters = { custrecord_eym_deur_num : { operator: 'is', value: recordIdStr } };

				// Create a columns variable.
				var columns = [ 'internalid', 'custrecord_eym_deur_total', 'custrecord_eym_eur_bqi' ];

				// Search all eur items under this id.
				var tryEURObj = trySearchRecord( EUR_EUR_REC_STR, null, filters, columns );

				// Check if there are data loaded.
				if( !tryEURObj.error && tryEURObj.value ) {

					// Assign the items to a variable.
					EURItems = tryEURObj.value;
				}

				// Post to project ledger.
				if( projectNum ) {

					if( subDeptNum && ( subDeptNum == EUR_LOGISTC_NUM ) ) {

						// Try to post the EUR.
						var posted = postToPL( EURRecordObj, EURItems, projectNum );

						// Change the status if all items were successfully posted.
						if( posted ) {

							// When the "Submit" button is clicked, change the record from 'Draft' to 'Submitted'.
							EURRecordObj.setFieldValue( EUR_STATUS_STR, EUR_POSTED_NUM );
						}
					}

				} else {

					// Check if there is an SOA value.
					var SOAVal = EURRecordObj.getFieldValue( EUR_SOA_FIELD );

					// If the SOA is not blank, change the status to posted.
					if( SOAVal ) EURRecordObj.setFieldValue( EUR_STATUS_STR, EUR_POSTED_NUM );
				}
			}

			// Submit the record.
			nlapiSubmitRecord( EURRecordObj );

			// Redirect the page to a record using nlapiSetRedirectURL.
	      	nlapiSetRedirectURL( EUR_RECSTR_STR, EUR_RECORD_STR, recordIdStr );

		} else {

			// Show an error screen.
		}
	}
}

function postToPL( eur, items, project )
{
	// Check if there are items passed.
	if( eur && items ) {

		// Declare scope variables.
		var posted 		= false;
		var bqiArray 	= [];

        // Declare function constants.
        const EUR_PROJECT_LGR	= 'customrecord_eym_proj_ledger_entry';
        const EUR_PROJCODE_FLD	= 'custrecord_eym_ple_proj_code';
        const EUR_EQPT_ASSET	= 'custrecord_eym_ple_equip_asset_num';
        const EUR_EQPT_EUR		= 'custrecord_eym_deur_h_equip_no';
        const EUR_PROJ_LDGR_DT	= 'custrecord_eym_ple_debit';
        const EUR_PROJ_LDGR_CT	= 'custrecord_eym_ple_credit';
        const EUR_PROJ_LDGR_BQI	= 'custrecord_eym_ple_bqi';
        const EUR_EURI_BQI		= 'custrecord_eym_eur_bqi';
        const EUR_EURI_TOTAL	= 'custrecord_eym_deur_total';
        const EUR_EUR_BQI_STR	= 'custrecord_eym_eur_h_bqi';
        const EUR_ACCT_FLD		= 'custrecord_eym_ple_acc_num';
        const EUR_EURI_ID_FLD	= 'internalid';
        const EUR_PRJLGR_EUR	= 'custrecord_eym_ple_deur_line_no';
        const EUR_PREF_LOG_DT	= 'logistics-equipment-rental';
        const EUR_PREF_LOG_CT	= 'logistics-rental-income';
        const EUR_PREF_EQPT_PJ	= 'equipment-dept-project';
        // This is edited for PNID-494
        // const EUR_TRANS_DATE	= 'custrecord_eym_deur_h_date';
        const EUR_TRANS_DATE	= 'custrecord_eym_eur_h_date_completed';
        const EUR_PLTRANS_DATE	= 'custrecord_eym_ple_date';

		// Create journal entry.
		if( !project ) {

			// For journal entry creation.
			// createJE();
		}

		// Create a counter.
		var postCounter = 0;

		// Get the preferences.
		var pref = getPreferences();

		// Loop thru all the items.
		for( var i = 0; i < items.length; i++ ) {

			// Assign the item to a variable.
			var item = items[i];

			// Do this twice
			for( var j = 0; j < 2; j++ ) {

				// Post to project ledger.
				var projectLedgerRec = nlapiCreateRecord( EUR_PROJECT_LGR );

				// Set values.
				projectLedgerRec.setFieldValue( EUR_EQPT_ASSET, eur.getFieldValue( EUR_EQPT_EUR ) );
				projectLedgerRec.setFieldValue( EUR_PRJLGR_EUR, item.getValue( EUR_EURI_ID_FLD ) );

				// Added for PNID-494
				projectLedgerRec.setFieldValue( EUR_PLTRANS_DATE, eur.getFieldValue( EUR_TRANS_DATE ) );

				// Currently hard coded, put to preference later for account.
				if( j == 0 ) {

					// Get the account ID for the debit line.
					var debitAccount = getPreference( pref, EUR_PREF_LOG_DT );

					// Set the account.
					projectLedgerRec.setFieldValue( EUR_ACCT_FLD, debitAccount );
					projectLedgerRec.setFieldValue( EUR_PROJ_LDGR_DT, item.getValue( EUR_EURI_TOTAL ) );
					projectLedgerRec.setFieldValue( EUR_PROJCODE_FLD, project );

					// Try to get the bqi from the EUR item, if not, get it from the EUR.
					var BQINum = item.getValue( EUR_EURI_BQI );
					if( !BQINum ) BQINum = eur.getFieldValue( EUR_EUR_BQI_STR );

					// Push the bqi to the bqi array.
					if( BQINum && ( bqiArray.indexOf( BQINum ) == -1 ) ) bqiArray.push( BQINum );

					// Set the project ledger BQI field.
					projectLedgerRec.setFieldValue( EUR_PROJ_LDGR_BQI, BQINum );

				} else if( j == 1 ) {

					// Get the preferences.
					var creditAccount 	= getPreference( pref, EUR_PREF_LOG_CT );
					var eqpmntProj		= getPreference( pref, EUR_PREF_EQPT_PJ );

					// Set the account.
					projectLedgerRec.setFieldValue( EUR_ACCT_FLD, creditAccount );
					projectLedgerRec.setFieldValue( EUR_PROJ_LDGR_CT, item.getValue( EUR_EURI_TOTAL ) );
					projectLedgerRec.setFieldValue( EUR_PROJCODE_FLD, eqpmntProj );
				}

				// Submit the record.
				var recPosted = nlapiSubmitRecord( projectLedgerRec, false, true );

				// If it was submitted, then increment the counter.
				if( recPosted ) postCounter++;
			}
		}

		// Change the posted status.
		if( ( postCounter / 2 ) == items.length ) posted = true;

		// Update all the bqi usage.
		for( var j = 0; j < bqiArray.length; j++ ) {

			// Update.
			updateBQIUsage( bqiArray[j] );
		}
	}

	return posted;
}