/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       13 Jun 2018     Roy Selim
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet( request, response )
{
	// Get the parameter.
	var id = request.getParameter( 'recordId' );

	// Check if there is a value.
	if( id ) {

		// Initialize scope variables.
		var eurDate 	= null;
		var subDept		= null;
		var tin			= null;
		var poNum		= null;
		var progress	= null;
		var termsDate	= null;
		var salesman	= null;

		// Declare scope constants.
		const SOA_JOB_COMPANY_NME	= 'companyname';
		const SOA_CUSTOMER_REC_NME	= 'customer';
		const SOA_CUSTOMER_ADDRESS	= 'defaultaddress';
      	const SOA_PROJCT_CUST_NME	= 'parent';
      	const SOA_PROJCT_NAME_FLD	= 'job';
      	const SOA_HEADER_DATE		= 'created';
      	const SOA_SUBJECT_STR		= 'Equipment Billing';
      	const SOA_HEADER_PROJ_FLD	= 'custrecord_eym_deur_h_proj';
      	const SOA_HEADER_DATE_FLD	= 'custrecord_eym_deur_h_date';
		const SOA_HEADER_EUR_TYPE	= 'customrecord_eym_eur_header';
		const SOA_HEADER_SUB_DEPT	= 'custrecord_eym_deur_h_ed_subdept';
		const SOA_RECORD_TYP_STR	= 'customrecord_eym_psc_eur';
		const SOA_SCRIPT_XML_STR	= 'custscript_print_soa';
	  	const SOA_BCKGND_IMAGE_LNK	= 'https://system.netsuite.com/core/media/media.nl?id=2263&c=4317943_SB1&h=1036c7fba3b92d8b362b';

		// Get the XML file ID, file info and contents. params: 'custscript_print_soa'
		var XMLFileId 	= nlapiGetContext().getSetting( 'SCRIPT', SOA_SCRIPT_XML_STR );
		var XMLFileInfo = nlapiLoadFile( XMLFileId );
		var xmlString 	= XMLFileInfo.getValue();

	  	// Change the background pdf/img.
	  	xmlString = xmlString.replace( '{backgroundImg}', nlapiEscapeXML( SOA_BCKGND_IMAGE_LNK ) );

		// Get the contents.
		var contents 		= generateContents( id )
		var contentStr		= contents.str;
		var contentTotal	= parseFloat( contents.total ).toFixed( 2 );
		var totalArr		= contents.total.toString().split( '.' );

		// Convert to amount in words the peso side only.
		var amountInWords 	= numberToEnglish( totalArr[0] ) + ( totalArr[1] ? ' and ' + totalArr[1] + '/100' : '' ) + ' Pesos Only';
		amountInWords 		= amountInWords.toUpperCase();

		// Get all the needed information in the header. 
		var projId 		= tryGet( 'nlapiLookupField( \''+ SOA_HEADER_EUR_TYPE +'\', '+ id +', \''+ SOA_HEADER_PROJ_FLD +'\' )' );
		var compName 	= tryGet( 'nlapiLookupField( \''+ SOA_PROJCT_NAME_FLD +'\', '+ projId +', \''+ SOA_JOB_COMPANY_NME +'\' )' );
		var custId 		= tryGet( 'nlapiLookupField( \''+ SOA_PROJCT_NAME_FLD +'\', '+ projId +', \''+ SOA_CUSTOMER_REC_NME +'\' )' );
		var custName	= tryGet( 'nlapiLookupField( \''+ SOA_CUSTOMER_REC_NME +'\', '+ custId +', \''+ SOA_JOB_COMPANY_NME +'\' )' );
		var custAdd		= tryGet( 'nlapiLookupField( \''+ SOA_CUSTOMER_REC_NME +'\', '+ custId +', \''+ SOA_CUSTOMER_ADDRESS +'\' )' );

		// Load the EUR record to get data from the EUR header.
		var eur = tryLoadRecord( SOA_HEADER_EUR_TYPE, id ).value;

		// Check if the EUR record was loaded.
		if( eur ) {

			eurDate = eur.getFieldValue( SOA_HEADER_DATE );
		}

        // Replace the placeholders with values.
		xmlString = replacePlaceHolders( xmlString, {
			customer	: custName 		? custName 		: '<br />',
			address		: custAdd		? custAdd 		: '<br />',
			contentData	: contentStr	? contentStr	: '<br />',
			subject		: 'Equipment Billing',
			date		: eurDate 		? eurDate 		: '<br />',
			total		: contentTotal  ? contentTotal 	: '<br />',
			total2		: contentTotal  ? contentTotal 	: '<br />',
			total3		: contentTotal  ? contentTotal 	: '<br />',
			amountInWords: amountInWords ? amountInWords: '<br />',
			tin			: tin 			? tin			: '<br />',
			poNum		: poNum 		? poNum			: '<br />',
			progress	: progress 		? progress		: '<br />',
			termsDate	: termsDate 	? termsDate		: '<br />',
			salesman	: salesman 		? salesman		: '<br />'
		});

	    // Convert the XML string to PDF, set to response and write on the page.
	    var file = nlapiXMLToPDF( xmlString );
	    response.setContentType( 'PDF', 'Print.pdf ', 'inline' );
	    response.write( file.getValue() );
	}
}

function tryGet( str ) 
{
	var tryValue = tryCatch( str );
	if( !tryValue.error && tryValue.value )
		return tryValue.value
	return null;
}

function replacePlaceHolders( str, obj )
{
	// Loop thru all the object properties.
	for( var n in obj ) {
		str = str.replace( '{' + n + '}', obj[n] );
	}

	// Return the value.
	return str;
}

function generateContents( id )
{
	// Initialize an xml string variable.
	var xmlStr 	= '';
	var total	= 0;

	// Create a search filter.
	var filtersObj = { custrecord_eym_deur_num: { operator: 'anyof', value: id } };

	// Set the return columns for the search.
	var columnsArr = [
		'custrecord_eym_deur_type',
		'custrecord_eym_deur_ea_id',
		'custrecord_eym_deur_date',
		'custrecord_eym_deur_plate_num',
		'custrecord_eym_deur_driver',
		'custrecord_eym_deur_unit_typ',
		'custrecord_eym_deur_start_time',
		'custrecord_eym_deur_end_time',
		'custrecord_eym_deur_no_of',
		'custrecord_eym_deur_price_cubic',
		'custrecord_eym_deur_total'
	];

	// Try to search for UER items under the UER header id.
	var trySearch = trySearchRecord( 'customrecord_eym_psc_eur', null, filtersObj, columnsArr );

	// Check if there are return values.
	if( !trySearch.error && trySearch.value ) {

		// Assign to a variable.
		var eursArr = trySearch.value;

		// Generate the table header.
		xmlStr += '<tr style="border-bottom: 1px solid black; padding-bottom:5px;"><th class="header_row">Type</th><th class="header_row" width="15%">Asset ID</th><th class="header_row">Date</th><th class="header_row">Plate #</th><th class="header_row">Driver</th><th class="header_row" width="4%">Unit</th><th class="header_row">Start</th><th class="header_row">End</th><th class="header_row" width="5%">Qty</th><th class="header_row" width="5%">Rate</th><th align="right" class="header_row">Total</th></tr>';

		// Loop thru all the array values.
		for( var i = 0; i < eursArr.length; i++ ) {

			// Assign the item to a variable.
			var itemObj = eursArr[i];

			// Append the opening <tr>.
			xmlStr += '<tr>';

			// Loop thru all the fields.
			for( var j = 0; j < columnsArr.length; j++ ){

              	// Design the background color.
              	var bgc = ( i % 2 == 1 ) ? '#eeeeee;' : '#ffffff;';

				// Append with data.
              	if( columnsArr[j] == 'custrecord_eym_deur_total' ) {

              		// Align to the right.
              		xmlStr += '<td align="right" style="background-color: '+ bgc +'">';

              		// Get the total.
              		total += Number( itemObj.getValue( columnsArr[j] ) );
              	} else {

              		// Align left.
              		xmlStr += '<td style="background-color: '+ bgc +'">';
              	}

              	// xmlStr += ( columnsArr[j] == 'custrecord_eym_deur_total' ) ? '<td align="right" style="background-color: '+ bgc +'">': '<td style="background-color: '+ bgc +'">';
				xmlStr += ( columnsArr[j] == 'custrecord_eym_deur_type' || columnsArr[j] == 'custrecord_eym_deur_ea_id' ) ?	itemObj.getText( columnsArr[j] ): itemObj.getValue( columnsArr[j] );
				xmlStr += '</td>';
			}

			// Append the closing <tr>.
			xmlStr += '</tr>';
		}
	}

	// Put to the debug.
	// nlapiLogExecution( 'DEBUG', 'html', xmlStr );

  	// Return the xml string.
  	return { str: xmlStr, total: total };
}

function numberToEnglish( n ) {

    var string = n.toString(), units, tens, scales, start, end, chunks, chunksLen, chunk, ints, i, word, words, and = '';

    /* Is number zero? */
    if( parseInt( string ) === 0 ) {
        return 'zero';
    }

    /* Array of units as words */
    units = [ '', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen' ];

    /* Array of tens as words */
    tens = [ '', '', 'Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety' ];

    /* Array of scales as words */
    scales = [ '', 'Thousand', 'Million', 'Billion', 'Trillion', 'Quadrillion', 'quintillion', 'sextillion', 'septillion', 'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quatttuor-decillion', 'quindecillion', 'sexdecillion', 'septen-decillion', 'octodecillion', 'novemdecillion', 'vigintillion', 'centillion' ];

    /* Split user arguemnt into 3 digit chunks from right to left */
    start = string.length;
    chunks = [];
    while( start > 0 ) {
        end = start;
        chunks.push( string.slice( ( start = Math.max( 0, start - 3 ) ), end ) );
    }
    /* Check if function has enough scale words to be able to stringify the user argument */
    chunksLen = chunks.length;
    if( chunksLen > scales.length ) {
        return '';
    }

    /* Stringify each integer in each chunk */
    words = [];
    for( i = 0; i < chunksLen; i++ ) {

        chunk = parseInt( chunks[i] );

        if( chunk ) {

            /* Split chunk into array of individual integers */
            ints = chunks[i].split( '' ).reverse().map( parseFloat );

            /* If tens integer is 1, i.e. 10, then add 10 to units integer */
            if( ints[1] === 1 ) {
                ints[0] += 10;
            }

            /* Add scale word if chunk is not zero and array item exists */
            if( ( word = scales[i] ) ) {
                words.push( word );
            }

            /* Add unit word if array item exists */
            if( ( word = units[ ints[0] ] ) ) {
                words.push( word );
            }

            /* Add tens word if array item exists */
            if( ( word = tens[ ints[1] ] ) ) {
                words.push( word );
            }

            /* Add 'and' string after units or tens integer if: */
            if( ints[0] || ints[1] ) {

                /* Chunk has a hundreds integer or chunk is the first of multiple chunks */
                if( ints[2] || ! i && chunksLen ) {
                    words.push( and );
                }
            }
            /* Add hundreds word if array item exists */
            if( ( word = units[ ints[2] ] ) ) {
                words.push( word + ' Hundred' );
            }
        }
    }

    return words.reverse().join( ' ' );
}