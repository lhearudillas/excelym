/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/runtime', 'N/search'],

function( record, runtime, search ) {

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {string} scriptContext.type - Trigger type
     * @param {Form} scriptContext.form - Current form
     * @Since 2015.2
     */
    function beforeLoad(scriptContext) {

    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function beforeSubmit(scriptContext)
    {
    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function afterSubmit( scriptContext )
    {
    	// Assign scriptContext to a shorter variable.
    	var cntx 	= scriptContext
    	var typ 	= cntx.UserEventType;

    	// Run the code for EDIT operations.
    	if( cntx.type === typ.XEDIT ) {

    		// Initialize scope constants.
    		const EUR_HEADER_SOA	= 'custrecord_eym_eur_h_soa';
    		const EUR_STATUS_FLD	= 'custrecord_eym_eur_h_status'; 

        	// Get the old and the new record.
        	var oRec = cntx.oldRecord;
        	var nRec = cntx.newRecord;

        	// Get the values of the SOA field.
        	var oSoa = oRec.getValue( {fieldId: EUR_HEADER_SOA} );
        	var nSoa = nRec.getValue( {fieldId: EUR_HEADER_SOA} );

    		// Load the record for editing. Context records cannot be edited.
    		var rec = record.load( {type: 'customrecord_eym_eur_header', id: nRec.id} );

        	// Get the project and the sub department.
        	var proj = rec.getValue( {fieldId: 'custrecord_eym_deur_h_proj'} );

	    	// If there is a value on the field, change it to the original value.
	    	if( proj ) {

        		// Set the SOA back to the old record's value.
        		rec.setValue( {fieldId: EUR_HEADER_SOA, value: oSoa} );
	    	}

	    	// Verify if there was a change on the field.
	    	else if( !oSoa && nSoa ) {

		    	// Get the EUR completed field value.
		    	var stts = rec.getValue( {fieldId: EUR_STATUS_FLD} );

		    	// Get the user role.
		    	var role = runtime.getCurrentUser().role;

		    	// Get the preferencs access.
		    	var pref = getPreference( {name: 'eur-post-button'} );

		    	// Initialize an variable for the preferences.
		    	var prefArr = null;

		    	// Check if there is a returned value.
		    	if( pref ) {

		    		// For multiple values, split the preferences.
		    		prefArr = pref.split(',');
		    	}

		    	// Change the record if status is completed and user is from accounting.
		    	if( stts == 2 && prefArr && ( prefArr.indexOf( role.toString() ) != -1 ) ) {

			    	// Set the status to posted.
		    		rec.setValue( {fieldId: EUR_STATUS_FLD, value: 3} );
		    	}
	    	}

	    	// Save the record.
	    	rec.save( {} );
    	}
    }

    // Get preference function.
	function getPreference( options )
	{
		// Initialize return value.
		var returnValue = null;

		// Attempt to get the preference
		try{
			returnValue = search.create({
				type: 'customrecord_eym_preferences',
				columns: [ {name:'custrecord_eym_pref_value'} ],
				filters: [ {name: 'name', operator: 'is', values: [ options.name ] } ]
			}).run().getRange( {start:0, end:1} )[0].getValue( {name:'custrecord_eym_pref_value'} );
		} catch(e){
			// Log some errors here.
		}

		// Return a value.
		return returnValue;
	}

    return {
        afterSubmit: afterSubmit
    };
});
