function getPreferences()
{
	var columns = new Array();
	columns.push(new nlobjSearchColumn('name'));
	columns.push(new nlobjSearchColumn('custrecord_eym_pref_value'));
	return nlapiSearchRecord('customrecord_eym_preferences', null, null, columns);	
}

function getPreference(epRess, name, isArray, separator)
{
	var value = "";
	
	if(epRess == undefined)
		epRess = null;
	
	if(isArray == undefined)
		isArray = false;
	
	if(separator == undefined)
		separator = ',';
	
	if(epRess != null){
		for(var i = 0; i < epRess.length; i++) {	
			if(epRess[i].getValue('name') == name){
				value = epRess[i].getValue('custrecord_eym_pref_value');
				break;
			}	
		}
	}
	
	if(isArray == true) {
		if(isNullOrEmpty(value)) {
			value = null;
		}else {
			value = value.split(separator);
		}
	}
	
	return value;
}

function isNullOrEmpty(value) {
	var isNullOrEmpty = true;
	
	if (value != undefined && value != null) {
		if (typeof (value) == 'string') {
			if (value.length > 0)
				isNullOrEmpty = false;
		}else {
			isNullOrEmpty = false;
		}
	}		
	
	return isNullOrEmpty;
}