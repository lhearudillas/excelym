/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       11 Jun 2018     Roy Selim
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 *
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad( type, form, request )
{
	// Run the script in view type.
	if( type == 'view' ){

		// Initialize scope variables.
		var btnScript = null;

		// Define scope integer constants.
		const EUR_STTS_NUM	= 2;
		const EUR_STTS_PSTD	= 3;
		const EUR_SBDPT_LOG	= 1;
		const EUR_SBDPT_RNT	= 2;

		// Define scope string constants.
		const EUR_ACTG_STR	= 'eur-post-button';
		const EUR_BQI_STR	= 'custrecord_eym_eur_h_bqi';
		const EUR_CUST_BTN	= 'custpage_post_eur_btn';
		const EUR_CUST_LBL	= 'Post EUR';
		const EUR_PRNT_BTN	= 'custpage_print_soa_btn';
		const EUR_PRNT_DPLY	= 'customdeploy_sl_eym_eur_soa_printing';
		const EUR_PRNT_LBL	= 'Print SOA';
		const EUR_PRNT_SCRT	= 'customscript_sl_eym_eur_soa_printing';
		const EUR_RECD_STR	= 'customrecord_eym_eur_header';
		const EUR_STLT_DPLY	= 'customdeploy_sl_eym_eur_change_status';
		const EUR_STLT_SCPT	= 'customscript_sl_eym_eur_change_status';
		const EUR_STTS_STR	= 'custrecord_eym_eur_h_status';
		const EUR_SUB_DEPT	= 'custrecord_eym_deur_h_ed_subdept';
		const EUR_PROJ_FLD	= 'custrecord_eym_deur_h_proj';
		const EUR_ERR_PROJ	= 'Please indicate the BQI number that this EUR will be charged to.';
		const EUR_ERR_RENT	= 'Please indicate the SOA record of this EUR.';
		const EUR_SOA_FLD	= 'custrecord_eym_eur_h_soa';
		const EUR_DEUR_FLD	= 'customrecord_eym_psc_eur';

		// Load the EYM preferences.
		var prefArr = getPreferences();

		// Get the roles where EUR post button is visible.
		var alwdRolesArr = getPreference( prefArr, EUR_ACTG_STR, true );

		// Get the roles of the logged in user.
		var userRolesNum = nlapiGetRole();

		// Check if the user has allowed roles.
		if( alwdRolesArr.indexOf( userRolesNum.toString() ) != -1 ) {

			// Get the EUR Id.
			var EURIdNum = nlapiGetRecordId();

			// Load the record.
			var EURRec = nlapiLoadRecord( EUR_RECD_STR, EURIdNum );

			// Get header field values.
			var BQINum 			= EURRec.getFieldValue( EUR_BQI_STR );
			var EURSubDept 		= EURRec.getFieldValue( EUR_SUB_DEPT );
			var EURStatusNum 	= EURRec.getFieldValue( EUR_STTS_STR );
			var EURProjId		= EURRec.getFieldValue( EUR_PROJ_FLD );
			var SOAVal 			= EURRec.getFieldValue( EUR_SOA_FLD );

			// For project ledger, make sure there is a bqi value.
			if( EURProjId && !BQINum ) 	btnScript = 'alert(\''+ EUR_ERR_PROJ +'\')';

			// For affiliates and rentals, make sure that there is an SOA associated.
			if( !EURProjId && !SOAVal ) btnScript = 'alert(\''+ EUR_ERR_RENT +'\')';

			// Make sure that there EUR items before showing the POST button.
			var filters = { custrecord_eym_deur_num: { operator: 'is', value: EURIdNum } };
			var trySrch = trySearchRecord( EUR_DEUR_FLD, null, filters, ['internalid'] );

			// Display post EUR button only if status is completed.
			if( EURStatusNum == EUR_STTS_NUM && trySrch.value ) {

				// Add a 'Post EUR' button to the form.
				form = generateButton( form, EURIdNum, EUR_STLT_SCPT, EUR_STLT_DPLY, EUR_CUST_BTN, EUR_CUST_LBL, btnScript );
			}

			/* Display a Print SOA button, if the EUR has already been posted.
			if( EURStatusNum == EUR_STTS_PSTD && EURSubDept == EUR_SBDPT_LOG ) {

				// Add a 'Print SOA' button to the form.
				form = generateButton( form, EURIdNum, EUR_PRNT_SCRT, EUR_PRNT_DPLY, EUR_PRNT_BTN, EUR_PRNT_LBL, btnScript );
			}
			*/
		}
	}
}

function generateButton( form, id, slScript, slDeploy, btnId, btnLbl, btnScript )
{
	// Generate the base URL.
	var baseURLStr = nlapiResolveURL( 'SUITELET', slScript, slDeploy );

	// Generate the redirect URL.
	var redirectURLStr = baseURLStr + '&recordId=' + id;

    // Create a script and associate it with the button.
	if( !btnScript )
		btnScript = "window.location.assign('"+ redirectURLStr +"')";

  	// Add the post button.
  	form.addButton( btnId, btnLbl, btnScript );

  	// Return the form.
  	return form;
 }