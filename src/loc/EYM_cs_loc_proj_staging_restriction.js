/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       13 Jun 2018     EYM-013
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType 
 * 
 * @param {String} type Access mode: create, copy, edit
 * @returns {Void}
 */


function clientSaveRecord()
{
	if (type == 'edit') {
		alert('You cannot edit this location.');
		return false;
	}
	
    return true;
}
