/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       13 Jun 2018     EYM-013
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
var IDS_WAREHOUSE_HEAD = 1015;


function userEventBeforeLoad(type, form, request)
{
  	if (type == 'view') {
        var role = nlapiGetRole();
		var locId = nlapiGetRecordId();
		
		if (locId == 102) {
			form.removeButton('edit');
		}
    }
}
