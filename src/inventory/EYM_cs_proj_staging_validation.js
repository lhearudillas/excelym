/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       14 Jun 2018     EYM-013
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Sublist internal id
 * @param {String} name Field internal id
 * @param {Number} linenum Optional line item number, starts from 1
 * @returns {Boolean} True to continue changing field value, false to abort value change
 */

var IDS_FROM_LOCATION = 'location';
var IDS_TO_LOCATION = 'transferlocation';
var IDS_ADJUSTMENT_LOCATION = 'adjlocation';


function clientValidateField(type, name, linenum)
{
   
	if (name == IDS_FROM_LOCATION || name == IDS_TO_LOCATION) {

		var fromLoc = nlapiGetFieldValue(IDS_FROM_LOCATION);
		var toLoc = nlapiGetFieldValue(IDS_TO_LOCATION);
		
		if (fromLoc == 102 || toLoc == 102) {
			alert("Project Staging is a virtual location. You are not allowed to do manual transfers.");
			return false;
		} 
	}
	
	if (name == IDS_ADJUSTMENT_LOCATION) {

		var adjLoc = nlapiGetFieldValue(IDS_ADJUSTMENT_LOCATION);
		
		if (adjLoc == 102) {
			alert("Project Staging is a virtual location. You are not allowed to do manual transfers.");
			return false;
		} 
	}
	
    return true;
}

