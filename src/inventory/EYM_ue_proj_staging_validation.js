/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       13 Jun 2018     EYM-013
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
var IDS_FROM_LOC = 'location';
var IDS_TO_LOC = 'transferlocation';


function userEventBeforeLoad(type, form, request)
{
	
	if (type == 'create' || type == 'edit') {
	    
	    
		form.getField(IDS_FROM_LOC).setDisplayType('hidden');
	    form.getField(IDS_TO_LOC).setDisplayType('hidden');
	    
	    var fromLoc = form.addField('custpage_from_loc', 'select', 'FROM LOCATION', 'location');
	    var toLoc = form.addField('custpage_to_loc', 'select', 'TO LOCATION');
	    
	    
	    
	    /*
	    var res = nlapiSearchRecord(null, 'customsearch_eym_loc_except_proj_staging');	
	    
	    fromLoc.addSelectOption('','');
	    toLoc.addSelectOption('','');

	    if (res) {
	    	
			for(var i = 0; i < res.length; i++) {
				var locId = res[i].getId();
				var locName = res[i].getValue('name');
				
				fromLoc.addSelectOption(locId, locName);   
				toLoc.addSelectOption(locId, locName);                	

			}	

	    }
	    */

	}
	
}

/*
function userEventAfterSubmit(type) 
{
	if (type == 'create') {
		
		var recId = nlapiGetRecordId();
		var rectype = nlapiGetRecordType();

		var recInv = nlapiLoadRecord(rectype, recId);
		
		var fromLocVal = recInv.getFieldValue('custpage_from_loc');
		var toLocVal = recInv.getFieldValue('custpage_to_loc');
		
		recInv.setFieldValue(IDS_FROM_LOC, fromLocVal);
		recInv.setFieldValue(IDS_TO_LOC, toLocVal);
		
	}
}
*/