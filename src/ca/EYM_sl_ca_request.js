/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       15 Mar 2018     Lhea
 *
 */

// Constants - ACAR
var IDS_ACAR_AMOUNT = 'custrecord_eym_acar_amount';
var IDS_ACAR_APPROVAL_STATUS = 'custrecord_eym_acar_approval_status';
var IDS_ACAR_APPROVED_BY = 'custrecord_eym_acar_approved_by';
var IDS_ACAR_BANK = 'custrecord_eym_acar_bank';
var IDS_ACAR_CHECK_REF = 'custrecord_eym_acar_check_ref';
var IDS_ACAR_DEPARTMENT = 'custrecord_eym_acar_department';
var IDS_ACAR_PROJECT = 'custrecord_eym_acar_project';
var IDS_ACAR_PURPOSE = 'custrecord_eym_acar_purpose';
var IDS_ACAR_REQUESTED_BY = 'custrecord_eym_acar_requested_by';
var IDS_APPROVER_PINCODE = 'custentity_eym_approver_pincode';

// Constants - CHEQUE
var IDS_CHECK_ACCOUNT = 'account';
var IDS_CHECK_CURRENCY = 'currency';
var IDS_CHECK_ENTITY = 'entity';
var IDS_CHECK_EXPENSE_AMOUNT = 'amount';
var IDS_CHECK_EXPENSE_CUSTOMER = 'customer';
var IDS_CHECK_EXPENSE_DEPARTMENT = 'department';
var IDS_CHECK_EXPENSE_LIST = 'expense';
var IDS_CHECK_EXPENSE_TAX_CODE = 'taxcode';
var IDS_CHECK_REMARKS = 'custbody_eym_ap_remarks';
var IDS_CHECK_TO_BE_PRINTED = 'tobeprinted';
var IDS_EXPENSE_ACCOUNT_PREFERENCE = 'aca-createcheck-expense';

// Constants - CUSTOM PAGE
var IDS_CUSTPAGE_RECORD_ID = 'custpage_rec_id';
var IDS_CUSTPAGE_RECORD_TYPE = 'custpage_rec_type';
var IDS_CUSTPAGE_STATUS = 'custpage_status';
var IDS_CUSTPAGE_APPROVER_PIN = 'custpage_employee_pin';

// Constants - RECORDS
var IDS_RECORD = 'RECORD';
var IDS_RECORD_CHECK = 'check';
var IDS_RECORD_EMPLOYEE = 'employee';
var IDS_RECORD_DEPARTMENT = 'department';

// Constants - OTHERS
var IDS_DEPARTMENT_PURCHASE_APPROVER = 'custrecord_eym_dept_approver';
var IDS_RECORD_ID = 'recId';
var IDS_RECORD_TYPE = 'recType';
var IDS_STATUS = 'status';

function verifyPin(request, response)
{
	if (request.getMethod() == 'GET') {	
		var recId = request.getParameter(IDS_RECORD_ID);
		var recType = request.getParameter(IDS_RECORD_TYPE);
		var status = request.getParameter(IDS_STATUS);
		
		// Status == Draft
		if (status == IDR_APPROVAL_STATUS_DRAFT) {
			var recordObj = {};
			recordObj[IDS_ACAR_APPROVAL_STATUS] = IDR_APPROVAL_STATUS_SUBMITTED;
			
			updateNSRecord(recType, recId, recordObj);
			nlapiSetRedirectURL(IDS_RECORD, recType, recId);
		} else {
			doGet(recId, recType, status, response);
		}
	} else if (request.getMethod() == 'POST') {
		doPost(request, response);
	}
}

/**
 * DO request - PIN UI
 */
function doGet(recId, recType, status, response)
{	
	var epRess = getPreferences();
	var mainCss = getPreference(epRess, 'main-css');
	
	var hiddenFields = {};
	hiddenFields[IDS_CUSTPAGE_RECORD_ID] = recId;
	hiddenFields[IDS_CUSTPAGE_RECORD_TYPE] = recType;
	hiddenFields[IDS_CUSTPAGE_STATUS] = status;
	
	var html = pinCodeUI(mainCss, hiddenFields);
	
	response.write(html);  
}

/**
 * POST request - Process and validate PIN
 * Set fields according to status
 */
function doPost(request, response)
{
	var recId = request.getParameter(IDS_CUSTPAGE_RECORD_ID);
	var recType = request.getParameter(IDS_CUSTPAGE_RECORD_TYPE);
	var status = request.getParameter(IDS_CUSTPAGE_STATUS);
	var pin = nlapiEncrypt(request.getParameter(IDS_CUSTPAGE_APPROVER_PIN), 'sha1');
	
	try {
		var approverIds = getApproverId();
		var IDR_EMPLOYEE_ACA_SCREENER = approverIds['IDR_EMPLOYEE_ACA_SCREENER'];
		var IDR_EMPLOYEE_LMUL = approverIds['IDR_EMPLOYEE_LMUL'];
		
		var recFields = new Array();
		recFields.push(IDS_ACAR_APPROVED_BY);
		recFields.push(IDS_ACAR_AMOUNT);
		
		var recValues = nlapiLookupField(recType, recId, recFields);
		var approvedById = recValues[IDS_ACAR_APPROVED_BY];
	    var amount = recValues[IDS_ACAR_AMOUNT];
		var errorMessage = '';
		
		// Approved by field is not empty
		if (approvedById) {
			var approverPin = nlapiLookupField(IDS_RECORD_EMPLOYEE, approvedById, IDS_APPROVER_PINCODE);
			var recordObj = {};
			
			// Correct PIN
			if (approverPin == pin) {
				// Status == Submitted
				if (status == IDR_APPROVAL_STATUS_SUBMITTED) { 
					var departmentId = nlapiLookupField(recType, recId, IDS_ACAR_DEPARTMENT);
					var departmentHeadId = nlapiLookupField(IDS_RECORD_DEPARTMENT, departmentId, IDS_DEPARTMENT_PURCHASE_APPROVER);
					
					recordObj[IDS_ACAR_APPROVAL_STATUS] = IDR_APPROVAL_STATUS_SUPERVISOR_APPROVED;
					recordObj[IDS_ACAR_APPROVED_BY] = departmentHeadId;
				} 
				// Status == Supervisor Approved
				else if (status == IDR_APPROVAL_STATUS_SUPERVISOR_APPROVED) { 
					var limit = 10000;
					
					// For amount 10K and below
					if (amount <= limit) { 
						// Write check, set status to Approved and populate check field
						var checkId = writeCheck(recId, recType);
						recordObj[IDS_ACAR_APPROVAL_STATUS] = IDR_APPROVAL_STATUS_APPROVED;
						
						if (checkId) {
							recordObj[IDS_ACAR_CHECK_REF] = checkId;
						}
					} else { // For amount greater than 10K
						recordObj[IDS_ACAR_APPROVAL_STATUS] = IDR_APPROVAL_STATUS_MANAGER_APPROVED;
					}
					// Approver == ACA screener for both
					recordObj[IDS_ACAR_APPROVED_BY] = IDR_EMPLOYEE_ACA_SCREENER;
				} 
				// Status == Manager Approved or Status == Approved and Approver == ACA Screener
				else if (status == IDR_APPROVAL_STATUS_MANAGER_APPROVED || 
						(status == IDR_APPROVAL_STATUS_APPROVED && approvedById == IDR_EMPLOYEE_ACA_SCREENER)) {
					// Redirect to edit page of ACA record
					var redirectCA = nlapiResolveURL(IDS_RECORD, recType, recId, true);
				} 
				// Status == Verified HR
				else if (status == IDR_APPROVAL_STATUS_VERIFIED_HR) {
					recordObj[IDS_ACAR_APPROVAL_STATUS] = IDR_APPROVAL_STATUS_VERIFIED_LMUL;
					recordObj[IDS_ACAR_APPROVED_BY] = IDR_EMPLOYEE_LMUL;
				} 
				// Status == Verified LMUL
				else if (status == IDR_APPROVAL_STATUS_VERIFIED_LMUL) {
					// Write check, set status to Approved and populate check field
					var checkId = writeCheck(recId, recType);
					recordObj[IDS_ACAR_APPROVAL_STATUS] = IDR_APPROVAL_STATUS_APPROVED;
					
					if (checkId) {
						recordObj[IDS_ACAR_CHECK_REF] = checkId;
					}
				} 
				// Status == Rejected
				else {
					var redirectCA = nlapiResolveURL(IDS_RECORD, recType, recId);
					recordObj[IDS_ACAR_APPROVAL_STATUS] = IDR_APPROVAL_STATUS_REJECTED;
				}
				
				if (status != IDR_APPROVAL_STATUS_MANAGER_APPROVED && status != IDR_APPROVAL_STATUS_REJECTED &&
						(status != IDR_APPROVAL_STATUS_APPROVED && approvedById != IDR_EMPLOYEE_ACA_SCREENER)) {
					updateNSRecord(recType, recId, recordObj);
					alertResponse(null, null, response);
				} else {
					if (status == IDR_APPROVAL_STATUS_REJECTED) {
						updateNSRecord(recType, recId, recordObj);
					}
					alertResponse(null, redirectCA, response);
				}
			} else { // Incorrect PIN
				// Error message for ACA Screener
				if (status == IDR_APPROVAL_STATUS_MANAGER_APPROVED) {
					errorMessage = 'ERROR: PIN Code entered does not match the assigned ACAR Screener.';
				} else { // Error message for other approver
					errorMessage = 'ERROR: PIN Code entered does not match the assigned approver.';
				}
				
				alertResponse(errorMessage, null, response);
			}
		} else { // Approved by field is empty
			errorMessage = 'ERROR: Empty field - Approved By.';
			alertResponse(errorMessage, null, response);
		}
	} catch (e) {
		if(e instanceof nlobjError) {
			errorMessage = e.getCode() + ': ' + e.getDetails();
		} else {
			errorMessage = 'UNEXPECTED ERROR: ' + e.toString();	
		}
        
		nlapiLogExecution('DEBUG', 'Error', errorMessage);
	}
}

/**
 * Alert UI
 */
function alertResponse(errorMessage, url, response) 
{
	var html = ''; 
	
	html = '<html>';
	html += '<head>';
	html += '<script language="JavaScript">';	
	
	if (!isNullOrEmpty(errorMessage)) {
		html += 'alert("' + errorMessage + '");';
		html += 'window.close();';
	}
	
	if (!isNullOrEmpty(url)) {
		html += 'window.close();';
		html += 'window.opener.location.href = "' + url + '";';
	}
	
	if (isNullOrEmpty(errorMessage) && isNullOrEmpty(url)) {
		html += 'window.close();';
		html += 'window.opener.location.reload();';
	}
		
	html += '</script>';
	html += '</head>';
	html += '<body>';
	html += '</body>';
	html += '</html>';
	
	response.write(html);
}

/**
 * Create Check
 */
function writeCheck(recId, recType) 
{
	// Get values from ACA record
	var recFields = new Array();
	recFields.push(IDS_ACAR_BANK);
	recFields.push(IDS_ACAR_REQUESTED_BY);
	recFields.push(IDS_ACAR_AMOUNT);
	recFields.push(IDS_ACAR_PURPOSE);
	recFields.push(IDS_ACAR_DEPARTMENT);
	recFields.push(IDS_ACAR_PROJECT);
	
	var recValues = nlapiLookupField(recType, recId, recFields);
	var account = recValues[IDS_ACAR_BANK];
    var requestedBy = recValues[IDS_ACAR_REQUESTED_BY];
    var amount = recValues[IDS_ACAR_AMOUNT];
    var purpose = recValues[IDS_ACAR_PURPOSE];
    var department = recValues[IDS_ACAR_DEPARTMENT];
    var project = recValues[IDS_ACAR_PROJECT];
    
    // Create Check record and set fields 
	var check = nlapiCreateRecord(IDS_RECORD_CHECK);
	check.setFieldValue(IDS_CHECK_ACCOUNT, account);
	check.setFieldValue(IDS_CHECK_ENTITY, requestedBy);
	check.setFieldValue(IDS_CHECK_REMARKS, purpose);
	check.setFieldValue(IDS_CHECK_CURRENCY, 1);
	check.setFieldValue(IDS_CHECK_TO_BE_PRINTED, 'T');
	
	// Get value of expense account from preferences
	var epRess = getPreferences();
	var accountRole = getPreference(epRess, IDS_EXPENSE_ACCOUNT_PREFERENCE, false);
    
	// Set line item fields
	for (var i = 1; i <= 1; i++) {
		check.setLineItemValue(IDS_CHECK_EXPENSE_LIST, IDS_CHECK_ACCOUNT, i, accountRole);
		check.setLineItemValue(IDS_CHECK_EXPENSE_LIST, IDS_CHECK_EXPENSE_AMOUNT, i, amount);
		check.setLineItemValue(IDS_CHECK_EXPENSE_LIST, IDS_CHECK_EXPENSE_TAX_CODE, i, 5);
		check.setLineItemValue(IDS_CHECK_EXPENSE_LIST, IDS_CHECK_EXPENSE_DEPARTMENT, i, department);
		check.setLineItemValue(IDS_CHECK_EXPENSE_LIST, IDS_CHECK_EXPENSE_CUSTOMER, i, project);
    }
	
	var checkId = nlapiSubmitRecord(check, true, true);
    nlapiLogExecution('DEBUG', 'Check', checkId);
    
    return checkId;
	
}

function getApproverId() 
{
	var environment = isProductionEnvironment();
	var approvers = {};
	
	if (environment) {
		var IDR_EMPLOYEE_ACA_SCREENER = 3206;
		var IDR_EMPLOYEE_HR_DIRECTOR = 1054;
		var IDR_EMPLOYEE_LMUL = 3198;
	} else {
		var IDR_EMPLOYEE_ACA_SCREENER = 3187;
		var IDR_EMPLOYEE_HR_DIRECTOR = 1054;
		var IDR_EMPLOYEE_LMUL = 3189;
	}
	
	approvers['IDR_EMPLOYEE_ACA_SCREENER'] = IDR_EMPLOYEE_ACA_SCREENER.toString();
	approvers['IDR_EMPLOYEE_HR_DIRECTOR'] = IDR_EMPLOYEE_HR_DIRECTOR.toString();
	approvers['IDR_EMPLOYEE_LMUL'] = IDR_EMPLOYEE_LMUL.toString();
	
	return approvers;
}