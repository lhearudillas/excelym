/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       15 Mar 2018     Lhea
 *
 */

/**
 *
 * Client script for Account CA Request 
 * 
 */

// Constants
var IDS_ACAR_DEDUCT = 'custrecord_eym_acar_atd';
var IDS_SL_DEPLOY = 'customdeploy_sl_eym_ca_request';
var IDS_SL_SCRIPT = 'customscript_sl_eym_ca_request';
var IDS_SUITELET = 'SUITELET';

function submitCA(recId, recType, status)
{
	var params = 'recId=' + recId + '&recType=' + recType + '&status=' + status;
	
	if (status == IDR_APPROVAL_STATUS_DRAFT) {
		var deduct = nlapiLookupField(recType, recId, IDS_ACAR_DEDUCT);

		if (deduct) {
			var url = nlapiResolveURL(IDS_SUITELET, IDS_SL_SCRIPT, IDS_SL_DEPLOY) + '&' + params;
			window.location.replace(url);
		} else {
			alert("Error: This ACA Request currently does not have an attached Authority To Deduct and could not be submitted for approval. To process this request, kindly attached a fully accomplished Authority To Deduct file.");
		}
	} else {
		pinCodePopup(IDS_SL_SCRIPT, IDS_SL_DEPLOY, params);
	}
}
