/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       14 Mar 2018     Lhea
 *
 */

// Constants - ACAR
var IDS_ACAR_AMOUNT = 'custrecord_eym_acar_amount';
var IDS_ACAR_APPROVED_BY = 'custrecord_eym_acar_approved_by';
var IDS_ACAR_APPROVAL_STATUS = 'custrecord_eym_acar_approval_status';
var IDS_ACAR_ATD = 'custrecord_eym_acar_atd';
var IDS_ACAR_ATD_ATTACHED = 'custrecord_eym_acar_atd_attached';
var IDS_ACAR_CUSTOM_FORM = 'customform';
var IDS_ACAR_CV_NUM = 'custrecord_eym_acar_cv';
var IDS_ACAR_DATE = 'custrecord_eym_acar_date';
var IDS_ACAR_DEPARTMENT = 'custrecord_eym_acar_department';
var IDS_ACAR_LIQUIDATED = 'custrecord_eym_acar_liquidated';
var IDS_ACAR_NOTED_BY = 'custrecord_eym_acar_noted_by';
var IDS_ACAR_PREPARED_BY = 'custrecord_eym_prepared_by';
var IDS_ACAR_PROJECT = 'custrecord_eym_acar_project';
var IDS_ACAR_PURPOSE = 'custrecord_eym_acar_purpose';
var IDS_ACAR_REQUESTED_BY = 'custrecord_eym_acar_requested_by';
var IDS_ACAR_ROLE_PREFERENCE = 'approve-ca-request-allowed-roles';
var IDS_ACAR_SCREENER_SUBMIT = 'custrecord_eym_acar_screener_submit';
var IDS_ACAR_TRAVEL_EXPENSE = 'custrecord_eym_acar_travel_expense';
var IDS_ACAR_VERIFIED = 'custrecord_eym_acar_verified';

//Constants - OTHERS
var IDS_EMPLOYEE_SUPERVISOR = 'supervisor';
var IDS_RECORD = 'RECORD';
var IDS_RECORD_EMPLOYEE = 'employee';

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request)
{
	try {
		var approverIds = getApproverId();
		var IDR_EMPLOYEE_ACA_SCREENER = approverIds['IDR_EMPLOYEE_ACA_SCREENER'];
		
		var approverStatus = nlapiGetFieldValue(IDS_ACAR_APPROVAL_STATUS);
		var approverId = nlapiGetFieldValue(IDS_ACAR_APPROVED_BY);
		var verified = nlapiGetFieldValue(IDS_ACAR_VERIFIED);
		
		if (type == 'view') {
			var context = nlapiGetContext();
			var recId = nlapiGetRecordId();
			var recType = nlapiGetRecordType();
			
			var role = context.getRole().toString();
			var epRess = getPreferences();
			var roles = getPreference(epRess, IDS_ACAR_ROLE_PREFERENCE, true);
			
			form.setScript('customscript_cs_eym_ca_request');
			
			if (approverStatus == IDR_APPROVAL_STATUS_DRAFT) {
				form.addButton('custpage_submit', 'Submit', "submitCA(" + recId + ", '" + recType + "', " + approverStatus + ");");
			}
			
			if (approverStatus != IDR_APPROVAL_STATUS_DRAFT && approverStatus != IDR_APPROVAL_STATUS_SUBMITTED) {
				form.removeButton('edit');
			}
			
			if(isIndexOfArray(roles, role)) {
				if (approverStatus != IDR_APPROVAL_STATUS_DRAFT && approverStatus != IDR_APPROVAL_STATUS_MANAGER_APPROVED
						&& approverStatus != IDR_APPROVAL_STATUS_APPROVED && approverStatus != IDR_APPROVAL_STATUS_REJECTED) {
					form.addButton('custpage_approve', 'Approve', "submitCA(" + recId + ", '" + recType + "', " + approverStatus + ");");
					form.addButton('custpage_reject', 'Reject', "submitCA(" + recId + ", '" + recType + "', " + IDR_APPROVAL_STATUS_REJECTED + ");");
				}
				
				if (approverId == IDR_EMPLOYEE_ACA_SCREENER && verified == 'F' &&
						(approverStatus == IDR_APPROVAL_STATUS_MANAGER_APPROVED || approverStatus == IDR_APPROVAL_STATUS_APPROVED)) {
					form.addButton('custpage_verify', 'Verify', "submitCA(" + recId + ", '" + recType + "', " + approverStatus + ");");
				}
			}
		}
		
		if (type == 'edit') {
			if (approverId == IDR_EMPLOYEE_ACA_SCREENER && (approverStatus == IDR_APPROVAL_STATUS_APPROVED || 
					approverStatus == IDR_APPROVAL_STATUS_MANAGER_APPROVED)) {
				nlapiGetField(IDS_ACAR_CUSTOM_FORM).setDisplayType('disabled');
				nlapiGetField(IDS_ACAR_DATE).setDisplayType('disabled');
				nlapiGetField(IDS_ACAR_REQUESTED_BY).setDisplayType('disabled');
				nlapiGetField(IDS_ACAR_DEPARTMENT).setDisplayType('disabled');
				nlapiGetField(IDS_ACAR_PROJECT).setDisplayType('disabled');
				nlapiGetField(IDS_ACAR_AMOUNT).setDisplayType('disabled');
				nlapiGetField(IDS_ACAR_PURPOSE).setDisplayType('disabled');
				nlapiGetField(IDS_ACAR_CV_NUM).setDisplayType('disabled');
				nlapiGetField(IDS_ACAR_PREPARED_BY).setDisplayType('disabled');
				nlapiGetField(IDS_ACAR_ATD_ATTACHED).setDisplayType('disabled');
				nlapiGetField(IDS_ACAR_SCREENER_SUBMIT).setDisplayType('disabled');
				nlapiGetField(IDS_ACAR_LIQUIDATED).setDisplayType('disabled');
				nlapiGetField(IDS_ACAR_NOTED_BY).setDisplayType('disabled');
				nlapiGetField(IDS_ACAR_ATD).setDisplayType('disabled');
			}
		}
	} catch (e) {
		var errMsg = '';
		if(e instanceof nlobjError) {
			errMsg = e.getCode() + ': ' + e.getDetails();
		} else {
			errMsg = 'UNEXPECTED ERROR: ' + e.toString();	
		}
        
		nlapiLogExecution('DEBUG', 'Error', errMsg);
	}
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type)
{
	try {
		var approverIds = getApproverId();
		var IDR_EMPLOYEE_ACA_SCREENER = approverIds['IDR_EMPLOYEE_ACA_SCREENER'];
		var IDR_EMPLOYEE_HR_DIRECTOR = approverIds['IDR_EMPLOYEE_HR_DIRECTOR'];
		var IDR_EMPLOYEE_LMUL = approverIds['IDR_EMPLOYEE_LMUL'];
		
		if (type != 'xedit') {
			var requestedById = nlapiGetFieldValue(IDS_ACAR_REQUESTED_BY);
			var status = nlapiGetFieldValue(IDS_ACAR_APPROVAL_STATUS);
			var amount = nlapiGetFieldValue(IDS_ACAR_AMOUNT);
			var travelExpense = nlapiGetFieldValue(IDS_ACAR_TRAVEL_EXPENSE);
			var approver = nlapiGetFieldValue(IDS_ACAR_APPROVED_BY);
			var supervisorId = nlapiLookupField(IDS_RECORD_EMPLOYEE, requestedById, IDS_EMPLOYEE_SUPERVISOR);
			
			
			if (type == 'create') {
				nlapiSetFieldValue(IDS_ACAR_APPROVED_BY, supervisorId);
				nlapiSetFieldValue(IDS_ACAR_APPROVAL_STATUS, IDR_APPROVAL_STATUS_DRAFT);
			}
			
			if (type == 'edit') {
				if ((status == IDR_APPROVAL_STATUS_DRAFT || status == IDR_APPROVAL_STATUS_SUBMITTED)) {
					nlapiSetFieldValue(IDS_ACAR_APPROVED_BY, supervisorId);
				}
				
				if (status == IDR_APPROVAL_STATUS_MANAGER_APPROVED) {
					var limit = 10000;
					if (amount > limit && travelExpense == 'T') {
						nlapiSetFieldValue(IDS_ACAR_APPROVAL_STATUS, IDR_APPROVAL_STATUS_VERIFIED_HR);
						nlapiSetFieldValue(IDS_ACAR_APPROVED_BY, IDR_EMPLOYEE_HR_DIRECTOR);
					} else if (amount > limit && travelExpense == 'F') {
						nlapiSetFieldValue(IDS_ACAR_APPROVAL_STATUS, IDR_APPROVAL_STATUS_VERIFIED_LMUL);
						nlapiSetFieldValue(IDS_ACAR_APPROVED_BY, IDR_EMPLOYEE_LMUL);
					}
				}
				
				if (approver == IDR_EMPLOYEE_ACA_SCREENER && (status == IDR_APPROVAL_STATUS_APPROVED || 
						status == IDR_APPROVAL_STATUS_MANAGER_APPROVED)) {
					nlapiSetFieldValue(IDS_ACAR_VERIFIED, 'T');
				}
			}
		}
	} catch (e) {
		var errMsg = '';
		if(e instanceof nlobjError) {
			errMsg = e.getCode() + ': ' + e.getDetails();
		} else {
			errMsg = 'UNEXPECTED ERROR: ' + e.toString();	
		}
        
		nlapiLogExecution('DEBUG', 'Error', errMsg);
	}
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type)
{
	try {
		var approverIds = getApproverId();
		var IDR_EMPLOYEE_ACA_SCREENER = approverIds['IDR_EMPLOYEE_ACA_SCREENER'];
		var IDR_EMPLOYEE_HR_DIRECTOR = approverIds['IDR_EMPLOYEE_HR_DIRECTOR'];
		var IDR_EMPLOYEE_LMUL = approverIds['IDR_EMPLOYEE_LMUL'];
		
		var recId = nlapiGetRecordId();
		var recType = nlapiGetRecordType();
		
		var recFields = new Array();
		recFields.push(IDS_ACAR_APPROVAL_STATUS);
		recFields.push(IDS_ACAR_APPROVED_BY);
		
		var recValues = nlapiLookupField(recType, recId, recFields);
		var status = recValues[IDS_ACAR_APPROVAL_STATUS];
	    var approver = recValues[IDS_ACAR_APPROVED_BY];
		
		if (status == IDR_APPROVAL_STATUS_APPROVED || status == IDR_APPROVAL_STATUS_VERIFIED_HR || 
				status == IDR_APPROVAL_STATUS_VERIFIED_LMUL) {
			if (approver == IDR_EMPLOYEE_ACA_SCREENER || approver == IDR_EMPLOYEE_HR_DIRECTOR || 
					approver == IDR_EMPLOYEE_LMUL) {
				nlapiSetRedirectURL(IDS_RECORD, recType, recId);
			}
		}
	} catch (e) {
		var errMsg = '';
		if(e instanceof nlobjError) {
			errMsg = e.getCode() + ': ' + e.getDetails();
		} else {
			errMsg = 'UNEXPECTED ERROR: ' + e.toString();	
		}
        
		nlapiLogExecution('DEBUG', 'Error', errMsg);
	}
}

function getApproverId() 
{
	var environment = isProductionEnvironment();
	var approvers = {};
	
	if (environment) {
		var IDR_EMPLOYEE_ACA_SCREENER = 3206;
		var IDR_EMPLOYEE_HR_DIRECTOR = 1054;
		var IDR_EMPLOYEE_LMUL = 3198;
	} else {
		var IDR_EMPLOYEE_ACA_SCREENER = 3187;
		var IDR_EMPLOYEE_HR_DIRECTOR = 1054;
		var IDR_EMPLOYEE_LMUL = 3189;
	}
	
	approvers['IDR_EMPLOYEE_ACA_SCREENER'] = IDR_EMPLOYEE_ACA_SCREENER.toString();
	approvers['IDR_EMPLOYEE_HR_DIRECTOR'] = IDR_EMPLOYEE_HR_DIRECTOR.toString();
	approvers['IDR_EMPLOYEE_LMUL'] = IDR_EMPLOYEE_LMUL.toString();
	
	return approvers;
}