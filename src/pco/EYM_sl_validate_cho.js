/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       03 Sep 2017     denny
 *
 */
function isEmpty(fldValue){
	return fldValue == '' || fldValue == null || fldValue == undefined || fldValue == 'null';
}

function validateCho(request, response){
	var subCount = request.getParameter('subCountPerm');
    var pcoTotal = 0;
    var grp = {};
    var error = [];
    for(var i=0;i<subCount;i++){
    	  if(!isEmpty(request.getParameter("txtBoqId-"+i))){
    		  var boqId = request.getParameter("txtBoqId-"+i);
    		  var amount = request.getParameter("txtAmount-"+i).replace(/,/g, '');
    		  if(!(boqId in grp)){
    			  grp[boqId] = [];
	      }
    		  grp[boqId].push(amount);
      }
    }
    
    for(var key in grp){
    		var boqNewTotal = 0;
    		var bqiData = nlapiLookupField('customrecord_eym_boq',key,['custrecord_eym_boq_base_cost','custrecord_eym_boq_totadj']);
    		nlapiLogExecution('DEBUG','bqiData.custrecord_eym_boq_base_cost',bqiData.custrecord_eym_boq_base_cost);
    		nlapiLogExecution('DEBUG','bqiData.custrecord_eym_boq_totadj',bqiData.custrecord_eym_boq_totadj);
    		for (var ss = 0; ss < grp[key].length; ss++ ){
    			boqNewTotal += (grp[key][ss] * 1 );
		}
    		nlapiLogExecution('DEBUG','boqNewTotal',boqNewTotal);
    		var newTotal = (bqiData.custrecord_eym_boq_base_cost*1) + (bqiData.custrecord_eym_boq_totadj*1) + boqNewTotal;
    		nlapiLogExecution('DEBUG','newTotal',newTotal);
    		if(newTotal < 0){
    			nlapiLogExecution('DEBUG','error',key);
    			error.push(key); 
    		}
    }
	var ret = {'error':error};
	response.write(JSON.stringify(ret));
}
