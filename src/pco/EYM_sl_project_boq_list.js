/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       03 Sep 2017     denny
 *
 */

function boqList(request, response){
	var projectId = request.getParameter('projectid');
	var boqFilter = [
		new nlobjSearchFilter('custrecord_eym_boq_projid',null,'is',projectId),
		new nlobjSearchFilter('custrecord_eym_adjto_itemid',null,'is','@NONE@')
	];
	var boqColumn = [
		new nlobjSearchColumn('name'),
		new nlobjSearchColumn('custrecord_eym_proj_subsystem'),
		new nlobjSearchColumn('custrecord_eym_boq_categories'),
		new nlobjSearchColumn('custrecord_eym_act_desc'),
		new nlobjSearchColumn('custrecord_eym_boq_class'),
		new nlobjSearchColumn('custrecord_eym_boq_amount'),
		new nlobjSearchColumn('custrecord_eym_boq_base_cost')
	]
	//classColumn[0].setSort();
	var ret = [];
	var boqs = nlapiSearchRecord('customrecord_eym_boq',null,boqFilter,boqColumn);
	nlapiLogExecution('DEBUG','boqs.length',boqs.length);
	for(var c=0;c<boqs.length;c++){
		var boq = boqs[c];
		nlapiLogExecution('DEBUG','boq',boq.getId());
		ret.push({
			"id" : boq.getId(),
			"name" : boq.getValue("name"),
			"subsystem" : boq.getValue("custrecord_eym_proj_subsystem"),
			"activity" : boq.getValue("custrecord_eym_boq_categories"),
			"description" : boq.getValue("custrecord_eym_act_desc"),
			"costType" : boq.getValue("custrecord_eym_boq_class"),
			"amount" : boq.getValue("custrecord_eym_boq_amount"),
			"baseCost" : boq.getValue("custrecord_eym_boq_base_cost")
		});
	}  
	response.write(JSON.stringify(ret));
	//return '1';
}
