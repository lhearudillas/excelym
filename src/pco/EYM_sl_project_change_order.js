/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       11 Aug 2017     dennysutanto
 *
 */

function isEmpty(fldValue){
	return fldValue == '' || fldValue == null || fldValue == undefined || fldValue == 'null';
}

function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function generateBoqOptions(pId){
	var boqFilter = [
		new nlobjSearchFilter('custrecord_eym_boq_projid',null,'is',pId),
		new nlobjSearchFilter('custrecord_eym_adjto_itemid',null,'is','@NONE@')
  ];
  var boqColumn = [
		new nlobjSearchColumn('name'),
		new nlobjSearchColumn('custrecord_eym_proj_subsystem'),
		new nlobjSearchColumn('custrecord_eym_boq_categories'),
		new nlobjSearchColumn('custrecord_eym_act_desc'),
		new nlobjSearchColumn('custrecord_eym_boq_class'),
		new nlobjSearchColumn('custrecord_eym_boq_base_cost')
  ]
	//classColumn[0].setSort();
  var options = '<option value=""></option>';
  var boqs = nlapiSearchRecord('customrecord_eym_boq',null,boqFilter,boqColumn);
  for(var c=0;c<boqs.length;c++){
		var boq = boqs[c];
		var datas = 'data-subsystem="'+boq.getValue("custrecord_eym_proj_subsystem")+'" ';
		datas += 'data-activity="'+boq.getValue("custrecord_eym_boq_categories")+'" ';
		datas += 'data-description="'+boq.getValue("custrecord_eym_act_desc")+'" ';
		datas += 'data-costtype="'+boq.getValue("custrecord_eym_boq_class")+'" ';
		datas += 'data-amount="'+boq.getValue("custrecord_eym_boq_base_cost")+'" ';
		datas += 'data-basecost="'+boq.getValue("custrecord_eym_boq_base_cost")+'"';
		
		options += '<option value="'+boq.getId()+'" '+datas+'>'+boq.getValue("name")+'</option>';
  }
  return options;
}
function changeOrderForm(request, response)
{
  var jqueryModalJs = 'https://system.netsuite.com/core/media/media.nl?id=536&c=4317943_SB1&h=d715f48c82e22e51d3a5&_xt=.js';
  var mainCss = 'https://system.netsuite.com/core/media/media.nl?id=626&c=4317943_SB1&h=80e5044fe17a116c6e29&_xt=.css';
  var jsChain = 'https://system.netsuite.com/core/media/media.nl?id=2051&c=4317943_SB1&h=96dc96bbd71a2974edb8&_xt=.js';
  var coFolder = '108';
  var coStatusDraft = '1';
  if(nlapiGetContext().getEnvironment()=='PRODUCTION'){
	  jqueryModalJs = 'https://system.na3.netsuite.com/core/media/media.nl?id=536&c=4317943&h=c1a0fa134372b1c1ccd9&_xt=.js';
	  mainCss = 'https://system.na3.netsuite.com/core/media/media.nl?id=626&c=4317943&h=6ae2a43efd859b421040&_xt=.css';
	  jsChain = 'https://system.netsuite.com/core/media/media.nl?id=2051&c=4317943_SB1&h=96dc96bbd71a2974edb8&_xt=.js';
	  coFolder = '108';
	  coStatusDraft = '1';
  }	
  
  var columns = [
    new nlobjSearchColumn('name')
  ];
  var subsystemList = nlapiSearchRecord('customlist_eym_proj_subsystem',null,null,columns);
  var actOptions = '<option value="">--Select Activity--</option>';
  for(var i=0;i<subsystemList.length;i++){
	  var subSystemId = subsystemList[i].getId();
	  var filters = [
	    new nlobjSearchFilter('custrecord_eym_projsub_main',null,'is',subSystemId)
	  ];
	  var columns = [
	    new nlobjSearchColumn('custrecord_eym_projsub_main'),
	    new nlobjSearchColumn('name'),
	    new nlobjSearchColumn('custrecord_boq_cat_descr')
	  ];
	  var activities = nlapiSearchRecord('customrecord_eym_boq_activity',null,filters,columns);
	  for(var j=0;j<activities.length;j++){
	      var activity = activities[j];
	      actOptions += '<option value="'+activity.getId()+'" data-chained="'+subSystemId+'">'+activity.getValue('name')+'</option>';
	   }
  }	  
  var projectId = request.getParameter('projectid');
  var projectObj = nlapiLoadRecord('job',projectId);
  var pcoId = request.getParameter('pcoid');
  var currentUserId = nlapiGetUser();
  var currentUserRole = nlapiGetRole();
  nlapiLogExecution('DEBUG','currentUserRole',currentUserRole);
  var pco = null;
  var pcoFileName = null;
  var correctApprover = false;
  var pcoJson = null;
  nlapiLogExecution('DEBUG','projectId',projectId);
  nlapiLogExecution('DEBUG','pcoId',pcoId);
  
  var createBoqRestletUrl = nlapiResolveURL('SUITELET', 'customscript_sl_eym_create_boq', 'customdeploy_sl_eym_create_boq')+'&projectid='+projectId+'&f=pco&pcoid='+pcoId;
  var approvalRestletUrl = nlapiResolveURL('SUITELET', 'customscript_sl_eym_pco_submit_approval', 'customdeploy_sl_eym_pco_submit_approval')+'&projectid='+projectId;
  var returnBoqRestletUrl = nlapiResolveURL('SUITELET', 'customscript_sl_eym_return_boq', 'customdeploy_sl_eym_return_boq')+'&projectid='+projectId+'&f=pco&pcoid='+pcoId;
  
  var boqListAjaxUrl = nlapiResolveURL('SUITELET','customscript_sl_eym_project_boq_list','customdeploy_sl_eym_project_boq_list')+'&projectid='+projectId;
  var choValidateUrl = nlapiResolveURL('SUITELET','customscript_sl_eym_validate_cho','customdeploy_sl_eym_validate_cho');
  var pinValidateUrl = nlapiResolveURL('SUITELET', 'customscript_sl_eym_validate_pin', 'customdeploy_sl_eym_validate_pin')+'&type=pco&id='+pcoId;
  
  var projectUrl = nlapiResolveURL('RECORD', 'job', projectId);
  var classFilter = [
    new nlobjSearchFilter('custrecord_eym_use_in_psc',null,'is','T')
  ];
  var classColumn = [
    new nlobjSearchColumn('name'),
    new nlobjSearchColumn('custrecord_eym_psc_tempsequence')
  ]
  classColumn[1].setSort();
  var classes = nlapiSearchRecord('classification',null,classFilter,classColumn);
  
  var extraHtml = "";
  var subCount = 0;
  var subsystemJson = [];
  
  if ( request.getMethod() == 'POST' ){
	subCount = request.getParameter('subCountPerm');
	nlapiLogExecution('DEBUG','subCount ',subCount);
    var pcoTotal = 0;
    for(var i=0;i<subCount;i++){
    	  if(!isEmpty(request.getParameter("txtAmount-"+i))){
	    	  var subsystemData = {};	
	      subsystemData.id=i;
	      subsystemData.boqId=request.getParameter("txtBoqId-"+i);
	      subsystemData.boq=request.getParameter("txtBoq-"+i);
	      subsystemData.name_id=request.getParameter("selSubsystem-"+i);
	      subsystemData.name=request.getParameter("txtSubsystem-"+i).replace(/"/g, '\"');
	      subsystemData.activity_id=request.getParameter("selActivity-"+i);
	      subsystemData.activity=request.getParameter("txtActivity-"+i).replace(/"/g, '\"');
	      subsystemData.description=request.getParameter("txtDesc-"+i).replace(/"/g, '\"');
	      subsystemData.sClass=request.getParameter("hidClass-"+i);
	      subsystemData.amount=request.getParameter("txtAmount-"+i).replace(/,/g, '');
	      subsystemJson.push(subsystemData);
	      pcoTotal += request.getParameter("txtAmount-"+i).replace(/,/g, '') * 1;
      }
    }
	  
	var newPco = null;
	if(!isEmpty(pcoId)){
		newPco = nlapiLoadRecord('customrecord_eym_change_order',pcoId);
	}else{
		newPco = nlapiCreateRecord('customrecord_eym_change_order');
	}
	
	newPco.setFieldValue('custrecord_eym_pco_projid',projectId);
	newPco.setFieldValue('custrecord_eym_pco_projname',projectObj.getFieldValue("companyname"));
	newPco.setFieldValue('custrecord_eym_pco_custname',projectObj.getFieldValue("parent"));
	newPco.setFieldValue('custrecord_eym_pco_projstatus',projectObj.getFieldText("entitystatus"));
	newPco.setFieldValue('custrecord_eym_pco_approver',request.getParameter('selCoApprover'));
	if(isEmpty(pcoId)){
		newPco.setFieldValue('custrecord_eym_pco_creator',currentUserId);
	}
	newPco.setFieldValue('custrecord_eym_pco_status',coStatusDraft);
	newPco.setFieldValue('custrecord_eym_total_co_amount',pcoTotal);
	
	pcoId = nlapiSubmitRecord(newPco);
	
	
    
    var jsonFile = {
      'projectId' : projectId,
      'pcoId' : pcoId,
      'pcoTotal' : pcoTotal,
      'pcoData' : subsystemJson
    }
    pcoFileName = 'change_order_template_'+projectId+'_'+pcoId;
    var tempFile = nlapiCreateFile(pcoFileName, 'PLAINTEXT', JSON.stringify(jsonFile));
    tempFile.setFolder(coFolder);
  	var newfile = nlapiSubmitFile(tempFile);
  	//nlapiSubmitField('job',projectId,['custentity_eym_boq_status','custentity_boq_approver','custentity_eym_boq_creator'],[4,request.getParameter("selBoqApprover"),currentUserId]);
  	if(request.getParameter("btnClick")!="2"){
  		extraHtml += '<script type="text/javascript">\n';
  	  	extraHtml += ' alert("Project Change Order Template Saved")\n';
  	    extraHtml += '</script>\n';
  	}else{
  		var approver = request.getParameter("selCoApprover");
  		extraHtml += '<script type="text/javascript">\n';
  	  	extraHtml += ' window.location = "'+approvalRestletUrl+'&approver='+approver+'&pcoid='+pcoId+'&total='+pcoTotal+'";\n';
  	    extraHtml += '</script>\n';
  	    response.write(extraHtml);
  	}
  }
  var json = null;
  if(!isEmpty(pcoId)){
	  pco = nlapiLoadRecord("customrecord_eym_change_order",pcoId);
	  pcoFileName = 'change_order_template_'+projectId+'_'+pcoId;
	  var costApprover = nlapiSearchRecord(null,'customsearch_eym_cost_approver_list',null);
	  if(costApprover){
		  for(var b=0;b<costApprover.length;b++){
			  nlapiLogExecution('DEBUG','costApprover[b].getId()',costApprover[b].getId());
			  if(currentUserId==costApprover[b].getId()){
				  correctApprover = true;
				  break;
		      }
		  }
	  }
	  var filters = [ new nlobjSearchFilter('name',null,'is',pcoFileName)];
	  var fileSearch = nlapiSearchRecord('file',null,filters);
	  if(fileSearch){
	    var tempFile = nlapiLoadFile(fileSearch[0].getId());
	    json = JSON.parse(tempFile.getValue());
	    pcoJson = json.pcoData;
	    subCount = pcoJson.length;
	  }
  }
  
  var boqApprover = nlapiSearchRecord(null,'customsearch_eym_cho_approver_list',null);
  
  var boqOptions = generateBoqOptions(projectId);
  
  var html = '<html>\n';
  html += '<head>\n';
  html += '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" type="text/css" media="screen" />\n';
  html += '<link rel="stylesheet" href="'+mainCss+'" type="text/css" media="screen" />\n';
  html += '<title>PSC Change Order Template</title>\n';
  html += '</head>\n';
  html += '<body>\n';
  html += '<div class="loading" style="display:none">Loading&#8230;</div>\n';
  
  html += '<form name="frmReturn" id="frmReturn" style="display:none" method="POST" action="'+returnBoqRestletUrl+'">\n';
  html += '<p><label>PIN:</label></p><p><input type="password" name="pin" class="form-control" id="pin"></p>\n';
  html += '<p><label>Reason for Return:</label></p><p><textarea name="returnMsg" class="form-control" id="returnMsg"></textarea></p>\n';
  html += '<p><input type="submit" id="btnOkReturn" class="btn btn-primary" value="OK" /></p>\n';
  html += '</form>\n';
  html += '<form name="frmPin" id="frmPin" style="display:none" method="POST">\n';
  html += '<p><label>PIN:</label></p><p><input type="password" name="pin" class="form-control" id="pin"></p>\n';
  html += '<p><input type="submit" id="btnOkPin" class="btn btn-primary" value="OK" /></p>\n';
  html += '</form>\n';
  html += '<form name="frmChangeOrder" id="frmChangeOrder" method="POST" action="">\n';
  var disabled = '';
  
  var showBtnSave = false;
  var showBtnSubmitApproval = false;
  var showBtnApprove = false;
  var showBtnReject = false;
  var showAddRemoveBtn = false;
  if(!pco){
	  showBtnSave = true;
	  showAddRemoveBtn = true;
  }else{
	  if(pco.getFieldValue('custrecord_eym_pco_creator')==currentUserId || currentUserRole=='3'){
		  if(pco.getFieldValue('custrecord_eym_pco_status')=='1' || pco.getFieldValue('custrecord_eym_pco_status')=='4'){
			  showBtnSave = true;
			  showAddRemoveBtn = true;
			  if(json){
				  if(json.pcoTotal!=''){
					  showBtnSubmitApproval = true;
				  }
			  }
		  }else{
			  disabled = 'disabled';
		  }
	  }else{
		  disabled = 'disabled';
	  }
	  
	  if((correctApprover || currentUserRole=='3') && pco.getFieldValue('custrecord_eym_pco_status')=='2'){
		  showBtnApprove = true;
		  showBtnReject = true;
	  }	  
  }
  nlapiLogExecution('DEBUG','showAddRemoveBtn',showAddRemoveBtn);
  nlapiLogExecution('DEBUG','pco',pco);
  html += '<input type="hidden" name="projectid" value="'+projectId+'">\n';
  html += '<input type="hidden" name="pcoid" value="'+pcoId+'">\n';
  html += '<input type="hidden" name="subCount" value="'+subCount+'" id="subCount">\n';
  html += '<input type="hidden" name="subCountPerm" value="'+subCount+'" id="subCountPerm">\n';
  html += '<input type="hidden" name="btnClick" id="btnClick" value="">\n';
  html += '<header class="actions stick-to-top">\n';
  html += '	<div class="card">\n';
  html += '		<div class="card-body"><div class="row"><div class="col">\n';
  if(showBtnSave){
	  html += '    <input type="submit" class="btn btn-primary" value="Save as Draft" name="btnSaveDraft" id="btnSaveDraft">\n';	
  }
  if(showBtnSubmitApproval){
	  html += '    <input type="submit" class="btn btn-primary" value="Submit for Approval" name="btnApproval" id="btnApproval">\n';
  }
  if(showBtnApprove){
	  html += '    <input type="button" class="btn btn-primary" value="Approve" name="btnApprove" id="btnApprove">\n';
  }
  if(showBtnReject){
	  html += '    <input type="button" class="btn btn-primary" value="Return" name="btnReturn" id="btnReturn">\n';
  }
  
  html += '    <input type="button" class="btn btn-default" value="Back" name="btnBack" id="btnBack"></div>\n';
  html += '		<div class="col-3"><dl class="row mb-0"><dt class="col-md-5">Project Code</dt><dd class="col-md-7">'+projectObj.getFieldValue('entityid')+'</dd></dl></div>\n';
  html += '		<div class="col-3"><dl class="row mb-0"><dt class="col-md-7">Total Change Order Amount</dt><dd class="col-md-5">'+(pco ? addCommas(Number(pco.getFieldValue('custrecord_eym_total_co_amount')).toFixed(2)) : "")+'</dd></dl></div>\n';
  html += '		</div></div></div></header>\n';
  
  html += '<main class="main-content">\n';
  html += ' <div class="container-fluid">\n';
  html += '	 <div class="row">\n';
  html += '		<div class="col">\n';
  html += '		<div class="card mb-3">\n';
  html += '		<div class="card-header"><h3 class="float-left mt-2 mb-1">Project Summary</h3></div>\n';
  html += '		<div class="card-body"><div class="row">\n';
  html += '			<div class="col-md-6">\n';
  html += '				<dl class="row">\n';
  html += '					<dt class="col-md-5">Project Change Order ID </dt>\n';
  html += '					<dd class="col-md-7">'+(pco ? pco.getFieldValue('name') : 'To Be Generated')+'</dd>\n';
  html += '					<dt class="col-md-5">Project Name</dt>\n';
  html += '					<dd class="col-md-7">'+projectObj.getFieldValue('companyname')+'</dt>\n';
  html += '					<dt class="col-md-5">Customer</dt>\n';
  html += '					<dd class="col-md-7">'+projectObj.getFieldText('parent')+'</dt>\n';
  html += '				</dl>\n';
  html += '			</div>\n';
  html += '			<div class="col-md-6">\n';
  html += '				<dl class="row">\n';
  html += '					<dt class="col-md-4">Project Status</dt>\n';
  html += '					<dd class="col-md-8">'+projectObj.getFieldText('entitystatus')+'</dt>\n';
  html += '					<dt class="col-md-4">Change Order Approver</dt>\n';
  html += '					<dd class="col-md-8"><select name="selCoApprover" id="selCoApprover" '+disabled+'>\n';
  html += '					<option value=""></option>\n';
  if(boqApprover){
	  for(var b=0;b<boqApprover.length;b++){
	      var selected = "";
		  if(pco && pco.getFieldValue('custrecord_eym_pco_approver')==boqApprover[b].getId()){
	    	  	selected = "selected";
	      }
		  html += '<option value="'+boqApprover[b].getId()+'" '+selected+'>'+boqApprover[b].getValue('entityid')+'</option>\n';
	  }
  }
  html += '         				</select></dd>\n';
  html += '					<dt class="col-md-4">Change Order Status</dt>\n';
  html += '					<dd class="col-md-8">'+(pco ? pco.getFieldText('custrecord_eym_pco_status') : "")+'</dt>\n';
  html += '				</dl>\n';
  html += '			</div>\n';
  html += '		</div></div>\n';
  html += '		</div>\n';
  html += '		</div>\n';
  html += '	 </div>\n';
  
  html += '	 <div class="row">\n';
  html += '		<div class="col">\n';
  html += '		<div class="card mb-3">\n';
  html += '		<div class="table-responsive-wrapper">\n';
  html += '		<div class="card-body p-0">\n';
  html += '			<table class="table table-collapsible table-small" border="0"><thead>\n';
  html += '				<tr>\n';
  html += '					<th width="15%">Bill of Quantity Item</th>\n';
  html += '					<th width="20%">Project Subsystem</th>\n';
  html += '					<th width="20%">Activity</th>\n';
  html += '					<th width="20%">Description</th>\n';
  html += '					<th width="10%" style="text-align:center;">Cost Type</th>\n';
  html += '					<th width="10%" style="text-align:center;">Amount</th>\n';
  html += '					<th width="5%" style="text-align:center;">Action</th>\n';
  html += '				</tr>\n';
  html += '			</thead>\n';
  html += '			<tbody>\n';
  if(pcoJson){
  	for(var i=0;i<pcoJson.length;i++){
  		var p = pcoJson[i];
  		html += '<tr class="dataRow" id="row-'+i+'">\n';
  		html += '  <td>';
  		html += '  <input '+disabled+' type="hidden" id="txtBoqId-'+i+'" value="'+p.boqId+'" name="txtBoqId-'+i+'">'
  		html += '  <input type="hidden" id="hidClass-'+i+'" value="'+p.sClass+'" name="hidClass-'+i+'">'
  		html += '  <input type="hidden" id="hidAct-'+i+'" value="'+p.activity_id+'" name="hidAct-'+i+'">'
  		html += '<select '+disabled+' id="txtBoq-'+i+'" name="txtBoq-'+i+'">'+boqOptions+'</select>\n'; 
  		html += '  </td>\n';
  		html += '  <td><input '+disabled+' class="form-control form-control-sm" type="text" id="txtSubsystem-'+i+'" value="'+p.name.replace(/"/g, "&quot;")+'" name="txtSubsystem-'+i+'">\n';
  		html += '      <select class="form-control form-control-sm selSub" id="selSubsystem-'+i+'" name="selSubsystem-'+i+'"><option value="">--Select Subsystem--</option>\n';
		for(var c=0;c<subsystemList.length;c++){
			var selected = "";
			if(subsystemList[c].getId()==p.name_id){
				selected = "selected";
			}
			html += '     <option value="'+subsystemList[c].getId()+'" '+selected+'>'+subsystemList[c].getValue('name')+'</option>\n';
		}
		html += '  </select></td>\n';
  		html += '  <td><input '+disabled+' class="form-control form-control-sm" type="text" id="txtActivity-'+i+'" value="'+p.activity.replace(/"/g, "&quot;")+'" name="txtActivity-'+i+'">\n';
  		html += '      <select class="form-control form-control-sm selAct" id="selActivity-'+i+'" name="selActivity-'+i+'">\n';
		html += actOptions+'\n';
		html += '  </select></td>\n';
  		html += '  <td><input '+disabled+' class="form-control form-control-sm" type="text" id="txtDesc-'+i+'" value="'+p.description.replace(/"/g, "&quot;")+'" name="txtDesc-'+i+'"></td>\n';
  		html += '  <td><select '+disabled+' class="form-control form-control-sm" id="selClass-'+i+'" name="selClass-'+i+'">\n';
  		for(var c=0;c<classes.length;c++){
  			var selected = "";
  		if(p.sClass==classes[c].getId()){
  			selected = "selected";
  		}
  		html += '	<option value="'+classes[c].getId()+'" '+selected+'>'+classes[c].getValue('name')+'</option>\n';
  		}
  		html += '  </select></td>\n';
  		html += '  <td><input '+disabled+' type="text" class="form-control form-control-sm numOnly amountClass" data-i="'+i+'" style="text-align:right" id="txtAmount-'+i+'" value="'+addCommas(Number(p.amount).toFixed(2))+'" name="txtAmount-'+i+'"></td>\n';
  		html += '  <td align="center"><div class="btn-desc"><div class="button"><button title="Remove" type="button" '+(showAddRemoveBtn ? "": "disabled")+' class="btn btn-danger btn-sm btn-icon btnRemoveRow" data-i="'+i+'"><i class="mdi mdi-delete"></i></button></div></div></td>\n';
  		html += '</tr>\n';
  	}
  		  
  }
  if(showAddRemoveBtn){
		html += '<tr id="trAdd" class="add-row">\n';
		html += '  <td colspan="6"><div class="btn-desc"><div class="button"><button type="button" class="btn btn-primary btn-sm" id="btnAddBoq"><i class="mdi mdi-plus"></i> Add BOQ Item</button></div></div></td>\n';
		html += '</tr>\n';
  }
  html += '			</tbody>\n';
  html += '			</table>\n';
  html += '		</div></div>\n';
  html += '		</div>\n';
  html += '		</div>\n';
  html += '	 </div>\n';
  
  
  html += '	</div>\n';
  html += '</main>\n';
  
 
  html += '</form>\n';
  html += '</body>\n';
  html += extraHtml;
  html += '<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>\n';
  html += '<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>\n';
  html += '<script type="text/javascript" src="'+jqueryModalJs+'"></script>\n';  
  html += '<script type="text/javascript" src="'+jsChain+'"></script>\n';
  html += '<script type="text/javascript">\n';
  html += ' $(document).ready(function(){\n';
  html += '   var l = $("#subCount").val()*1;\n';
  html += '   for(var r=0;r<l;r++){\n';
  html += '   	$("#txtBoq-"+r).select2({ width:"100%"}).val($("#txtBoqId-"+r).val()).trigger("change");\n';
  html += '   	if($("#txtBoqId-"+r).val()){\n';
  html += '   		$("#txtSubsystem-"+r).show();$("#selSubsystem-"+r).hide();\n';
  html += '   		$("#txtActivity-"+r).show();$("#selActivity-"+r).hide();\n';
  html += '   		$("#txtSubsystem-"+r).prop("readonly", true);\n';
  html += '   		$("#txtActivity-"+r).prop("readonly", true);\n';
  html += '   		$("#txtDesc-"+r).prop("readonly", true);\n';
  html += '   		$("#selClass-"+r).prop("disabled", true);\n';
  html += '   	}else{\n';
  html += '   		$("#txtSubsystem-"+r).hide();$("#selSubsystem-"+r).show();\n';
  html += '   		$("#txtActivity-"+r).hide();$("#selActivity-"+r).show();\n';
  html += '   		$("#txtDesc-"+r).prop("readonly", false);\n';
  html += '   		$("#selClass-"+r).prop("disabled", false);\n';
  html += '   	}\n';
  html += '   	$("#txtBoq-"+r).on("select2:select", (function(r){ return function (e) {\n';
  html += '   		$("#txtBoqId-"+r).val(e.params.data.id);\n';
  html += '   		$("#txtSubsystem-"+r).val($(this).find(":selected").data("subsystem"));\n';
  html += '   		$("#txtActivity-"+r).val($(this).find(":selected").data("activity"));\n';
  html += '   		$("#txtDesc-"+r).val($(this).find(":selected").data("description"));\n';
  html += '   		$("#selClass-"+r).val($(this).find(":selected").data("costtype"));\n';
  html += '   		$("#hidClass-"+r).val($(this).find(":selected").data("costtype"));\n';
  html += '   		$("#txtAmount-"+r).val($(this).find(":selected").data("amount"));\n';
  html += '   		if(e.params.data.id){\n';
  html += '   			$("#txtSubsystem-"+r).show();$("#selSubsystem-"+r).hide();\n';
  html += '   			$("#txtActivity-"+r).show();$("#selActivity-"+r).hide();\n';
  html += '   			$("#txtSubsystem-"+r).prop("readonly", true);\n';
  html += '   			$("#txtActivity-"+r).prop("readonly", true);\n';
  html += '   			$("#txtDesc-"+r).prop("readonly", true);\n';
  html += '   			$("#selClass-"+r).prop("disabled", true);\n';
  html += '   		}else{\n';
  html += '   			$("#txtSubsystem-"+r).hide();$("#selSubsystem-"+r).show();\n';
  html += '   			$("#txtActivity-"+r).hide();$("#selActivity-"+r).show();\n';
  html += '   			$("#txtDesc-"+r).prop("readonly", false);\n';
  html += '   			$("#selClass-"+r).prop("disabled", false);\n';
  html += '   		}\n';
  html += '   	};})(r))\n';
  html += '   	$("#selActivity-"+r).val($("#hidAct-"+r).val());\n';
  html += '   	$("#selActivity-"+r).chained("#selSubsystem-"+r);\n';
  html += '   }\n';
  html += '   $(document).on("change",".selSub", function(){\n'; 
  html += '   	var idPart = $(this).attr("id").split("-");\n';
  html += '   	$("#txtSubsystem-"+idPart[1]).val($("#selSubsystem-"+idPart[1]+" option:selected").text());\n';
  html += '   });\n';
  html += '   $(document).on("change", ".selAct", function(){\n'; 
  html += '   	var idPart = $(this).attr("id").split("-");\n';
  html += '   	if($(this).val()!=""){ $("#txtActivity-"+idPart[1]).val($("#selActivity-"+idPart[1]+" option:selected").text()); }else{ $("#txtActivity-"+idPart[1]).val("");}\n';
  html += '   });\n';
  html += '   $("#btnBack").click(function(){\n';
  html += '     window.location = "'+projectUrl+'"; return false; \n';
  html += '   })\n';
  
  //html += '   $(".numOnly").on("keydown",function (e) {\n';
  html += '   $(document).on("keydown",".numOnly",function(e){\n';
  html += '     if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 37, 39, 110, 173, 189, 190]) !== -1){ return; }\n';
  html += '     if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) { e.preventDefault(); }\n';
  html += '   })\n';
  
  html += '   $(document).on("click",".numOnly",function(e){\n';
  html += '     if($(this).val()=="0.00"){ $(this).val("")}\n';
  html += '   })\n';
  
  html += '   $(document).on("blur",".numOnly",function(e){\n';
  html += '     if($(this).val()==""){ $(this).val("0.00")}\n';
  html += '   })\n';
  
  //html += '   $("#btnApprove").click(function(){\n';
  //html += '     window.location = "'+createBoqRestletUrl+'"; return false; \n';
  //html += '   })\n';
  
  html += '   $("#btnApprove").click(function(){\n';
  html += '     $("#frmPin").modal({ modalClass:"jModal", escapeClose: false, clickClose: false});\n';
  html += '   })\n';
  
  html += '   $("#btnOkPin").click(function(e){\n';
  html += '     	e.preventDefault();\n';
  html += '     	var btn = $(this); btn.attr("disabled", "disabled");\n';
  html += '     	var ajaxCheck = $.post("'+pinValidateUrl+'", $("#frmPin").serialize());\n';
  html += '     	ajaxCheck.done(function(resp){\n';
  html += '     		var jsonRes = JSON.parse(resp);\n';
  //html += '     		$(".loading").hide();\n';
  html += '     		if(jsonRes.error.length>0){\n';
  html += '     			alert("ERROR: Incorrect PIN Code. Please enter the assigned approver\'s PIN Code."); btn.removeAttr("disabled")\n';
  html += '     		}else{ window.location = "'+createBoqRestletUrl+'";}\n';
  html += '     	});\n';
  html += '   })\n';
  
  html += '   $("#btnOkReturn").click(function(e){\n';
  html += '     	e.preventDefault();\n';
  html += '     	if($("#returnMsg").val()==""){ alert("Please input reason for return"); }else{\n';
  html += '     	var btn = $(this); btn.attr("disabled", "disabled");\n';
  html += '     	var ajaxCheck = $.post("'+pinValidateUrl+'", $("#frmReturn").serialize());\n';
  html += '     	ajaxCheck.done(function(resp){\n';
  html += '     		var jsonRes = JSON.parse(resp);\n';
  //html += '     		$(".loading").hide();\n';
  html += '     		if(jsonRes.error.length>0){\n';
  html += '     			alert("ERROR: Incorrect PIN Code. Please enter the assigned approver\'s PIN Code."); btn.removeAttr("disabled")\n';
  html += '     		}else{ $("#frmReturn").submit();}\n';
  html += '     	});}\n';
  html += '   })\n';
  
  
  html += '   $("#btnReturn").click(function(){\n';
  html += '     $("#frmReturn").modal({ modalClass:"jModal", escapeClose: false, clickClose: false});\n';
  //html += '     window.location = "'+returnBoqRestletUrl+'"; return false; \n';
  html += '   })\n';
  
  html += '   $("#btnSaveDraft").click(function(){\n';
  html += '     $("#btnClick").val(1);\n';
  html += '   })\n';
  
  html += '   $("#btnApproval").click(function(){\n';
  html += '     $("#btnClick").val(2);\n';
  html += '     var approver = $("#selCoApprover").val();\n';
  html += '     if(approver==""){ alert("Please choose Change Order Approver"); return false; }\n';
  html += '   })\n';
  
  html += '   $("#btnAddBoq").click(function(){\n';
  //html += '     var r = $(".dataRow").length;\n';
  html += '     $("#subCount").val(($("#subCount").val()*1)+1);\n';
  html += '     var r = $("#subCountPerm").val();\n';
  html += '     $("#subCountPerm").val(($("#subCountPerm").val()*1)+1);\n';
  html += '     var htmlTable1 = \'<tr class="dataRow" id="row-\'+r+\'"><td>\'\n';
  html += '     htmlTable1 += \'<input type="hidden" id="txtBoqId-\'+r+\'" name="txtBoqId-\'+r+\'">\'\n';
  html += '     htmlTable1 += \'<input type="hidden" id="hidClass-\'+r+\'" name="hidClass-\'+r+\'">\'\n';
  html += '     htmlTable1 += \'<select id="txtBoq-\'+r+\'" name="txtBoq-\'+r+\'">'+boqOptions+'</select>\'\n';
  html += '     htmlTable1 += \'</td>\'\n';
  
  html += '     htmlTable1 += \'<td><input type="text" style="display:none" class="form-control form-control-sm" id="txtSubsystem-\'+r+\'" name="txtSubsystem-\'+r+\'">\'\n';
  html += '     htmlTable1 += \'<select class="form-control form-control-sm selSub" id="selSubsystem-\'+r+\'" name="selSubsystem-\'+r+\'"><option value="">--Select Subsystem--</option>\'\n';
  for(var c=0;c<subsystemList.length;c++){
	  html += '     htmlTable1 += \'<option value="'+subsystemList[c].getId()+'">'+subsystemList[c].getValue('name')+'</option>\'\n';
  }
  html += '     htmlTable1 += \'</select></td>\'\n';
  html += '     htmlTable1 += \'<td><input type="text" style="display:none" class="form-control form-control-sm" id="txtActivity-\'+r+\'" name="txtActivity-\'+r+\'">\'\n';
  html += '     htmlTable1 += \'<select class="form-control form-control-sm selAct" id="selActivity-\'+r+\'" name="selActivity-\'+r+\'">\'\n';
  html += '     htmlTable1 += \''+actOptions+'\'\n';
  html += '     htmlTable1 += \'</select></td>\'\n';
  html += '     htmlTable1 += \'<td><input type="text" class="form-control form-control-sm" id="txtDesc-\'+r+\'" name="txtDesc-\'+r+\'"></td>\'\n';
  html += '     htmlTable1 += \'<td><select class="form-control form-control-sm" id="selClass-\'+r+\'" name="selClass-\'+r+\'">\'\n';
  for(var c=0;c<classes.length;c++){
	  html += '     htmlTable1 += \'<option value="'+classes[c].getId()+'">'+classes[c].getValue('name')+'</option>\'\n';
  }
  html += '     htmlTable1 += \'</select></td>\'\n';
  html += '     htmlTable1 += \'<td><input type="text" class="form-control form-control-sm numOnly amountClass" data-i="\'+r+\'" value="0.00" id="txtAmount-\'+r+\'" style="text-align:right" name="txtAmount-\'+r+\'"></td>\'\n';
  html += '     htmlTable1 += \'<td align="center"><div class="btn-desc"><div class="button"><button type="button" title="Remove" class="btn btn-danger btn-sm btn-icon btnRemoveRow" data-i="\'+r+\'"><i class="mdi mdi-delete"></i></button></div></div></td></tr>\'\n';
  html += '     $("#trAdd").before(htmlTable1)\n';
  html += '   	$("#txtBoq-"+r).select2({ width: "100%" });\n';
  html += '   	$("#txtBoq-"+r).on("select2:select", function (e) {\n';
  html += '   		$("#txtBoqId-"+r).val(e.params.data.id);\n';
  html += '   		$("#txtSubsystem-"+r).val($(this).find(":selected").data("subsystem"));\n';
  html += '   		$("#txtActivity-"+r).val($(this).find(":selected").data("activity"));\n';
  html += '   		$("#txtDesc-"+r).val($(this).find(":selected").data("description"));\n';
  html += '   		$("#selClass-"+r).val($(this).find(":selected").data("costtype"));\n';
  html += '   		$("#hidClass-"+r).val($(this).find(":selected").data("costtype"));\n';
  html += '   		$("#txtAmount-"+r).val($(this).find(":selected").data("amount"));\n';
  html += '   		if(e.params.data.id){\n';
  html += '   			$("#txtSubsystem-"+r).show();$("#selSubsystem-"+r).hide();\n';
  html += '   			$("#txtActivity-"+r).show();$("#selActivity-"+r).hide();\n';
  html += '   			$("#txtSubsystem-"+r).prop("readonly", true);\n';
  html += '   			$("#txtActivity-"+r).prop("readonly", true);\n';
  html += '   			$("#txtDesc-"+r).prop("readonly", true);\n';
  html += '   			$("#selClass-"+r).prop("disabled", true);\n';
  html += '   		}else{\n';
  html += '   			$("#txtSubsystem-"+r).hide();$("#selSubsystem-"+r).show();\n';
  html += '   			$("#txtActivity-"+r).hide();$("#selActivity-"+r).show();\n';
  html += '   			$("#txtDesc-"+r).prop("readonly", false);\n';
  html += '   			$("#selClass-"+r).prop("disabled", false);\n';
  html += '   		}\n';
  html += '   	})\n';
  html += '   	$("#selActivity-"+r).chained("#selSubsystem-"+r);\n';
  html += '   })\n';
  
  html += '   $(document).on("click",".btnRemoveRow",function(){\n';
  html += '     var r = $(this).data("i");\n';
  html += '     $("#subCount").val($("#subCount").val()-1);\n';
  html += '     $("#row-"+r).remove()\n';
  html += '   })\n';
  
  html += '   $("#frmChangeOrder").on("submit",function(e){\n';
  html += '     var r = $("#subCount").val(); var form = this;\n';
  html += '     if(r<=0){\n';
  html += '     		alert("Please add at least 1 line item"); submit = false; return false;\n';
  html += '     }\n';
  html += '     var submit = true;\n';
  html += '     $(".amountClass").each(function(row, val){\n';
  html += '     		var amt = $(this);\n';
  html += '     		var i = $(this).data("i");\n';
  html += '     		if((amt.val()*1)==0){\n';
  html += '     			alert("Please input correct amount for line "+(row+1)+", amount cannot be 0"); submit = false; return false;\n';
  html += '     		}\n';
  html += '     		if($("#txtBoq-"+i).val()=="" && (amt.val()*1)<=0){\n';
  html += '     			alert("Please input correct amount for line "+(row+1)+", negative amount is only allowed if BQI is not empty"); submit = false;\n';
  html += '     		}\n';
  html += '     		if($("#txtSubsystem-"+i).val()==""){\n';
  html += '     			alert("Please input subsystem "+(row+1)); submit = false; return false;\n';
  html += '     		}\n';
  html += '     		if($("#txtActivity-"+i).val()==""){\n';
  html += '     			alert("Please input activity "+(row+1)); submit = false; return false;\n';
  html += '     		}\n';
  html += '     		if($("#hidClass-"+i).val()!=$("#selClass-"+i).val()){\n';
  html += '     			$("#hidClass-"+i).val($("#selClass-"+i).val());\n';
  html += '     		}\n';
  html += '     });\n';
  html += '     $(".numOnly").each(function(){\n';
  html += '     		if(isNaN($(this).val().replace(/,/g, ""))){\n';
  html += '     			alert("Please input valid number for amount"); submit = false; return false;\n';
  html += '     		}\n';
  html += '     });\n';
  html += '   	if(!submit){\n';
  html += '   		return false;\n';
  html += '   	}else{\n';
  html += '   		if($("#btnClick").val()=="2"){\n';
  html += '     			e.preventDefault();\n';
  html += '     			$(".loading").show();\n';
  html += '     			var ajaxCheck = $.post("'+choValidateUrl+'", $("#frmChangeOrder").serialize());\n';
  html += '     			ajaxCheck.done(function(resp){\n';
  html += '     				var jsonRes = JSON.parse(resp);\n';
  html += '     				if(jsonRes.error.length>0){\n';
  html += '     					var errBqi = "";\n';
  html += '     					for(var x=0;x<jsonRes.error.length;x++){\n';
  html += '     						errBqi += "BQI"+jsonRes.error[x]+"\\n";\n';
  html += '     					}\n';
  html += '     					alert("ERROR: Saving this record will result to negative values of the following BQI:\\n"+errBqi+"Please change the values of the affecting items on this CHO to proceed.")\n';
  html += '     					$(".loading").hide();\n';
  html += '     				}else{ form.submit(); }\n';
  html += '     			});\n';
  html += '     			//if(errBqi==""){ $("#frmChangeOrder").submit(); }\n';
  html += '   		}\n';
  html += '   	}\n';
  html += '   })\n';
  
  html += ' })\n';
  html += '</script>\n';
  html += '</html>\n';
  response.write(html);

}
