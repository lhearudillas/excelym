/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       03 Sep 2017     denny
 *
 */

function approvePco(request, response){
	var projectId = request.getParameter('projectid');
	var pcoId = request.getParameter('pcoid');
	nlapiSubmitField('customrecord_eym_change_order',pcoId,['custrecord_eym_pco_status','custrecord_eym_pco_approver','custrecord_eym_total_co_amount'],[2,request.getParameter('approver'),request.getParameter('total')]);
	nlapiLogExecution('Debug','PCO ID',pcoId);
	nlapiSetRedirectURL('RECORD','job',projectId);
	//return '1';
}
