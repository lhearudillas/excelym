/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       11 Aug 2017     EYM-013
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */


function userEventBeforeLoad(type, form, request){
	var projStatus = nlapiGetFieldValue('entitystatus');
	var recID = nlapiGetRecordId();
	var pscForm = nlapiGetFieldValue('customform');
	var context=nlapiGetContext();
    var role=context.getRole();
    
    var blockedRole = [1013,1014,1011,1019,1020];
    if(context.getEnvironment()=='PRODUCTION'){
    		blockedRole = [1013,1014,1011,1019,1020];
    }
    
    nlapiLogExecution('DEBUG','role',role + ' -- '+blockedRole.indexOf(role));
	if(type == 'view' || type == 'edit'){
		if(blockedRole.indexOf(role)<0){			
	      if(projStatus == 2){
	        var createCOlink = "window.location.replace('" + nlapiResolveURL('SUITELET', 'customscript_sl_eym_project_change_order', 'customdeploy_sl_eym_project_change_order') + "&projectid=" + recID + "')";
	        form.addButton('custpage_createcho_btn', 'Create Change Order', createCOlink);
	      }
		}
	}
	
	try{

		var filters = new Array();
			filters[0] = new nlobjSearchFilter('custrecord_eym_pco_projid', null, 'anyOf', recID);	
			
		var columns = new Array();
			columns[0] = new nlobjSearchColumn('name');
	 		columns[1] = new nlobjSearchColumn('custrecord_eym_pco_approver');
	 		columns[2] = new nlobjSearchColumn('custrecord_eym_pco_status');
	 		columns[3] = new nlobjSearchColumn('custrecord_eym_total_co_amount');

		var searchResCO = nlapiSearchRecord('customrecord_eym_change_order',null,filters,columns);
		
		changeOrderSubtab(searchResCO);
		}
		catch(ex){
			nlapiLogExecution('DEBUG', 'Exception', ex.message == '' ? ex : ex.message);
		} 
} 


function changeOrderSubtab(searchResCO){
	if (type == 'create' || type == 'edit' || type == 'view'){ 
		var recID = nlapiGetRecordId();
		var coStatus = nlapiGetFieldValue('custrecord_eym_pco_status');

		var coTab = form.addTab('custpage_change_orders_tab', 'Change Orders');
	    var coSubTab = form.addSubTab('custpage_change_orders_subtab', 'Change Orders','custpage_change_orders_tab');
		var coSubList = form.addSubList('custpage_change_orders_sublist', 'list', 'Change Orders', 'custpage_change_orders_subtab');
		
		coSubList.addField('custpage_co_projid', 'text', 'Change Order No.').setDisplayType('inline'); 
		coSubList.addField('custpage_co_status', 'select', 'Status', 'customlist_eym_change_order_status').setDisplayType('inline');
		coSubList.addField('custpage_co_amount', 'currency', 'Amount');
		coSubList.addField('custpage_co_approver', 'select', 'Approver', 'employee').setDisplayType('inline');

		if(searchResCO){		
			for(var i = 0; i < searchResCO.length; i++) {
				
				var outputResultCO =  searchResCO[i];
				//nlapiLogExecution('DEBUG','outputResultBOQ', outputResultBOQ);

				var listCO = outputResultCO.getId();
					    	
				var  coApprover = outputResultCO.getValue('custrecord_eym_pco_approver');
				var  coAmt = outputResultCO.getValue('custrecord_eym_total_co_amount');
				var  coStatus = outputResultCO.getValue('custrecord_eym_pco_status');

				
		    	if(coStatus == 1 || coStatus == 2 || coStatus == 4){
		    		var createCOlink2 = nlapiResolveURL('SUITELET', 'customscript_sl_eym_project_change_order', 'customdeploy_sl_eym_project_change_order') + "&projectid=" + recID + "&pcoid=" + listCO;
		    		coSubList.setLineItemValue('custpage_co_projid', i + 1, "<a href='"+createCOlink2+"'>"+ outputResultCO.getValue('name')  +"</a>");
		    	}
				
		    	if(coStatus == 3){
		    		var coLink = nlapiResolveURL('RECORD', 'customrecord_eym_change_order', listCO);
		    		coSubList.setLineItemValue('custpage_co_projid', i + 1, "<a href='"+coLink+"'>"+ outputResultCO.getValue('name')  +"</a>");
		    	}
		    	
		    	coSubList.setLineItemValue('custpage_co_approver', i + 1, coApprover);
				coSubList.setLineItemValue('custpage_co_amount', i + 1, coAmt);
				coSubList.setLineItemValue('custpage_co_status', i + 1, coStatus);

			}
		} 
	} 
}
