/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Sublist internal id
 * @param {String} name Field internal id
 * @param {Number} linenum Optional line item number, starts from 1
 * @returns {Void}
 */
function clientFieldChanged(type, name, linenum){
	if(name == 'custrecord_eym_mrfi_reqqty'){
		
		var qty = nlapiGetFieldValue('custrecord_eym_mrfi_quantity');
		var req_qty = nlapiGetFieldValue('custrecord_eym_mrfi_reqqty');
		var unit_price = nlapiGetFieldValue('custrecord_eym_mrfi_unitprice');

		if(req_qty > qty){
			alert('Requested Quantity is greater than the Procurement Plan Quantity.');
			var req_qty2 = nlapiSetFieldValue('custrecord_eym_mrfi_reqqty', '');
		}
		else{
			amt = req_qty * unit_price;
			
			var total_amt = nlapiSetFieldValue('custrecord_eym_mrfi_amount', amt);
			
		}
		
		
		
		
		   /*
		   var recId = nlapiGetRecordId();
		   var rec = nlapiLoadRecord('customrecord_eym_material_req', recId);
		   var lineItem = rec.getLineItemCount('recmachcustrecord_eym_mrfi_mrfid');

		   if(lineItem > 0){
			   for(var a=1; a <= lineItem; a++){

				   var req_qty = nlapiGetLineItemValue('recmachcustrecord_eym_mrfi_mrfid', 'custrecord_eym_mrfi_reqqty',a);
				   var unit_price = nlapiGetLineItemValue('recmachcustrecord_eym_mrfi_mrfid', 'custrecord_eym_mrfi_unitprice',a);
				   
				   amt = req_qty * unit_price;
				   
				   var set_amt = nlapiSetLineItemValue('recmachcustrecord_eym_mrfi_mrfid', 'custrecord_eym_mrfi_amount', a, amt);
				   
				   nlapiCommitLineItem(set_amt);
			   }
		   } */
		
	   }
}
