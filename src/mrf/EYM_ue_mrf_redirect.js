/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       24 Jan 2018     Roy Selim
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 *
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request){

    // Get the value of MRF Status.
    var mrfStatus = nlapiGetFieldValue('custrecord_eym_mrf_status');

    // Generate the URL.
    var recordId = nlapiGetRecordId();
    var baseUrl = nlapiResolveURL('SUITELET', 'customscript_sl_eym_mrf_status', 'customdeploy_sl_eym_mrf_status');
    var redirectUrl = baseUrl + '&recordID=' + recordId;

    // Create a script and associate it with the button.
  	var buttonScript = "window.location.assign('"+ redirectUrl +"')";

  	// Run this script when a user creates a new form
  	if(type == 'create') {

  		// Get the query string from the URL.
  		var projectID = request.getParameter('projID');

		// Load the project record using the Project ID.
  		var projectRecord = nlapiLoadRecord('job', projectID);

  		// Get the values of required fields from the loaded object.
  		var projectCodeId = projectRecord.getId();
  		var projectName = projectRecord.getFieldValue('companyname');
      	var customerId = projectRecord.getFieldValue('parent');
		var projectInChargeId = projectRecord.getFieldValue('custentity_eym_proj_in_charge');

  		// Create an object with a name-value pair representing data.
  		var valuePairs = {
          	custrecord_eym_mr_projcode: projectCodeId,
      		custrecord_eym_mr_projname: projectName,
      		custrecord_eym_cust_name: customerId,
      		custrecord_eym_mr_pic: projectInChargeId,
    	};

        // Pre-populate the MRF with Project data.
  		form.setFieldValues(valuePairs);
    } else if(type == 'edit') {

      	// Verify if there are line items in the MRF.
      	var lineItemCount = nlapiGetLineItemCount('recmachcustrecord_eym_mrfi_mrfid');

      	// Test the value of MRF status, if 1, show a "submit"" button.
      	if((mrfStatus === '1' || mrfStatus === null) && lineItemCount !== 0) {

          	// Create a variable to determine if there are line items with no quantity.
          	var lineItemHasZeroQuantity = false;

          	// Loop thru the line items and verify that there is a quantity value.
          	for(var i = 1; i < lineItemCount + 1; i++) {
              	var lineItemQuantity = Number(nlapiGetLineItemValue('recmachcustrecord_eym_mrfi_mrfid', 'custrecord_eym_mrfi_quantity', i));
              	lineItemHasZeroQuantity = lineItemQuantity ? true: false;
            }

          	// Add a "submit" button to the form if all validations are true.
          	if(lineItemHasZeroQuantity) {
      			form.addButton('custpage_submit_btn', 'Submit', buttonScript);
            }
        }
    } else if(type == 'view') {
		/* Temporarily removed for demo and testing.
      	// Hide the 'Edit' button
      	if(mrfStatus === '2') {
        	 form.removeButton('edit');
        }
		*/
      	// Show submit button upon meeting criteria.
      	var filters = new Array();
      	filters.push(new nlobjSearchFilter('custrecord_eym_mrfi_mrfid', null, 'anyof', recordId));
		filters.push(new nlobjSearchFilter('custrecord_eym_mrfi_reqqty', null, 'isnotempty'));
      	var mrfiResults = nlapiSearchRecord('customrecord_eym_material_req_item', null, filters);

      	// Display submit button
      	if(mrfStatus === '1' && mrfiResults !== null) {
          	form.addButton('custpage_submit_btn', 'Submit', buttonScript);
        }
    }
}