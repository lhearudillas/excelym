function submitClosePopup()
{
	nlapiSetFieldValue('custpage_mode', 'close');
	window.ischanged = false;
	document.forms[0].submit();
	return true;
}


function submitSearchAsset()
{
	nlapiSetFieldValue('custpage_mode','search');
	window.ischanged = false;
	document.forms[0].submit();
	return true;
}

function actionReset()
{
	nlapiSetFieldValue('custpage_mode','reset');
	window.ischanged = false;
	document.forms[0].submit();
	return true;
}


function submitAddToCase()
{
	var count = nlapiGetLineItemCount('custpage_asset_search_result');
	var hasSelected = false;
    
    for(var i = 1; i <= count; i++) {
        var isSelected = nlapiGetLineItemValue('custpage_asset_search_result','sublist_as_select', i);        
        if(isSelected == 'T') {                        
            hasSelected = true;
            break;
        }
    }

    if(!hasSelected) {
        alert("Please select an asset."); 
        return false;       
    }else {
    	nlapiSetFieldValue('custpage_mode', 'addtocase');
		window.ischanged = false;
		document.forms[0].submit();
		return true;
    }        
}

function markAll(){
	alert('The maximum items to be selected is 25.');
	var resultList = nlapiGetLineItemCount('custpage_proc_plan_sublist');
	if(resultList > 0){
		
		if(resultList > 25){
			resultList = 25; // Maximum of 25 items to check
		}
		
		for(var i = 1;i<=resultList;i++){ 
			nlapiSetLineItemValue('custpage_proc_plan_sublist','custpage_ap_select',i,'T');
		}
	}
}

function unMarkAll(){
	var resultList = nlapiGetLineItemCount('custpage_proc_plan_sublist');
	if(resultList > 0){
		for(var i = 1;i<=resultList;i++){
			nlapiSetLineItemValue('custpage_proc_plan_sublist','custpage_ap_select',i,'F');
		}
	}
}



function clientFieldChanged(type, name, linenum){
	
	/*if(name == 'custpage_req_qty'){
		var index = nlapiGetCurrentLineItemIndex('custpage_proc_plan_sublist');

		var req_qty = nlapiGetCurrentLineItemValue('custpage_proc_plan_sublist', 'custpage_req_qty');
		var qty = Number(nlapiGetLineItemValue('custpage_proc_plan_sublist', 'custpage_qty', index));
				
		if(req_qty > qty){
			alert('Requested Quantity is greater than the Procurement Plan Quantity.');
			var req_qty2 = nlapiSetCurrentLineItemValue('custpage_proc_plan_sublist', 'custpage_req_qty', '');
		}
	}*/
	
	if(name == 'custpage_proj_subsystem' || name == 'custpage_act_group'){
		
		nlapiSetFieldValue('custpage_mode','search');
		window.ischanged = false;
		document.forms[0].submit();
		return true;
		
	}
	
}


function pageInit(type){
	
	var mrfPrps = window.opener.nlapiGetFieldValue('custrecord_eym_mrf_prps')
	nlapiSetFieldValue('custpage_mrfprps',mrfPrps);
}

function clientSaveRecord(){
	
	var resCount = nlapiGetLineItemCount('custpage_proc_plan_sublist');
	var maxCount = 25;
	if(resCount > 0){
		var count = 0;
		var countT = 0;
		for(var a=1; a <= resCount; a++) {
			var isSelected = nlapiGetLineItemValue('custpage_proc_plan_sublist','custpage_ap_select',a); 
					
			if(isSelected == 'F') {
				count++; 
			}else{
				countT++;
			}
			
		}
		
		if(Number(countT) > Number(maxCount)){
			alert('Maximum of 25 items to Select.');
			return false;
		}
		
		if (count == resCount) {
			alert('Notice: No items selected.');
			return false;
		}
	
	//Window will close once Add to MRF button is clicked - sublist is empty
	} else {
		alert('Notice: No items selected.');
		return false;
	}
	
	return true;
		
}
