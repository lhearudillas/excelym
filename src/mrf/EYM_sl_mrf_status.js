/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       30 Jan 2018     Roy Selim
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response){

	// Run the script for GET request.
	if(request.getMethod() == 'GET') {

		// Get querystring from the request object.
		var recordID = request.getParameter('recordID');

		// Load the record object.
		var mrfRecord = nlapiLoadRecord('customrecord_eym_material_req', recordID);

		// When the "Submit" button is clicked, change the record from 'Draft' to 'Submitted'.
		mrfRecord.setFieldValue('custrecord_eym_mrf_status', '2');

		// Submit the record.
		nlapiSubmitRecord(mrfRecord);

		// Redirect the page to a record using nlapiSetRedirectURL.
      	nlapiSetRedirectURL('RECORD', 'customrecord_eym_material_req', recordID);
	}
}