/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       24 Jan 2018     EYM-013
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request){
	
	var context = nlapiGetContext();
    var role = context.getRole();	
    var status = nlapiGetFieldValue('custrecord_eym_mrf_status');
    
	form.setScript('customscript_eym_cs_popup_window'); 
	
	if(type == 'create' || type == 'edit'){
		if(status != 2){
			var projCode = nlapiGetFieldValue('custrecord_eym_mr_projcode');	
			var openProPlanBtn = "var prps = new Array(); var lineCnt = nlapiGetLineItemCount('recmachcustrecord_eym_mrfi_mrfid'); 	for(var i = 1; i <= lineCnt; i++) { 		prps.push(nlapiGetLineItemValue('recmachcustrecord_eym_mrfi_mrfid', 'custrecord_eym_mrfi_prpitem', i)); 	} 	 	  nlapiSetFieldValue('custrecord_eym_mrf_prps', prps.join(',')); showPopupFull('customscript_sl_eym_add_proc_plan_item','customdeploy_sl_eym_add_proc_plan_item','projCode="+projCode+"',800,600)";
			form.addButton('custpage_open_proc_plan_btn', 'Open Procurement Plan', openProPlanBtn);
		}
	}
}



