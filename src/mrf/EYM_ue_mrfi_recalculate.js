/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       17 Apr 2018     Roy Selim
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type)
{
	// Run this script only when item is edited.
	if( type == 'edit' ) {

		// Declare local scope constants.
		const MRC_STOCK_REQUEST_FLD_STR		= 'custrecord_eym_mrfi_srqty';
		const MRC_REQUESTED_QTY_FLD_STR		= 'custrecord_eym_mrfi_reqqty';
		const MRC_PURCHASE_REQ_FLD_STR		= 'custrecord_eym_mrfi_prqty';

		// Get the value of Stock Request field.
		var stockRequestValue = safeNum( nlapiGetFieldValue( MRC_STOCK_REQUEST_FLD_STR ) );

		// Get the value of the Requested Quantity field.
		var requestedQtyValue = safeNum( nlapiGetFieldValue( MRC_REQUESTED_QTY_FLD_STR ) );

		// Get the current Purchase Quantity value.
		var purchaseReqValue = safeNum( nlapiGetFieldValue( MRC_PURCHASE_REQ_FLD_STR ) );

		// Calculate the value of the Transferred Quantity.
		if( stockRequestValue <= requestedQtyValue && stockRequestValue >= 0 ) {

			purchaseReqValue = requestedQtyValue - stockRequestValue;

		// Calculate the value of the Stock Request Quntity.
		} else {

			stockRequestValue = requestedQtyValue - purchaseReqValue;
		}

		// Put the values back to the fields.
		nlapiSetFieldValue( MRC_STOCK_REQUEST_FLD_STR, stockRequestValue );
		nlapiSetFieldValue( MRC_PURCHASE_REQ_FLD_STR, purchaseReqValue );
	}
}

function safeNum(value)
{
  	if( isNaN( Number( value ) ) ) return 0;
  	return Number( value );
}