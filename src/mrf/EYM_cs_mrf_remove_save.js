/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       30 Jan 2018     Roy Selim
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType 
 * 
 * @param {String} type Access mode: create, copy, edit
 * @returns {Void}
 */
function clientPageInit(type){
	// Temporarily removed for testing and demo.
  	/*
    // Get the value of MRF Status.
    var mrfStatus = nlapiGetFieldValue('custrecord_eym_mrf_status');

	// Check if type is 'Edit'.
	if(type == 'edit' && mrfStatus == '2') {
		// alert(jQuery('#spn_multibutton_submitter')[0].className + ' ' + mrfStatus);

      	// Hide the save button.
      	jQuery('#spn_multibutton_submitter').parent().empty();
      	jQuery('#spn_secondarymultibutton_submitter').parent().empty();
      	jQuery('#spn_ACTIONMENU_d1').parent().empty();
      	jQuery('#spn_secondaryACTIONMENU_d1').parent().empty();
	}
    */
}
