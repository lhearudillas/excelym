/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       05 Jan 2015     dashikoy
 *
 */
function showPopupFull(scriptId, deploymentId, args, w, h)
{
	var url = nlapiResolveURL('SUITELET', scriptId, deploymentId);
	if(!isEmpty(args)) {
		url += '&';
		url += args;
	}
	windowOpenerFull(url, w, h);
}

function windowOpenerFull(url, a, b)
{
	newWindow = window.open(url, "PopUp", 'height='+screen.height+', width='+screen.width);
	newWindow.focus();
	return newWindow.name
}

function showPopup(scriptId, deploymentId, args, w, h)
{
	var url = nlapiResolveURL('SUITELET', scriptId, deploymentId);
	if(!isEmpty(args)) {
		url += '&';
		url += args;
	}
	windowOpener(url, w, h);
}

function windowOpener(url, a, b)
{
	newWindow = window.open(url, "PopUp", "resizable=1,width=" + a + ",height=" + b + ",left=" + (window.screen.width - a) / 2 + ",top=" + (window.screen.height - b) / 2 + ",scrollbars=yes");
	newWindow.focus();
	return newWindow.name
};

function isEmpty(fldValue)
{
	return fldValue == '' || fldValue == null || fldValue == undefined;
}