/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       24 Jan 2018     EYM-013
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */

function suitelet(request, response){
	
	if (request.getMethod() == 'GET') {	
		doGet(request, response);
	}else if (request.getMethod() == 'POST') {
		doPost(request, response);
	}

}

function doGet(request, response){

	var projCode2 = request.getParameter('projCode');	
	
	var filters = new Array();	
		filters.push(new nlobjSearchFilter('custrecord_eym_procplan_projcode', null, 'is', projCode2));
	var assetRes = nlapiSearchRecord('customrecord_eym_procplan_item', null, filters, null); 
	
	response.writePage(openProcurementPlan(null, projCode2, null, null, null, null, null, false));
  

}


function openProcurementPlan(abRecs, projectCode, projectSub, activity, keyVal, boq, cost, disabled){
	var values = new Array();
	
	var form = nlapiCreateForm('Procurement Plan', true);
	form.setScript('customscript_cs_eym_search_proc_plan'); 

	var projSubsystem = form.addField('custpage_proj_subsystem', 'select', 'Project Subsystem');
	if(disabled == true && projectSub){
		projSubsystem.setDisplayType('disabled');
	}
		
	var projCode = form.addField('custpage_proj_code', 'text', 'Project Code').setDisplayType('hidden');
		projCode.setDefaultValue(projectCode);
	var actGroup = form.addField('custpage_act_group', 'select', 'Activity Group');
		
	var billofQty = form.addField('custpage_bill_of_qty', 'select', 'Bill of Quantity');
		billofQty.setDefaultValue(boq);
	var costType = form.addField('custpage_cost_type', 'select', 'Cost Type', 'classification'); 
		costType.setDefaultValue(cost);
	var keyword = form.addField('custpage_keyword', 'text', 'Keywords');
		keyword.setDefaultValue(keyVal);
		keyword.setDisplaySize(40,40);
		keyword.setLayoutType('outsidebelow','startrow');
		
	var mrfPrps = form.addField('custpage_mrfprps','textarea','MRF PRPS').setDisplayType('hidden');
		
	
	var projSubarr = new Array();
	var actGrparr = new Array();
	
	//Search for BQIs with the same project code in MRF
	
	var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_eym_boq_projid', null, 'is', projectCode));
		if(projectSub){
			filters.push(new nlobjSearchFilter('custrecord_eym_proj_subsystem', null, 'is', projectSub));	
		}
		if(activity){
			filters.push(new nlobjSearchFilter('custrecord_eym_boq_categories', null, 'is', activity));
		}
	var columns = new Array();
		columns[0] = new nlobjSearchColumn('name');
		columns[1] = new nlobjSearchColumn('custrecord_eym_proj_subsystem');
		columns[2] = new nlobjSearchColumn('custrecord_eym_boq_categories');
		
	var FileRes = nlapiSearchRecord('customrecord_eym_boq', null, filters, columns);
		
	billofQty.addSelectOption('','');
	projSubsystem.addSelectOption('','');
	actGroup.addSelectOption('','');
	
		
		if(FileRes){
			for(var i = 0; i < FileRes.length; i++) {
				var fileid = FileRes[i].getId();
				var finale = FileRes[i].getValue('name');
				var projSubsystem2 = FileRes[i].getValue('custrecord_eym_proj_subsystem');
				var actGroup2 = FileRes[i].getValue('custrecord_eym_boq_categories');
				
									  
				//Only BQIs with Procurement Plan will be displayed in the list
				var recBQI = nlapiLoadRecord('customrecord_eym_boq', fileid);
				var procCount = recBQI.getLineItemCount('recmachcustrecord_eym_procplan_bqi');
					 
				if(procCount > 0){
					billofQty.addSelectOption(fileid, finale);                	
					projSubarr.push(projSubsystem2);
					actGrparr.push(actGroup2);
				}
					 
			}
				 
		}
		
	var resultProjSub = new Array();
		resultProjSub = removeDuplicates(projSubarr);
	if(resultProjSub){
		for(var i = 0; i < resultProjSub.length; i++) {
			projSubsystem.addSelectOption(resultProjSub[i], resultProjSub[i]);
		}
		projSubsystem.setDefaultValue(projectSub);
	}
		
	var resultActGrp = new Array();
		resultActGrp = removeDuplicates(actGrparr);
	if(resultActGrp){
		for(var i = 0; i < resultActGrp.length; i++) {
			actGroup.addSelectOption(resultActGrp[i], resultActGrp[i]);
		}
		actGroup.setDefaultValue(activity);
	}
					
	var fld = form.addField('custpage_mode', 'text', 'Customer');	
		fld.setDisplayType('hidden');
		
	var sublistFld = form.addSubList('custpage_proc_plan_sublist', 'list', 'Procurement Plan Items');
	  
		sublistFld.addButton('custpage_mark_all','Mark All','markAll()');
		sublistFld.addButton('custpage_unmark_all','Unmark All','unMarkAll()');
		
		sublistFld.addField('custpage_ap_select', 'checkbox', 'Select');
		sublistFld.addField('custpage_bill_of_quantity', 'select', 'Bill of Quantity', 'customrecord_eym_boq').setDisplayType('inline');
		sublistFld.addField('custpage_proc_plan_item', 'select', 'Procurement Plan Item', 'customrecord_eym_procplan_item').setDisplayType('inline');
		sublistFld.addField('custpage_item', 'select', 'Item', 'item').setDisplayType('inline');
		sublistFld.addField('custpage_uom', 'select', 'UOM', 'unit').setDisplayType('inline');
		sublistFld.addField('custpage_unit_price', 'currency', 'Unit Price').setDisplayType('inline');
		sublistFld.addField('custpage_qty', 'integer', 'Quantity').setDisplayType('inline');
		sublistFld.addField('custpage_req_qty', 'float', 'Transferred Quantity').setDisplayType('inline');
		sublistFld.addField('custpage_for_req_qty', 'float', 'For Request Quantity').setDisplayType('inline');
		
		sublistFld.addField('custpage_amount', 'integer', 'Amount').setDisplayType('hidden');
		sublistFld.addField('custpage_received_date', 'text', 'Receive By Date').setDisplayType('hidden');
		sublistFld.addField('custpage_location', 'text', 'Location').setDisplayType('hidden');
		sublistFld.addField('custpage_remarks', 'text', 'Remarks').setDisplayType('hidden');

		
	if(abRecs != null) {	
		for(var i = 0; i < abRecs.length; i++) {
				
			var sl_boq;
			var sl_ppi;
			var sl_item;
			var sl_uom;
			var sl_qty;
			var sl_unit_price;
			var sl_trans_qty_value;
			var sl_for_req_qty;
			var sl_date;
			var sl_loc;
			var sl_remarks;

			var abRec = abRecs[i];

			sl_boq = abRec.getValue('custrecord_eym_procplan_bqi');
			sl_ppi = abRec.getValue('internalid');
			sl_item = abRec.getValue('custrecord_eym_procplan_item');
			sl_uom = abRec.getValue('custrecord_eym_procplan_uom');
			sl_qty = abRec.getValue('custrecord_eym_procplan_qty');
			sl_unit_price = abRec.getValue('custrecord_eym_prp_unitprice');
			sl_date = abRec.getValue('custrecord_eym_procplan_deldate');
			sl_loc = abRec.getValue('custrecord_eym_procplan_recloc');
			sl_remarks = abRec.getValue('custrecord_eym_procplan_remarks');

			sl_trans_qty_value = getPRPAggregateQty(sl_ppi, sl_item, true, true);

			sl_for_req_qty = sl_qty - sl_trans_qty_value;
					

			if(sl_for_req_qty > 0){
					
			values.push({					
				'custpage_bill_of_quantity' : sl_boq,
				'custpage_proc_plan_item' : sl_ppi,
				'custpage_item' : sl_item,							
				'custpage_uom' : sl_uom,
				'custpage_qty' : sl_qty, 
				'custpage_unit_price' : sl_unit_price, 
				'custpage_req_qty' : sl_trans_qty_value, 
				'custpage_for_req_qty' : sl_for_req_qty,
				'custpage_received_date' : sl_date,
				'custpage_location' : sl_loc,
				'custpage_remarks' : sl_remarks,

				});	
				
			sublistFld.setLineItemValues(values);
				
			}
				
		}
	} else {
			
	}
	
	form.addSubmitButton('Add to MRF');
	form.addButton('custpage_search', 'Search', 'submitSearchAsset()');
	form.addButton('custpage_close', 'Close', 'submitClosePopup()');
	form.addButton('custpage_reset','Reset','actionReset()');

	return form;
	
}


function doPost(request, response)
{
	var mode = request.getParameter('custpage_mode');
	if(mode == 'close') {
		doPostClose(response);
	}else if(mode == 'search'){ 
		doPostSearch(request, response);
	}else if(mode == 'reset'){
		doPostReset(request, response);
	}else{
		doPostAddToMRF(request,response);
	}
}


function doPostClose(response)
{
	var html = ''; 
	var itemHtml = '';
	
	html = '<html>';
	html += '<head>';
	html += '<script language="JavaScript">';
	html += 'window.close();';
    html += '</script>';
    html += '</head>';
    html += '<body>';
    html += '</body>';
    html += '</html>';
	
    response.write(html);
  
}

function doPostReset(request, response)
{
	var projCode = request.getParameter('custpage_proj_code');
	response.writePage(openProcurementPlan(null, projCode, null, null, null, null, null, false));
}

function doPostSearch(request, response)
{
	
	var projSub = request.getParameter('custpage_proj_subsystem');
	var projCode = request.getParameter('custpage_proj_code');
	var actGrp = request.getParameter('custpage_act_group');
	var BofQty = request.getParameter('custpage_bill_of_qty');
	var costPage = request.getParameter('custpage_cost_type');
	var keyword2 = request.getParameter('custpage_keyword');
	
	var filters = new Array();	
	if(!IsNullOrEmpty(projCode)){
		if(!IsNullOrEmpty(projSub)){
			filters.push(new nlobjSearchFilter('custrecord_eym_procplan_proj_subsystem', null, 'is', projSub).setOr(false));
		}
		
		if(!IsNullOrEmpty(actGrp)){
			filters.push(new nlobjSearchFilter('custrecord_eym_procplan_act', null, 'is', actGrp).setOr(false));
		}
		
		if(!IsNullOrEmpty(BofQty)){
			filters.push(new nlobjSearchFilter('custrecord_eym_procplan_bqi', null, 'is', BofQty).setOr(false));
		}
		
		if(!IsNullOrEmpty(costPage)){
			var costpage = nlapiLoadRecord('classification', costPage);
			var costpageName = costpage.getFieldValue('name');
			filters.push(new nlobjSearchFilter('custrecord_eym_procplan_cost_type', null, 'is', costpageName).setOr(false));
		}
		var items = new Array();
		if(!IsNullOrEmpty(keyword2)){	
			var filt = new Array();
			filt.push(new nlobjSearchFilter('itemid',null,'contains',keyword2));
			var itemRes = nlapiSearchRecord('inventoryitem', null, filt);
			
			if(itemRes){
				for(var c=0;c<itemRes.length;c++){
					var itemID = itemRes[c].getId();
					
					items.push(itemID);
				}
				filters.push(new nlobjSearchFilter('custrecord_eym_procplan_item', null, 'anyof', items));
			}
		}
		
		var columns = new Array();
			columns.push(new nlobjSearchColumn('custrecord_eym_procplan_bqi'));
			columns.push(new nlobjSearchColumn('custrecord_eym_procplan_proj_subsystem'));
			columns.push(new nlobjSearchColumn('custrecord_eym_procplan_act_des'));
			columns.push(new nlobjSearchColumn('internalid'));
			columns.push(new nlobjSearchColumn('custrecord_eym_procplan_item'));
			columns.push(new nlobjSearchColumn('custrecord_eym_procplan_uom'));
			columns.push(new nlobjSearchColumn('custrecord_eym_procplan_qty'));
			columns.push(new nlobjSearchColumn('custrecord_eym_prp_unitprice'));
			columns.push(new nlobjSearchColumn('custrecord_eym_procplan_amt'));
			columns.push(new nlobjSearchColumn('custrecord_eym_procplan_deldate'));
			columns.push(new nlobjSearchColumn('custrecord_eym_procplan_recloc'));
			columns.push(new nlobjSearchColumn('custrecord_eym_procplan_remarks'));

		if (filters.length > 0) {
			filters.push(new nlobjSearchFilter('custrecord_eym_procplan_projcode', null, 'is', projCode).setOr(false));
			var assetRes = nlapiSearchRecord('customrecord_eym_procplan_item', null, filters,columns); 
		} else {
			filters.push(new nlobjSearchFilter('custrecord_eym_procplan_projcode', null, 'is', projCode).setOr(false));
			var assetRes = nlapiSearchRecord('customrecord_eym_procplan_item', null, filters,columns); 
		}
		response.writePage(openProcurementPlan(assetRes, projCode, projSub, actGrp, keyword2, BofQty, costPage,false));
	} else {
		response.writePage(openProcurementPlan(null, projCode, projSub, actGrp, keyword2, BofQty, costPage,false));
	}
	
}

function getAvailableQuantity(itemRess, itemId)
{
	var availableQty = 0;
	if(itemRess != null) {
		for(var i = 0; i < itemRess.length; i++) {
			var itemRes = itemRess[i];
			if(itemRes.getId() == itemId) {
				availableQty = itemRes.getValue('locationquantityavailable');
				break;
			}
		}
	}
	
	return availableQty;
}

function doPostAddToMRF(request,response)
{

	var html = ''; 
	var itemHtml = '';
	
	html = '<html>';
	html += '<head>';
	html += '<script language="JavaScript">';

	//get values
	var resCount = request.getLineItemCount('custpage_proc_plan_sublist');
	var mrfs = request.getParameter('custpage_mrfprps');
	
	//loop
	if(resCount > 0){
		var itemIds = new Array();
		for(var i = 1; i <= resCount; i++) {
			var isSelected = request.getLineItemValue('custpage_proc_plan_sublist', 'custpage_ap_select', i); 
			if(isSelected == 'T') {
				var itemId = request.getLineItemValue('custpage_proc_plan_sublist', 'custpage_item', i);
				itemIds.push(itemId);
			}
		}
		
		var itemRess = null;
		if(itemIds.length > 0) {
			itemRess = nlapiSearchRecord('item', 'customsearch_eym_item_with_cw_stock', ['internalid', 'anyof', itemIds]);
		}
		
		for(var a=1; a <= resCount; a++) {
			var isSelected = request.getLineItemValue('custpage_proc_plan_sublist','custpage_ap_select',a); 
						
			if(isSelected == 'T') {
				var ppi_boq = request.getLineItemValue('custpage_proc_plan_sublist','custpage_bill_of_quantity',a);
				var ppi_procplanitem = request.getLineItemValue('custpage_proc_plan_sublist','custpage_proc_plan_item',a);
				var ppi_item = request.getLineItemValue('custpage_proc_plan_sublist','custpage_item',a);
				var ppi_uom = request.getLineItemValue('custpage_proc_plan_sublist','custpage_uom',a);
				var ppi_unit_price = request.getLineItemValue('custpage_proc_plan_sublist','custpage_unit_price',a);
				var ppi_qty = request.getLineItemValue('custpage_proc_plan_sublist','custpage_qty',a);
				var ppi_req_qty = request.getLineItemValue('custpage_proc_plan_sublist','custpage_req_qty',a);
				var ppi_for_req_qty = request.getLineItemValue('custpage_proc_plan_sublist','custpage_for_req_qty',a);
				var ppi_date = request.getLineItemValue('custpage_proc_plan_sublist','custpage_received_date',a);
				var ppi_loc = request.getLineItemValue('custpage_proc_plan_sublist','custpage_location',a);
				var ppi_remarks = request.getLineItemValue('custpage_proc_plan_sublist','custpage_remarks',a);
				
				var prpExist = valuesFound(mrfs,ppi_procplanitem);
				if(prpExist == false){
					var ppi_cw_value = getAvailableQuantity(itemRess, ppi_item);
							
					//Formula for Amount
					var amt = ppi_for_req_qty * ppi_unit_price;
					
					
					//Computation for Stock Request and Purchase Request
					// Roy Selim added this code
					var for_stock_request_qty = ((ppi_cw_value - ppi_for_req_qty) > 0) ? ppi_for_req_qty : ppi_cw_value;
					for_stock_request_qty = (for_stock_request_qty > 0) ? for_stock_request_qty: 0;
					var for_purchase_qty = ppi_for_req_qty - for_stock_request_qty;
					// Until here
	              
	                // Roy Selim commented this line below and added a new line under it.
					// html += createLineItem(ppi_boq,ppi_procplanitem,ppi_item,ppi_uom,ppi_qty,ppi_req_qty,ppi_cw_value,trans_qty,amt,for_req_qty);
	                html += createLineItem(ppi_boq,ppi_procplanitem,ppi_item,ppi_uom,ppi_qty,ppi_req_qty,ppi_cw_value,amt,ppi_for_req_qty,for_stock_request_qty,for_purchase_qty,ppi_unit_price,ppi_date,ppi_loc,ppi_remarks);
				}
			} 
		}
		
		if(ppi_boq != null){
			//Window will close once the items are selected and Add to MRF button is clicked
			html += 'window.close();';
			html += '</script>';
			html += '</head>';
			html += '<body>';
			html += '</body>';
			html += '</html>';
		} 
		
	} 
	/*
	//Window will close once Add to MRF button is clicked - sublist is empty
	else {
		html += 'alert("Notice: No items selected.");';
		html += 'window.close();';
		html += '</script>';
		html += '</head>';
		html += '<body>';
		html += '</body>';
		html += '</html>';
	}
	*/
	
	response.write(html);
}

function createLineItem(ppi_boq,ppi_procplanitem,ppi_item,ppi_uom,ppi_qty,ppi_req_qty,ppi_cw_value,amt,ppi_for_req_qty,for_stock_request_qty,for_purchase_qty,ppi_unit_price,ppi_date,ppi_loc,ppi_remarks)
{	

	var itemHtml = '  window.opener.nlapiSelectNewLineItem(\'recmachcustrecord_eym_mrfi_mrfid\');';
	
	itemHtml += '   window.opener.isinited = true;';	
	
	if(!IsNullOrEmpty(ppi_boq)) itemHtml += '   window.opener.nlapiSetCurrentLineItemValue(\'recmachcustrecord_eym_mrfi_mrfid\', \'custrecord_eym_mrfi_boq\', \'' + ppi_boq + '\', ' + false + ', ' + false + ');'; 
	if(!IsNullOrEmpty(ppi_procplanitem)) itemHtml += '   window.opener.nlapiSetCurrentLineItemValue(\'recmachcustrecord_eym_mrfi_mrfid\', \'custrecord_eym_mrfi_prpitem\', \'' + ppi_procplanitem + '\', ' + false + ', ' + false + ');'; 
	if(!IsNullOrEmpty(ppi_item)) itemHtml += '   window.opener.nlapiSetCurrentLineItemValue(\'recmachcustrecord_eym_mrfi_mrfid\', \'custrecord_eym_mrfi_item\', \'' + ppi_item + '\', ' + false + ', ' + false + ');'; 
	if(!IsNullOrEmpty(ppi_uom)) itemHtml += '   window.opener.nlapiSetCurrentLineItemValue(\'recmachcustrecord_eym_mrfi_mrfid\', \'custrecord_eym_mrfi_uom\', \'' + ppi_uom + '\', ' + false + ', ' + false + ');'; 
	if(!IsNullOrEmpty(ppi_qty)) itemHtml += '   window.opener.nlapiSetCurrentLineItemValue(\'recmachcustrecord_eym_mrfi_mrfid\', \'custrecord_eym_mrfi_quantity\', \'' + ppi_qty + '\', ' + true + ', ' + true + ');'; 
	if(!IsNullOrEmpty(ppi_req_qty)) itemHtml += '   window.opener.nlapiSetCurrentLineItemValue(\'recmachcustrecord_eym_mrfi_mrfid\', \'custrecord_eym_mrfi_st_qty\', \'' + ppi_req_qty + '\', ' + true + ', ' + true + ');'; 
	if(!IsNullOrEmpty(ppi_cw_value)) itemHtml += '   window.opener.nlapiSetCurrentLineItemValue(\'recmachcustrecord_eym_mrfi_mrfid\', \'custrecord_eym_mrfi_cw_qty\', \'' + ppi_cw_value + '\', ' + true + ', ' + true + ');'; 
	if(!IsNullOrEmpty(ppi_for_req_qty)) itemHtml += '   window.opener.nlapiSetCurrentLineItemValue(\'recmachcustrecord_eym_mrfi_mrfid\', \'custrecord_eym_mrfi_reqqty\', \'' + ppi_for_req_qty + '\', ' + true + ', ' + true + ');'; 
	if(!IsNullOrEmpty(ppi_unit_price)) itemHtml += '   window.opener.nlapiSetCurrentLineItemValue(\'recmachcustrecord_eym_mrfi_mrfid\', \'custrecord_eym_mrfi_unitprice\', \'' + ppi_unit_price + '\', ' + true + ', ' + true + ');'; 
	if(!IsNullOrEmpty(amt)) itemHtml += '   window.opener.nlapiSetCurrentLineItemValue(\'recmachcustrecord_eym_mrfi_mrfid\', \'custrecord_eym_mrfi_amount\', \'' + amt + '\', ' + true + ', ' + true + ');'; 
	if(!IsNullOrEmpty(ppi_date)) itemHtml += '   window.opener.nlapiSetCurrentLineItemValue(\'recmachcustrecord_eym_mrfi_mrfid\', \'custrecord_eym_mrfi_recdate\', \'' + ppi_date + '\', ' + true + ', ' + true + ');'; 
	if(!IsNullOrEmpty(ppi_loc)) itemHtml += '   window.opener.nlapiSetCurrentLineItemValue(\'recmachcustrecord_eym_mrfi_mrfid\', \'custrecord_eym_mrfi_loc\', \'' + ppi_loc + '\', ' + true + ', ' + true + ');'; 
	if(!IsNullOrEmpty(ppi_remarks)) itemHtml += '   window.opener.nlapiSetCurrentLineItemValue(\'recmachcustrecord_eym_mrfi_mrfid\', \'custrecord_eym_mrfi_remarks\', \'' + ppi_remarks + '\', ' + true + ', ' + true + ');'; 

    // Roy Selim added 2 more lines of code below.
    if(!IsNullOrEmpty(for_stock_request_qty)) itemHtml += '   window.opener.nlapiSetCurrentLineItemValue(\'recmachcustrecord_eym_mrfi_mrfid\', \'custrecord_eym_mrfi_srqty\', \'' + for_stock_request_qty + '\', ' + true + ', ' + true + ');'; 
	if(!IsNullOrEmpty(for_purchase_qty)) itemHtml += '   window.opener.nlapiSetCurrentLineItemValue(\'recmachcustrecord_eym_mrfi_mrfid\', \'custrecord_eym_mrfi_prqty\', \'' + for_purchase_qty + '\', ' + true + ', ' + true + ');'; 
	

	itemHtml += '   window.opener.isinited = true;';        
	itemHtml += '   window.opener.nlapiCommitLineItem(\'recmachcustrecord_eym_mrfi_mrfid\');';
		
	return itemHtml; 
	
}


function getSpaceIndentation(level) 
{
	var space = '';
	if(level == 2) {
		//space = '--- ';
		space = ('<html>&nbsp;&nbsp;&nbsp;</html>');
	}else if(level == 3) {
		//space = '------ ';
		space = ('<html>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</html>');
	}else if(level == 4) {
		//space = '--------- ';
		space = ('<html>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</html>');
	}else if(level == 5) {
		//space = '------------ ';
		space = ('<html>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</html>');
	}else if(level == 6) {
		//space = '--------------- ';
		space = ('<html>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</html>');
	}else if(level == 7) {
		//space = '------------------ ';
		space = ('<html>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</html>');
	}else if(level == 8) {
		//space = '--------------------- ';
		space = ('<html>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</html>');
	}else if(level == 9) {
		//space = '------------------------ ';
		space = ('<html>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</html>');
		
	}else if(level == 10) {
		//space = '--------------------------- ';
		space = ('<html>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</html>');
	}	
	
	return space;
}

function removeDuplicates(arr){
    var unique_array = new Array();
    for(var i = 0;i < arr.length; i++){
        if(unique_array.indexOf(arr[i]) == -1){
            unique_array.push(arr[i]);
        }
    }
    return unique_array;
}

function valuesFound(mrfs,val){
	
	if(mrfs){
		var mrfsArr = mrfs.toString().split(',');
		if(mrfsArr){
			var exists = mrfsArr.indexOf(val);
			if(exists == -1){
				return false;
			}else{
				return true;
			}
		}
		
	}
	return false;
}