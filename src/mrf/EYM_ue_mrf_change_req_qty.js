/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       06 Mar 2018     Roy Selim
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type){

	// Run this script on 'EDIT' type.
	if( type == 'edit' ) {

		// Define function variables.
		var errorArray = [];

		// Define function constants.
		const LINE_TYPE = 'recmachcustrecord_eym_mrfi_mrfid';
		const REQST_QTY = 'custrecord_eym_mrfi_reqqty';
		const STOCK_REQ = 'custrecord_eym_mrfi_srqty';
		const PURCH_REQ = 'custrecord_eym_mrfi_prqty';

		// Get the number of lines
		var numberOfItems = nlapiGetLineItemCount( 'recmachcustrecord_eym_mrfi_mrfid' );

		// Loop thru all the items.
		for(var i = 1; i <= numberOfItems; i++) {

			// Get the needed quantities.
			var forRequestQty         = Number(nlapiGetLineItemValue(LINE_TYPE, REQST_QTY, i));
			var forStockTransferQty   = Number(nlapiGetLineItemValue(LINE_TYPE, STOCK_REQ, i));
          	var oldPurchaseRequestQty = Number(nlapiGetLineItemValue(LINE_TYPE, PURCH_REQ, i));
			var forPurchaseRequestQty = (!isNaN(forRequestQty) && !isNaN(forStockTransferQty)) ? forRequestQty - forStockTransferQty : null;

            if(forPurchaseRequestQty != null) {

                // Validate the new value
                if( forPurchaseRequestQty >= 0 && forPurchaseRequestQty <= forRequestQty && forStockTransferQty >= 0) {

                    // Attempt to set the value of the line item.
                    try{
                        nlapiSetLineItemValue(LINE_TYPE, PURCH_REQ, i, forPurchaseRequestQty);
                    } catch(e) {
                        errorArray.push(e);
                    }
                } else {

                    // Attempt to load the original.
                    try{
                        nlapiSetLineItemValue(LINE_TYPE, STOCK_REQ, i, (forRequestQty - oldPurchaseRequestQty));
                    } catch(e) {
                        errorArray.push(e);
                    }
                }
            }
		}
	}
}
