/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       24 Jan 2018     EYM-013
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 *
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request){
	// Run this script only when customer is viewing a project
  	if(type == 'view') {
  		// Get the record ID, this will be used during search.
  		var recordId = nlapiGetRecordId();

  		// Validation #1, status of the project must be "In-Progress".
  		var projectStatus = nlapiGetFieldText('entitystatus'); // entitystatus: Internal ID of the field in context.

  		// validation #2: Bill of Quantity Baselines have been established
  		// Bill of Quantity line items should have at least 1 entry.
  		var billOfQtyItemCount = nlapiGetLineItemCount('custpage_boq_sublist'); //custpage_boq_sublist: Internal ID of the sublist.

   		// Validation #3: The project has at least 1 Procurement plan.
  		var filters = new Array();
    	filters.push(new nlobjSearchFilter('custrecord_eym_procplan_projcode', null, 'anyof', recordId));
    	var procurementPlanSearchResults = nlapiSearchRecord('customrecord_eym_procplan_item', null, filters);
    	var ppsrLen = (procurementPlanSearchResults !== null)?
        	procurementPlanSearchResults.length: 0;

      	// Validation #4: User Role must be Operations Engineer (1013).
      	var context = nlapiGetContext();
    	var role = context.getRole();

  		// Add a button to the main form.
		if(projectStatus === 'In Progress' && billOfQtyItemCount > 0 && ppsrLen > 0 && role == 1013) {

      		// Create redirect link.
      		var redirectLink = nlapiResolveURL('RECORD', 'customrecord_eym_material_req');
  	  		redirectLink += '&projID=' + recordId;

      		// Create a script and associate it with the button.
  	  		var buttonScript = "window.location.assign('"+ redirectLink +"')";

      		// Add button to the form.
      		form.addButton('custpage_request_items_btn', 'Request Items', buttonScript);

    	}
    }
}
