/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       07 Jun 2018     Roy Selim
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit( type )
{
	// Run this script on a delete operation.
	if( type == 'delete' ) {

		// Get the record ID.
		var vendorBillID = nlapiGetRecordId();

		// Create a try search column variable.
		var columns = [ 'internalid', 'custrecord_eym_ple_bqi' ];

		// Try to get the project ledger entries using the ID.
		var tryPles = getProjectLedgerEntries( vendorBillID, null, columns );

		// Initialize a bqi array.
		var bqiArray = [];

		// Delete all the project ledger entries. Start with looping thru all PLEs
		if( !tryPles.error && tryPles.value ) {

			// Assign the current item to a variable.
			var ples = tryPles.value;

			// Loop thru all the ples
			for( var i = 0; i < ples.length; i++ ) {

				// Get the bqi.
				var bqi = ples[i].getValue( 'custrecord_eym_ple_bqi' );

				// Check if there is a value.
				if( bqi && ( bqiArray.indexOf( bqi ) == -1 ) ) bqiArray.push( bqi );
			}

			// Delete the ples
			deletePles( ples );

			// Update all the affected bqis.
			for( var j = 0; j < bqiArray.length; j++ ) {

				// Update.
				updateBQIUsage( bqiArray[j] );
			}
		}
	}
}

function userEventAfterSubmit( type )
{
	// Get the record ID.
	var vendorBillID 	= nlapiGetRecordId();

	// For create operations, create the project ledger entries.
	if( type == 'create' ) {

		// Create the project ledger entries.
		projectLedgerEntry( vendorBillID );

	// For edit operations, edit the vendor project ledger.
	} else if( type == 'edit' ) {

		// Initialize function variables.
		var glWithBQI	= null;
		var ples		= null;
		var projID		= null;
		var plID		= null;

		// Get the project ID.
		projID = nlapiLookupField( 'vendorbill', vendorBillID, 'custbody_eym_pr_projcode' );

		// Get all the ples for this vendor bill.
		// var tryPles = getProjectLedgerEntries( vendorBillID, null, [ 'internalid', 'custrecord_eym_ple_pl_no' ] );
      	var tryPles = getProjectLedgerEntries( vendorBillID, null, [ 'internalid' ] );

		// Check if there are returned values.
		if( !tryPles.error && tryPles.value ) {

			// Get the project ledger id.
			// if( projID ) plID = getCreateProjectLedgerId( projID );

			// Assign the value to oa variable.
			ples = tryPles.value;

			// Get the gl impact.
			var tryGL = getGLImpact( 'vendorbill', null, vendorBillID );

			// Check if there are results from the search.
			if( !tryGL.error && tryGL.value ) {

				// Assign the gl with BQIs to a variable.
				glWithBQI = addBQIToBillGL( tryGL.value, vendorBillID, 'vendorbill', 'item' );

				// Initialize a bqi array.
				var bqiArray = [];

				// Loop thru all the gl with bqis.
				for( var i = 0; i < glWithBQI.length; i++ ) {

					// Initialize a container variable.
					var glBqi = glWithBQI[i].bqi;

					// If there is a bqi value, push it to the array.
					if( glBqi && ( bqiArray.indexOf( glBqi ) == -1 ) ) bqiArray.push( glBqi );

					// Assign a project ledger item to a variable.
					var plItem = ( ples[i] != undefined ) ? ples[i] : null;

					// Initialize a ple id variable.
					var pleId 		= null;
					var pleRecord 	= null;

					// Assign a project ledger item to a variable.
					var plItem = ( ples[i] != undefined ) ? ples[i] : null;

					// Get the id of the project ledger search result.
					if( plItem ) {

						// Get the ID.
						pleId = plItem.getValue( 'internalid' );

						// Load the project ledger entry record.
						var trypleRecord = tryLoadRecord( 'customrecord_eym_proj_ledger_entry', pleId );

						// Check if the project ledger entry was loaded.
						if( !trypleRecord.error && trypleRecord.value ); {

							// Assign the record to the variable.
							pleRecord = trypleRecord.value;
						}
					}
					
					// Get the transaction date.
					var transDate = nlapiLookupField( 'vendorbill', vendorBillID, 'trandate' );

					// Enter data to the ledger entry record.
                  	ledgerEntry( glWithBQI[i], pleRecord, vendorBillID, projID, transDate );
				}

				// If there are excess ples, just delete them after IF edit.
				if( ples.length > glWithBQI.length ) {

					// Loop thru all the excess ples and delete them.
					var delPles = ples.slice( glWithBQI.length );

					// Delete the excess ples
					deletePles( delPles );

					// Loop thru all the deleted ples.
					for( var k = 0; k < delPles.length; k++ ) {

						// Get the bqi.
						var delBqi = delPles[k].getValue( 'custrecord_eym_ple_bqi' );

						// Check if the bqi is already on the array.
						if ( delBqi && ( bqiArray.indexOf( delBqi ) == -1 ) ) bqiArray.push( delBqi );
					}
				}

				// Update all the affected bqis.
				for( var l = 0; l < bqiArray.length; l++ ) {

					// Update.
					updateBQIUsage( bqiArray[l] );
				}
			}

		// If there were no previously created ples, create them instead.
		} else {

			// Create the project ledger entries.
			projectLedgerEntry( vendorBillID );
		}
	}
}