/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       08 Mar 2018     Lhea
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request)
{
	try {
		if (type == 'create') {
			var stringVal = nlapiGetFieldValue('entryformquerystring');
			
			if(stringVal) {
				var url = stringVal.split('&');
				var poId = '';
				
				for(var i = 0; i < url.length; i++ ) {
					var p = url[i].split('=');
					if(p[0] == 'id') {
						poId = p[1];
						if(poId) {
							nlapiSetFieldValue('custbody_eym_downpayment_po_num', poId);
							
							var dpAmount = populateDownPayment(poId);
							nlapiSetFieldValue('custbody_eym_dp_balance', dpAmount);
						}
					}
				}
			}
		}
	} catch(e) {
		var errMsg = '';
		if(e instanceof nlobjError) {
			errMsg = e.getCode() + ': ' + e.getDetails();
		} else {
			errMsg = 'UNEXPECTED ERROR: ' + e.toString();	
		}
        
		nlapiLogExecution('DEBUG', 'Error', errMsg);
	}
}

function populateDownPayment(poId) 
{
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custcol_eym_downpayment_po', null, 'is', poId));
	
	var searchCheck = nlapiSearchRecord(null, 'customsearch_eym_down_payment_balance', filters);
	
	if (searchCheck) {
		for (var i = 0; i < searchCheck.length; i++) {
			var checkAmount = Number(searchCheck[i].getValue('amount', null, 'SUM'));
			
			if (checkAmount) {
				var dpAmount = checkAmount.toFixed(2);
			}
		}
	}
	
	return dpAmount;
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type)
{
	try {
		var recId = nlapiGetRecordId();
		var recType = nlapiGetRecordType();
		
		var recFields = new Array();
		recFields.push('entity');
		recFields.push('trandate');
		recFields.push('account');
		recFields.push('custbody_eym_downpayment_applied');
		recFields.push('custbody_eym_downpayment_po_num');
		
		var recValues = nlapiLookupField(recType, recId, recFields);
		var vendor = recValues['entity'];
	    var date = recValues['trandate'];
	    var account = recValues['account'];
	    var poNum = recValues['custbody_eym_downpayment_po_num'];
	    var appliedDP = recValues['custbody_eym_downpayment_applied'];
	    
	    if (poNum && appliedDP && appliedDP != 0) {
	    	nlapiSubmitField(recType, recId, 'custbody_eym_dp_balance', '');
	    	
		    var columns = new Array();
			columns.push(new nlobjSearchColumn('custbody_eym_downpayment_created_from'));
			
			var filters = new Array();	
			filters.push(new nlobjSearchFilter('custbody_eym_downpayment_created_from', null, 'is', recId));
			filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));	
			
			var journalEntry = nlapiSearchRecord('journalentry', null, filters, columns); 
			
			if (journalEntry != null) {
				var journalId = journalEntry[0].getId();
				var journal = createOrEditJournal(recId, date, account, appliedDP, vendor, poNum, journalId);
			} else {
				var journal = createOrEditJournal(recId, date, account, appliedDP, vendor, poNum, journalEntry);
			}
			
			if (journal != null) {
				createBillPayment(recId, recType, poNum, journal);
			}
	    }
	} catch(e) {
		var errMsg = '';
		if(e instanceof nlobjError) {
			errMsg = e.getCode() + ': ' + e.getDetails();
		} else {
			errMsg = 'UNEXPECTED ERROR: ' + e.toString();	
		}
        
		nlapiLogExecution('DEBUG', 'Error', recType + '/' + recId + ' ' + errMsg);
	}
}

function createOrEditJournal(billId, date, account, appliedDp, vendor, poNum, journalId) 
{
	var journal;
	
	if (journalId != null) {
		// Edit Journal Entry
		var oldRecord = nlapiGetOldRecord();
		var oldAppliedDp = oldRecord.getFieldValue('custbody_eym_downpayment_applied');
		
		if (oldAppliedDp != appliedDp) {
			journal = nlapiLoadRecord('journalentry', journalId);
			var lineCount = journal.getLineItemCount('line');
			
			if (lineCount > 0) {
				for (var i = 1; i <= lineCount; i++) {
					var accountValue = journal.getLineItemValue('line', 'account', i);
					
					if (accountValue == 356) {
						journal.setLineItemValue('line', 'account', i, 356);
						journal.setLineItemValue('line', 'credit', i, appliedDp);
					} else {
						journal.setLineItemValue('line', 'account', i, account);
						journal.setLineItemValue('line', 'debit', i, appliedDp);
					}
				}
			}
		}
		var journalEntry = nlapiSubmitRecord(journal, true, true);
		nlapiLogExecution('DEBUG', 'Journal', journalEntry);
	} else {
		// Create Journal Entry
		journal = nlapiCreateRecord('journalentry');
		journal.setFieldValue('custbody_eym_downpayment_created_from', billId);
	    journal.setFieldValue('trandate', date);
	    journal.setFieldValue('custbody_eym_downpayment_po_num', poNum);
	    
	    for (var i = 1; i <= 2; i++) {
	    	if (i == 1) {
	    		// Add account of the bill
	    		journal.setLineItemValue('line', 'account', i, account);
	    	    journal.setLineItemValue('line', 'debit', i, appliedDp);
	    	} else {
	    		// Add advances to supplier
	    		journal.setLineItemValue('line', 'account', i, 356);
	    	    journal.setLineItemValue('line', 'credit', i, appliedDp);
	    	}
	    	journal.setLineItemValue('line', 'entity', i, vendor);
	 	    journal.setLineItemValue('line', 'custcol_eym_downpayment_po', i, poNum);
	    }
	    var journalEntry = nlapiSubmitRecord(journal, true, true);
	    nlapiLogExecution('DEBUG', 'Journal', journalEntry);
	}
	
	return journalEntry;
}

function createBillPayment(billId, billType, poNum, journalId) 
{
	var paymentRecord = nlapiTransformRecord(billType, billId, 'vendorpayment', {recordmode: 'dynamic'});
	var applyCount = paymentRecord.getLineItemCount('apply');
	paymentRecord.setFieldValue('custbody_eym_downpayment_po_num', poNum);
	
	var journalAmount = getJournalAmount(journalId);
	
	if (applyCount > 0) {
		for(var i = 1; i <= applyCount; i++) {
			var internalId = paymentRecord.getLineItemValue('apply', 'internalid', i);
			var applyType = paymentRecord.getLineItemValue('apply', 'type', i);
			
			paymentRecord.selectLineItem("apply", i);
			if(internalId == billId && applyType == 'Bill') {
				paymentRecord.setCurrentLineItemValue("apply", "amount", journalAmount);
			}
			
			if (applyType == 'Journal') {
				var applyCreatedFrom = nlapiLookupField('journalentry', internalId, 'custbody_eym_downpayment_created_from');
				
				if (applyCreatedFrom == billId) {
					paymentRecord.setCurrentLineItemValue("apply", "apply", 'T'); 
				}
			}
			paymentRecord.commitLineItem('apply');
		}
		
	}
	
	var vendorPayment = nlapiSubmitRecord(paymentRecord, true, true);
	nlapiLogExecution('DEBUG', 'Bill Payment', vendorPayment);
}

function getJournalAmount(journalId) 
{
	var journal = nlapiLoadRecord('journalentry', journalId);
	var journalCount = journal.getLineItemCount('line');
	
	if (journalCount > 0) {
		for (var i = 1; i < journalCount; i++) {
			var accountValue = journal.getLineItemValue('line', 'account', i);
			var amount;
			
			if (accountValue == 356) {
				amount = journal.getLineItemValue('line', 'credit', i);
			} else {
				amount = journal.getLineItemValue('line', 'debit', i);
			}
		}
	}
	
	return amount;
}
