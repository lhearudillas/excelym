/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       10 Mar 2018     Lhea
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType 
 * 
 * @param {String} type Access mode: create, copy, edit
 * @returns {Void}
 */
function clientPageInit(type)
{
	if (type == 'edit') {
		var poId = nlapiGetFieldValue('custbody_eym_downpayment_po_num');
		
		if (poId) {
			var dpAmount = populateDownPayment(poId);
			nlapiSetFieldValue('custbody_eym_dp_balance', dpAmount);
		}
	}
}

function populateDownPayment(poId) 
{
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custcol_eym_downpayment_po', null, 'is', poId));
	
	var searchCheck = nlapiSearchRecord(null, 'customsearch_eym_down_payment_balance', filters);
	
	if (searchCheck) {
		for (var i = 0; i < searchCheck.length; i++) {
			var checkAmount = Number(searchCheck[i].getValue('amount', null, 'SUM'));
			
			if (checkAmount) {
				var dpAmount = checkAmount.toFixed(2);
			}
		}
	}
	
	return dpAmount;
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @returns {Boolean} True to continue save, false to abort save
 */
function clientSaveRecord(){
	var dpBalance = nlapiGetFieldValue('custbody_eym_dp_balance');
	var appliedDP = nlapiGetFieldValue('custbody_eym_downpayment_applied');
	var amount = nlapiGetFieldValue('usertotal');
	
	if (Number(appliedDP) > Number(dpBalance)) {
		alert("ERROR: Applied downpayment is greater than downpayment balance.");
		return false;
	}
	
	if (Number(appliedDP) > Number(amount)) {
		alert("ERROR: Applied downpayment is greater than bill amount.");
		return false;
	}
	
    return true;
}