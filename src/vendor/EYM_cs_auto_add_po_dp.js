/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       21 Jun 2018     Lhea
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @returns {Boolean} True to continue save, false to abort save
 */
function autoPOSaveRecord()
{
	var poNum = nlapiGetFieldValue('custbody_eym_downpayment_po_num');
	var expenseCount = nlapiGetLineItemCount('expense');
		
	if (expenseCount > 0) {
		for (var i = 1; i <= expenseCount; i++) {
			var account = nlapiGetLineItemValue('expense', 'account', i);
			var poNumLine = nlapiGetLineItemValue('expense', 'custcol_eym_downpayment_po', i);
			
			if (account == 356 && poNumLine != poNum) {
				alert('ERROR: The PO No on the line items does not match the Downpayment for PO No.');
				return false;
			}
		}
	}
	
	return true;
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Sublist internal id
 * @param {String} name Field internal id
 * @param {Number} linenum Optional line item number, starts from 1
 * @returns {Void}
 */
function autoPOFieldChanged(type, name, linenum)
{
	var poNum = nlapiGetFieldValue('custbody_eym_downpayment_po_num');
	var expenseCount = nlapiGetLineItemCount('expense');
	var account;
	
	if (name == 'custbody_eym_downpayment_po_num') {
		if (expenseCount > 0) {
			for (linenum = 1; linenum <= expenseCount; linenum++) {
				account = nlapiGetLineItemValue('expense', 'account', linenum);
				
				if (account == 356) {
					nlapiSelectLineItem('expense', linenum);
					nlapiSetCurrentLineItemValue('expense', 'custcol_eym_downpayment_po', poNum);
					nlapiCommitLineItem('expense');
				}
			}
		}
	}
	
	if (name == 'account') {
		account = nlapiGetCurrentLineItemValue('expense', 'account');
		
		if (account == 356) {
			nlapiSetCurrentLineItemValue('expense', 'custcol_eym_downpayment_po', poNum);
		}
	}
}
