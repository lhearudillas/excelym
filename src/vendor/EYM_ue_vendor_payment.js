/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       20 Mar 2018     Lhea
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request)
{
	try {
		if (type == 'create') {
			var stringVal = nlapiGetFieldValue('entryformquerystring');
			
			if(stringVal) {
				var url = stringVal.split('&');
				var billId = '';
				
				for(var i = 0; i < url.length; i++ ) {
					var p = url[i].split('=');
					if(p[0] == 'bill') {
						billId = p[1];
						if(billId) {
							var poNum = nlapiLookupField('vendorbill', billId, 'custbody_eym_downpayment_po_num');
							if(poNum) {
								nlapiSetFieldValue('custbody_eym_downpayment_po_num', poNum);
							}
						}
					}
				}
			}
		}
	} catch(ex) {
		nlapiLogExecution('DEBUG', 'EXCEPTION', ex.message == '' ? ex : ex.message);
	}
}
