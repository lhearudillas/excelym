/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       30 Jan 2018     Roy Selim
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function createPdf(request, response){

    // Get the id of the purchase order.
    var purchaseOrderId = request.getParameter( 'id' );

    // Check if there is a valid record id from the URL.
    if( purchaseOrderId != null ) {

        // Load the purchase order record.
        var purchaseOrderRecord = nlapiLoadRecord('purchaseorder', purchaseOrderId);

        // Get Purchase Order details.
        var billAddressee = purchaseOrderRecord.getFieldValue('billaddressee');
        var billAddress = purchaseOrderRecord.getFieldValue('billaddress');
        var transactionId = purchaseOrderRecord.getFieldValue('tranid');
        var transactionDate = purchaseOrderRecord.getFieldValue('trandate');
        var mrfNumber = purchaseOrderRecord.getFieldValue('custbody_eym_pr_mrf');
        var projectCode = purchaseOrderRecord.getFieldValue('custbody_eym_pr_projcode');
        var lineItems = purchaseOrderRecord.getLineItemCount('item');

        // Get the vendor/entity id.
      	var vendorId = purchaseOrderRecord.getFieldValue('entity');
        var vendorRecord, terms, suppliersVat = null;
        if(vendorId != null) {
            // Load a vendor record.
            vendorRecord = nlapiLoadRecord('vendor', vendorId);
        }
        if(vendorRecord != null) {
            terms = vendorRecord.getFieldText('terms');
            suppliersVat = vendorRecord.getFieldValue('vatregnumber');
        }

        // Get the total pages to be printed.
        var linesPerPage = 28;
        var pages = getNumberOfPage(lineItems, linesPerPage);

        // Top table format.
        var topDetails = '';
        topDetails +=     '<div id="top_table">';
	    topDetails +=       '<table>';
        topDetails +=         '<tr position="relative" top="7">';
        topDetails +=           '<td class="row_cell1 next_row_height">';
        topDetails +=           '</td>';
        topDetails +=           '<td class="row_cell2">';
        topDetails +=           '</td>';
        topDetails +=           '<td class="row_cell3">';
        topDetails +=           '</td>';
        topDetails +=           '<td class="row_cell4">';
        topDetails +=           '</td>';
        topDetails +=           '<td class="row_cell5">';
        topDetails +=           '</td>';
        topDetails +=           '<td class="row_cell6">';
        topDetails +=             pages;
        topDetails +=           '</td>';
        topDetails +=         '</tr>';
        topDetails +=         '<tr>';
        topDetails +=           '<td class="top_row_cells">';
        topDetails +=             '<span>' + (suppliersVat != null) ? suppliersVat: '' + '</span>';
        topDetails +=           '</td>';
        topDetails +=           '<td colspan="2" class="top_row_cells">';
        topDetails +=             '<span></span>';
        topDetails +=           '</td>';
        topDetails +=           '<td colspan="3" class="top_row_cells" style="padding-left: 120px; ">';
        topDetails +=             '<span font-size="15">PO'+ transactionId +'</span>';
        topDetails +=           '</td>';
        topDetails +=         '</tr>';
        topDetails +=         '<tr>';
        topDetails +=           '<td colspan="2" rowspan="4" style="padding-top: 15px; padding-left: 7px;">';
        topDetails +=             '<span font-size="12">'+ (billAddressee != null) ? billAddressee.toUpperCase(): '' +'</span>';
        topDetails +=           '</td>';
        topDetails +=           '<td colspan="2" rowspan="3" style="padding-top: 15px; padding-left: 7px;">';
        topDetails +=             '<span font-size="10">'+ (billAddress != null) ? billAddress: '' +'</span>';
        topDetails +=           '</td>';
        topDetails +=           '<td class="next_row_height last_cells" colspan="2">';
        topDetails +=             '<span>'+ (transactionDate != null) ? transactionDate: '' +'</span>';
        topDetails +=           '</td>';
        topDetails +=         '</tr>';
        topDetails +=         '<tr>';
        topDetails +=           '<td class="next_row_height last_cells" colspan="2">';
        topDetails +=               '<span font-size="15">'+ (mrfNumber != null) ? mrfNumber: '' +'</span>';
        topDetails +=           '</td>';
        topDetails +=         '</tr>';
        topDetails +=         '<tr>';
        topDetails +=           '<td class="next_row_height last_cells" colspan="2">';
        topDetails +=             '<span>5</span>';
        topDetails +=           '</td>';
        topDetails +=         '</tr>';
        topDetails +=         '<tr>';
        topDetails +=           '<td  colspan="2" class="last_cells">';
        topDetails +=             '<span></span>';
        topDetails +=           '</td>';
        topDetails +=           '<td class="next_row_height last_cells" colspan="2">';
        topDetails +=             '<span>'+ (terms != null) ? terms: '' +'</span>';
        topDetails +=           '</td>';
        topDetails +=         '</tr>';
      	topDetails +=       '</table>';
        topDetails +=     '</div>';

        // Get data for amount table.
        var totalAmount = purchaseOrderRecord.getFieldValue('total');
        var totalAmountWhole = splitNumber(totalAmount, 0);
        var totalAmountArray = totalAmount.toString().split('.');
        var totalAmountDecimal = totalAmountArray[1] ? totalAmountArray[1] : '0';
        totalAmountDecimal = Number(totalAmountDecimal);
        totalAmountDecimal = totalAmountDecimal ? ' and 1/' + totalAmountDecimal.toString(): '';
        var totalAmountWords = numberToEnglish(totalAmountWhole);
        var currencySymbol = purchaseOrderRecord.getFieldValue('currencysymbol');

        // Generate the amount in words.
        var amountInWordsData = '';
        amountInWordsData += '<tr>';
        amountInWordsData +=   '<td width="32%">';
        amountInWordsData +=     'Amount in words: ';
        amountInWordsData +=   '</td>';
        amountInWordsData +=   '<td width="50%">';
        amountInWordsData +=     totalAmountWords + totalAmountDecimal + ' ' + currencySymbol +' Only'
        amountInWordsData +=   '</td>';
        amountInWordsData +=   '<td align="right" width="18%">';
        amountInWordsData +=     'TOTAL ' + currencySymbol;
        amountInWordsData +=   '</td>';
        amountInWordsData +=   '<td  width="25%" style="font-size: 14px; padding-right: 10px; border-top: 2px; border-style: double; border-color: black; font-weight: bold;" align="right">';
        amountInWordsData +=     totalAmount;
        amountInWordsData +=   '</td>';
        amountInWordsData += '</tr>';

        // Data for the bottom table
        var orderedBy = purchaseOrderRecord.getFieldText('employee');
        var approvalsItem = purchaseOrderRecord.getLineItemCount('approvals');
        var approvedBy = '';
      	if(approvalsItem > 0) {
            approvedBy = purchaseOrderRecord.getLineItemValue('approvals', 'approvalentity', 1);
        }
        var bottomTableData = '';
        bottomTableData += '<tr>';
        bottomTableData +=   '<td width="21%" align="center">';
        bottomTableData +=     'Testing';
        bottomTableData +=   '</td>';
        bottomTableData +=   '<td width="26%" align="center">';
        bottomTableData +=     orderedBy ? orderedBy: '';
        bottomTableData +=   '</td>';
        bottomTableData +=   '<td width="26%" align="center">';
        bottomTableData +=     'Testing';
        bottomTableData +=   '</td>';
        bottomTableData +=   '<td width="27%" align="center">';
        bottomTableData +=     approvedBy;
        bottomTableData +=   '</td>';
        bottomTableData += '</tr>';

        // Create an XML variable that holds the XML string.
  	    var xml = '';

	    // Append the string with XML data.
	  	xml += '<?xml version="1.0"?>';
	    xml += '<!DOCTYPE pdf PUBLIC "-//big.faceless.org//report" "report-1.1.dtd">';
	    xml += '<pdf>';
	    xml +=   '<head>';
	    xml +=     '<style>';
	    xml +=       'body { font-family: Arial, "Helvetica Neue", Helvetica, sans-serif }';
        xml +=       'table {border-collapse: collapse; width: 107%; left: -22px; table-layout: fixed;}';
        xml +=       'td {border: 0px solid black}';
        xml +=       '#top_table { padding-top: 52px }';
        xml +=       '.middle_table { padding-top: 10px; font-size: 10px; }';
        xml +=       '#amount_in_words_table { padding-top: 10px; font-size: 10px; }';
        xml +=       '.top_row { height: 50px }';
        xml +=       '.top_row_cells { padding-top: 15px; padding-left: 10px; padding-bottom: 12px;}';
        xml +=       '.next_row_height { height: 31px; }';
        xml +=       '.last_cells { padding-top: 9px; padding-left: 35px; }';

        // Top table column formatting.
        xml +=       '.row_cell1 { width: 32%; }';
        xml +=       '.row_cell2 { width: 7%; }';
        xml +=       '.row_cell3 { width: 29%; }';
        xml +=       '.row_cell4 { width: 11%; }';
        xml +=       '.row_cell5 { width: 14%; }';
        xml +=       '.row_cell6 { width: 7%; padding-left: 20px;}';

        // Middle table column formatting.
        xml +=       '.mid_row_cell1 { width: 8.5%; }';
        xml +=       '.mid_row_cell2 { width: 4.5%; }';
        xml +=       '.mid_row_cell3 { width: 13%; }';
        xml +=       '.mid_row_cell4 { width: 44%; }';
        xml +=       '.mid_row_cell5 { width: 10%; text-align: center;}';
        xml +=       '.mid_row_cell6 { width: 4%; }';
        xml +=       '.mid_row_cell7 { width: 12%; text-align: center;}';
        xml +=       '.mid_row_cell8 { width: 4%; }';

	    xml +=     '</style>';
	    xml +=     '<macrolist>';
        xml +=       '<macro id="header_details">';
        xml +=         topDetails;
        xml +=       '</macro>';
        xml +=       '<macro id="footer_details">';
        xml +=         '<table position="relative" top="20">'+ bottomTableData +'</table>';
        xml +=       '</macro>';
	    xml +=     '</macrolist>';
	    xml +=   '</head>';

        // Generate the pages
        for(var p = 1; p <= pages; p++) {
          var contentData = generateContent(p, linesPerPage, purchaseOrderRecord, lineItems);
          xml += '<body background-pdf="'+ nlapiEscapeXML("https://system.netsuite.com/core/media/media.nl?id=2237&c=4317943_SB1&h=330e673a93554a03cdc6&_xt=.pdf") +'" footer="footer_details" footer-height="20mm" header="header_details" header-height="74mm">';
          xml +=     '<p width="20" position="relative" top="-158" left="490">'+ p +'</p>';
          xml +=     '<div class="middle_table">';
      	  xml +=       '<table>';
          xml +=         contentData;
          xml +=       '</table>';
          xml +=     '</div>';

          // Add the total on the last page.
          if(p == pages) {
        	// Amount in words table format.
        	xml +=     '<div id="amount_in_words_table">';
      		xml +=       '<table>';
        	xml +=         amountInWordsData;
        	xml +=       '</table>';
        	xml +=     '</div>';
          }

          xml +=  '</body>';
        }

        // End the root element.
        xml += '</pdf>';

	    // Convert the XML string to PDF.
	    var file = nlapiXMLToPDF(xml);

	    // Set the response content type.
	    response.setContentType('PDF', 'Print.pdf ', 'inline');

	    // Output the PDF file.
	    response.write(file.getValue());

    } else {

		// Dump response.
		dumpResponse(request,response);
	}
}

function splitNumber(num, index) {
   if(typeof(num) == 'number') {
     num = num.toString();
   }

   if(typeof(num) == 'string') {
     var a = num.split('.');
     return (index == 0) ? a[0] : '.' + ((index == 1) ? (a[1] ? a[1]: '00'): null);
   } else {
     return null;
   }
}

function numberToEnglish( n ) {

    var string = n.toString(), units, tens, scales, start, end, chunks, chunksLen, chunk, ints, i, word, words, and = '';

    /* Is number zero? */
    if( parseInt( string ) === 0 ) {
        return 'zero';
    }

    /* Array of units as words */
    units = [ '', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen' ];

    /* Array of tens as words */
    tens = [ '', '', 'Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety' ];

    /* Array of scales as words */
    scales = [ '', 'Thousand', 'Million', 'Billion', 'Trillion', 'Quadrillion', 'quintillion', 'sextillion', 'septillion', 'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quatttuor-decillion', 'quindecillion', 'sexdecillion', 'septen-decillion', 'octodecillion', 'novemdecillion', 'vigintillion', 'centillion' ];

    /* Split user arguemnt into 3 digit chunks from right to left */
    start = string.length;
    chunks = [];
    while( start > 0 ) {
        end = start;
        chunks.push( string.slice( ( start = Math.max( 0, start - 3 ) ), end ) );
    }
    /* Check if function has enough scale words to be able to stringify the user argument */
    chunksLen = chunks.length;
    if( chunksLen > scales.length ) {
        return '';
    }

    /* Stringify each integer in each chunk */
    words = [];
    for( i = 0; i < chunksLen; i++ ) {

        chunk = parseInt( chunks[i] );

        if( chunk ) {

            /* Split chunk into array of individual integers */
            ints = chunks[i].split( '' ).reverse().map( parseFloat );

            /* If tens integer is 1, i.e. 10, then add 10 to units integer */
            if( ints[1] === 1 ) {
                ints[0] += 10;
            }

            /* Add scale word if chunk is not zero and array item exists */
            if( ( word = scales[i] ) ) {
                words.push( word );
            }

            /* Add unit word if array item exists */
            if( ( word = units[ ints[0] ] ) ) {
                words.push( word );
            }

            /* Add tens word if array item exists */
            if( ( word = tens[ ints[1] ] ) ) {
                words.push( word );
            }

            /* Add 'and' string after units or tens integer if: */
            if( ints[0] || ints[1] ) {

                /* Chunk has a hundreds integer or chunk is the first of multiple chunks */
                if( ints[2] || ! i && chunksLen ) {
                    words.push( and );
                }
            }
            /* Add hundreds word if array item exists */
            if( ( word = units[ ints[2] ] ) ) {
                words.push( word + ' Hundred' );
            }
        }
    }

    return words.reverse().join( ' ' );
}

function getNumberOfPage(lines, numLinesPerPage) {
  var bp = Math.floor(lines/numLinesPerPage);
  var mo = lines%numLinesPerPage;
  var ap = (mo > 0) ? 1: 0;
  return bp + ap;
}

function generateContent(pageNumber, linesPerPage, purchaseOrderRecord, lineItems) {

        var countStart = ((pageNumber - 1) * linesPerPage) + 1;
        var countEnd = pageNumber * linesPerPage;
        countEnd = (countEnd < lineItems) ? countEnd : lineItems;

        // Data for the middle table.
        var middleTableData = '';
        // Loop thru all the line items.
        for(var i = countStart; i < countEnd + 1; i++) {
            middleTableData += '<tr>';
            middleTableData +=   '<td class="mid_row_cell1">';
            middleTableData +=     purchaseOrderRecord.getLineItemValue('item', 'quantity', i);
            middleTableData +=   '</td>';
            middleTableData +=   '<td class="mid_row_cell2">';
            middleTableData +=     purchaseOrderRecord.getLineItemText('item', 'units', i);
            middleTableData +=   '</td>';
            middleTableData +=   '<td class="mid_row_cell3">';
            middleTableData +=     'Code';
            middleTableData +=   '</td>';
            middleTableData +=   '<td class="mid_row_cell4" overflow="hidden">';
            middleTableData +=     nlapiEscapeXML(purchaseOrderRecord.getLineItemValue('item', 'description', i));
            middleTableData +=   '</td>';

            // Get the rate value.
            var rateValue = purchaseOrderRecord.getLineItemValue('item', 'rate', i);
            var rateValueWhole, rateValueDecimal = 0;
            if(rateValue != null) {
                rateValueWhole = splitNumber(rateValue, 0);
                rateValueDecimal = splitNumber(rateValue, 1);
            }

            middleTableData +=   '<td class="mid_row_cell5" align="right">';
            middleTableData +=     rateValueWhole;
            middleTableData +=   '</td>';
            middleTableData +=   '<td class="mid_row_cell6">';
            middleTableData +=     rateValueDecimal;
            middleTableData +=   '</td>';

            // Get the gross amount.
            var grossValue = purchaseOrderRecord.getLineItemValue('item', 'grossamt', i);
            var grossValueWhole, grossValueDecimal = 0;
            if(rateValue != null) {
                grossValueWhole = splitNumber(grossValue, 0);
                grossValueDecimal = splitNumber(grossValue, 1);
            }

            middleTableData +=   '<td class="mid_row_cell7" align="right">';
            middleTableData +=     grossValueWhole;
            middleTableData +=   '</td>';
            middleTableData +=   '<td class="mid_row_cell8">';
            middleTableData +=     grossValueDecimal;
            middleTableData +=   '</td>';
            middleTableData += '</tr>';
        }

    return middleTableData;
}