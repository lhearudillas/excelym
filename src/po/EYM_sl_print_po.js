/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       06 Jun 2018     Lhea
 *
 */

// Field Constants
var IDS_PRINT_PO_ADDRESS = 'address';
var IDS_PRINT_PO_ADDRESS_JOIN = 'Address';
var IDS_PRINT_PO_ADD_NOTES = 'custbody_eym_ap_remarks';
var IDS_PRINT_PO_ALT_PHONE = 'altphone';
var IDS_PRINT_PO_APPROVED_BY = 'custbody_eym_ap_approved';
var IDS_PRINT_PO_BIC_CONTACT = 'custbody_eym_po_bic_contact';
var IDS_PRINT_PO_BILLING_IC = 'custbody_eym_ap_billing_ic';
var IDS_PRINT_PO_CHECKED_BY = 'custbody_eym_ap_checked';
var IDS_PRINT_PO_CHECKED_BY_2ND = 'custbody_eym_po_checked_by_2';
var IDS_PRINT_PO_COMPANY_NAME = 'companyname';
var IDS_PRINT_PO_CUSTOMFORM = 'customform';
var IDS_PRINT_PO_DEFAULT_ADDRESS = 'defaultaddress';
var IDS_PRINT_PO_DP_NOTES = 'custbody_eym_po_down_pay_note';
var IDS_PRINT_PO_DUE_DATE = 'duedate';
var IDS_PRINT_PO_ENTITY = 'entity';
var IDS_PRINT_PO_ENTITYID = 'entityid';
var IDS_PRINT_PO_FOREIGN_DISCOUNT = 'custbody_eym_po_forex_total_discs';
var IDS_PRINT_PO_MRF = 'custbody_eym_pr_mrf';
var IDS_PRINT_PO_NAME = 'name';
var IDS_PRINT_PO_ORDERED_BY = 'custbody_eym_ap_prepared';
var IDS_PRINT_PO_PROJCODE = 'custbody_eym_pr_projcode';
var IDS_PRINT_PO_RECEIVING_IC = 'custbody_eym_ap_receiving_ic';
var IDS_PRINT_PO_REMARKS = 'memo';
var IDS_PRINT_PO_RIC_CONTACT = 'custbody_eym_po_proj_ric_contact';
var IDS_PRINT_PO_SHIP_ADDRESS = 'shipaddress';
var IDS_PRINT_PO_TERMS = 'custbody_eym_po_vendor_terms';
var IDS_PRINT_PO_TOTAL = 'total';
var IDS_PRINT_PO_TOTAL_FOREIGN_AMOUNT = 'custbody_eym_total_foreign_amount';
var IDS_PRINT_PO_TOTAL_TAX = 'taxtotal';
var IDS_PRINT_PO_TRANDATE = 'trandate';
var IDS_PRINT_PO_TRANID = 'tranid';
var IDS_PRINT_PO_VAT_REG_NUM = 'vatregnum';
var IDS_PRINT_PO_VENDOR_ADDRESS = 'vendorAddress';
var IDS_PRINT_PO_VENDOR_BILLING = 'custbody_eym_po_vendor_billing';
var IDS_PRINT_PO_VENDOR_COMPLAINTS = 'custbody_eym_po_vendor_complaints';
var IDS_PRINT_PO_VENDOR_CONTACT = 'custbody_eym_po_vendor_contact';
var IDS_PRINT_PO_VENDOR_DELIVERY = 'custbody_eym_po_vendor_delivery';
var IDS_PRINT_PO_VENDOR_NAME = 'vendorName';
var IDS_PRINT_PO_VENDOR_PRODUCTION = 'custbody_eym_po_vendor_production';
var IDS_PRINT_PO_VENDOR_TECH = 'custbody_eym_po_vendor_tech';

// Line Item Constants
var IDS_PRINT_PO_BASE_AMOUNT = 'custcol_eym_pur_item_base_amount';
var IDS_PRINT_PO_CURRENCY = 'custbody_eym_po_currency';
var IDS_PRINT_PO_DISCOUNT = 'custcol_eym_pur_itemdisc_amount';
var IDS_PRINT_PO_FOREIGN_AMOUNT = 'custcol_eym_foreign_amount';
var IDS_PRINT_PO_FOREIGN_RATE = 'custcol_eym_foreign_rate';
var IDS_PRINT_PO_GROSS_AMOUNT = 'grossamt';
var IDS_PRINT_PO_GROSS_PRICE = 'custcol_eym_po_gross_price';
var IDS_PRINT_PO_ITEM = 'item';
var IDS_PRINT_PO_ITEMID = 'itemid';
var IDS_PRINT_PO_ITEM_AMOUNT = 'amount';
var IDS_PRINT_PO_ITEM_CODE = 'upccode';
var IDS_PRINT_PO_ITEM_DESCRIPTION = 'description';
var IDS_PRINT_PO_ITEM_LINENUM = 'lineNum';
var IDS_PRINT_PO_ITEM_NAME = 'itemName';
var IDS_PRINT_PO_ITEM_PRICE = 'price';
var IDS_PRINT_PO_ITEM_QUANTITY = 'quantity';
var IDS_PRINT_PO_ITEM_SYMBOL = 'symbol';
var IDS_PRINT_PO_ITEM_UNITS = 'units';
var IDS_PRINT_PO_PHP = 'PHP';

// Record Constants
var IDS_PRINT_PO_CURRENCY_RECORD = 'currency';
var IDS_PRINT_PO_DESCRIPTION_ITEM = 'descriptionitem';
var IDS_PRINT_PO_EMPLOYEE_RECORD = 'employee';
var IDS_PRINT_PO_JOB = 'job';
var IDS_PRINT_PO_MAT_REQ = 'customrecord_eym_material_req';
var IDS_PRINT_PO_TERM_RECORD = 'term';
var IDS_PRINT_PO_VENDOR_DETAILS_SS = 'customsearch_eym_vendor_details_po_print';

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function previewPO(request, response)
{
	var param = {
			'recordType' : request.getParameter('type'),
			'recordId' : request.getParameter('id')
		};
	
	try {
		showPDF(response, param);
	} catch (ex) {
		var msg = ex.message + ' - ' + response.stack;
		
		nlapiLogExecution('ERROR', 'EXCEPTION', msg)
		response.write('PARAM : ' + JSON.stringify(param) +  '<br /><br />' + 'ERROR : ' + msg);
	}	
}

function showPDF(response, param) 
{
	var recordType = param['recordType'];
	var recordId = param['recordId'];
	
	var fileId = nlapiGetContext().getSetting('SCRIPT', 'custscript_print_purchase_order');
	var htmlFile = nlapiLoadFile(fileId);
	var html = htmlFile.getValue();
	
	var itemConcat = '';
	var notesAndInstructionsConcat = '';
	var totalAmtDisConcat = '';
	var amtInWordsNetTaxConcat = '';
	var remarksConcat = '';
	var checkedByConcat = '';
	var bodyContent = '';
	var itemTotalGross = null;
	var itemTotalDiscount = null;
	var projectCode, projectName, itemName, itemCode;;
	
	var loadPORecord = nlapiLoadRecord(recordType, recordId);
	var vendorId = loadPORecord.getFieldValue(IDS_PRINT_PO_ENTITY);
	var mrfId = loadPORecord.getFieldValue(IDS_PRINT_PO_MRF);
	var terms = getTermValue(loadPORecord.getFieldValue(IDS_PRINT_PO_TERMS));
	
	var vendorDetails = getVendorDetails(vendorId);
	var receivingInCharge = getEmployee(loadPORecord.getFieldValue(IDS_PRINT_PO_RECEIVING_IC));
	var receivingContactNum = loadPORecord.getFieldValue(IDS_PRINT_PO_RIC_CONTACT);
	var receivingConcat = receivingBillingConcat(receivingInCharge, receivingContactNum);
	var billingInCharge = getEmployee(loadPORecord.getFieldValue(IDS_PRINT_PO_BILLING_IC));
	var billingContactNum = loadPORecord.getFieldValue(IDS_PRINT_PO_BIC_CONTACT);
	var billingConcat = receivingBillingConcat(billingInCharge, billingContactNum);
	
	var orderedBy = getEmployee(loadPORecord.getFieldValue(IDS_PRINT_PO_ORDERED_BY));
	var checkedBy = getEmployee(loadPORecord.getFieldValue(IDS_PRINT_PO_CHECKED_BY));
	var checkedBy2nd = getEmployee(loadPORecord.getFieldValue(IDS_PRINT_PO_CHECKED_BY_2ND));
	var approvedBy = getEmployee(loadPORecord.getFieldValue(IDS_PRINT_PO_APPROVED_BY));
	
	var custForm = loadPORecord.getFieldValue(IDS_PRINT_PO_CUSTOMFORM);
	var forexFormId = getForexFormId();
	var itemCount = loadPORecord.getLineItemCount(IDS_PRINT_PO_ITEM);
	var totalAmount = loadPORecord.getFieldValue(IDS_PRINT_PO_TOTAL);
	var totalTax = loadPORecord.getFieldValue(IDS_PRINT_PO_TOTAL_TAX);
	var foreignTotalAmount = loadPORecord.getFieldValue(IDS_PRINT_PO_TOTAL_FOREIGN_AMOUNT);
	var remarks = loadPORecord.getFieldValue(IDS_PRINT_PO_REMARKS);
	var additionalNotes = loadPORecord.getFieldValue(IDS_PRINT_PO_ADD_NOTES);
	var dpInstructions = loadPORecord.getFieldValue(IDS_PRINT_PO_DP_NOTES);
	var projectCodeId = loadPORecord.getFieldValue(IDS_PRINT_PO_PROJCODE);
	
	if (projectCodeId) {
		projectCode = getProjectCode(nlapiLookupField(IDS_PRINT_PO_JOB, projectCodeId, IDS_PRINT_PO_ENTITYID));
		projectName = nlapiLookupField(IDS_PRINT_PO_JOB, projectCodeId, IDS_PRINT_PO_COMPANY_NAME);
	} else {
		projectCode = '';
		projectName = '';
	}
	
	notesAndInstructionsConcat = concatenateNotesAndInstructions(nlapiEscapeXML(additionalNotes), nlapiEscapeXML(dpInstructions));
	remarksConcat = concatenateRemarks(nlapiEscapeXML(remarks));
	checkedByConcat = concatenateCheckedBy(checkedBy, checkedBy2nd);
	
	bodyContent += '<body width="8.5in" height="11in" header="poHeader" header-height="86mm" footer="poFooter" footer-height="30mm">';
	bodyContent += '<table style="font-size: 11px; padding-top: 25px;">';
	
	if (itemCount != null && itemCount > 0) {
		for (var i = 1; i <= itemCount; i++) {
			var itemId = loadPORecord.getLineItemValue(IDS_PRINT_PO_ITEM, IDS_PRINT_PO_ITEM, i);
			var itemQuantity = loadPORecord.getLineItemValue(IDS_PRINT_PO_ITEM, IDS_PRINT_PO_ITEM_QUANTITY, i);
			var itemUnit = loadPORecord.getLineItemText(IDS_PRINT_PO_ITEM, IDS_PRINT_PO_ITEM_UNITS, i);
			var itemDescription = loadPORecord.getLineItemValue(IDS_PRINT_PO_ITEM, IDS_PRINT_PO_ITEM_DESCRIPTION, i);
			
			if (itemId) {
				var itemType = nlapiLookupField(IDS_PRINT_PO_ITEM, itemId, 'recordtype');
				
				if (!isNullOrEmpty(itemType)) {
					itemName = nlapiLookupField(itemType, itemId, IDS_PRINT_PO_ITEMID);
					itemCode = nlapiLookupField(itemType, itemId, IDS_PRINT_PO_ITEM_CODE);
				} else {
					itemName = '';
					itemCode = '';
				}
			} else {
				itemName = '';
				itemCode = '';
			}
			
			if (custForm == 101) {
				// Local
				var itemPrice = loadPORecord.getLineItemValue(IDS_PRINT_PO_ITEM, IDS_PRINT_PO_GROSS_PRICE, i);
				var itemAmount = loadPORecord.getLineItemValue(IDS_PRINT_PO_ITEM, IDS_PRINT_PO_GROSS_AMOUNT, i);
				var itemGross = Number(loadPORecord.getLineItemValue(IDS_PRINT_PO_ITEM, IDS_PRINT_PO_BASE_AMOUNT, i));
				var itemDiscount = Number(loadPORecord.getLineItemValue(IDS_PRINT_PO_ITEM, IDS_PRINT_PO_DISCOUNT, i));
				var currencySymbol = IDS_PRINT_PO_PHP;
				
				itemTotalGross += itemGross;
				itemTotalDiscount += itemDiscount;
			} else if (custForm == forexFormId) {
				// Foreign
				var itemPrice = loadPORecord.getLineItemValue(IDS_PRINT_PO_ITEM, IDS_PRINT_PO_FOREIGN_RATE, i);
				var itemAmount = loadPORecord.getLineItemValue(IDS_PRINT_PO_ITEM, IDS_PRINT_PO_FOREIGN_AMOUNT, i);
				var currencySymbol = getForeignCurrencySymbol(loadPORecord.getFieldValue(IDS_PRINT_PO_CURRENCY));
			} else {
				// Sub Con
				var itemPrice = loadPORecord.getLineItemValue(IDS_PRINT_PO_ITEM, IDS_PRINT_PO_GROSS_PRICE, i);
				var itemAmount = loadPORecord.getLineItemValue(IDS_PRINT_PO_ITEM, IDS_PRINT_PO_GROSS_AMOUNT, i);
				var currencySymbol = IDS_PRINT_PO_PHP;
			}
			
			var itemParams = {
					'lineNum' : i,
					'itemName' : itemName,
					'quantity' : itemQuantity,
					'units' : itemUnit,
					'code' : itemCode,
					'description' : itemDescription,
					'symbol' : currencySymbol,
					'price' : itemPrice,
					'amount' : itemAmount
			};
			itemConcat += concatenateItem(itemParams);
		}
	}
	
	if (custForm == 101) {
		// Local
		totalAmtDisConcat = concatTotalAmtDis(itemTotalGross, itemTotalDiscount, IDS_PRINT_PO_PHP, 'Local');
		amtInWordsNetTaxConcat = concatenateWordsNetAndTax(totalAmount, totalTax, IDS_PRINT_PO_PHP, 'Local');
	} else if (custForm == forexFormId) {
		// Foreign
		var foreignCurrencySymbol = getForeignCurrencySymbol(loadPORecord.getFieldValue(IDS_PRINT_PO_CURRENCY));
		var foreignCurrencyDiscount = loadPORecord.getFieldValue(IDS_PRINT_PO_FOREIGN_DISCOUNT);
		
		totalAmtDisConcat = concatTotalAmtDis(foreignTotalAmount, foreignCurrencyDiscount, foreignCurrencySymbol, 'Foreign');
	} else {
		// Sub Con
		totalAmtDisConcat = concatTotalAmtDis(totalAmount, totalTax, IDS_PRINT_PO_PHP, 'Sub Con');
	}
	
	bodyContent += itemConcat;
	bodyContent += notesAndInstructionsConcat;
	bodyContent += appendUnderline();
	bodyContent += totalAmtDisConcat;
	bodyContent += '</table>';
	bodyContent += '<br />';
	
	if (amtInWordsNetTaxConcat) {
		bodyContent += '<table style="font-size: 11px;">';
		bodyContent += amtInWordsNetTaxConcat;
		bodyContent += '</table>';
	}
	
	bodyContent += '<table style="font-size: 11px;">';
	bodyContent += remarksConcat;
	bodyContent += '</table>';
	bodyContent += '</body>';
	
	if (mrfId) {
		html = html.replace('{mrf_num}', replaceNull(nlapiLookupField(IDS_PRINT_PO_MAT_REQ, mrfId, IDS_PRINT_PO_NAME)));
	} else {
		html = html.replace('{mrf_num}', '');
	}
	
	html = html.replace('{body}', bodyContent);
	html = html.replace('{tax_id}', replaceNull(loadPORecord.getFieldValue(IDS_PRINT_PO_VAT_REG_NUM)));
	html = html.replace('{vendor_name}', replaceNull(vendorDetails[IDS_PRINT_PO_VENDOR_NAME]).toUpperCase());
	html = html.replace('{vendor_address}', replaceNull(vendorDetails[IDS_PRINT_PO_VENDOR_ADDRESS]));
	html = html.replace('{vendor_contact}', replaceNull(loadPORecord.getFieldValue(IDS_PRINT_PO_VENDOR_CONTACT)));
	html = html.replace('{vendor_billing}', replaceNull(loadPORecord.getFieldValue(IDS_PRINT_PO_VENDOR_BILLING)));
	html = html.replace('{vendor_tech}', replaceNull(loadPORecord.getFieldValue(IDS_PRINT_PO_VENDOR_TECH)));
	html = html.replace('{vendor_delivery}', replaceNull(loadPORecord.getFieldValue(IDS_PRINT_PO_VENDOR_DELIVERY)));
	html = html.replace('{vendor_complaints}', replaceNull(loadPORecord.getFieldValue(IDS_PRINT_PO_VENDOR_COMPLAINTS)));
	html = html.replace('{vendor_production}', replaceNull(loadPORecord.getFieldValue(IDS_PRINT_PO_VENDOR_PRODUCTION)));
	
	html = html.replace('{ship_to_address}', replaceNull(loadPORecord.getFieldValue(IDS_PRINT_PO_SHIP_ADDRESS)).toUpperCase());
	html = html.replace('{receiving_in_charge}', replaceNull(receivingConcat));
	html = html.replace('{billing_in_charge}', replaceNull(billingConcat));
	
	html = html.replace('{po_num}', 'PO' + replaceNull(loadPORecord.getFieldValue(IDS_PRINT_PO_TRANID)));
	html = html.replace('{date_created}', replaceNull(loadPORecord.getFieldValue(IDS_PRINT_PO_TRANDATE)));
	html = html.replace('{delivery_date}', replaceNull(loadPORecord.getFieldValue(IDS_PRINT_PO_DUE_DATE)));
	
	html = html.replace('{project_code}', replaceNull(projectCode));
	html = html.replace('{terms}', replaceNull(terms));
	html = html.replace('{project_name}', replaceNull(projectName.toUpperCase()));
	
	html = html.replace('{current_date}', getCurrentDate());
	html = html.replace('{ordered_by}', replaceNull(orderedBy));
	html = html.replace('{checked_by}', checkedByConcat);
	html = html.replace('{approved_by}', approvedBy);
	
	var pdfFile = nlapiXMLToPDF(html);
	var fileName = 'PO' + loadPORecord.getFieldValue(IDS_PRINT_PO_TRANID) + '.pdf';
	response.setContentType('PDF', fileName, 'inline');
	response.write(pdfFile.getValue());
}

function getProjectCode(code)
{
	var name;
	
	if (code) {
		code = code.substr(code.lastIndexOf(":") + 1);
		name = code.trim();
	} else {
		name = '';
	}
	
	return name;
}

function getTermValue(termId) 
{
	var termName;
	
	if (termId) {
		var termResult = nlapiSearchRecord(IDS_PRINT_PO_TERM_RECORD, null, 
				[['internalid', 'is', termId]], 
				[new nlobjSearchColumn(IDS_PRINT_PO_NAME)]);
		
		if (termResult) {
			for (var i = 0; i < 1; i++) {
				termName = termResult[i].getValue(IDS_PRINT_PO_NAME);
			}
		}
	} else {
		termName = '';
	}
	
	return termName;
}

function getForeignCurrencySymbol(currencyId)
{
	var symbol;
	
	if (currencyId) {
		var currencyResult = nlapiSearchRecord(IDS_PRINT_PO_CURRENCY_RECORD, null, 
				[['internalid', 'is', currencyId]], 
				[new nlobjSearchColumn(IDS_PRINT_PO_ITEM_SYMBOL)]);
		
		if (currencyResult) {
			for (var i = 0; i < 1; i++) {
				symbol = currencyResult[i].getValue(IDS_PRINT_PO_ITEM_SYMBOL);
			}
		}
	} else {
		symbol = '';
	}
	
	return symbol;
}

function getVendorDetails(vendorId) 
{
	if (vendorId) {
		var vendorName, vendorAddress, vendorParam;
		
		var vendorFilter = new Array();
		vendorFilter.push(new nlobjSearchFilter('internalid', null, 'is', vendorId));

		var vendorResult = nlapiSearchRecord(null, IDS_PRINT_PO_VENDOR_DETAILS_SS, vendorFilter);
		
		if (vendorResult != null && vendorResult.length > 0) {
			vendorName = vendorResult[0].getValue(IDS_PRINT_PO_COMPANY_NAME);	
			vendorAddress = vendorResult[0].getValue(IDS_PRINT_PO_ADDRESS, IDS_PRINT_PO_ADDRESS_JOIN);
		} else {
			var entityType = nlapiLookupField(IDS_PRINT_PO_ENTITY, vendorId, 'recordtype');
			var loadEntityRecord = nlapiLoadRecord(entityType, vendorId);
			
			vendorName = loadEntityRecord.getFieldValue(IDS_PRINT_PO_COMPANY_NAME);	
			vendorAddress = loadEntityRecord.getFieldValue(IDS_PRINT_PO_DEFAULT_ADDRESS);
		}
		
		if (!isNullOrEmpty(vendorAddress) && vendorAddress.indexOf(vendorName) != -1) {
			vendorAddress = vendorAddress.replace(vendorName, "");
			vendorAddress = vendorAddress.trim();
		}
		
		vendorParam = {
				'vendorName' : vendorName,
				'vendorAddress' : vendorAddress
			};
	} else {
		vendorParam = {
				'vendorName' : '',
				'vendorAddress' : ''
			};
	}
	
	return vendorParam;
}

function getEmployee(employeeId) 
{
	var fullName;
	
	if (employeeId) {
		fullName = nlapiLookupField(IDS_PRINT_PO_EMPLOYEE_RECORD, employeeId, IDS_PRINT_PO_ENTITYID); 
	} else {
		fullName = '';
	}
		
	return fullName;
	
}

function receivingBillingConcat(name, number) 
{
	var employeeParam;
	
	if (name) {
		if (number) {
			employeeParam = name + ' - ' + number;
		} else {
			employeeParam = name;
		}
	} else {
		employeeParam = '';
	}

	return employeeParam;
}

function concatenateItem(itemParams) 
{
	var lineNum = itemParams[IDS_PRINT_PO_ITEM_LINENUM];
	var name = itemParams[IDS_PRINT_PO_ITEM_NAME];
	var quantity = itemParams[IDS_PRINT_PO_ITEM_QUANTITY] ? toCurrency(itemParams[IDS_PRINT_PO_ITEM_QUANTITY]) : '';
	var unit = itemParams[IDS_PRINT_PO_ITEM_UNITS] ? itemParams[IDS_PRINT_PO_ITEM_UNITS] : '';
	var code = itemParams[IDS_PRINT_PO_ITEM_CODE] ? itemParams[IDS_PRINT_PO_ITEM_CODE] : '';
	var description = itemParams[IDS_PRINT_PO_ITEM_DESCRIPTION] ? itemParams[IDS_PRINT_PO_ITEM_DESCRIPTION] : '';
	var symbol = itemParams[IDS_PRINT_PO_ITEM_SYMBOL] ? itemParams[IDS_PRINT_PO_ITEM_SYMBOL] : '';
	var price = itemParams[IDS_PRINT_PO_ITEM_PRICE] ? toCurrency(itemParams[IDS_PRINT_PO_ITEM_PRICE]) : '';
	var amount = itemParams[IDS_PRINT_PO_ITEM_AMOUNT] ? toCurrency(itemParams[IDS_PRINT_PO_ITEM_AMOUNT]) : '';
	var row = '';
	
	if (description) {
		description = nlapiEscapeXML(description.toUpperCase());
		description = description.replace(/\n/g, "<br />");
	} else {
		description = nlapiEscapeXML(name.toUpperCase());
		description = description.replace(/\n/g, "<br />");
	}
	
	if (price && price == 0) {
		price = '';
	}
	
	if (amount && amount == 0) {
		amount = '';
	}
	
	if (lineNum == 1) {
		row += '<tr style="padding-top: 15px;"><td></td></tr>';
	}
	
	row += '<tr>';
	row += '<td valign="top" width="65">' + quantity + '</td>';
	row += '<td valign="top" width="55">' + unit.toUpperCase() + '</td>';
	row += '<td valign="top" width="100">' + code + '</td>';
	row += '<td valign="top" width="255" style="text-align: justify; padding-right: 15px;">' + description + '</td>';
	
	if (lineNum == 1) {
		row += '<td valign="top" width="40"><b>' + symbol + '</b></td>';
	} else {
		row += '<td></td>';
	}
	
	row += '<td align="right" valign="top" width="85" style="padding-right: 35px;">' + price + '</td>';
	row += '<td align="right" valign="top" width="90">' + amount + '</td>';
	row += '</tr>';
	
	return row;
}

function concatenateNotesAndInstructions(notes, instructions)
{
	var row = '';
	
	if (notes) {
		notes = notes.replace(/\n/g, "<br />");
		
		row += '<tr style="padding-top: 5px;">';
		row += '<td></td>';
		row += '<td></td>';
		row += '<td colspan="2" width="355" style="text-align: justify; padding-right: 15px;">' + notes + '</td>';
		row += '<td></td>';
		row += '<td></td>';
		row += '<td></td>';
		row += '</tr>';
	}
	
	if (instructions) {
		instructions = instructions.replace(/\n/g, "<br />");
		
		row += '<tr style="padding-top: 5px;">';
		row += '<td></td>';
		row += '<td></td>';
		row += '<td></td>';
		row += '<td width="255" style="text-align: justify; padding-right: 15px;">' + instructions + '</td>';
		row += '<td></td>';
		row += '<td></td>';
		row += '<td></td>';
		row += '</tr>';
	}
	
	return row;
}

function concatTotalAmtDis(totalGross, totalDiscount, symbol, type) 
{
	var row = '';
		
	if (symbol == IDS_PRINT_PO_PHP && type == 'Local') {
		// Local
		row += '<tr style="padding-top: 5px;">';
		row += '<td></td>';
		row += '<td></td>';
		row += '<td></td>';
		row += '<td></td>';
		row += '<td colspan="2" align="right"><b>TOTAL ' + symbol + '</b></td>';
		row += '<td align="right"><b>' + toCurrency(totalGross) + '</b></td>';
		row += '</tr>';
		
		if (!isNullOrEmpty(totalDiscount) && totalDiscount > 0) {
			row += '<tr>';
			row += '<td></td>';
			row += '<td></td>';
			row += '<td></td>';
			row += '<td></td>';
			row += '<td colspan="2" align="center"><b>LESS</b></td>';
			row += '<td></td>';
			row += '</tr>';
			
			row += '<tr>';
			row += '<td></td>';
			row += '<td></td>';
			row += '<td></td>';
			row += '<td></td>';
			row += '<td colspan="2" align="right"><b>Discount : </b></td>';
			row += '<td align="right"><b>' + toCurrency(totalDiscount) + '</b></td>';
			row += '</tr>';
		}
	} else if (symbol == IDS_PRINT_PO_PHP && type == 'Sub Con') {
		// Sub Con
		row += concatenateWordsNetAndTax(totalGross, null, symbol, 'Sub Con');
		
		row += '<tr>';
		row += '<td></td>';
		row += '<td></td>';
		row += '<td></td>';
		row += '<td colspan="3" align="right"><b>Tax (VI12 - 12.00% / .00%)</b></td>';
		row += '<td align="right"><b>' + toCurrency(totalDiscount) + '</b></td>';
		row += '</tr>';
	} else {
		// Foreign
		row += concatenateWordsNetAndTax(totalGross, null, symbol, 'Foreign');
		
		if (!isNullOrEmpty(totalDiscount) && totalDiscount > 0) {
			row += '<tr>';
			row += '<td></td>';
			row += '<td></td>';
			row += '<td></td>';
			row += '<td></td>';
			row += '<td colspan="2" align="center"><b>LESS</b></td>';
			row += '<td></td>';
			row += '</tr>';
			
			row += '<tr>';
			row += '<td></td>';
			row += '<td></td>';
			row += '<td></td>';
			row += '<td></td>';
			row += '<td colspan="2" align="right"><b>Discount : </b></td>';
			row += '<td align="right"><b>' + toCurrency(totalDiscount) + '</b></td>';
			row += '</tr>';
		}
	}
	
	return row;
}

function concatenateWordsNetAndTax(net, tax, symbol, type) 
{
	var amountInWords;
	
	if (net) {
		var splitAmount = net.split('.');
		var wholeAmount = splitAmount[0];
	  	var decimalAmount = splitAmount[1];
	  	
	  	amountInWords = numberToEnglish( Number(wholeAmount) ) + ' ' + symbol + ((splitAmount[1] == undefined || Number(splitAmount[1]) == 0) ? '' :
	    	' And ' + decimalAmount + '/100');
	  	amountInWords += ' Only';
	} else {
		amountInWords = '';
	}
	
	var row = '';
	
	if (type == 'Local') {
		row += '<tr style="padding-top: 20px;">';
		row += '<td width="90" rowspan="2" valign="top">Amount in Words : </td>';
		row += '<td width="300" rowspan="2" valign="top">' + amountInWords + '</td>';
		row += '<td align="right" width="130"><b>NET AMOUNT (PHP)</b></td>';
		row += '<td align="right" width="80"><b>' + toCurrency(net) + '</b></td>';
		row += '</tr>';
		
		row += '<tr style="padding-bottom: 30px;">';
		row += '<td align="right" width="130"><b>Tax (VI12 - 12.00% / .00%)</b></td>';
		row += '<td align="right" width="80"><b>' + toCurrency(tax) + '</b></td>';
		row += '</tr>';
	} else if (type == 'Sub Con') {
		row += '<tr style="padding-top: 10px;">';
		row += '<td colspan="2" width="130">Amount in Words : </td>';
		row += '<td colspan="2" width="300">' + amountInWords + '</td>';
		row += '<td colspan="2" align="right" width="100"><b>TOTAL ' + symbol + '</b></td>';
		row += '<td align="right" width="70"><b>' + toCurrency(net) + '</b></td>';
		row += '</tr>';
	} else {
		row += '<tr style="padding-top: 10px;">';
		row += '<td colspan="2" width="130">Amount in Words : </td>';
		row += '<td colspan="2" width="300">' + amountInWords + '</td>';
		row += '<td colspan="2" align="right" width="100"><b>TOTAL ' + symbol + '</b></td>';
		row += '<td align="right" width="70"><b>' + toCurrency(net) + '</b></td>';
		row += '</tr>';
	}
	
	return row;
}

function concatenateRemarks(remarks)
{
	var row = '';
	
	if (remarks) {
		row += '<tr style="padding-top: 20px;">';
		row += '<td width="70">REMARK : </td>';
		row += '<td align="left" width="320">' + remarks.toUpperCase() + '</td>';
		row += '<td width="210"></td>';
		row += '</tr>';
	}
	
	return row;
}

function concatenateCheckedBy(checkedBy, checkedBy2nd)
{
	var row = '';
	
	if (checkedBy && checkedBy2nd) {
		row += '<td valign="bottom" align="center" width="180">' + replaceNull(checkedBy) + '<br /><br />' + replaceNull(checkedBy2nd) + '</td>';
	} else {
		if (checkedBy) {
			row += '<td valign="middle" align="center" width="180">' + replaceNull(checkedBy) + '</td>';
		} else if (checkedBy2nd) {
			row += '<td valign="middle" align="center" width="180">' + replaceNull(checkedBy2nd) + '</td>';
		} else {
			row += '<td width="180"></td>';
		}
	}
	
	return row;
}

function appendUnderline() 
{
	var row = '';
	
	row += '<tr style="padding-top: 10px;">';
	row += '<td></td>';
	row += '<td></td>';
	row += '<td></td>';
	row += '<td></td>';
	row += '<td></td>';
	row += '<td></td>';
	row += '<td style="border-bottom: 1px solid;"></td>';
	row += '</tr>';
	
	row += '<tr style="padding-top: 1px;">';
	row += '<td></td>';
	row += '<td></td>';
	row += '<td></td>';
	row += '<td></td>';
	row += '<td></td>';
	row += '<td></td>';
	row += '<td style="border-top: 1px solid;"></td>';
	row += '</tr>';
	
	return row;
}

function getForexFormId() 
{
	var environment = isProductionEnvironment();
	var IDR_CUSTOMFORM;
	
	if (environment) {
		IDR_CUSTOMFORM = 116;
	} else {
		IDR_CUSTOMFORM = 127;
	}
	
	return IDR_CUSTOMFORM;
}
