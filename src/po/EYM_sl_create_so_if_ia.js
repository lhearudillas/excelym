/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       05 Sep 2017     denny
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */

function isEmpty(fldValue){
	return fldValue == '' || fldValue == null || fldValue == undefined || fldValue == 'null';
}


function createIf(soId, irId){
	nlapiLogExecution('DEBUG','soId',soId);
	var ifObj = nlapiTransformRecord('salesorder',soId,'itemfulfillment');
	ifObj.setFieldValue('customform','106');
	for(var i=1; i<=ifObj.getLineItemCount('item'); i++){
		ifObj.setLineItemValue('item', 'itemreceive', i, 'T');
	}
	var newIf = nlapiSubmitRecord(ifObj);

  	// This code was added by Roy Selim for PNID-405.
	// Make sure to include the library EYM_lib_project_ledger.js.
	if( newIf && irId ) {

		// Create a project ledger entry.
		projectLedgerEntry( newIf, irId );
	}
	// PNID-405 code ends here.

	nlapiSubmitField('itemreceipt',irId,'custbody_eym_link_if_auto',newIf);
	nlapiLogExecution('DEBUG','newIf',newIf);

	var so = nlapiLoadRecord('salesorder',soId);
	for(var i=1;i<=so.getLineItemCount('item'); i++){
		so.selectLineItem('item', i);
		so.setCurrentLineItemValue('item', 'isclosed', 'T');
		so.commitLineItem('item');
	}
	nlapiSubmitRecord(so);
	return newIf;
}

function createIa(ifId, location, projectid){
	var ia = nlapiCreateRecord('inventoryadjustment', {recordmode: 'dynamic'});
	var ifObj = nlapiLoadRecord('itemfulfillment',ifId);
	ia.setFieldValue('customform','109');
	ia.setFieldValue('customer',projectid);
	ia.setFieldValue('account',1018);
	ia.setFieldValue('custbody_eym_pr_projcode',projectid);
	ia.setFieldValue('memo',ifObj.getFieldValue('memo'));
	ia.setFieldValue('adjlocation',location);
	nlapiLogExecution('DEBUG','location',location);
	
//	var binFilter = [
//         new nlobjSearchFilter('inactive',null,'is','F'),
//         new nlobjSearchFilter('binnumber',null,'contains','MAIN-'),
//         new nlobjSearchFilter('location',null,'is',location)
//        ]
//	var binSearch = nlapiSearchRecord('bin', null, binFilter);
//	if(!binSearch){
//		throw nlapiCreateError('MISSING_CONFIG', 'There is no MAIN bin configured for Location internal id'+location, true);
//	}
	for(var i=1; i<=ifObj.getLineItemCount('item'); i++){
		var item = ifObj.getLineItemValue('item', 'item', i);
		var quantity = ifObj.getLineItemValue('item', 'quantity', i);
		
		ia.selectNewLineItem('inventory');
		ia.setCurrentLineItemValue('inventory', 'item', item);
		ia.setCurrentLineItemValue('inventory', 'adjustqtyby', quantity);
		ia.setCurrentLineItemValue('inventory', 'location', location);
		var useBin = nlapiLookupField('item',item,'usebins');
		if(useBin=='T'){
			var subRec = ia.viewCurrentLineItemSubrecord('inventory','inventorydetail');
	    		if(subRec)
	    			subRec = ia.createCurrentLineItemSubrecord('inventory', 'inventorydetail');
	    		else
	    			subRec = ia.createCurrentLineItemSubrecord('inventory', 'inventorydetail');
			subRec.selectLineItem('inventoryassignment', 1);
			//subRec.setCurrentLineItemValue('inventoryassignment', 'binnumber', binSearch[0].getId()); // set to RB.IB.HOLD	
			subRec.setCurrentLineItemValue('inventoryassignment', 'quantity', quantity);
			subRec.commitLineItem('inventoryassignment');
			subRec.commit();
		}
		/*
		var subRec = ia.viewCurrentLineItemSubrecord('inventory','inventorydetail');
    		if(subRec)
    			subRec = ia.createCurrentLineItemSubrecord('inventory', 'inventorydetail');
    		else
    			subRec = ia.createCurrentLineItemSubrecord('inventory', 'inventorydetail');
		var binCount = ia.getLineItemCount('inventoryassignment');
		if(parseInt(binCount) > 0) {
			for(var j = 1; j <= binCount; j++) {
				subRec.removeLineItem('inventoryassignment', j);
			}
		}
		subRec.selectNewLineItem('inventoryassignment');
		subRec.setCurrentLineItemValue('inventoryassignment', 'binnumber', binSearch[0].getId()); // set to RB.IB.HOLD
		subRec.setCurrentLineItemValue('inventoryassignment', 'quantity', quantity);
		subRec.commitLineItem('inventoryassignment');
		subRec.commit();
		*/
		ia.commitLineItem('inventory');
	}
	var newIa = nlapiSubmitRecord(ia);
	return newIa;
}

function confirm(request, response) {
	nlapiLogExecution('DEBUG','request.getMethod()',request.getMethod());
	if (request.getMethod() == 'GET') {
		doGet(request, response);
	}else if(request.getMethod() == 'POST') {
		doPost(request, response);
	}
}

function doGet(request, response) {	
	var id = request.getParameter('recId');
	var from = request.getParameter('from');
	var autoSr = request.getParameter('autoSr');
	var autoIf = request.getParameter('autoIf');
	var autoIa = request.getParameter('autoIa');
	nlapiLogExecution('DEBUG','doGet', 'Generating pin UI');
	var form = nlapiCreateForm("Pin", true);
	
	form.addField('custpage_rec_id', 'text', '').setDisplayType('hidden').setDefaultValue(id);
	form.addField('custpage_from', 'text', '').setDisplayType('hidden').setDefaultValue(from);
	form.addField('custpage_auto_sr', 'text', '').setDisplayType('hidden').setDefaultValue(autoSr);
	form.addField('custpage_auto_if', 'text', '').setDisplayType('hidden').setDefaultValue(autoIf);
	form.addField('custpage_auto_ia', 'text', '').setDisplayType('hidden').setDefaultValue(autoIa);
	form.addField('custpage_employee_pin', 'password', 'Pin').setMandatory(true).setLayoutType('outside','startrow');
	form.addSubmitButton('Submit'); 
	
	response.writePage(form);    
}

function doPost(request, response) {
	var id = request.getParameter('custpage_rec_id');
	var from = request.getParameter('custpage_from');
	var autoSr = request.getParameter('custpage_auto_sr');
	var autoIf = request.getParameter('custpage_auto_if');
	var autoIa = request.getParameter('custpage_auto_ia');
	var pin = nlapiEncrypt(request.getParameter('custpage_employee_pin'), 'sha1');
	var html = ''; 
	nlapiLogExecution('DEBUG','from',from);
	try {
		if (from == 'ir') {
			var projCodeId = nlapiLookupField('itemreceipt', id, 'custbody_eym_pr_projcode');

			if (projCodeId) {
				var projInChargeId = nlapiLookupField('job', projCodeId, 'custentity_eym_proj_in_charge');
				nlapiLogExecution('DEBUG','projInChargeId',projInChargeId);
				var employeePin = nlapiLookupField('employee', projInChargeId, 'custentity_eym_approver_pincode');
				nlapiLogExecution('DEBUG','employeePin',employeePin);
				if (!projInChargeId) {
					throw nlapiCreateError('MISSING_ID', 'Project In-Charge is empty: Proj - ' + projCodeId, true);
				}

				if (!employeePin) {
					throw nlapiCreateError('MISSING_DATA', 'Pin Code is empty: Emp - ' + projInChargeId, true);
				}

				if (employeePin == pin) {
					//record.setFieldValue('custbody_eym_confirm_status', 2);
					//nlapiSubmitRecord(record);
					var sts = create(id, from, autoIf, autoSr, autoIa);
					var stsId = sts=="completed" ? 2 : 1;
					nlapiSubmitField('itemreceipt',id,'custbody_eym_confirm_status',stsId);

					html = '<html>';
					html += '<head>';
					html += '<script language="JavaScript">';
					html += 'window.close();';
					html += 'window.opener.location.reload();';
					html += '</script>';
					html += '</head>';
					html += '<body>';
					html += '</body>';
					html += '</html>';
				} else {
					html = '<html>';
					html += '<head>';
					html += '<script language="JavaScript">';		
					html += 'alert("ERROR: PIN Code does not match. Only the assigned Project In-Charge can Confirm this Item Receipt.");';
					html += 'window.close();';
					html += '</script>';
					html += '</head>';
					html += '<body>';
					html += '</body>';
					html += '</html>';
				}
			} else {
				html = '<html>';
				html += '<head>';
				html += '<script language="JavaScript">';
				html += 'alert("ERROR: Empty Project Code.");';
				html += 'window.close();';
				html += '</script>';
				html += '</head>';
				html += '<body>';
				html += '</body>';
				html += '</html>';
				
				throw nlapiCreateError('MISSING_ID', 'Project Code is empty: IR - ' + id, true);
			}
		} else {
			var createdFrom = nlapiLookupField('itemfulfillment', id, 'createdfrom');
			var createdFromType = nlapiLookupField('transaction', createdFrom, 'recordtype');
			
			var loadTransaction = nlapiLoadRecord(createdFromType, createdFrom);
			var projId = loadTransaction.getFieldValue('job');
			var projInChargeId = nlapiLookupField('job', projId, 'custentity_eym_proj_in_charge');
			
			if (projInChargeId) {
				var employeePin = nlapiLookupField('employee', projInChargeId, 'custentity_eym_approver_pincode');

				if (!employeePin) {
					throw nlapiCreateError('MISSING_DATA', 'Pin Code is empty: Emp - ' + projInChargeId, true);
				}

				if (employeePin == pin) {
					//record.setFieldValue('custbody_eym_confirm_status', 2);
					//nlapiSubmitRecord(record);
					//var sts = create(id, from, autoIf, autoSr, autoIa);

                    // Code added by Roy Selim for PNID-406
					var recType = loadTransaction.getFieldValue('custbody_eym_req_type');

					// Add project ledger entry only when the transaction is a stock transfer from CW.
					if( recType == 1 ) {

						// Add project ledger entry.
						projectLedgerEntry( id, null );
					}
                    // Code edit ends here.

					var sts = 'completed';
					var stsId = sts=="completed" ? 2 : 1;
					nlapiSubmitField('itemfulfillment',id,'custbody_eym_confirm_status',stsId);
					html = '<html>';
					html += '<head>';
					html += '<script language="JavaScript">';
					html += 'window.close();';
					html += 'window.opener.location.reload();';
					html += '</script>';
					html += '</head>';
					html += '<body>';
					html += '</body>';
					html += '</html>';
				} else {
					html = '<html>';
					html += '<head>';
					html += '<script language="JavaScript">';		
					html += 'alert("ERROR: PIN Code does not match. Only the assigned Project In-Charge can Confirm this Item Fulfillment.");';
					html += 'window.close();';
					html += '</script>';
					html += '</head>';
					html += '<body>';
					html += '</body>';
					html += '</html>';
				}
			} else {
				html = '<html>';
				html += '<head>';
				html += '<script language="JavaScript">';
				html += 'alert("ERROR: Empty Project In-Charge.");';
				html += 'window.close();';
				html += '</script>';
				html += '</head>';
				html += '<body>';
				html += '</body>';
				html += '</html>';
				
				throw nlapiCreateError('MISSING_ID', 'Project In-Charge is empty: IF - ' + id, true);
			}
		}
	} catch(e) {
		if (e instanceof nlobjError)
			errMsg = e.getCode() + ': ' + e.getDetails();
        else
        		errMsg = 'UNEXPECTED ERROR: '+ e.toString();
		
		html = '<html>';
		html += '<head>';
		html += '<script language="JavaScript">';
		html += 'alert("'+errMsg+'");';
		html += 'window.close();';
		html += '</script>';
		html += '</head>';
		html += '<body>';
		html += '</body>';
		html += '</html>';
		nlapiLogExecution('DEBUG', 'errMsg', errMsg);
	}
	
	response.write(html);
}

function create(id, from, autoIf, autoSr, autoIa) {
	nlapiLogExecution('DEBUG', 'Start', 'Starting..');
	var recType = '';
	nlapiLogExecution('DEBUG', 'from', from);
	nlapiLogExecution('DEBUG', 'id', id);
	var ret = "not completed";
	if(from=="ir"){
		recType = 'itemreceipt';
		var irObj = nlapiLoadRecord('itemreceipt',id);
		nlapiLogExecution('DEBUG', 'irObj', irObj);
		var irCount = irObj.getLineItemCount('item');
		var soId = isEmpty(autoSr) ? '' : autoSr;
		var ifId = isEmpty(autoIf) ? '' : autoIf;
		var jobObj = nlapiLoadRecord('job',irObj.getFieldValue('custbody_eym_pr_projcode'));
		var entity = jobObj.getFieldValue('parent');
		if(isEmpty(autoSr)){
			var pic = jobObj.getFieldValue('custentity_eym_proj_in_charge');
			//var mrf_id = nlapiLookupField('purchaseorder',irObj.getFieldValue('createdfrom'),'custbody_eym_pr_mrf');
			var sr = nlapiCreateRecord('salesorder', {recordmode: 'dynamic'});
			sr.setFieldValue('customform','110');
			sr.setFieldValue('entity',entity);
			sr.setFieldValue('custbody_eym_req_type',4);
			sr.setFieldValue('orderstatus','B');
			sr.setFieldValue('job',irObj.getFieldValue('custbody_eym_pr_projcode'));
			sr.setFieldValue('memo',irObj.getFieldValue('memo'));
			sr.setFieldValue('custbody_eym_pr_pic',pic);
			sr.setFieldValue('location',102);
			for(var i=1; i<=irCount; i++){
              
              	// This code was added by ROY SELIM for PNID-387
				// Get the BQI.
				var bqi = irObj.getLineItemValue( 'item', 'custcol_eym_pur_bqi', i );
              
				var item = irObj.getLineItemValue('item', 'item', i);
				var description = irObj.getLineItemValue('item', 'description', i);
				var quantity = irObj.getLineItemValue('item', 'quantity', i);
				
				sr.selectNewLineItem('item');
				sr.setCurrentLineItemValue('item', 'item', item);
				sr.setCurrentLineItemValue('item', 'description', description);
				sr.setCurrentLineItemValue('item', 'quantity', quantity);
				sr.setCurrentLineItemValue('item', 'rate', 0);
				sr.setCurrentLineItemValue('item', 'taxcode', 5);
              
              	// This code was added by ROY SELIM for PNID-387
				// Get the BQI.
				sr.setCurrentLineItemValue( 'item', 'custcol_eym_pur_bqi', bqi );
              
				sr.commitLineItem('item');
			}
			soId = nlapiSubmitRecord(sr);
			if(soId){
				nlapiSubmitField('itemreceipt',id,'custbody_eym_link_sr_auto',soId);
				nlapiLogExecution('DEBUG','SO id', soId);
				ifId = createIf(soId,id);
			}
		}else if(isEmpty(autoIf)){
			ifId = createIf(soId, id);
		}
		
		if(isEmpty(autoIa) && ifId!=''){
			var newIa = createIa(ifId, jobObj.getFieldValue('custentity_eym_proj_loc_whouse'), irObj.getFieldValue('custbody_eym_pr_projcode'));
			if(newIa){
				nlapiSubmitField('itemreceipt',id,'custbody_eym_link_invadj_auto',newIa);
				nlapiLogExecution('DEBUG','IA id', newIa);
			}

		}
		if(soId && ifId && newIa){
			ret = "completed";
		}
	}else if(from=="if"){
		recType = 'itemfulfillment';
		var ifObj = nlapiLoadRecord('itemfulfillment',id);
		var jobObj = nlapiLoadRecord('job',ifObj.getFieldValue('entity'));
		nlapiLogExecution('DEBUG','id',id);
		var newIa = createIa(id,jobObj.getFieldValue('custentity_eym_proj_loc_whouse'),ifObj.getFieldValue('entity'));
		nlapiLogExecution('DEBUG','newIa',newIa);
		if(newIa){
			nlapiSubmitField('itemfulfillment',id,'custbody_eym_link_invadj_auto',newIa);
			nlapiLogExecution('DEBUG','IA id', newIa);
		}

	}
	return ret;
	//nlapiSetRedirectURL('SUITELET','customscript_sl_eym_create_request_sts','customdeploy_sl_eym_create_request_sts',null,params);
	//nlapiSetRedirectURL('RECORD',recType,id);
}
