/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       12 Mar 2018     Lhea
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */


function userEventAfterSubmit(type){
	nlapiLogExecution('DEBUG','userEventAfterSubmit type',type);
	if(type=="create" || type=="edit"){
		var id =  nlapiGetRecordId();
		var recType = nlapiGetRecordType();
		var confirmStatus = nlapiGetFieldValue('custbody_eym_confirm_status');
		
		if (isEmpty(confirmStatus)) {
			nlapiSubmitField(recType, id, 'custbody_eym_confirm_status', 1);
		}
	}
}

function isEmpty(fldValue){
	return fldValue == '' || fldValue == null || fldValue == undefined || fldValue == 'null';
}
