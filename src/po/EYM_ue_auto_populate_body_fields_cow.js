/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 * @NAmdConfig  /SuiteScripts/excelym/config/configuration.json
 */
define(['N/log', 'N/record', 'EYM/amountToWords', 'EYM/nsHelpers'],

function(log, record, convert, nsHelper) {

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function afterSubmit(scriptContext) {
    	try {
    		var recordSubmitted = scriptContext.newRecord;
    		var cowFormId = getCOWFormId();
    		
    		var currentRecord = record.load({
    		    type: record.Type.PURCHASE_ORDER, 
    		    id: recordSubmitted.id,
    		    isDynamic: true,
    		});
    		
    		var customForm = currentRecord.getValue({
        		fieldId: 'customform'
        	});
    		
    		if (customForm == cowFormId) {
    			var itemCount = currentRecord.getLineCount({
					sublistId: 'item'
                });
    			
    			if (itemCount > 0) {
    				var scopeId = currentRecord.getSublistValue({
        			    sublistId: 'item',
        			    fieldId: 'custcol_eym_cow_service_descr',
        			    line: 0
        			});
    				
    				if (scopeId) {
    					var scopeRecord = record.load({
    					    type: 'customlist_eym_cow_service_descr',
    					       id: scopeId,
    					       isDynamic: true                       
    					   });
    					
    					var scope = scopeRecord.getValue({
    						fieldId: 'name'
    					});
    					
    					if (scope) {
    						currentRecord.setValue({
                                fieldId: 'custbody_eym_cow_item_remarks',
                                value: scope
                            });
    					}
    				}
    				
    				var remarks = currentRecord.getSublistValue({
        			    sublistId: 'item',
        			    fieldId: 'custcol_eym_pur_remarks',
        			    line: 0
        			});
    				
    				
    				if (remarks) {
    					currentRecord.setValue({
                            fieldId: 'custbody_eym_cow_item_description',
                            value: remarks
                        });
    				}
    			}
    			
    			// Convert total amount to words
        		var totalAmount = currentRecord.getValue({
            		fieldId: 'total'
            	});
        		
        		var amountInWords;
        		var amountObject = totalAmount.toString();
    			
        		if (totalAmount) {
        			var splitAmount = amountObject.split('.');
        			var wholeAmount = splitAmount[0];
        		  	var decimalAmount = splitAmount[1];
        		  	
        		  	amountInWords = convert.numberToEnglish(Number(wholeAmount) ) + ' PHP ' + ((splitAmount[1] == undefined || Number(splitAmount[1]) == 0) ? '' :
        		    	' And ' + decimalAmount + '/100');
        		  	amountInWords += ' Only';
        		} else {
        			amountInWords = '';
        		}
        		
        		currentRecord.setValue({
                    fieldId: 'custbody_eym_amount_in_words',
                    value: amountInWords
                });
        		
        		currentRecord.save();
    		}
    	} catch (e) {
    		log.error({
    			title: e.name,
                details: e.message
            }); 
    	}
    }
    
    function getCOWFormId() 
    {
    	var environment = nsHelper.isProductionEnvironment();
    	var IDR_CUSTOMFORM;
    	
    	if (environment) {
    		IDR_CUSTOMFORM = 128;
    	} else {
    		IDR_CUSTOMFORM = 119;
    	}
    	
    	return IDR_CUSTOMFORM;
    }

    return {
        afterSubmit: afterSubmit
    };
    
});
