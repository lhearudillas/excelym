/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/ui/dialog', 'N/log', 'N/search'],

function(record, dialog, log, search) {

    /**
     * Validation function to be executed when sublist line is committed.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     *
     * @returns {boolean} Return true if sublist line is valid
     *
     * @since 2015.2
     */
    function validateLine(scriptContext) 
    {
    	var currentRecord = scriptContext.currentRecord;
    	var sublistName = scriptContext.sublistId;
        var recordType = currentRecord.type;
        var custForm = currentRecord.getValue({fieldId: 'customform'});
        
        if (recordType == 'purchaserequisition' && custForm == 100) {
        	if (sublistName == 'item') {
        		var bqi = currentRecord.getCurrentSublistValue({
                    sublistId: 'item',
                    fieldId: 'custcol_eym_pur_bqi'
                });
        		
        		if (bqi) {
        			var recordFilter = [
                        search.createFilter({
                            name: 'internalid',
                            operator: search.Operator.IS,
                            values: bqi
                        })
                    ];
        			
        			var recordColumn = [
                        search.createColumn({
                            name: 'custrecord_eym_boq_class',
                        })
                    ];
        			
        			var recordSearch = search.create({
        				type: 'customrecord_eym_boq',
        				title: 'BQI Search',
        				filters: recordFilter,
        				columns: recordColumn
        			});
        			
        			recordSearch = recordSearch.run().getRange({
                        start: 0, 
                        end: 1
                    });
        			
        			if (recordSearch.length > 0) {
        				for (var i = 0; i < recordSearch.length; i++) {
        					var costType = recordSearch[i].getValue({
    	    					name: 'custrecord_eym_boq_class'
    	    				});
        					
        					if (costType) {
        						currentRecord.setCurrentSublistValue({
    	    						sublistId: 'item',
    	    						fieldId: 'class',
    	    						value: costType
    	                       });
        					}
        				}
        			}
        		}
        	}
        }
        
        return true;
    }

    return {
        validateLine: validateLine
    };
    
});
