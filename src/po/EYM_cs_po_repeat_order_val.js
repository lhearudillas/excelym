/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       11 Feb 2018     EYM-013
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @returns {Boolean} True to continue save, false to abort save
 */
function clientSaveRecord(){
	var custForm = nlapiGetFieldValue('customform');
	
	if(custForm == 101){
		var projCode = nlapiGetFieldValue('custbody_eym_pr_projcode');
		var repeatOrder = nlapiGetFieldValue('custbody_eym_po_repeat_order');
		
		if(!IsNullOrEmpty(projCode) && (IsNullOrEmpty(repeatOrder))){
			alert('Please indicate if this PO is a Repeat Order or not.');
			return false;
		}
	}
    return true;
}
