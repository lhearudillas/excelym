/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       05 Jan 2015     dashikoy
 *
 */
 
function verifyApprover(request, response)
 {
	if (request.getMethod() == 'GET') {		
		doGet(request, response);
	}else if(request.getMethod() == 'POST') {
		doPost(request, response);
	}
}

function doGet(request, response) 
{	
	var form = nlapiCreateForm("Enter PIN Code to proceed", true);
	
	var empId = request.getParameter('employee_id');
	var empNm = request.getParameter('employee_name');
	var status = request.getParameter('status');
	var recId = request.getParameter('rec_id');
	var recType = request.getParameter('rec_type');
	var appType = request.getParameter('app_type');
  
  nlapiLogExecution('DEBUG', 'ans', empNm);
          nlapiLogExecution('DEBUG', 'empId', empId);
	
	form.addField('custpage_rec_id', 'text', '').setDisplayType('hidden').setDefaultValue(recId);
	form.addField('custpage_rec_type', 'text', '').setDisplayType('hidden').setDefaultValue(recType);
	form.addField('custpage_status', 'text', '').setDisplayType('hidden').setDefaultValue(status);
	form.addField('custpage_employee_id', 'text', '').setDisplayType('hidden').setDefaultValue(empId);
	form.addField('custpage_app_type', 'text', '').setDisplayType('hidden').setDefaultValue(appType);
	form.addField('custpage_employee_name', 'text', 'User').setDisplayType('inline').setLayoutType('outside','startrow').setDefaultValue(empNm);
	form.addField('custpage_employee_pin', 'password', 'Pin').setMandatory(true).setLayoutType('outside','startrow');
		
    form.addSubmitButton('Submit');            
	response.writePage(form);    
}

function doPost(request, response) 
{
	var user = request.getParameter('custpage_employee_id');
	var pin = nlapiEncrypt(request.getParameter('custpage_employee_pin'), 'sha1');
	var status = request.getParameter('custpage_status');
	var recId = request.getParameter('custpage_rec_id');
	var recType = request.getParameter('custpage_rec_type');
	var appType = request.getParameter('custpage_app_type');
	    var newrec = nlapiLoadRecord(recType, recId);
  		var returnyes = newrec.getFieldValue('custbody_eym_po_repeat_order');
 		 var projCode = newrec.getFieldValue('custbody_eym_pr_projcode');
  			var total = newrec.getFieldValue('total');
  
	var columns = new Array();
	columns.push(new nlobjSearchColumn('custentity_eym_approver_pincode'));
	
	var filters = new Array();
	filters.push(new nlobjSearchFilter('internalid', null, 'is', user));	
	filters.push(new nlobjSearchFilter('custentity_eym_approver_pincode', null, 'is', pin));	
	
	var html = ''; 
	var res = nlapiSearchRecord('employee', null, filters, columns);
	if(res != null) {
		var empId = res[0].getId();
		var empPIN = nlapiLookupField('employee',empId,'custentity_eym_approver_pincode');
		
      if(status == 2){
			nlapiSubmitField(recType, recId, 'custbody_eym_wf_approval_status', 5);
            nlapiSubmitField(recType, recId, 'nextapprover', 1161);
      //  alert('1st approve');
		} 
       if(status == 5){
          	if(total < 500000  && !IsNullOrEmpty(projCode) && returnyes == 1){
               nlapiSubmitField(recType, recId, 'approvalstatus', 2);
              nlapiSubmitField(recType, recId, 'custbody_eym_wf_approval_status', 3);
            
             // nlapiLogExecution('DEBUG', 'stat', 'jenny approve less than 500 with project return yes');
              
            }
         	else if(total < 500000  && IsNullOrEmpty(projCode)){
              nlapiSubmitField(recType, recId, 'approvalstatus', 2);
              nlapiSubmitField(recType, recId, 'custbody_eym_wf_approval_status', 3);
              //nlapiLogExecution('DEBUG', 'stat', 'jenny approve less than 500 without project');
              
            }
         else{
           nlapiSubmitField(recType, recId, 'custbody_eym_wf_approval_status', 6);
           nlapiSubmitField(recType, recId, 'nextapprover', 3125);
           //nlapiLogExecution('DEBUG', 'stat', 'jenny approve else everything');
           
         }
       }
      if(status == 6){
         nlapiSubmitField(recType, recId, 'approvalstatus', 2);
         nlapiSubmitField(recType, recId, 'custbody_eym_wf_approval_status', 3);
        // nlapiLogExecution('DEBUG', 'stat', 'william approve else everything');
      }
			
			html = '<html>';
			html += '<head>';
			html += '<script language="JavaScript">';		
			html += 'window.opener.location.reload();';
			html += 'window.close();';
			html += '</script>';
			html += '</head>';
			html += '<body>';
			html += '</body>';
			html += '</html>';
	
				
	}else{					
		html = '<html>';
		html += '<head>';
		html += '<script language="JavaScript">';		
		html += 'alert("Invalid PIN for this approver.");';
		html += 'window.close();';
		html += '</script>';
		html += '</head>';
		html += '<body>';
		html += '</body>';
		html += '</html>';
	}
			
	response.write(html);
}

