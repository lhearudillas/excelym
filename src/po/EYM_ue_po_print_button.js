/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       22 Feb 2018     EYM-013
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */

function userEventBeforeLoad(type, form, request){

	// Run the script if the type is view.
	if( type == 'view' ) {

		// Get the purchase order internal id.
		var purchaseOrderInternalId = nlapiGetRecordId();

		// Check if purchase order internal id is not null.
		if( purchaseOrderInternalId != null ) {

			// Create a PDF URL.
			var createPdfUrl = nlapiResolveURL('SUITELET', 
				'customscript_sl_eym_po_pdf', 
				'customdeploy_sl_eym_po_pdf');

			// Append the id to the query string.
			createPdfUrl += '&id=' + purchaseOrderInternalId;

			// Define the script that will run when a 'Print' button is clicked.
			var buttonScript = "window.location.assign('"+ createPdfUrl +"')";

			// Add a 'Print' button which, when clicked, redirects to the Suitelet.
			form.addButton('custpage_eym_add_print_button',	'Print', buttonScript);
		} else {

			// Log an error message.
            nlapiLogExecution('DEBUG', 'Error', 'Internaal id of the record is null');
		}
	}
}