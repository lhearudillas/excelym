/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       29 May 2018     EYM-013
 *
 */

var IDS_RECORD_ID = 'recId';
var IDS_RECORD_TYPE = 'recType';
var IDS_REQUESTED_BY = 'requestedBy';
var IDS_APP_WORKFLW_STAT = 'status';
var IDS_CURRENT_APPROVER = 'crrntapprvr';
var IDS_PROJECT_CODE = 'projCode';


var IDS_APP_WORKFLW_STAT_2 = 'custbody_eym_wf_approval_status';
var IDS_APP_STAT = 'approvalstatus';
var IDS_CURRENT_APPROVER_2 = 'nextapprover';

//Approvers
var IDS_PO_SECOND_APPROVER = 1161; //Jennifer Latoga
var IDS_PO_FINAL_APPROVER = 3125;  //Wally Liu

var IDS_CUSTPAGE_RECORD_ID = 'custpage_rec_id';
var IDS_CUSTPAGE_RECORD_TYPE = 'custpage_rec_type';
var IDS_CUSTPAGE_REQUESTED_BY = 'custpage_requested_by';
var IDS_CUSTPAGE_APPROVAL_WRKFLW_STATUS = 'custpage_approval_wrkflw_stat';
var IDS_CUSTPAGE_APPROVER_PIN = 'custpage_employee_pin';
var IDS_CUSTPAGE_PROJECT_CODE = 'custpage_project_code';
var IDS_CUSTPAGE_CURRENT_APPROVER = 'custpage_current_approver';


function verifyPin(request, response)
{
	if (request.getMethod() == 'GET') {	
		var recId = request.getParameter(IDS_RECORD_ID);
		var recType = request.getParameter(IDS_RECORD_TYPE);
		var requestedBy = request.getParameter(IDS_REQUESTED_BY);
		var appWorkflowStat = request.getParameter(IDS_APP_WORKFLW_STAT);
		var crrntapprvr = request.getParameter(IDS_CURRENT_APPROVER);
		var projCode = request.getParameter(IDS_PROJECT_CODE);

		doGet(recId, recType, requestedBy, appWorkflowStat, crrntapprvr, projCode, response);
	} else if (request.getMethod() == 'POST') {
		doPost(request, response);
	}
}

function doGet(recId, recType, requestedBy, appWorkflowStat, crrntapprvr, projCode, response)
{	
	var epRess = getPreferences();
	var mainCss = getPreference(epRess, 'main-css');
	var hiddenFields = {};
	hiddenFields[IDS_CUSTPAGE_RECORD_ID] = recId;
	hiddenFields[IDS_CUSTPAGE_RECORD_TYPE] = recType;
	hiddenFields[IDS_CUSTPAGE_REQUESTED_BY] = requestedBy;
	hiddenFields[IDS_CUSTPAGE_APPROVAL_WRKFLW_STATUS] = appWorkflowStat;
	hiddenFields[IDS_CUSTPAGE_CURRENT_APPROVER] = crrntapprvr;
	hiddenFields[IDS_CUSTPAGE_PROJECT_CODE] = projCode;

	var html = pinCodeUI(mainCss, hiddenFields);
	
	response.write(html);  
}

function doPost(request, response)
{
	var recId = request.getParameter(IDS_CUSTPAGE_RECORD_ID);
	var recType = request.getParameter(IDS_CUSTPAGE_RECORD_TYPE);
	var requestedBy = request.getParameter(IDS_CUSTPAGE_REQUESTED_BY);
	var appWorkflowStat = request.getParameter(IDS_APP_WORKFLW_STAT);
	var crrntapprvr = request.getParameter(IDS_CURRENT_APPROVER);
	var projCode = request.getParameter(IDS_PROJECT_CODE);

	var pin = nlapiEncrypt(request.getParameter(IDS_CUSTPAGE_APPROVER_PIN), 'sha1');
	
	var approverPIN = nlapiLookupField('employee', crrntapprvr, 'custentity_eym_approver_pincode');
		
	var html = ''; 
	if (approverPIN == pin) {
		
		if (appWorkflowStat == 2) {

			if (projCode == 'null') {
				nlapiSubmitField(recType, recId, IDS_APP_WORKFLW_STAT_2, 7);
				nlapiSubmitField(recType, recId, IDS_CURRENT_APPROVER_2, IDS_PO_SECOND_APPROVER);
			
				html = '<html>';
				html += '<head>';
				html += '<script language="JavaScript">';	
				html += 'window.close();';
				html += 'window.opener.location.reload();';
				html += '</script>';
				html += '</head>';
				html += '<body>';
				html += '</body>';
				html += '</html>';
				
			} else {
				nlapiSubmitField(recType, recId, IDS_APP_WORKFLW_STAT_2, 5);
				nlapiSubmitField(recType, recId, IDS_CURRENT_APPROVER_2, IDS_PO_SECOND_APPROVER);
			
				html = '<html>';
				html += '<head>';
				html += '<script language="JavaScript">';	
				html += 'window.close();';
				html += 'window.opener.location.reload();';
				html += '</script>';
				html += '</head>';
				html += '<body>';
				html += '</body>';
				html += '</html>';
			}
		
		}
				
		if (appWorkflowStat == 7 || appWorkflowStat == 5) {
			nlapiSubmitField(recType, recId, IDS_APP_WORKFLW_STAT_2, 6);
			nlapiSubmitField(recType, recId, IDS_CURRENT_APPROVER_2, IDS_PO_FINAL_APPROVER);
			
			html = '<html>';
			html += '<head>';
			html += '<script language="JavaScript">';	
			html += 'window.close();';
			html += 'window.opener.location.reload();';
			html += '</script>';
			html += '</head>';
			html += '<body>';
			html += '</body>';
			html += '</html>';
			
		} 
		
		if (appWorkflowStat == 6) {
			nlapiSubmitField(recType, recId, IDS_APP_WORKFLW_STAT_2, 3);
			nlapiSubmitField(recType, recId, IDS_APP_STAT, IDR_STATUS_APPROVED);
			
			html = '<html>';
			html += '<head>';
			html += '<script language="JavaScript">';	
			html += 'window.close();';
			html += 'window.opener.location.reload();';
			html += '</script>';
			html += '</head>';
			html += '<body>';
			html += '</body>';
			html += '</html>';
		}
		
	} else {
		html = '<html>';
		html += '<head>';
		html += '<script language="JavaScript">';		
		html += 'alert("Invalid PIN for this approver.");';
		html += 'window.close();';
		html += '</script>';
		html += '</head>';
		html += '<body>';
		html += '</body>';
		html += '</html>';
	}
		
	response.write(html);

}