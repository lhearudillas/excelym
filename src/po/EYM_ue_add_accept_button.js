/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       11 Aug 2017     EYM-013
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */

function userEventBeforeLoad(type, form, request){
	if(type=='view'){
		form.setScript('customscript_eym_cs_popup_window'); 
		
		var id = nlapiGetRecordId();
		var type = nlapiGetRecordType();
		nlapiLogExecution('DEBUG','type',type);
		
		try{
			if(type=="itemreceipt"){
				var poId = nlapiGetFieldValue('createdfrom');
				nlapiLogExecution('DEBUG','poId',poId);
				var createdFromType = nlapiLookupField('transaction',poId,'recordtype');
				if(createdFromType=='purchaseorder'){
					var mrfId = nlapiLookupField('purchaseorder',poId,'custbody_eym_pr_mrf');
					var autoSr = nlapiGetFieldValue('custbody_eym_link_sr_auto');
					var autoIf = nlapiGetFieldValue('custbody_eym_link_if_auto');
					var autoIa = nlapiGetFieldValue('custbody_eym_link_invadj_auto');
					
					
					nlapiLogExecution('DEBUG','auto',autoIf+'-'+autoSr+'-'+autoIa);
					if(!autoSr || !autoIa || !autoIf){
						var irLineCount = nlapiGetLineItemCount('item');
						var wrongLoc = false;
						for(var i=1;i<=irLineCount; i ++){
							var lineLoc = nlapiGetLineItemValue('item','location',i);
							if(lineLoc!='102'){
								wrongLoc = true;
								break;
							}
							nlapiLogExecution('DEBUG','lineLoc',lineLoc);
						}
						nlapiLogExecution('DEBUG','wrongLoc',wrongLoc);
						if(!wrongLoc){
							var filters = new Array();
							nlapiLogExecution('DEBUG','mrfId',mrfId);
							filters[0] = new nlobjSearchFilter('custbody_eym_pr_mrf', null, 'is', mrfId);
							filters[1] = new nlobjSearchFilter('mainline', null, 'is', 'T');
							filters[2] = new nlobjSearchFilter('customform', null, 'is', '100');
					 		
							var prSearch = nlapiSearchRecord('purchaserequisition',null,filters);
							nlapiLogExecution('DEBUG','prSearch',prSearch);
							if(prSearch){
								
								//var params = '&autoSr='+autoSr+'&autoIf='+autoIf+'&autoIa='+autoIa;
								//var url = nlapiResolveURL('SUITELET', 'customscript_sl_eym_create_so_if_ia', 'customdeploy_sl_eym_create_so_if_ia')+'&from=ir&id='+id+params;
								//form.addButton('custpage_confirmacceptancebutton','Confirm Acceptance', "document.location='"+ url +"'");
								var params = '&from=ir&recId='+id+'&autoSr='+autoSr+'&autoIf='+autoIf+'&autoIa='+autoIa;
								nlapiLogExecution('DEBUG','params',params);
								var url =  "showPopup('customscript_sl_eym_create_so_if_ia', 'customdeploy_sl_eym_create_so_if_ia', '" + params + "', 800, 600);";
								form.addButton('custpage_confirmacceptancebutton', 'Confirm Acceptance', url);
							}
						}
					}
					
				}
			}else if(type=="itemfulfillment"){
				var soId = nlapiGetFieldValue('createdfrom');
				var ia = nlapiGetFieldValue('custbody_eym_link_invadj_auto');
				nlapiLogExecution('DEBUG','soId',soId);
				nlapiLogExecution('DEBUG','ia',ia);
				var status = nlapiGetFieldValue('custbody_eym_confirm_status');
				if(soId && ia && status==1){
					var createdFromType = nlapiLookupField('transaction',soId,'recordtype');
					if(createdFromType=='salesorder'){
						var customform = nlapiLookupField('salesorder',soId,'customform');
						nlapiLogExecution('DEBUG','customform',customform);
						if(customform=='104' || customform=='108'){
							//var url = nlapiResolveURL('SUITELET', 'customscript_sl_eym_create_so_if_ia', 'customdeploy_sl_eym_create_so_if_ia')+'&from=if&id='+id;
							//form.addButton('custpage_confirmacceptancebutton','Confirm Acceptance', "document.location='"+url +"'");
							var params = '&from=if&recId='+id;
							var url =  "showPopup('customscript_sl_eym_create_so_if_ia', 'customdeploy_sl_eym_create_so_if_ia', '" + params + "', 800, 600);";
							form.addButton('custpage_confirmacceptancebutton', 'Confirm Acceptance', url);
						}
					}
				}
			}
			if (status == 2) {
				form.removeButton('custpage_confirmacceptancebutton');
			}
			
		}catch(e){
			var errMsg = '';
			if(e instanceof nlobjError)
				errMsg = e.getCode()+': '+e.getDetails();
	        else
	        		errMsg = 'UNEXPECTED ERROR: '+e.toString();	
			nlapiLogExecution('DEBUG','Error', type+'/'+id+' '+errMsg);
		}	
	}
}