/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       11 May 2018     eym
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType 
 * 
 * @param {String} type Access mode: create, copy, edit
 * @returns {Void}
 */
function csPageInit(type){
	
	try{
		
		
		//nlapiDisableLineItemField('item', 'amount', true);
		//nlapiDisableLineItemField('item', 'rate', true);
		//nlapiDisableLineItemField('item', 'custcol_eym_pur_item_base_unit', true);
		
	}catch(ex){
		nlapiLogExecution('DEBUG', 'Exception', ex.message == '' ? ex : ex.message);
	}
   
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Sublist internal id
 * @param {String} name Field internal id
 * @param {Number} linenum Optional line item number, starts from 1
 * @returns {Void}
 */
function clientFieldChanged(type, name, linenum){
 
}
