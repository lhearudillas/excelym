/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       24 May 2018     EYM-013
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
//For Receive Button
var IDS_PO_APP_STATUS = 'approvalstatus'
var IDS_PO_EX_RATE = 'custbody_eym_po_exrate';
var IDS_PO_CUSTFORM = 'customform';
var IDS_PO_ROLE_PREFERENCE = 'po-receive-btn-allowed-roles';

// For edit button
var IDS_PO_ADMIN_ROLE = 'admin-only-role';
var IDS_PO_WORKFLOW_STATUS = 'custbody_eym_wf_approval_status';

//For Approval Process
var IDS_PO_APP_WORKFLW_STAT = 'custbody_eym_wf_approval_status';
var IDS_REQUESTED_BY = 'employee';
var IDS_CURRENT_APPROVER = 'nextapprover';
var IDS_PO_PROJECT_CODE = 'custbody_eym_pr_projcode';

var IDS_PO_EXCHANGE_RATE = 'custbody_eym_po_exrate';

function userEventBeforeLoad(type, form, request)
{
	if(type == 'edit' || type == 'view')
	{
		var recId = nlapiGetRecordId();
		var recType = nlapiGetRecordType();
		var recLoad = nlapiLoadRecord(recType, recId);
		var custform = recLoad.getFieldValue(IDS_PO_CUSTFORM);
		var exchangeRate = recLoad.getFieldValue(IDS_PO_EXCHANGE_RATE);
		var workflowStatus = recLoad.getFieldValue(IDS_PO_WORKFLOW_STATUS);
		
		var context = nlapiGetContext();
		var role = context.getRole().toString();
		var epRess = getPreferences();
		var roles = getPreference(epRess, IDS_PO_ROLE_PREFERENCE, true);
		var adminRole = getPreference(epRess, IDS_PO_ADMIN_ROLE, true);
		
		var forexFormId = getForexFormId();
		var subConFormId = getSubConFormId();
		
		// Hide Edit button if status is "Approval Verified" for non Administrator roles
		if (workflowStatus == 3) {
			if (!isIndexOfArray(adminRole, role)) {
				form.removeButton('edit');
			}
		}
		
		// PSC Purchase Order form
		if (custform == 101 && type == 'view') {
			// Apply roles to buttons - PSC Purchaser and PSC Purchasing Head
			if(isIndexOfArray(roles, role)) {
				// For PO printing - Remove NS Print button and add Custom Print PO button
				nlapiSetFieldValue("custbody_eym_custom_html", hideNSPrintButton(), false);
				
				var printPO = nlapiResolveURL('SUITELET', 'customscript_sl_eym_print_purchase_order', 'customdeploy_sl_eym_print_purchase_order');
				printPO += '&type=' + recType + '&id=' + recId;
				
				var buttonScript = "window.location.assign('"+ printPO +"')";
				form.addButton('custpage_eym_print_po_button', 'Print PO', buttonScript);
			}
		}
		
		// PSC ED Work Order (Sub Con)
		if (custform == subConFormId && type == 'view') {
			// Apply roles to buttons - PSC Purchaser and PSC Purchasing Head
			if(isIndexOfArray(roles, role)) {
				// For PO printing - Remove NS Print button and add Custom Print PO button
				nlapiSetFieldValue("custbody_eym_custom_html", hideNSPrintButton(), false);
				
				var printPO = nlapiResolveURL('SUITELET', 'customscript_sl_eym_print_purchase_order', 'customdeploy_sl_eym_print_purchase_order');
				printPO += '&type=' + recType + '&id=' + recId;
				
				var buttonScript = "window.location.assign('"+ printPO +"')";
				form.addButton('custpage_eym_print_po_button', 'Print PO', buttonScript);
			}
		}

		// PSC Forex Purchase Order form
		if(custform == forexFormId) {	
			var exRate = recLoad.getFieldValue(IDS_PO_EX_RATE);
			var appStatus = recLoad.getFieldValue(IDS_PO_APP_STATUS);
			var appWorkflowStat = recLoad.getFieldValue(IDS_PO_APP_WORKFLW_STAT);
			var requestedBy = recLoad.getFieldValue(IDS_REQUESTED_BY);
			var crrntapprvr = recLoad.getFieldValue(IDS_CURRENT_APPROVER);
            var projCode_2 = recLoad.getFieldValue(IDS_PO_PROJECT_CODE);
            nlapiLogExecution('DEBUG', 'projCode_2', projCode_2);
          
            if (!isNullOrEmpty(projCode_2)) {
				var projCode = projCode_2;
				nlapiLogExecution('DEBUG', 'projCode', projCode);
								
			} else {
				var projCode = null;
			}

			form.setScript('customscript_cs_eym_po_foreign_currency');
			
			// Apply roles to buttons - PSC Purchaser and PSC Purchasing Head
			if(isIndexOfArray(roles, role)) {
				if (isNullOrEmpty(exRate) && appStatus == IDR_PO_PENDING_RECEIPT) {
					form.removeButton('receive');
				}
				
				if (type == 'view') {
					// For PO printing - Remove NS Print button and add Custom Print PO button
					nlapiSetFieldValue("custbody_eym_custom_html", hideNSPrintButton(), false);
					
					if (exchangeRate) {
						var printPO = nlapiResolveURL('SUITELET', 'customscript_sl_eym_print_purchase_order', 'customdeploy_sl_eym_print_purchase_order');
						printPO += '&type=' + recType + '&id=' + recId;
						
						var buttonScript = "window.location.assign('"+ printPO +"')";
						form.addButton('custpage_eym_print_po_button', 'Print PO', buttonScript);
					}
				}
			}
			
			if ((appWorkflowStat == IDR_WORKFLOW_SUBMITTED_FOR_APPROVAL || appWorkflowStat == IDR_WORKFLOW_PURCHASING_DIRECTOR_APPROVED ||
					appWorkflowStat == IDR_WORKFLOW_SUPERVISOR_APPROVED || appWorkflowStat == IDR_WORKFLOW_DOM_APPROVED) && appStatus == IDR_STATUS_PENDING_APPROVAL) {
				form.addButton('custpage_forex_approve_btn', 'Approve', "approve(" + recId + ", '" + recType + "', '" + appWorkflowStat + "', '" + requestedBy + "', '" + crrntapprvr + "', " + projCode + ");");
			}
		}
	}
}

// Function for hiding NS Print button
function hideNSPrintButton()
{
	var script = "";
	script += "<script type=\"text/javascript\">";
	script += " function hideNSPrintButton()";
	script += " {";
	script += "  var printButton = document.getElementById(\"spn_PRINT_d1\");";
	script += "  if(printButton)";
	script += "  {";
	script += "   printButton.parentNode.style.display = \"none\";";
	script += "  }";
	script += "  else";
	script += "  {";
	script += "   window.setTimeout(\"hideNSPrintButton()\", 50);";
	script += "  }";
	script += " }";
	script += "";
	script += " hideNSPrintButton();";
	script += "</script>";

	 return script;
}

function getForexFormId() 
{
	var environment = isProductionEnvironment();
	var IDR_CUSTOMFORM;
	
	if (environment) {
		IDR_CUSTOMFORM = 116;
	} else {
		IDR_CUSTOMFORM = 127;
	}
	
	return IDR_CUSTOMFORM;
}

function getSubConFormId() 
{
	var environment = isProductionEnvironment();
	var IDR_CUSTOMFORM;
	
	if (environment) {
		IDR_CUSTOMFORM = 120;
	} else {
		IDR_CUSTOMFORM = 134;
	}
	
	return IDR_CUSTOMFORM;
}