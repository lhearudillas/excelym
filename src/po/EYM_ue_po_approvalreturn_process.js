/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       30 Jan 2018     EYM-013
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request){
	
	try{
		
		if(type == 'edit' || type == 'view'){
			
			form.setScript('customscript_eym_cs_popup_window'); 
			
			//var rec = nlapiGetNewRecord();
			var recId = nlapiGetRecordId(); 
			var recType = nlapiGetRecordType();
			var rec = nlapiLoadRecord(recType, recId);
            

			var budgetStatus = rec.getFieldValue('custbody_eym_pur_budget_stat');
			var appStatus = rec.getFieldValue('custbody_eym_wf_approval_status');
			var isInternal = rec.getFieldValue('custbody_eym_is_internal');
			var custForm = rec.getFieldValue('customform');
          	var empId = rec.getFieldValue('nextapprover');
			var empNm = rec.getFieldText('nextapprover');
            var approvalstatus = rec.getFieldValue('approvalstatus');
          	var projCode =  rec.getFieldValue('custbody_eym_pr_projcode');
            var result = 0;
         	var custf = EDOrderCustomForm();

			if(custForm == 101 || custForm == custf || custForm == 119){
                if(approvalstatus == 3){
                 
                }
				
				//else if(approvalstatus != 2 && (isInternal == 'F' || isInternal == null) && !IsNullOrEmpty(empId) && appStatus != 1){
                else if(approvalstatus != 2 && !IsNullOrEmpty(empId) && appStatus != 1){
				    if(!IsNullOrEmpty(projCode) && budgetStatus != 1 ){
                      
                    } else {
						var args = '&rec_id=' + recId + '&rec_type=' + recType + '&employee_id=' + empId + '&employee_name=' + empNm + '&status=' + appStatus +'&appstatus=' + approvalstatus + '&custform='+ custForm;
						var script =  "showPopup('customscript_sl_eym_po_pincode_popup', 'customdeploy_sl_eym_po_pincode_popup', '" + args + "', 800, 600);";
						form.addButton('custpage_approve', 'Approve', script);	
                    }
				} else if(approvalstatus == 3) {

                }
			}	
		}
		
	} catch(ex){
		if(ex.getDetails != undefined){
			nlapiLogExecution('DEBUG', 'Exception', ex.message == '' ? ex : ex.message);
			throw ex;
		} else {
            nlapiLogExecution('ERROR', 'Unexpected Error', ex.toString());
            throw nlapiCreateError('99999', ex.toString());
        }
	}
 
}


function EDOrderCustomForm() 
{
	var environment = isProductionEnvironment();
	var IDR_CUSTOMFORM;
	
	if (environment) {
		IDR_CUSTOMFORM = 120;
	} else {
		IDR_CUSTOMFORM = 134;
	}
	
	return IDR_CUSTOMFORM;
}