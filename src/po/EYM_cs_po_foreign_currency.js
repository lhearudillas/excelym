/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       23 May 2018     EYM-013
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Sublist internal id
 * @param {String} name Field internal id
 * @param {Number} linenum Optional line item number, starts from 1
 * @returns {Void}
 */

var IDS_PO_CUSTFORM = 'customform';
var IDR_PO_EX_RATE = 'custbody_eym_po_exrate';
var IDR_PO_FRGN_PRICE = 'custcol_eym_foreign_rate';
var IDR_PO_FRGN_AMT = 'custcol_eym_foreign_amount';
var IDR_PO_FRGN_DISCOUNT = 'custcol_eym_po_forex_discount';
var IDR_PO_QTY = 'quantity';
var IDR_PO_RATE = 'rate';
var IDR_PO_PESO_AMT = 'amount';
var IDR_PO_PESO_DISCOUNT = 'custcol_eym_pur_itemdisc_amount';

var IDS_SL_SCRIPT = 'customscript_sl_eym_po_forex_approval';
var IDS_SL_DEPLOY = 'customdeploy_sl_eym_po_forex_approval';

function clientFieldChanged(type, name, linenum)
{
	var custform = nlapiGetFieldValue(IDS_PO_CUSTFORM);
	
	if(custform == 127) {
	      
		if(name == IDR_PO_FRGN_PRICE) {
			var poExRate = nlapiGetFieldValue(IDR_PO_EX_RATE);
			var foreignPrice = nlapiGetCurrentLineItemValue('item', IDR_PO_FRGN_PRICE);
			var qty = nlapiGetCurrentLineItemValue('item', IDR_PO_QTY);
			var foreignDiscount = nlapiGetCurrentLineItemValue('item', IDR_PO_FRGN_DISCOUNT);
			
			var pesoPrice = poExRate * foreignPrice;
			var foreignAmt = qty * foreignPrice;
			var pesoDiscount = foreignDiscount * poExRate;
			
			nlapiSetCurrentLineItemValue('item', IDR_PO_RATE, pesoPrice);
			//nlapiSetCurrentLineItemValue('item', IDR_PO_FRGN_AMT, foreignAmt);
			nlapiSetCurrentLineItemValue('item', IDR_PO_PESO_DISCOUNT, pesoDiscount);
		}
		
		if(name == IDR_PO_QTY) {
			var qty2 = nlapiGetCurrentLineItemValue('item', IDR_PO_QTY);
			var foreignPrice2 = nlapiGetCurrentLineItemValue('item', IDR_PO_FRGN_PRICE);
			
			var foreignAmt2 = qty2 * foreignPrice2;
			
			//nlapiSetCurrentLineItemValue('item', IDR_PO_FRGN_AMT, foreignAmt2);		
		}
		
		if(name == IDR_PO_EX_RATE) {
			var lineCount = nlapiGetLineItemCount('item');
			
			if(lineCount > 0) {
				var poExRate3 = nlapiGetFieldValue(IDR_PO_EX_RATE);
					
				for(var x=1; x <= lineCount; x++) {
					var foreignPrice3 =	 nlapiGetLineItemValue('item', IDR_PO_FRGN_PRICE, x);
					var qty3 = nlapiGetLineItemValue('item', IDR_PO_QTY, x);
					var pesoPriceVal = nlapiGetLineItemValue('item', IDR_PO_RATE, x);
					var pesoAmtVal = nlapiGetLineItemValue('item', IDR_PO_PESO_AMT, x);
					var foreignDiscount = nlapiGetLineItemValue('item', IDR_PO_FRGN_DISCOUNT, x);

					var pesoPrice3 = poExRate3 * foreignPrice3;
					var foreignAmt3 = qty3 * foreignPrice3;
					var pesoDiscount = foreignDiscount * poExRate3;
						
					nlapiSelectLineItem('item', x)

					nlapiSetCurrentLineItemValue('item', IDR_PO_RATE, pesoPrice3);	
					nlapiSetCurrentLineItemValue('item', IDR_PO_PESO_DISCOUNT, pesoDiscount);
				}
				
			}
			
		}	

    }
}

function approve(recId, recType, appWorkflowStat, requestedBy, crrntapprvr, projCode) 
{
	var params = '&recId=' + recId + '&recType=' + recType + '&status=' + appWorkflowStat + '&requestedBy=' + requestedBy + '&crrntapprvr=' + crrntapprvr + '&projCode=' + projCode;
	
	if(!isNullOrEmpty(requestedBy)) {
		pinCodePopup(IDS_SL_SCRIPT, IDS_SL_DEPLOY, params);
	} else {
		alert('Error: No requesting employee.');
	}
}
