function suitelet(request, response){
	
	if (request.getMethod() == 'GET') {	
		doGet(request, response);
	} else if (request.getMethod() == 'POST') {
		doPost(request, response);
	}
	
}

function doGet(request, response){
	
	var currentLine = Number(request.getParameter('row'))  +1;
	var itemId = request.getParameter('item');
	var taxRate = request.getParameter('tax');
	var unitPrice = request.getParameter('rate');
	var grossAmount = request.getParameter('gross');
	var currentFile = request.getParameter('file');
	var vendor = request.getParameter('vendor');
	var customForm = request.getParameter('form');
	var epRess = getPreferences();
	var poForeignForm = Number(getPreference(epRess, 'po-foreign-form'));
	
	var epRess = getPreferences();
	var mainCss = getPreference(epRess, 'main-css');
	
	if(customForm == poForeignForm){
		taxRate = 0;
	}
	
	var currentItem = '';
	if(itemId){
		currentItem = nlapiLookupField('item',itemId,'itemid');
	}
	
	var jqueryModalJs = 'https://system.netsuite.com/core/media/media.nl?id=536&c=4317943_SB1&h=d715f48c82e22e51d3a5&_xt=.js';
	if(nlapiGetContext().getEnvironment()=='PRODUCTION'){
	  jqueryModalJs = 'https://system.na3.netsuite.com/core/media/media.nl?id=536&c=4317943&h=c1a0fa134372b1c1ccd9&_xt=.js';
	}	
	
	var html = nlapiCreateForm('Discounts');
	
	var disc1 = '';
	var disc1Total = '';
	var disc2 = '';
	var disc2Total = '';
	var disc3 = '';
	var disc3Total = '';
	var disc4 = '';
	var disc4Total = '';
	var disc5 = '';
	var disc5Total = '';
	var discAmnt = '';
	var totalDisc = '';
	var amntIncVat = '';
	var unitIncVat = '';
	var taxAmnt = '';
	var quantity = request.getParameter('qty');
	var basePrice = unitPrice;
	var baseAmount = grossAmount;
	
	if(currentFile){
		var fileRec = nlapiLoadFile(currentFile);
		var values = fileRec.getValue();
			values = JSON.parse(values);
		
			disc1 = values.percent1;
			disc1Total = values.amount1;
			disc2 = values.percent2;
			disc2Total = values.amount2;
			disc3 = values.percent3;
			disc3Total = values.amount3;
			disc4 = values.percent4;
			disc4Total = values.amount4;
			disc5 = values.percent5;
			disc5Total = values.amount5;
			discAmnt = values.discount_amount;
			totalDisc = values.total_discount;
			amntIncVat = values.amount_inc_vat;
			unitIncVat = values.unit_inc_vat;
			taxAmnt = values.tax_amount;
			quantity = values.quantity;
			basePrice = values.base_price;
			baseAmount = values.base_amount;
		
	}
	
	var disabled = 'inline';
	html += '<html>\n';
	html += '<head>\n';
	html += '<link rel="stylesheet" href="'+mainCss+'" type="text/css" media="screen" />\n';
	html += '<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" type="text/css" media="screen" />\n';
	html += '<title>Line Item Details</title>\n';
	html += '<style type="text/css">';
	html += ' input { ';
	html += '  text-align: right; ';
	html += '}';
	html += '</style>';
	html += '</head>\n';
	
	html += '<body>\n';
	html += '<form name="frmDiscount" id="frmDiscount" method="POST" action="">\n';
	html += '<header class="actions stick-to-top">\n';
	html += '	<div class="card">\n';
		 html += '		<div class="card-body"><div class="row"><div class="col">\n';
		 html += '    <input type="submit" class="btn btn-primary" value="Save" name="btnSave" id="btnSave">\n';
	//html += '<main class="main-content">\n';
	html += '<div style="padding-top:25" ></div>\n';
	html += ' <div class="container-fluid">\n';
	html += '	 <div class="row">\n';
	html += '		<div class="col">\n';
	html += '           <input type="hidden" name="custpage_tax_rate" id="custpage_tax_rate" value="' + taxRate + '">';
	html += '			<div class="col-sm-16">';
	html += '				<dl class="row">\n';
	html += '					<dt class="col-md-2">Line No.</dt>\n';
	html += '					<dt class="col-md-4">'+ currentLine +'</dt>\n';
	html += '				</dl>\n';
	html += '				<dl class="row">\n';
	html += '					<dt class="col-md-2">Item</dt>\n';
	html += '					<dt class="col-md-4">'+ currentItem +'</dt>\n';
	html += '				</dl>\n';
	html += '				<dl class="row">\n';
	html += '					<dt class="col-md-2">Qty</dt>\n';
	html += '					<dt class="col-md-3"><input type="number" step="0.01" name="custpage_quantity" id="custpage_quantity" value="' +quantity+ '"></dt>\n';
	html += '					<dt class="col-md-2" style="color: RED">DISCOUNTS</dt>\n';
	html += '				</dl>\n';
	html += '				<dl class="row">\n';
	if(customForm == poForeignForm){
		html += '					<dt class="col-md-2">Foreign Base Price</dt>\n';	
	}else{
		html += '					<dt class="col-md-2">Base Price</dt>\n';	
	}
	html += '					<dt class="col-md-3"><input type="number" step="0.01" name="custpage_base_price" id="custpage_base_price" value="' +basePrice+ '"></dt>\n';
	html += '					<dt class="col-md-2">Discount 1 (%)</dt>\n';
	html += '					<dt class="col-md-2"><input type="number" name="custpage_disc1_percent" id="custpage_disc1_percent" value="' + disc1 + '"></dt>\n';
	html += '					<dt class="col-md-2"><input type="number" step="0.01" name="custpage_disc1_total" id="custpage_disc1_total" value="' + disc1Total + '" disabled></dt>\n';
	//Can't getRequestParameter if input type is disabled
	html += '					<dt class="col-md-2"><input type="hidden" name="custpage_disc1_total_p" id="custpage_disc1_total_p" value="' + disc1Total + '" ></dt>\n';
	html += '				</dl>\n';
	html += '				<dl class="row">\n';
	if(customForm == poForeignForm){
		html += '					<dt class="col-md-2">Foreign Base Amount</dt>\n';
	}else{
		html += '					<dt class="col-md-2">Base Amount</dt>\n';
	}
	html += '					<dt class="col-md-3"><input type="number" step="0.01" name="custpage_base_amount" id="custpage_base_amount" value="' +baseAmount+ '"></dt>\n';
	if(customForm == poForeignForm){
		html += '					<dt class="col-md-2">Discount Amount</dt>\n';
		html += '					<dt class="col-md-2"><input type="number" step="0.01" name="custpage_disc_amount" id="custpage_disc_amount" value="' + discAmnt + '"></dt>\n';
	}else{
		html += '					<dt class="col-md-2">Discount 2 (%)</dt>\n';
		html += '					<dt class="col-md-2"><input type="number" name="custpage_disc2_percent" id="custpage_disc2_percent" value="' + disc2 + '"></dt>\n';
		html += '					<dt class="col-md-2"><input type="number" step="0.01" name="custpage_disc2_total" id="custpage_disc2_total" value="' + disc2Total + '" disabled></dt>\n';
		//Can't getRequestParameter if input type is disabled
		html += '					<dt class="col-md-2"><input type="hidden" name="custpage_disc2_total_p" id="custpage_disc2_total_p" value="' + disc2Total + '" ></dt>\n';
	}
	html += '				</dl>\n';
	html += '				<dl class="row">\n';
	html += '					<dt class="col-md-2" style="color: RED">LESS DISCOUNTS</dt>\n';
	html += '					<dt class="col-md-3"></dt>\n';
	if(customForm != poForeignForm){
		html += '					<dt class="col-md-2">Discount 3 (%)</dt>\n';
		html += '					<dt class="col-md-2"><input type="number" name="custpage_disc3_percent" id="custpage_disc3_percent" value="' + disc3 + '"></dt>\n';
		html += '					<dt class="col-md-2"><input type="number" step="0.01" name="custpage_disc3_total" id="custpage_disc3_total" value="' + disc3Total + '" disabled></dt>\n';
		//Can't getRequestParameter if input type is disabled
		html += '					<dt class="col-md-2"><input type="hidden" name="custpage_disc3_total_p" id="custpage_disc3_total_p" value="' + disc3Total + '" ></dt>\n';
	}
	html += '				</dl>\n';
	html += '				<dl class="row">\n';
	html += '					<dt class="col-md-2">Total Discount</dt>\n';
	html += '					<dt class="col-md-2"><input type="text" name="custpage_total_discount" id="custpage_total_discount"  value="' + totalDisc + '" disabled></dt>\n';
	//Can't getRequestParameter if input type is disabled
	html += '					<dt class="col-md-1"><input type="hidden" name="custpage_total_discount_p" id="custpage_total_discount_p" value="' + totalDisc + '" ></dt>\n';
	if(customForm != poForeignForm){
		html += '					<dt class="col-md-2">Discount 4 (%)</dt>\n';
		html += '					<dt class="col-md-2"><input type="number" name="custpage_disc4_percent" id="custpage_disc4_percent" value="' + disc4 + '"></dt>\n';
		html += '					<dt class="col-md-2"><input type="number" step="0.01" name="custpage_disc4_total" id="custpage_disc4_total" value="' + disc4Total + '" disabled></dt>\n';
		//Can't getRequestParameter if input type is disabled
		html += '					<dt class="col-md-2"><input type="hidden" name="custpage_disc4_total_p" id="custpage_disc4_total_p" value="' + disc4Total + '" ></dt>\n';
	}
	html += '				</dl>\n';
	html += '				<dl class="row">\n';
	if(customForm == poForeignForm){
		html += '					<dt class="col-md-2">Foreign Amount</dt>\n';
	}else{
		html += '					<dt class="col-md-2">Amount Inc-VAT</dt>\n';
	}
	html += '					<dt class="col-md-2"><input type="text" name="custpage_amnt_inc_vat" id="custpage_amnt_inc_vat"  value="' + amntIncVat + '" disabled></dt>\n';
	//Can't getRequestParameter if input type is disabled
	html += '					<dt class="col-md-1"><input type="hidden" name="custpage_amnt_inc_vat_p" id="custpage_amnt_inc_vat_p" value="' + amntIncVat + '" ></dt>\n';
	if(customForm != poForeignForm){
		html += '					<dt class="col-md-2">Discount 5 (%)</dt>\n';
		html += '					<dt class="col-md-2"><input type="number" name="custpage_disc5_percent" id="custpage_disc5_percent" value="' + disc5 + '"></dt>\n';
		html += '					<dt class="col-md-2"><input type="number" step="0.01" name="custpage_disc5_total" id="custpage_disc5_total" value="' + disc5Total + '" disabled></dt>\n';
		//Can't getRequestParameter if input type is disabled
		html += '					<dt class="col-md-2"><input type="hidden" name="custpage_disc5_total_p" id="custpage_disc5_total_p" value="' + disc5Total + '" ></dt>\n';
	}
	html += '				</dl>\n';
	html += '				<dl class="row">\n';
	if(customForm == poForeignForm){
		html += '					<dt class="col-md-2">Foreign Price</dt>\n';
	}else{
		html += '					<dt class="col-md-2">Unit Price Inc-VAT</dt>\n';
	}
	html += '					<dt class="col-md-2"><input type="text" name="custpage_unit_inc_vat" id="custpage_unit_inc_vat"  value="' + unitIncVat + '" disabled></dt>\n';
	//Can't getRequestParameter if input type is disabled
	html += '					<dt class="col-md-1"><input type="hidden" name="custpage_unit_inc_vat_p" id="custpage_unit_inc_vat_p" value="' + unitIncVat + '" ></dt>\n';
	if(customForm != poForeignForm){
		html += '					<dt class="col-md-2">Discount Amount</dt>\n';
		html += '					<dt class="col-md-2"><input type="number" step="0.01" name="custpage_disc_amount" id="custpage_disc_amount" value="' + discAmnt + '"></dt>\n';
	}
	html += '				</dl>\n';
	
	html += '				<dl class="row">\n';
	if(customForm != poForeignForm){
		html += '					<dt class="col-md-2">Tax Amount</dt>\n'; 
		html += '					<dt class="col-md-2"><input type="text" name="custpage_tax_total" id="custpage_tax_total"  value="' + taxAmnt + '" disabled></dt>\n';
		//Can't getRequestParameter if input type is disabled
		html += '					<dt class="col-md-1"><input type="hidden" name="custpage_tax_total_p" id="custpage_tax_total_p" value="' + taxAmnt + '" ></dt>\n';
	}else{
		html += '					<dt class="col-md-2"><input type="hidden" name="custpage_tax_total" id="custpage_tax_total"  value="' + taxAmnt + '" disabled></dt>\n';
		//Can't getRequestParameter if input type is disabled
		html += '					<dt class="col-md-1"><input type="hidden" name="custpage_tax_total_p" id="custpage_tax_total_p" value="' + taxAmnt + '" ></dt>\n';
	}
		html += '				</dl>\n';
	
	html += '			</div>';
	
	html += '		</div>';
	html += '	</div>';
	html += ' </div>';
	
	html += '</form>\n';
	html += '</body>\n';
	
	html += '<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>\n';
	html += '<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>\n';
	html += '<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>\n';
	html += '<script type="text/javascript" src="'+jqueryModalJs+'"></script>\n';  
	html += '<script type="text/javascript">\n';
	
	html += ' $(document).ready(function(){\n';
	html += '   var qty = Number( $("#custpage_quantity").val() );';
	html += '	if(isNaN(qty)){qty = 0;}';
	html += '   var basePrice = Number( $("#custpage_base_price").val() );';
	html += '	if(isNaN(basePrice)){basePrice = 0;}';
	html += '	var baseAmount = Number(qty * basePrice).toFixed(2);';
	html += '	$("#custpage_base_amount").val(baseAmount); \n';
	
	html += '   var disc1 = $("#custpage_disc1_percent").val();';
	html += '	if(isNaN(disc1)){disc1 = 0;}';
	html += '   var disc2 = $("#custpage_disc2_percent").val();';
	html += '	if(isNaN(disc2)){disc2 = 0;}';
	html += '   var disc3 = $("#custpage_disc3_percent").val();';
	html += '	if(isNaN(disc3)){disc3 = 0;}';
	html += '   var disc4 = $("#custpage_disc4_percent").val();';
	html += '	if(isNaN(disc4)){disc4 = 0;}';
	html += '   var disc5 = $("#custpage_disc5_percent").val();';
	html += '	if(isNaN(disc5)){disc5 = 0;}';
	html += '  var disc1Percent = Number( disc1 / 100 );';
	html += '  var disc2Percent = Number( disc2 / 100 );';
	html += '  var disc3Percent = Number( disc3 / 100 );';
	html += '  var disc4Percent = Number( disc4 / 100 );';
	html += '  var disc5Percent = Number( disc5 / 100 );';
	html += '  var discAmnt = Number( $("#custpage_disc_amount").val() );';
	html += '	if(isNaN(discAmnt)){discAmnt = 0;}';
	html += '  var grossAmount = Number( $("#custpage_base_amount").val() );';
	html += '  var compute = discountComputation(grossAmount,disc1Percent,disc2Percent,disc3Percent,disc4Percent,disc5Percent,discAmnt);';
	html += '   })\n';
	
	html += '   function discountComputation(grossAmount,disc1,disc2,disc3,disc4,disc5,discAmnt){\n';
	//Variables
	html += '		var disc1Amnt = 0;';
	html += '		var disc2Amnt = 0;';
	html += '		var disc3Amnt = 0;';
	html += '		var disc4Amnt = 0;';
	html += '		var disc5Amnt = 0;';
	html += '		var quantity  = Number($("#custpage_quantity").val());';
	html += '		var taxRate = Number($("#custpage_tax_rate").val() / 100);';
	//Compute for Base Price when Base Amount Changes
	html += '		var basePrice = Number(grossAmount / quantity).toFixed(2);';
	html += ' 			$("#custpage_base_price").val(basePrice); ';
	//Discount 1
	html += '       if(disc1 > 0){ \n';
	html += '			var disc1Amnt = Number(grossAmount * disc1).toFixed(2);';
	html += ' 			$("#custpage_disc1_total").val(disc1Amnt); ';
	html += ' 			$("#custpage_disc1_total_p").val(disc1Amnt); ';
	html += ' 			var newGrossAmnt = Number(grossAmount - disc1Amnt);';
	html += '		}\n';
	html += '		else{ \n';
	html += ' 			$("#custpage_disc1_total").val(0); ';
	html += ' 			$("#custpage_disc1_total_p").val(0); ';
	html +=	'		} \n';
	//Discount 2
	html += '       if(disc2 > 0){ \n';
	html += '			var disc2Amnt = Number(newGrossAmnt * disc2).toFixed(2); \n';
	html += '			$("#custpage_disc2_total").val(disc2Amnt); \n';
	html += ' 			$("#custpage_disc2_total_p").val(disc2Amnt); ';
	html += '			newGrossAmnt = Number(newGrossAmnt - disc2Amnt);';
	html += '		}\n';
	html += '		else{ \n';
	html += ' 			$("#custpage_disc2_total").val(0); ';
	html += ' 			$("#custpage_disc2_total_p").val(0); ';
	html +=	'		} \n';
	//Discount 3
	html += '       if(disc3 > 0){ \n';
	html += '			var disc3Amnt = Number(newGrossAmnt * disc3).toFixed(2); \n';
	html += '			$("#custpage_disc3_total").val(disc3Amnt); \n';
	html += ' 			$("#custpage_disc3_total_p").val(disc3Amnt); ';
	html += '			newGrossAmnt = Number(newGrossAmnt - disc3Amnt);';
	html += '		}\n';
	html += '		else{ \n';
	html += ' 			$("#custpage_disc3_total").val(0); ';
	html += ' 			$("#custpage_disc3_total_p").val(0); ';
	html +=	'		} \n';
	//Discount 4
	html += '       if(disc4 > 0){ \n';
	html += '			var disc4Amnt = Number(newGrossAmnt * disc4).toFixed(2); \n';
	html += '			$("#custpage_disc4_total").val(disc4Amnt); \n';
	html += ' 			$("#custpage_disc4_total_p").val(disc4Amnt); ';
	html += '			newGrossAmnt = Number(newGrossAmnt - disc4Amnt);';
	html += '		}\n';
	html += '		else{ \n';
	html += ' 			$("#custpage_disc4_total").val(0); ';
	html += ' 			$("#custpage_disc4_total_p").val(0); ';
	html +=	'		} \n';
	//Discount 5
	html += '       if(disc5 > 0){ \n';
	html += '			var disc5Amnt = Number(newGrossAmnt * disc5).toFixed(2); \n';
	html += '			$("#custpage_disc5_total").val(disc5Amnt); \n';
	html += ' 			$("#custpage_disc5_total_p").val(disc5Amnt); ';
	html += '			newGrossAmnt = Number(newGrossAmnt - disc5Amnt);';
	html += '		}\n';
	html += '		else{ \n';
	html += ' 			$("#custpage_disc5_total").val(0); ';
	html += ' 			$("#custpage_disc5_total_p").val(0); ';
	html +=	'		} \n';
	//Value For Total Discount
	html += '	if(isNaN(disc1Amnt)){disc1Amnt = 0;}';
	html += '	if(isNaN(disc2Amnt)){disc2Amnt = 0;}';
	html += '	if(isNaN(disc3Amnt)){disc3Amnt = 0;}';
	html += '	if(isNaN(disc4Amnt)){disc4Amnt = 0;}';
	html += '	if(isNaN(disc5Amnt)){disc5Amnt = 0;}';
	html += '	if(isNaN(discAmnt)){discAmnt = 0;}';
	
	html += '      var totalDiscount = Number (Number(disc1Amnt) + Number(disc2Amnt) + Number(disc3Amnt) + Number(disc4Amnt) + Number(disc5Amnt) + Number(discAmnt) ).toFixed(2); ';
	html += '		var amntIncVat = Number(grossAmount - totalDiscount).toFixed(2); ';
	html += '		if(isNaN(amntIncVat)){amntIncVat = 0;}';
	html += '		var unitIncVat = Number( amntIncVat / quantity ).toFixed(2);';
	html += '		if(isNaN(unitIncVat)){unitIncVat = 0;}';
	html += '		var amountTax = Number(amntIncVat / (1 + taxRate )).toFixed(2); ';
	html += '		if(isNaN(amountTax)){amountTax = 0;}';
	html += '		var taxAmnt = Number(amountTax * taxRate).toFixed(2);';
	html += '		if(isNaN(taxAmnt)){taxAmnt = 0;}';
	html += ' 		$("#custpage_amnt_inc_vat").val(amntIncVat); ';
	html += ' 		$("#custpage_amnt_inc_vat_p").val(amntIncVat); ';
	html += ' 		$("#custpage_unit_inc_vat").val(unitIncVat); ';
	html += ' 		$("#custpage_unit_inc_vat_p").val(unitIncVat); ';
	html += ' 		$("#custpage_total_discount").val(totalDiscount); ';
	html += ' 		$("#custpage_total_discount_p").val(totalDiscount); ';
	html += ' 		$("#custpage_tax_total").val(taxAmnt); ';
	html += ' 		$("#custpage_tax_total_p").val(taxAmnt); ';
	html += '   }\n';
	
	
	html += '   $(document).on("change","#custpage_disc1_percent, #custpage_disc2_percent, #custpage_disc3_percent, #custpage_disc4_percent, #custpage_disc5_percent, #custpage_disc_amount,#custpage_base_amount", function(){\n'; 
	html += '   var disc1 = $("#custpage_disc1_percent").val();';
	html += '	if(isNaN(disc1)){disc1 = 0;}';
	html += '   var disc2 = $("#custpage_disc2_percent").val();';
	html += '	if(isNaN(disc2)){disc2 = 0;}';
	html += '   var disc3 = $("#custpage_disc3_percent").val();';
	html += '	if(isNaN(disc3)){disc3 = 0;}';
	html += '   var disc4 = $("#custpage_disc4_percent").val();';
	html += '	if(isNaN(disc4)){disc4 = 0;}';
	html += '   var disc5 = $("#custpage_disc5_percent").val();';
	html += '	if(isNaN(disc5)){disc5 = 0;}';
	html += '  var disc1Percent = Number( disc1 / 100 );';
	html += '  var disc2Percent = Number( disc2 / 100 );';
	html += '  var disc3Percent = Number( disc3 / 100 );';
	html += '  var disc4Percent = Number( disc4 / 100 );';
	html += '  var disc5Percent = Number( disc5 / 100 );';
	html += '  var discAmnt = Number( $("#custpage_disc_amount").val() );';
	html += '	if(isNaN(discAmnt)){discAmnt = 0;}';
	html += '  var grossAmount = Number( $("#custpage_base_amount").val() );';
	html += '  var compute = discountComputation(grossAmount,disc1Percent,disc2Percent,disc3Percent,disc4Percent,disc5Percent,discAmnt);';
	
	html += '   });\n';
	
	html += '   $(document).on("change","#custpage_quantity, #custpage_base_price", function(){\n'; 
	html += '   var qty = Number( $("#custpage_quantity").val() );';
	html += '	if(isNaN(qty)){qty = 0;}';
	html += '   var basePrice = Number( $("#custpage_base_price").val() );';
	html += '	if(isNaN(basePrice)){basePrice = 0;}';
	html += '	var baseAmount = Number(qty * basePrice).toFixed(2);';
	html += '	$("#custpage_base_amount").val(baseAmount); \n';
	
	html += '   var disc1 = $("#custpage_disc1_percent").val();';
	html += '	if(isNaN(disc1)){disc1 = 0;}';
	html += '   var disc2 = $("#custpage_disc2_percent").val();';
	html += '	if(isNaN(disc2)){disc2 = 0;}';
	html += '   var disc3 = $("#custpage_disc3_percent").val();';
	html += '	if(isNaN(disc3)){disc3 = 0;}';
	html += '   var disc4 = $("#custpage_disc4_percent").val();';
	html += '	if(isNaN(disc4)){disc4 = 0;}';
	html += '   var disc5 = $("#custpage_disc5_percent").val();';
	html += '	if(isNaN(disc5)){disc5 = 0;}';
	html += '  var disc1Percent = Number( disc1 / 100 );';
	html += '  var disc2Percent = Number( disc2 / 100 );';
	html += '  var disc3Percent = Number( disc3 / 100 );';
	html += '  var disc4Percent = Number( disc4 / 100 );';
	html += '  var disc5Percent = Number( disc5 / 100 );';
	html += '  var discAmnt = Number( $("#custpage_disc_amount").val() );';
	html += '	if(isNaN(discAmnt)){discAmnt = 0;}';
	html += '  var grossAmount = Number( $("#custpage_base_amount").val() );';
	html += '  var compute = discountComputation(grossAmount,disc1Percent,disc2Percent,disc3Percent,disc4Percent,disc5Percent,discAmnt);';
	html += '   });\n';
	

	html += '</script>\n';
	html += '</html>\n';
	
	
	response.write(html);
	
}


function doPost(request, response){
	
	var html = ''; 
	
	var epRess = getPreferences();
	var poForeignForm = Number(getPreference(epRess, 'po-foreign-form'));
	var vendorId = request.getParameter("custpage_vendor");
	//var nsGrossAmount = request.getParameter("custpage_gross_amnt_ns");
	//var newGrossAmount = request.getParameter("custpage_gross_amnt");
	var disc1Percent = request.getParameter("custpage_disc1_percent");
	var disc1Amount = request.getParameter("custpage_disc1_total_p");
	var disc2Percent = request.getParameter("custpage_disc2_percent");
	var disc2Amount = request.getParameter("custpage_disc2_total_p");
	var disc3Percent = request.getParameter("custpage_disc3_percent");
	var disc3Amount = request.getParameter("custpage_disc3_total_p");
	var disc4Percent = request.getParameter("custpage_disc4_percent");
	var disc4Amount = request.getParameter("custpage_disc4_total_p");
	var disc5Percent = request.getParameter("custpage_disc5_percent");
	var disc5Amount = request.getParameter("custpage_disc5_total_p");
	var discountAmnt = request.getParameter("custpage_disc_amount");
	var totalDiscount = Number(request.getParameter("custpage_total_discount_p"));
	var customForm = request.getParameter("form");
	nlapiLogExecution('DEBUG','CUSTOM FORM',customForm);
	var totalDiscFld = 'custcol_eym_pur_itemdisc_amount';
	var amntIncVatFld = 'grossamt';
	if(customForm == poForeignForm){
		totalDiscFld = 'custcol_eym_po_forex_discount';
		amntIncVatFld = 'custcol_eym_foreign_amount';
	}
	
	var discountFileId = request.getParameter("file");
	var quantity = request.getParameter("custpage_quantity");
	var basePrice = request.getParameter("custpage_base_price");
	var baseAmount = request.getParameter("custpage_base_amount");
	var amountIncVat = request.getParameter("custpage_amnt_inc_vat_p");
	var unitIncVat = request.getParameter("custpage_unit_inc_vat_p");
	var taxAmount = request.getParameter("custpage_tax_total_p");
	
	var discount = {};
	discount.quantity = quantity;
	discount.base_price = basePrice;
	discount.base_amount =  baseAmount;
	discount.unit_inc_vat = unitIncVat;
	discount.amount_inc_vat = amountIncVat;
	discount.tax_amount = taxAmount;
	discount.percent1 = disc1Percent;
	discount.amount1 = disc1Amount;
	discount.percent2 = disc2Percent;
	discount.amount2 = disc2Amount;
	discount.percent3 = disc3Percent;
	discount.amount3 = disc3Amount;
	discount.percent4 = disc4Percent;
	discount.amount4 = disc4Amount;
	discount.percent5 = disc5Percent;
	discount.amount5 = disc5Amount;
	discount.discount_amount = discountAmnt;
	discount.total_discount = totalDiscount;
	
	
	var discVal = JSON.stringify(discount);
	
	var jsonRecId = createDiscountFile(vendorId);
	
	nlapiLogExecution('DEBUG','totalDiscount',totalDiscount);
	
	var folderId = 275;
	if(nlapiGetContext().getEnvironment()=='PRODUCTION'){
		folderId = 164;
	}
	
	if(IsNullOrEmpty(discountFileId)){
		
		var fileObj = nlapiCreateFile('discount_file_' + jsonRecId + '.json', 'PLAINTEXT', discVal);		
		fileObj.setFolder(folderId);
		var fileId = nlapiSubmitFile(fileObj);
		
		nlapiSubmitField('customrecord_eym_discounts_json_file', jsonRecId, 'custrecord_json_discount_file', fileId);
		
	}else{
		
		var fileRec = nlapiLoadFile(discountFileId);
		var fileObj = nlapiCreateFile(fileRec.getName() + '.json', 'PLAINTEXT', discVal);		
		fileObj.setFolder(folderId);
		var fileId = nlapiSubmitFile(fileObj);
		
	}
	
	
	html = '<html>';
	html += '<head>';
	html += '<script language="JavaScript">';
	html += 'if (window.opener)';
	html += '{'; 
	
	html += '  window.opener.isinited = true;';
	
	html += '   window.opener.nlapiSetCurrentLineItemValue(\'item\', \'quantity\', \'' + quantity + '\', ' + true + ', ' + true + ');';
	html += '   window.opener.nlapiSetCurrentLineItemValue(\'item\', \''+amntIncVatFld+'\', \'' + amountIncVat + '\', ' + true + ', ' + true + ');';
	html += '   window.opener.nlapiSetCurrentLineItemValue(\'item\', \''+ totalDiscFld +'\', \'' + totalDiscount + '\', ' + true + ', ' + true + ');';	
	html += '   window.opener.nlapiSetCurrentLineItemValue(\'item\', \'custcol_eym_po_gross_price\', \'' + unitIncVat + '\', ' + true + ', ' + true + ');';
	html += '   window.opener.nlapiSetCurrentLineItemValue(\'item\', \'custcol_eym_pur_item_base_unit\', \'' + basePrice + '\', ' + true + ', ' + true + ');';
	
	
	//For Foreign Price
	html += '   window.opener.nlapiSetCurrentLineItemValue(\'item\', \'custcol_eym_foreign_rate\', \'' + unitIncVat + '\', ' + true + ', ' + true + ');';
	html += '   window.opener.nlapiSetCurrentLineItemValue(\'item\', \'custcol_eym_pur_item_base_amount\', \'' + baseAmount + '\', ' + true + ', ' + true + ');';	
	html += '   window.opener.nlapiSetCurrentLineItemValue(\'item\', \'custcol_eym_discount_json\', \'' + fileId + '\', ' + true + ', ' + true + ');';	
	html += '   window.opener.isinited = true;';        
	html += '   window.opener.nlapiCommitLineItem(\'item\');';
	
	html += '}';    
    html += '';
    html += '';
	
    html += 'window.close();';
    html += '</script>';
    html += '</head>';
    html += '<body>';
    html += '</body>';
    html += '</html>';

    response.write(html);
	
}

function createDiscountFile(vendorId){
	
	var rec = nlapiCreateRecord('customrecord_eym_discounts_json_file');
	
	rec.setFieldValue('custrecord_json_vendor', vendorId);
	
	return nlapiSubmitRecord(rec, true, true);
	
}
