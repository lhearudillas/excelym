/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       27 Feb 2018     Lhea
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function previewVoucher(request, response){
	var recordType = request.getParameter('type');
	var recordId = request.getParameter('id');
	var html = nlapiGetContext().getSetting('SCRIPT', 'custscript_print_voucher');

	var loadRecord = nlapiLoadRecord(recordType, recordId);
	var cvNumValue = loadRecord.getFieldValue('custbody_eym_payment_cv_num');
	var cvNum = isEmpty(cvNumValue) ? '' : cvNumValue;

	var entityId = loadRecord.getFieldValue('entity');
	var entityType = isEmpty(entityId) ? '' : nlapiLookupField('entity', entityId, 'recordtype');
	var entity = isEmpty(entityType) ? '' : nlapiLookupField(entityType, entityId, 'entityid');

	var date = loadRecord.getFieldValue('trandate');
	var trandate = isEmpty(date) ? '' : date;

  	// Get the check number.
  	var checkNo = loadRecord.getFieldValue('tranid');
  	var checkNum = isEmpty(checkNo) ? '' : checkNo;

	var apRemarks = loadRecord.getFieldValue('custbody_eym_ap_remarks');
	var remarks = isEmpty(apRemarks) ? '' : apRemarks;

	var checkedById = loadRecord.getFieldValue('custbody_eym_ap_checked');
	var checkedBy = isEmpty(checkedById) ? '' : nlapiLookupField('employee', checkedById, 'entityid');

	var approvedById = loadRecord.getFieldValue('custbody_eym_ap_approved');
	var approvedBy = isEmpty(approvedById) ? '' : nlapiLookupField('employee', approvedById, 'entityid');

	var preparedById = loadRecord.getFieldValue('custbody_eym_ap_prepared');
	var preparedBy = isEmpty(preparedById) ? '' : nlapiLookupField('employee', preparedById, 'entityid');

  	// Initialize a variable to hold the total amount in words.
  	var totalAmountInWords = null;

	if (recordType == 'vendorpayment') {
		var vendorTotal = loadRecord.getFieldValue('total');
      	totalAmountInWords = vendorTotal;
		var total = isEmpty(vendorTotal) ? '' : addCommas(vendorTotal);
		var applyCount = loadRecord.getLineItemCount('apply');

		if (applyCount > 0) {
			var billId = loadRecord.getLineItemValue('apply', 'internalid', 1);

			if (billId) {
				var projCodeId = nlapiLookupField('vendorbill', billId, 'custbody_eym_pr_projcode');

				if (projCodeId) {
					// var projCodeValue = nlapiLookupField('job', projCodeId, 'entityid');
                  	var projCodeValue = nlapiLookupField('job', projCodeId, 'companyname');
					var projCode = isEmpty(projCodeValue) ? '' : projCodeValue;
				} else {
					var projCode = '';
				}
			}
		} else {
			var projCode = '';
		}
	}

	if (recordType == 'check') {
		var checkTotal = loadRecord.getFieldValue('usertotal');
      	totalAmountInWords = checkTotal;
		var total = isEmpty(checkTotal) ? '' : addCommas(checkTotal);

      	// This code was added by ROY SELIM for PNID-449
		var expenseTotalCount = loadRecord.getLineItemCount( 'expense' );

		// Initialize project ID.
		var projCodeId = null;

		// Check if there are line items.
		if( expenseTotalCount > 0 ) {

			// Get the project ID from the first line.
			projCodeId = loadRecord.getLineItemValue( 'expense', 'customer', 1 );
		}

		// var projCodeId = loadRecord.getFieldValue('custbody_eym_pr_projcode');
		// var projCode = isEmpty(projCodeId) ? '' : nlapiLookupField('job', projCodeId, 'entityid');
      	var projCode = isEmpty(projCodeId) ? '' : nlapiLookupField('job', projCodeId, 'companyname', false);
	}

  	// Code below edited by ROY SELIM for PNID-395
	var glImpact = getGlImpact(recordType, recordId);
  
	// Append GL impact with total.
  	var totalAmt = getTotal( glImpact );
  	glImpact.push( totalAmt );
  
  	// Append amount in words.
  	// var amtSplit = totalAmt.credit.split('.');
  	var amtSplit = totalAmountInWords.split('.');
  	var amtWhole = amtSplit[0];
  	var amtDecimal = amtSplit[1];
  	/* var amtWords = numberToEnglish( Number(amtWhole) ) + ' pesos ' + ((amtSplit[1] == undefined || Number(amtSplit[1]) == 0)? '' :
    	' and ' + numberToEnglish( Number(amtDecimal) ) + ' cents');
    */

  	var amtWords = numberToEnglish( Number(amtWhole) ) + ' Pesos ' + ((amtSplit[1] == undefined || Number(amtSplit[1]) == 0)? '' :
    	' And ' + amtDecimal + '/100 ');
  	amtWords += ' Only';

  	/* Remove the amount in words in the gl impact items.
	glImpact.push({
		accountId: null,
      	account: '<b>Total amount in words:		</b>' + amtWords,
		debit: '0,0',
		credit: '0,0'
	});
    */
  	// Code edit ends here.
  
	var glImpactLength = glImpact.length;
	var concat = '';
	var concatContinuation = '';
	
	if (glImpactLength > 0) {
		if (glImpactLength > 8) {
			glImpactLength = 8;
			
			var glImpactOriginalLength = glImpact.length;
			concatContinuation += "<div id='glImpactContinuation'>";
			concatContinuation += "<TABLE>";
			
			for (var i = 8; i < glImpactOriginalLength; i++) {
				var account = glImpact[i].account;
				var debit = glImpact[i].debit;
				var credit = glImpact[i].credit;
				
				if (!isEmpty(account) && (!isEmpty(debit) || !isEmpty(credit))) {
					concatContinuation += htmlConcat(account, debit, credit);
				}
			}
			
			concatContinuation += "</TABLE>";
			concatContinuation += "</DIV>";
		}
		
		for (var i = 0; i < glImpactLength; i++) {
			var account = glImpact[i].account;
			var debit = glImpact[i].debit;
			var credit = glImpact[i].credit;
			
			if (!isEmpty(account) && (!isEmpty(debit) || !isEmpty(credit))) {
				concat += htmlConcat(account, debit, credit);
			}
		}
	}
	
	html = html.replace('{cvNum}', cvNum);
	html = html.replace('{entity}', entity);
	html = html.replace('{trandate}', trandate);
	html = html.replace('{remarks}', remarks);
	html = html.replace('{total}', total);
	html = html.replace('{projectCode}', projCode);
  	html = html.replace('{check}', checkNum);
    html = html.replace('{amountInWords}', amtWords);
  	// html = html.replace('{amountInWords}', numberToEnglish(4082552));
	html = html.replace('{preparedBy}', preparedBy);
	html = html.replace('{checkedBy}', checkedBy);
	html = html.replace('{approvedBy}', approvedBy);
	html = html.replace('{body}', concat);
	
	if (concatContinuation) {
		html = html.replace('{bodyContinuation}', concatContinuation);
	} else {
		html = html.replace('{bodyContinuation}', '');
	}
	
	response.write(html);
}

function addCommas(nStr) {
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	
	return x1 + x2;
}

// Function getGlImpact added by ROY SELIM
function getGlImpact( recordType, recordId )
{
	var results = nlapiSearchRecord(recordType, null, [
         new nlobjSearchFilter('internalid', null, 'anyof', recordId)
     ], [
         new nlobjSearchColumn('name', 'account', 'group'),
         new nlobjSearchColumn('account', null, 'group'),
         new nlobjSearchColumn('debitamount', null, 'sum'),
         new nlobjSearchColumn('creditamount', null, 'sum')
     ]);

	var ret = (results || []).map(function(line) {
         return {
             account: line.getValue('name', 'account', 'group' ),
             accountId: line.getValue('account', null, 'group' ),
             debit: line.getValue('debitamount', null, 'sum'),
             credit: line.getValue('creditamount', null, 'sum' )
         };
     });
  
  	// Sort the return value.
    return ret.sort(function(a,b){return (b.debit  - a.debit) });
}

function htmlConcat(account, debit, credit) {
	var row = '';
	debit = (debit == null || debit == '') ? '' : nlapiFormatCurrency(debit);
	credit = (credit == null || credit == '') ? '' : nlapiFormatCurrency(credit);
	row += "<TR>";
	row += "<TD width='470' text-align='justify'>" + account + "</TD>";
	row += "<TD width='200'></TD>";
	row += "<TD width='200' vertical-align='top' align='right'>" + addCommas(debit) + "</TD>";
	row += "<TD width='170' vertical-align='top' align='right'>" + addCommas(credit) + "</TD>";
	row += "</TR>";
	return row;
}

function isEmpty(fldValue){
	return fldValue == '' || fldValue == null || fldValue == undefined;
}

// This function was added by ROY SELIM for PNID 395.
function numberToEnglish( n )
{
    var string = n.toString(), units, tens, scales, start, end, chunks, chunksLen, chunk, ints, i, word, words, and = '';

    /* Is number zero? */
    if( parseInt( string ) === 0 ) {
        return 'zero';
    }

    /* Array of units as words */
    units = [ '', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen' ];

    /* Array of tens as words */
    tens = [ '', '', 'Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety' ];

    /* Array of scales as words */
    scales = [ '', 'Thousand', 'Million', 'Billion', 'Trillion', 'Quadrillion', 'quintillion', 'sextillion', 'septillion', 'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quatttuor-decillion', 'quindecillion', 'sexdecillion', 'septen-decillion', 'octodecillion', 'novemdecillion', 'vigintillion', 'centillion' ];

    /* Split user arguemnt into 3 digit chunks from right to left */
    start = string.length;
    chunks = [];
    while( start > 0 ) {
        end = start;
        chunks.push( string.slice( ( start = Math.max( 0, start - 3 ) ), end ) );
    }
    /* Check if function has enough scale words to be able to stringify the user argument */
    chunksLen = chunks.length;
    if( chunksLen > scales.length ) {
        return '';
    }

    /* Stringify each integer in each chunk */
    words = [];
    for( i = 0; i < chunksLen; i++ ) {

        chunk = parseInt( Number(chunks[i]) );

        if( chunk ) {

            /* Split chunk into array of individual integers */
            ints = chunks[i].split( '' ).reverse().map( parseFloat );

            /* If tens integer is 1, i.e. 10, then add 10 to units integer */
            if( ints[1] === 1 ) {
                ints[0] += 10;
            }

            /* Add scale word if chunk is not zero and array item exists */
            if( ( word = scales[i] ) ) {
                words.push( word );
            }

            /* Add unit word if array item exists */
            if( ( word = units[ ints[0] ] ) ) {
                words.push( word );
            }

            /* Add tens word if array item exists */
            if( ( word = tens[ ints[1] ] ) ) {
                words.push( word );
            }

            /* Add 'and' string after units or tens integer if: */
            if( ints[0] || ints[1] ) {

                /* Chunk has a hundreds integer or chunk is the first of multiple chunks */
                if( ints[2] || ! i && chunksLen ) {
                    words.push( and );
                }
            }
            /* Add hundreds word if array item exists */
            if( ( word = units[ ints[2] ] ) ) {
                words.push( word + ' Hundred' );
            }
        }
    }

    return jsUcfirst(words.reverse().join( ' ' ));
}

function jsUcfirst(string)
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function getTotal( values ) {
	var total = {
		accountId: null,
		account: '<b>TOTAL</b>',
		credit: 0,
		debit: 0
	}
	values.forEach( function(value) {
		total['credit'] += Number( value.credit );
		total['debit']	+= Number( value.debit )
	});
  
  	total.credit 	= parseFloat(total.credit).toFixed(2).toString();
  	total.debit 	= parseFloat(total.debit).toFixed(2).toString();
	
	return total;
}