/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       27 Feb 2018     Lhea
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request){
	if (type == 'view') {
		var recordType = nlapiGetRecordType();
		var recordId = nlapiGetRecordId();
		
		 if( recordType == 'vendorpayment' || recordType == 'check') {
			 var paymentVoucher = nlapiResolveURL('SUITELET', 'customscript_sl_eym_print_paymnt_voucher', 'customdeploy_sl_eym_print_paymnt_voucher');
			 paymentVoucher += '&type=' + recordType + '&id=' + recordId;
			 
			 var buttonScript = "window.location.assign('"+ paymentVoucher +"')";
			 form.addButton('custpage_eym_add_payment_voucher_button', 'Payment Voucher', buttonScript);
		 } 
	}
}