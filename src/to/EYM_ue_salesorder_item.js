function userEventAfterSubmit(type)
{
	// Run this code on create type.
     if( type == 'create' ){

    	 // Get the record id.
    	 var recId = nlapiGetRecordId();

    	 // Get the record type.
    	 var rectype = nlapiGetRecordType();

    	 // Load the record.
      	 var recordSO = nlapiLoadRecord( rectype, recId );

      	 // Get the number of items.
      	 var count = nlapiGetLineItemCount( 'item' );

      	 // Get the type of custom form.
      	 var customForm = recordSO.getFieldValue( 'customform' );

      	 // Iterate thru all the items.
      	 for(var i = 1; i <= count; i++){

      		 // If custom form is 104, run this code.
             if(customForm == 104){

            	 // Get the item ID.
            	 var itemId = nlapiGetLineItemValue( 'item', 'item', i );

            	 // Get the location.
            	 var location = nlapiGetFieldValue( 'location' );

            	 // If item ID and location are not null, continue the script.
            	 if( !IsNullOrEmpty( itemId ) && !IsNullOrEmpty (location ) ){

            		 // Create a filters array.
            		 var filters = new Array();

            		 // Filter by item ID.
            		 filters[0] = new nlobjSearchFilter('internalid', null, 'anyof', itemId);

            		 // Filter by location.
            		 filters[1] = new nlobjSearchFilter( 'internalid', 'inventoryLocation', 'anyof', location );

            		 // Search a saved search.
            		 var searchRes = nlapiSearchRecord( null, 'customsearch_eym_list_of_item_location', filters, null );

            		 // If there are search results, continue with the script.
            		 if(searchRes){

            			 // Get the average cost.
            			 var avecost = searchRes[0].getValue( 'locationaveragecost' );

            			 // Set the rate for requisition field.
            			 recordSO.setLineItemValue('item','custcol_eym_item_rate_srst', i, avecost);

					}
       			}
            }


            // If the custom form is 108, run this script.
            else if(customForm == 108 || 117){

            	// Get the item ID.
        		var itemId = nlapiGetLineItemValue('item', 'item', i);

        		// Get the location for type 108.
        		if(customForm == 108 ){

        			// Get the location.
        			var location = nlapiGetLineItemValue('item', 'location', i);
        		}

        		// Get the location for type 117
        		else if(customForm == 117 ){

        			// Get the location.
             		location = nlapiGetFieldValue('location');

        		}

        		// Verify if the location variable is not null.
                if(location){

                	// Create a filters array.
            	 	var filters = new Array();

            	 	// Filter by location.
     			    filters[0] = new nlobjSearchFilter('internalid', null, 'anyof', location);

     			    // Create a returns column array.
                	var columns = new Array();

                	// Return the project code field.
		            columns[0] = new nlobjSearchColumn('custrecord_eym_projectcode');

		            // Perform a search for the locations using filters and columns.
     				var locRes = nlapiSearchRecord('location', null, filters, columns);

     				// Check if there are results.
           			if(locRes){

           				// Get the top result.
                        var locName = locRes[0].getValue('custrecord_eym_projectcode');
                    }

           			// Check if item ID and locname are not empty.
        			if(!IsNullOrEmpty(itemId) && !IsNullOrEmpty(locName)){

        				// Create a filters array.
         	   			var filters = new Array();

         	   			// Filter by item ID.
     					filters[0] = new nlobjSearchFilter( 'item', null, 'anyof', itemId );

     					// Filter by locname.
                		filters[1] = new nlobjSearchFilter( 'name', null, 'anyof', locName );

     					// Perform a saved search with filters.
                		var searchRes2 = nlapiSearchRecord(null, 'customsearch_eym_cogs_gl_if', filters, null );

                		// Check if there are results.
        	     		if(searchRes2) {

        	     			// Get the top results for quantity.
                    		var quantity = searchRes2[0].getValue( 'quantity' );

                    		// Get the top results for cogs amount.
                   			var cogsamount = searchRes2[0].getValue( 'cogsamount' );

                   			// Get the absolute vaue.
        	 				var cogsamount2 = Math.abs( cogsamount )

        	 				// Compute the rate amount.
                    		var rateAmount = cogsamount2/quantity;

        	 				// Set the rate for requisition column.
							recordSO.setLineItemValue( 'item','custcol_eym_item_rate_srst', i, rateAmount );
                 		}
             		}
                }
    		}

            // ADDED BY ROY SELIM
            // Check if there is a value in the rate for requisition.
            var rateForReq = recordSO.getLineItemValue( 'item', 'custcol_eym_item_rate_srst', i );

            // Check if the column has a null value.
            if( rateForReq == null ) {

            	// Set the value to zero instead.
            	recordSO.setLineItemValue( 'item','custcol_eym_item_rate_srst', i, 0 );
            }
		}

    	// This line is added by ROY SELIM
    	// 5/4/2018

    	var TOType = null;

    	if( rectype == 'transferorder' ) {

          	// Get the TO type
          	TOType = Number(recordSO.getFieldValue('custbody_eym_transfer_type'));
        }

    	if( TOType != 3 ) {

          	// Submit the record.
       		nlapiSubmitRecord(recordSO, true, true);
        }
	}
}

