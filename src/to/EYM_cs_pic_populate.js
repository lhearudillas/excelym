/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       23 Mar 2018     EYM-013
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Sublist internal id
 * @param {String} name Field internal id
 * @param {Number} linenum Optional line item number, starts from 1
 * @returns {Void}
 */
function clientFieldChanged(type, name, linenum){
  
	if(name == 'location' ){
		var locname = nlapiGetFieldValue('location');

		 var filters = new Array();
  			 filters[0] = new nlobjSearchFilter('internalid', null, 'is', locname);

  		var columns = new Array();
 			 columns[0] = new nlobjSearchColumn('custrecord_eym_loc_pic');
 		 var empResult = nlapiSearchRecord('location', null, filters, columns);

  		if(empResult){
  			 var poNumber = empResult[0].getValue('custrecord_eym_loc_pic');
  
         }
		 nlapiSetFieldValue('custbody_eym_pr_pic',poNumber);
		
	}
  
  
	if(name == 'transferlocation') {
      	var locname2 = nlapiGetFieldValue('transferlocation');

		 var filters = new Array();
  			 filters[0] = new nlobjSearchFilter('internalid', null, 'is', locname2);

  		var columns = new Array();
  			columns[0] = new nlobjSearchColumn('custrecord_eym_loc_pic');
  		var empResult = nlapiSearchRecord('location', null, filters, columns);

  		if(empResult){
   				var poNumber = empResult[0].getValue('custrecord_eym_loc_pic');
         }
		 nlapiSetFieldValue('employee',poNumber);
		
	}
  
  return true;
}
