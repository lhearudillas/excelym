/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       26 Mar 2018     Roy Selim
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 *
 * @returns {Boolean} True to continue save, false to abort save
 */

// Global variables.
var EYM_cs_to_if_and_ir_pin_fromTO		= false;
var EYM_cs_to_if_and_ir_pin_TORec		= null;
var EYM_cs_to_if_and_ir_pin_PIN			= null;

function clientPageInitPNID178(type)
{
    if( type == 'copy' || type == 'edit' ) {

    	// Declare function constants.
      	const TOV_CREATED_FRM 		= 'createdfrom';
      	const TOV_CREATED_FRM_REC 	= 'transferorder';

      	// Get the TO ID.
      	var createdFrom = nlapiGetFieldValue( TOV_CREATED_FRM );

        // Try to load the TO.
        var tryObjTO = tryLoadRecord( TOV_CREATED_FRM_REC, createdFrom );

        // Check the try object
        if( !tryObjTO.error && tryObjTO.value ) {

            // Return a true value;
            EYM_cs_to_if_and_ir_pin_fromTO = true;

          	// Assign the record to a variable.
          	EYM_cs_to_if_and_ir_pin_TORec = tryObjTO.value;

            // Load the needed libraries.
            insertJQUI();
        }
    }
}

function clientSaveRecordPNID178()
{
	// Check if the form creates from TO.
  	if( EYM_cs_to_if_and_ir_pin_fromTO ) {

  		// Declare scope constants.
  	  	const TOV_ITM_FLFLMT	= 'itemfulfillment';
  	  	const TOV_ITM_RCPT		= 'itemreceipt';
  	  	const TOV_PIC_REQ_FLD 	= 'employee';
      	const TOV_PIC_ISS_FLD	= 'custbody_eym_pr_pic';
  	  	const TOV_GEN_ERROR		= 'ERROR: The PIN Code does not match the PIN Code of the Project In-Charge of the ';
      	const TOV_COLOR_THEME	= 'custpage_eym_pnid_178_color_theme';

  	  	// Declare scope variables
  	  	var submitButton		= null;
  	  	var pinErrorStr			= null;
      	var picField			= null;

  		// Get the record type.
  		var recType = nlapiGetRecordType();

  		// Check the record type.
  		if( recType == TOV_ITM_RCPT ) {

  			// Fill up the variables for item receipt.
  			submitButton = 'submitter';
  			pinErrorStr  = TOV_GEN_ERROR + 'destination location.';
			picField 	 = TOV_PIC_REQ_FLD;

  		} else if( recType == TOV_ITM_FLFLMT ) {

  			// Fill up the variables for item receipt.
  			submitButton = 'btn_multibutton_submitter';
  			pinErrorStr  = TOV_GEN_ERROR + 'source location.';
			picField 	 = TOV_PIC_ISS_FLD;
  		}

      	// Get the project in charge to validate.
      	var pic = EYM_cs_to_if_and_ir_pin_TORec.getFieldValue( picField );

      	// Verify if the global pin is correct.
      	if( verifyPIN( pic, EYM_cs_to_if_and_ir_pin_PIN ) ) return true;

      	// Verify if there is a src location project-in-charge.
      	if( pic ) {

            // Call the validate PIN function.
            validatePIN( pic, submitButton, pinErrorStr, 'EYM_cs_to_if_and_ir_pin_PIN' );

          	// Return a false value to stop the process.
          	return false;

        } else {

          	// Return false here to not fulfill the TO.
          	return false;
        }
    }

  	// Always return true at this point to allow save of other IFs.
  	return true;
}