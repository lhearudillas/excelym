/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       27 Mar 2018     Roy Selim
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 *
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */

// Declare global scope variables.
const TOV_AMOUNT_FLD		= 'custcol_eym_item_amount_srst';
const TOV_CREATED_FROM_TO	= 'custbody_eym_downpayment_created_from';
const TOV_ITEM_SUBLIST  	= 'item';
const TOV_QTY_SUBLIST		= 'quantity';
const TOV_RATE_FLD			= 'custcol_eym_item_rate_srst';
const TOV_REC_TYPE			= 'transferorder';
const TOV_TRANS_TYPE		= 'custbody_eym_transfer_type';
const TOV_TO_RETURNED		= 'custcol_eym_to_returned';

function userEventBeforeLoad(type, form, request)
{
    // declare function constants.
    const TOV_AMOUNT_SUBLIST	= 'amount';
  	const TOV_APPLY_TRANS		= 'applyingTransaction';
  	const TOV_BQI_FLD			= 'custcol_eym_pur_bqi';
  	const TOV_BTN_ID			= 'custpage_return_items_btn';
  	const TOV_BTN_LABEL			= 'Return';
    const TOV_CLOSE_BTN_ID		= 'closeremaining';
  	const TOV_DES_LOC_FLD		= 'transferlocation';
    const TOV_DES_LOC_PIC		= 'employee';
    const TOV_DESC_SUBLIST		= 'description';
    const TOV_FROM_TO_PRMS		= 'from_transfer_order';
  	const TOV_FROM_BQI			= 'custcol_eym_from_bqi';
  	const TOV_FULFILL_BTN		= 'process';
  	const TOV_ITM_FLMT			= 'Item Fulfillment';
  	const TOV_ITM_RCPT			= 'Item Receipt';
  	const TOV_RATE_SUBLIST		= 'rate';
    const TOV_RECORD			= 'RECORD';
  	const TOV_REQ_FULFILL		= 'transfer-order-borrowing-request-fulfill';
  	const TOV_RET_FULFILL		= 'transfer-order-borrowing-return-fulfill';
	const TOV_SRC_LOC_FLD 		= 'location';
  	const TOV_SRC_LOC_PIC		= 'custbody_eym_pr_pic';
    const TOV_STATUS_FLD		= 'status';
    const TOV_STATUS_RCVD		= 'Received';
  	const TOV_TO_IF_IR_SS		= 'customsearch_script_eym_to_if_and_ir';
    const TOV_TRANS_TYPE_NEW 	= 3;
  	const TOV_TYPE_FLD			= 'type';
  	const TOV_UNITS_SUBLIST		= 'units';

  	// Declare function variables.
  	var rolePrefStr 			= null;
  	var reqPrefStr				= null;

	// Run this script only when customer is viewing.
	if( type == 'view' ) {

		// Get the transfer type.
		var transferType = nlapiGetFieldValue( TOV_TRANS_TYPE );

		// Check the type of TO.
		if( transferType == 1 ) {

			// Get the preferences.
			rolePrefStr = TOV_REQ_FULFILL;
			reqPrefStr  = TOV_RET_FULFILL;

		} else if( transferType == 3 ) {

			// Get the preferences.
			rolePrefStr = TOV_REQ_FULFILL;
			reqPrefStr  = TOV_REQ_FULFILL;
		}

		// Load the preferences
		var pref = getPreferences();

		// Get the preference based on transfer type
		var getRolePref = getPreference( pref, rolePrefStr );
		var getReqPref	= getPreference( pref, reqPrefStr );

		// Get the status of the TO.
		var toStatus = nlapiGetFieldValue( TOV_STATUS_FLD );

      	// Get the recird ID.
      	var toID = nlapiGetRecordId();

      	// Create a search filter object.
      	var sf = { custbody_eym_downpayment_created_from: { operator: 'is', value: toID } };

      	// Attempt to perform a search
      	var tryTOSearch = trySearchRecord( null, TOV_TO_IF_IR_SS, sf );

		// Get the user role.
		var userRole = nlapiGetRole();

		// Check if the value is 1 for Borrowing only.
		if( transferType && transferType == 1 && toStatus == TOV_STATUS_RCVD && !verifyReturned( TOV_ITEM_SUBLIST, TOV_TO_RETURNED, TOV_QTY_SUBLIST ) ) {

            // Remove the Close Order button.
            form.removeButton( TOV_CLOSE_BTN_ID );

          	// Create a link URL.
            var linkURL = nlapiResolveURL( TOV_RECORD, TOV_REC_TYPE );

          	// Add the ID to the linkURL.
          	linkURL += '&' + TOV_FROM_TO_PRMS + '=' + toID;

          	// Define the button script.
            var buttonScript = "window.location.assign('" + linkURL + "')";

            // Show the return button only to the requester.
            if( getReqPref && userRole == getReqPref ) {

            // Logic bypass for admin testing only.
          	// if( true ){

            	// Add the return button.
            	form.addButton( TOV_BTN_ID, TOV_BTN_LABEL, buttonScript );
            }
		}

		// Check if there is already a return TO.
		if( !tryTOSearch.error && tryTOSearch.value ) {

			// Assign the value to a variable.
			var TOSearch = tryTOSearch.value;

			// Create an applying transaction array variable.
			var applyTransArray = [];

			// Loop thru all the results.
			for( var i = 0; i < TOSearch.length; i++ ) {

				// Get the value of the appyling transaction column.
				var colText = TOSearch[i].getText( TOV_TYPE_FLD, TOV_APPLY_TRANS );

				// Check if the variable is not empty and add to the array.
				if( colText ) applyTransArray.push( colText );
			}

			// Check for Item Fulfillment and Item Receipt.
			if( applyTransArray.indexOf( TOV_ITM_FLMT ) == -1 || applyTransArray.indexOf( TOV_ITM_RCPT ) == -1) {

				// Remove the close order button.
				form.removeButton( TOV_CLOSE_BTN_ID );
			}
		}

		// Check if the fulfill button is showing.
		if( form.getButton( TOV_FULFILL_BTN ) ) {

			// Check if the correct role is viewing.
			if( getRolePref && getRolePref != userRole ) {

				// Do not show the fulfill button.
				form.removeButton( TOV_FULFILL_BTN );
			}
		}

    // If the TO is a create form.
	} else if( type == 'create' ) {

      	// Get the from_transfer_order parameter.
		var fromTO = request.getParameter( TOV_FROM_TO_PRMS );

        // Check if the TO is a return from another TO.
      	if( fromTO ) {

          	// Load the fromTO to pre-populate the new TO.
          	var tryLoadFromTO = tryLoadRecord( TOV_REC_TYPE, fromTO );

          	// Check if there is a record loaded.
          	if( !tryLoadFromTO.error && tryLoadFromTO.value ) {

				// Assign the loaded record to a variable.
              	var fromTORecord = tryLoadFromTO.value;

              	// Set the new TO form's type to borrowing-return.
              	nlapiSetFieldValue( TOV_TRANS_TYPE, TOV_TRANS_TYPE_NEW );

              	// Get the from location issuer.
              	var toLocationRequester = fromTORecord.getFieldValue( TOV_DES_LOC_FLD );

              	// Set the new TO form's from location issuer.
              	nlapiSetFieldValue( TOV_SRC_LOC_FLD, toLocationRequester );

              	// Get the PIC of the issuer.
              	var requesterPIC = fromTORecord.getFieldValue( TOV_DES_LOC_PIC );

              	// Set the new forms issuer PIC to the old TO's requester PIC.
              	nlapiSetFieldValue( TOV_SRC_LOC_PIC, requesterPIC );

              	// Get the PIC of the issuer.
              	var issuerPIC = fromTORecord.getFieldValue( TOV_SRC_LOC_PIC );

              	// Set the new forms issuer PIC to the old TO's requester PIC.
              	nlapiSetFieldValue( TOV_DES_LOC_PIC, issuerPIC );

              	// Get the to location requester.
              	var fromLocationIssuer = fromTORecord.getFieldValue( TOV_SRC_LOC_FLD );

                // Set the new TO form's to location requester.
              	nlapiSetFieldValue( TOV_DES_LOC_FLD, fromLocationIssuer );

              	// Set the original TO number.
              	nlapiSetFieldValue( TOV_CREATED_FROM_TO, fromTO );

              	// Get all the items from the old TO.
              	var oldTOItems = fromTORecord.getLineItemCount( TOV_ITEM_SUBLIST );

              	// Initialize a tracker object to combine same items.
              	var trackSameItems = {};
              	
              	// Create a loop counter variable.
              	var loopCounter = 1;

                // Loop thru all the items.
				for( var i = 1; i <= oldTOItems; i++ ) {

                  	// Get the quantity.
                  	var qty = Number( fromTORecord.getLineItemValue( TOV_ITEM_SUBLIST, TOV_QTY_SUBLIST, i ) );

                  	// Get the rate per line.
                  	var rate = fromTORecord.getLineItemValue( TOV_ITEM_SUBLIST, TOV_RATE_FLD, i );

                  	// Get the item.
                  	var item = fromTORecord.getLineItemValue( TOV_ITEM_SUBLIST, TOV_ITEM_SUBLIST, i );

					// Get the replaced quantity.
					var returnedQty = Number( fromTORecord.getLineItemValue( TOV_ITEM_SUBLIST, TOV_TO_RETURNED, i ) );
					
                  	// Initialize the tracker.
                  	if( trackSameItems[item] == undefined ) trackSameItems[item] = { line: null, qty: 0, ret: 0 };

                  	// Update the tracker.
                  	trackSameItems[item].qty += qty;
                  	trackSameItems[item].ret += returnedQty;

					// Get the remaining quantity to be returned per line.
					var toBeReturned = trackSameItems[item].qty - trackSameItems[item].ret;

					// Compute the returned amount.
					var returnedAmount = toBeReturned * rate;

					// Add only to the line item those that has not yet been fully returned.
					if( toBeReturned > 0 ) {

						// Check if the item has already been tracked.
						if( trackSameItems[item].line == null ) {

							// Assign a line item.
							trackSameItems[item].line = loopCounter;

							// Increment the loop counter.
							loopCounter += 1;
						}

						// Determine the line item the values are to be inserted.
						var lineItem = trackSameItems[item].line;

	                  	// Add item to the new form.
	                  	nlapiSetLineItemValue( TOV_ITEM_SUBLIST, TOV_ITEM_SUBLIST, lineItem, item );

	                    // Add qty to the new form.
	                  	nlapiSetLineItemValue( TOV_ITEM_SUBLIST, TOV_QTY_SUBLIST, lineItem, toBeReturned );

	                  	// Set the transfer price and the amount to 0.
	                  	nlapiSetLineItemValue( TOV_ITEM_SUBLIST, TOV_RATE_SUBLIST, lineItem, 0 );
	                    nlapiSetLineItemValue( TOV_ITEM_SUBLIST, TOV_AMOUNT_SUBLIST, lineItem, 0 );

	                  	// Get the units from the old TO.
	                  	var oldUnits = fromTORecord.getLineItemValue( TOV_ITEM_SUBLIST, TOV_UNITS_SUBLIST, i );

	                  	// Set the units on the new form.
	                  	nlapiSetLineItemValue( TOV_ITEM_SUBLIST, TOV_UNITS_SUBLIST, lineItem, oldUnits );

	                  	// Get the description of the old item.
	                  	var oldDesc = fromTORecord.getLineItemValue( TOV_ITEM_SUBLIST, TOV_DESC_SUBLIST, i );

	                   	// Set the description of the new form.
	                  	nlapiSetLineItemValue( TOV_ITEM_SUBLIST, TOV_DESC_SUBLIST, lineItem, oldDesc );	                  	
	                  	
						// Get the from bqi.
						var fromBQI = fromTORecord.getLineItemValue( TOV_ITEM_SUBLIST, TOV_FROM_BQI, i );
						
						// Set the to bqi.
						nlapiSetLineItemValue( TOV_ITEM_SUBLIST, TOV_BQI_FLD, lineItem, fromBQI );

	                  	// Set the rate and the amount of the new form.
	                  	nlapiSetLineItemValue( TOV_ITEM_SUBLIST, TOV_RATE_FLD, lineItem, rate );
					}
                }
            }
        }
   	}
}

function userEventAfterSubmit(type)
{
	// Run the script only if the type is create.
	if( type == 'create' ) {

		// Get the id.
		var recordId = nlapiGetRecordId();

		// Try to load the record
		var tryRecord = tryLoadRecord( TOV_REC_TYPE, recordId );

		// Verify if the record is loaded.
		if( !tryRecord.error && tryRecord.value ) {

			// Assign the record to a variable.
			var currentRecord = tryRecord.value;

			// Get the TO transfer type.
			var transferType = Number( currentRecord.getFieldValue( TOV_TRANS_TYPE ) );

			// Continue only if the TO is a return TO.
			if( transferType == 3 ) {

              	// Get the original TO.
				var originalTO = currentRecord.getFieldValue( TOV_CREATED_FROM_TO );

				// Attempt to load the record.
				var tryOrigTO = tryLoadRecord( TOV_REC_TYPE, originalTO );

				// Create a variable to track the items of the orig TO.
				var oldTOItems = {};

				// Check if there is a loaded TO.
				if( !tryOrigTO.error && tryOrigTO.value ) {

					// Assign the loaded record to a variable.
					var fromTORecord = tryOrigTO.value;

					// Get the number of itmes from the original TO.
					var origTOItemCount = fromTORecord.getLineItemCount( TOV_ITEM_SUBLIST );

					// Loop thru all the items in the original TO.
					for( var i = 1; i <= origTOItemCount; i++ ) {

						// Get the item ID on that line.
						var item = fromTORecord.getLineItemValue( TOV_ITEM_SUBLIST, TOV_ITEM_SUBLIST, i );

						// Get the quantity.
						var qty = Number( fromTORecord.getLineItemValue( TOV_ITEM_SUBLIST, TOV_QTY_SUBLIST, i ) );

						// Get the returned qty.
						var ret = Number( fromTORecord.getLineItemValue( TOV_ITEM_SUBLIST, TOV_TO_RETURNED, i ) );

						// Check if the item exists.
						if( oldTOItems[item] == undefined ) {

							// Initialize to zero.
							oldTOItems[item] = 0;
						}

						// Add the value to the current item.
						oldTOItems[item] += ( qty - ret );
					}
				}

				// Get the number of items.
				var lineItems = currentRecord.getLineItemCount( TOV_ITEM_SUBLIST );

				// Loop thru all the items that were saved.
				for( var i = 1; i <= lineItems; i++ ) {

	              	// Get the quantity.
	              	var qty = currentRecord.getLineItemValue( TOV_ITEM_SUBLIST, TOV_QTY_SUBLIST, i );

	              	// Get the rate per line.
	              	var rate = currentRecord.getLineItemValue( TOV_ITEM_SUBLIST, TOV_RATE_FLD, i );

	              	// Get the item
	              	var item = currentRecord.getLineItemValue( TOV_ITEM_SUBLIST, TOV_ITEM_SUBLIST, i );

	              	// Initialize a quantity validation variable.
	              	var qtyValid = true;

	              	// Check the original TO qty.
	              	if( oldTOItems[item] != undefined ) {

	              		// Verify if the return qty is above the original qty.
	              		if( (oldTOItems[item] - qty) < 0 ) qtyValid = false;
	              	}

	              	// Verify if quantity is valid.
	              	if( !qtyValid ) qty = oldTOItems[item];

	              	// Check if the qty and rate are not null.
	              	if( qty && rate ) {

                      	// Set the quantity field.
	              		currentRecord.setLineItemValue( TOV_ITEM_SUBLIST, TOV_QTY_SUBLIST, i, qty );

	              		// Set the amount field.
	              		currentRecord.setLineItemValue( TOV_ITEM_SUBLIST, TOV_AMOUNT_FLD, i, ( qty * rate ) );
	              	}
				}

				// Submit the record.
				nlapiSubmitRecord( currentRecord, true, true );
			}
		}
	}
}

function verifyReturned( lineType, returned, quantity )
{
	// Get the number of items in the TO.
	var itemCount = nlapiGetLineItemCount( lineType );

	// Loop thru all the items.
	for( var i = 1; i <= itemCount; i++ ) {

		// Get the quantity returned of the line item.
		var qtyReturned = Number( nlapiGetLineItemValue( lineType, returned, i ) );

		// Get the quantity borrowed on the line.
		var qtyBorrowed = Number( nlapiGetLineItemValue( lineType, quantity, i ) );

		// Check if there are items that are not yet returned completely.
		if( ( qtyBorrowed - qtyReturned ) > 0 ) {

			// Return a false value.
			return false;
		}
	}

	// Return a true value.
	return true;
}