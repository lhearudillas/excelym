/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       27 Mar 2018     Roy Selim
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 *
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only)
 *                      paybills (vendor payments)
 * @returns {Void}
 */

function userEventAfterSubmit(type)
{
	// Run the script only if the type is create.
	if( type == 'create' ) {

		// Declare function constants.
        const TOV_BQI_FLD		= 'custcol_eym_pur_bqi';
        const TOV_COGS_SS		= 'customsearch_eym_cogs_gl_if';
		const TOV_IF_REC_TYPE 	= 'itemfulfillment';2
		const TOV_TO_REC_TYPE	= 'transferorder';
		const TOV_JRNL_REC		= 'journalentry';
		const TOV_CRNCY_PHIL	= '1';
		const TOV_XCHGE_RATE	= "1.00";
		const TOV_CRNCY_FIELD	= 'currency';
		const TOV_XCHGE_FIELD	= 'exchangerate';
		const TOV_TRANS_DATE	= 'trandate';
		const TOV_ITEM_SUBLIST  = 'item';
		const TOV_ITEM_RECORD	= 'inventoryitem';
		const TOV_COGS_ACCT 	= 'cogsaccount';
		const TOV_JE_LINE		= 'line';
		const TOV_JE_ACCT		= 'account';
		const TOV_JE_DEBIT		= 'debit';
		const TOV_JE_CREDIT		= 'credit';
		const TOV_TO_FROM		= 'createdfrom';
		const TOV_RATE_REQ		= 'custcol_eym_item_rate_srst';
		const TOV_SRC_LOC_FLD 	= 'location';
		const TOV_DES_LOC_FLD	= 'transferlocation';
		const TOV_JE_NAME_FLD	= 'entity';
		const TOV_IF_JE_FLD		= 'custbody_eym_link_journentry_auto';
        const TOV_PROJ_CODE		= 'custrecord_eym_projectcode';
        const TOV_QTY_FLD		= 'quantity';
      	const TOV_IS_STR		= 'is';
      	const TOV_FROM_BQI		= 'custcol_eym_from_bqi';
      	const TOV_CREATED_FROM	= 'custbody_eym_downpayment_created_from';

		// Declare function variables.
		var fromItemBQI 	= {};
		var compareObject 	= {};
		var rqtrLocation 	= null;
        var rqtrProjCode	= null;
		var issrLocation 	= null;
      	var issrProjCode	= null;

		// Get the id.
		var recordId = nlapiGetRecordId();

		// Try to load the record
		var tryRecord = tryLoadRecord( TOV_IF_REC_TYPE, recordId );

		// Verify if the record is loaded.
		if( !tryRecord.error && tryRecord.value ) {

			// Assign the record to a variable.
			var record = tryRecord.value;

			// Load the transfer order transaction.
			var transferOrder = record.getFieldValue( TOV_TO_FROM );

			// Try to load the TO.
			var tryLoadTO = tryLoadRecord( TOV_TO_REC_TYPE, transferOrder );

			// Check if the TO was loaded successfully.
			if( !tryLoadTO.error && tryLoadTO.value ) {

				// Assign the record to a variable.
				var toRec = tryLoadTO.value

				// Get the source location.
				rqtrLocation = toRec.getFieldValue( TOV_DES_LOC_FLD );

              	// Get the project code for the requester.
				rqtrProjCode = getValueFromRecord( TOV_SRC_LOC_FLD, rqtrLocation, TOV_PROJ_CODE );

				// Get the destination location.
				issrLocation = toRec.getFieldValue( TOV_SRC_LOC_FLD );

                // Get the project code for the requester.
				issrProjCode = getValueFromRecord( TOV_SRC_LOC_FLD, issrLocation, TOV_PROJ_CODE );

				// Get the number of items.
				var toLineItems = toRec.getLineItemCount( TOV_ITEM_SUBLIST );

				// Loop thru all the TO line items.
				for( var i = 1; i <= toLineItems; i++ ) {

					// Get the item
					var toItem = toRec.getLineItemValue( TOV_ITEM_SUBLIST, TOV_ITEM_SUBLIST, i);

					// Append compare object with rate on the item.
					compareObject[toItem] = toRec.getLineItemValue( TOV_ITEM_SUBLIST, TOV_RATE_REQ, i );
				}

				// Create a journal entry.
				var journal = nlapiCreateRecord( TOV_JRNL_REC );

				// Set the currency.
				journal.setFieldValue( TOV_CRNCY_FIELD, TOV_CRNCY_PHIL );

				// Set the exchange rate.
				journal.setFieldValue( TOV_XCHGE_FIELD, TOV_XCHGE_RATE );

				// Get the date when the item fulfillment was created.
				var date = record.getFieldValue( TOV_TRANS_DATE );

				// Set the journal created date.
				journal.setFieldValue( TOV_TRANS_DATE, date );

              	// This code is added for PNID-407
				journal.setFieldValue( TOV_CREATED_FROM, recordId );

				// Get the number of items.
				var numberOfItems = record.getLineItemCount( TOV_ITEM_SUBLIST );

				// Loop thru all the items.
				for( var i = 1; i <= numberOfItems; i++ ) {

					// Get the IF item.
					var ifItem = record.getLineItemValue( TOV_ITEM_SUBLIST, TOV_ITEM_SUBLIST, i );

					// Load the item.
					var tryItemRecord = tryLoadRecord( TOV_ITEM_RECORD, ifItem );

					// Check if a record is loaded.
					if( !tryItemRecord.error && tryItemRecord.value ) {

						// Assign the record to a variable.
						var itemRecord = tryItemRecord.value;

						// Get the cogs account.
						var cogsAcct = itemRecord.getFieldValue( TOV_COGS_ACCT );

						// Get the rate for the item.
						var ifItemRate = Number( compareObject[ifItem] );

						// Get the quantity of the item.
						var lineQty = record.getLineItemValue( TOV_ITEM_SUBLIST, TOV_QTY_FLD, i );

                      	// Get the item.
						var item = record.getLineItemValue( TOV_ITEM_SUBLIST, TOV_ITEM_SUBLIST, i );

                      	// Get the BQI for debit line.
						var debitBQI = record.getLineItemValue( TOV_ITEM_SUBLIST, TOV_BQI_FLD, i );

						// Create a search filter.
						var ssFilter = { item: { operator: TOV_IS_STR, value: item }, name: { operator: TOV_IS_STR, value: issrProjCode } };

						// Attempt to search the saved search.
						var tryCreditBQI = trySearchRecord( null, TOV_COGS_SS, ssFilter );

						// Compute the amout.
						var lineAmt = lineQty * ifItemRate;

						// Add a debit and credit line
						var debitLine 	= ( i * 2 ) - 1;
						var creditLine 	= i * 2;

						// Add a debit line.
						journal.setLineItemValue( TOV_JE_LINE, TOV_JE_ACCT, debitLine, cogsAcct );
						journal.setLineItemValue( TOV_JE_LINE, TOV_JE_DEBIT, debitLine, lineAmt );
						journal.setLineItemValue( TOV_JE_LINE, TOV_JE_NAME_FLD, debitLine, rqtrProjCode );
						journal.setLineItemValue( TOV_JE_LINE, TOV_BQI_FLD, debitLine, debitBQI );

						// Add a credit line.
						journal.setLineItemValue( TOV_JE_LINE, TOV_JE_ACCT, creditLine, cogsAcct );
						journal.setLineItemValue( TOV_JE_LINE, TOV_JE_CREDIT, creditLine, lineAmt );
						journal.setLineItemValue( TOV_JE_LINE, TOV_JE_NAME_FLD, creditLine, issrProjCode );

                      	// Check if there is a credit bqi value.
						if( !tryCreditBQI.error && tryCreditBQI.value) {

							// Get the value of the BQI.
							var bqiValue = tryCreditBQI.value[0].getValue( TOV_BQI_FLD );

							// Check if there is a value.
							if( bqiValue ) {

								// Add the credit bqi on the JE.
								journal.setLineItemValue( TOV_JE_LINE, TOV_BQI_FLD, creditLine, bqiValue );

								// Set the item-bqi object value.
								fromItemBQI[item] = bqiValue;
							}
						}
					}
				}

				// Add the from BQIs to the TO Record by looping thru the items.
				for( var j = 1; j <= toLineItems; j++ ) {

					// Get the item
					var toItem2 = toRec.getLineItemValue( TOV_ITEM_SUBLIST, TOV_ITEM_SUBLIST, j );

					// Get the bqi.
					var toRecLineBQI = fromItemBQI[toItem2] != undefined ? fromItemBQI[toItem2] : null;

					// Check if there is a BQI value.
					if( toRecLineBQI ) {

						// Add the bqi value to the line field.
						toRec.setLineItemValue( TOV_ITEM_SUBLIST, TOV_FROM_BQI, j, toRecLineBQI );
					}
				}

				// Submit the record.
				nlapiSubmitRecord( toRec );

				// Submit the journal record.
				var jeID = nlapiSubmitRecord( journal );

				// Set the custom related journal entry field.
				record.setFieldValue( TOV_IF_JE_FLD, jeID );

				// Submit the IF record.
				nlapiSubmitRecord( record, true, true );

				// If the journal entry was successfully created, create the project ledger. Added for PNID-420.
				if( jeID ) postProjectLedger( jeID );
			}

		// Log errors
		} else {
			nlapiLogExecution( 'DEBUG', 'Verify if the record is loaded.', 'Record load error.' );
		}
	}
}

function getValueFromRecord( recordInternalId, recordId, recordField )
{
    var returnValue = null;

    // Load the location record.
    var tryRec = tryLoadRecord( recordInternalId, recordId );

    // Check if record was loaded
    if( !tryRec.error && tryRec.value ) {

      // Get the value of the project code.
      returnValue = tryRec.value.getFieldValue( recordField );
    }

  	return returnValue;
}

// Added for PNID-420
function postProjectLedger( jeID )
{
	// Verify if the je ID is not empty.
	if( jeID ) {

		// Declare scope variables.
		var bqiArray = [];

		// Declare scope constants.
		const TOV_JRNL_REC	= 'journalentry';
		const TOV_JE_LINE	= 'line';
		const TOV_PL_REC	= 'customrecord_eym_proj_ledger_entry';

		// Added for PNID-494
		const TOV_JE_DATE	= 'trandate';

		// Load the je record.
		var tryJERec = tryLoadRecord( TOV_JRNL_REC, jeID );

		// Check the loaded record.
		if( !tryJERec.error && tryJERec.value ) {

			// Assign the record to a variable
			var jeRec = tryJERec.value;

			// This is added for PNID-494
			var transDate = jeRec.getFieldValue( TOV_JE_DATE );

			// Get the number of lines in the JE.
			var jeLines = jeRec.getLineItemCount( TOV_JE_LINE );

			// Loop thru all the line items.
			for( var i = 1; i <= jeLines; i++ ) {

    			// Get field values from the journal entry.
    			var accountId 	= jeRec.getLineItemValue( TOV_JE_LINE, 'account', i );
    			var debit		= jeRec.getLineItemValue( TOV_JE_LINE, 'debit', i );
    			var credit		= jeRec.getLineItemValue( TOV_JE_LINE, 'credit', i );
    			var bqi			= jeRec.getLineItemValue( TOV_JE_LINE, 'custcol_eym_pur_bqi', i );
    			var project		= jeRec.getLineItemValue( TOV_JE_LINE, 'entity', i );

    			// Add the BQI to the array.
    			if( bqi && bqiArray.indexOf( bqi ) == -1 ) bqiArray.push( bqi );

    			// Create a project ledger record.
    			var pl = nlapiCreateRecord( TOV_PL_REC );

				// Set the values.
    			pl.setFieldValue( 'custrecord_eym_ple_tran_id',	 	jeID );
    			pl.setFieldValue( 'custrecord_eym_ple_acc_num',	 	accountId );
    			pl.setFieldValue( 'custrecord_eym_ple_debit',	 	debit );
    			pl.setFieldValue( 'custrecord_eym_ple_credit',	 	credit );
    			pl.setFieldValue( 'custrecord_eym_ple_bqi',		 	bqi );
    			pl.setFieldValue( 'custrecord_eym_ple_proj_code', 	project );

    			// This is added for PNID-494
    			pl.setFieldValue( 'custrecord_eym_ple_date', 		transDate );

    			// Save the project ledger.
    			var plId = nlapiSubmitRecord( pl );
			}

			// Update all the affected BQIs
			for( var j = 0; j < bqiArray.length; j++ ) {

				// Update the bqi usage.
				updateBQIUsage( bqiArray[j] );
			}
		}
	}
}

// Added for PNID-420
function updateBQIUsage( bqi )
{
  	// Initialize scope variables.
	var usage = null;

	// Try to load the bqi record.
	var tryBqi = tryLoadRecord( 'customrecord_eym_boq', bqi );

	// Check if the bqi record was loaded.
	if( !tryBqi.error && tryBqi.value ) {

		// Assign the record to a variable.
		var bqiRecord = tryBqi.value;

		// Create a search filter.
		var filters = { custrecord_eym_ple_bqi: { operator: 'is', value: bqi } };

		// Create a columns return array.
		var columns = [
			[
				'custrecord_eym_ple_bqi',
				null,
				'group'
			],
			[
				'custrecord_eym_ple_debit',
				null,
				'sum'
			],
			[
				'custrecord_eym_ple_credit',
				null,
				'sum'
			]
		];

		// Try to get all usage from PLES.
		var tryUsage = trySearchRecord( 'customrecord_eym_proj_ledger_entry', null, filters, columns );

		// Check the return value.
		if( !tryUsage.error && tryUsage.value ) {

			// Assign the value to a variable.
			var usage = tryUsage.value[0];

			// Get the total debit
			var totalDebit = usage.getValue( 'custrecord_eym_ple_debit', null, 'sum' );

			// Get the total credit.
			var totalCredit = usage.getValue( 'custrecord_eym_ple_credit', null, 'sum' );

			// To comute BQI usage from all PLEs, subtract total credit from total debit.
			usage = totalDebit - totalCredit;
		}

	    // Update the record.
		if( usage ){

			// Update the record.
		    bqiRecord.setFieldValue( 'custrecord_eym_bqi_usage', usage );

		    // Update the record.
		    nlapiSubmitRecord( bqiRecord );
		}
	}
}