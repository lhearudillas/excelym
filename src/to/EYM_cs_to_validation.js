/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       23 Mar 2018     Roy Selim
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 *
 * @param {String} type Sublist internal id
 * @param {String} name Field internal id
 * @param {Number} linenum Optional line item number, starts from 1
 * @returns {Boolean} True to continue changing field value, false to abort value change
 */
// Global variables.
var saveErrors = {};

function clientValidateField(type, name, linenum){

  	// Declare function constants.
  	const TOV_PROJ_LOC_SS 	= 'customsearch_script_eym_project_location';
  	const TOV_LOC			= 'location';
  	const TOV_TRANS_LOC		= 'transferlocation';
  	const TOV_TRANS_TYPE	= 'custbody_eym_transfer_type';
  	const TOV_INVALID_LOC	= 'Field value error! location must be a valid project location and not a CW or excess locations. ';
  	const TOV_OP_IS			= 'is';
  	const TOV_FROM_TO		= 'custbody_eym_downpayment_created_from';
  	const TOV_INVALID_TYPE	= 'Field value error! type \'Borrowing-Return\' is only allowed if there is a corresponding \'Borrowing-Request\' reference. ';

	// Run this validation field if name matches to any of the following.
  	var fieldNamesToValidate = [ TOV_LOC, TOV_TRANS_LOC, TOV_TRANS_TYPE ];

  	// Check if the name variable passed by the event is in the array.
  	if( fieldNamesToValidate.indexOf( name ) != -1 ) {

		// For locations, values must not be from exess.
      	if( name == TOV_LOC || name == TOV_TRANS_LOC ) {

            // Get the new value.
            var newFieldValue = nlapiGetFieldText( name );

          	// Create a search filter.
          	var filter = { name: { operator: TOV_OP_IS, value: newFieldValue } };

            // Load the saved search for project locations.
            var tryLoadSearch = trySearchRecord( null, TOV_PROJ_LOC_SS, filter );

          	// Check for errors.
          	if( !tryLoadSearch.error && !tryLoadSearch.value ) {

          		// Call the error task.
          		return errorTask( name, TOV_INVALID_LOC );

            } else {
              	saveErrors[name] = { value: true };
            }
        } else if( name == TOV_TRANS_TYPE ) {

        	// Get the value.
        	var transTypeValue = nlapiGetFieldValue( name );

    		// Get the value of the original TO.
    		var fromTO = nlapiGetFieldValue( TOV_FROM_TO );

        	// Check if the type is borrowing-return.
        	if( transTypeValue == 3 && fromTO == '' ) {

          		// Call the error task.
          		return errorTask( name, TOV_INVALID_TYPE );

            } else {
              	saveErrors[name] = { value: true };
            }
        }
    }

  	return true;
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 *
 * @returns {Boolean} True to continue save, false to abort save
 */
function clientSaveRecord(){

  	// Create an alert variable.
  	var forAlert = '';

  	// Create a return value.
	var retValue = true;

	// Check saveErrors.
	for( var n in saveErrors ) {

		if( saveErrors[n] && !saveErrors[n].value ) {

          	// Return a false value.
          	retValue = false;

          	// Append the error alert.
          	forAlert += saveErrors[n].error;
        }
    }

  	// Check if the return value is false.
  	if( !retValue ) {

      	// Alert the error messages.
      	alert( forAlert );
    }

  	// Return a true value.
  	return retValue;
}

function errorTask( name, error )
{
    // Show a message that project location is not valid.
    alert( error );

	// Add to save errors.
	saveErrors[name] = { value: false, error: nlapiGetField( name ).getLabel() + ' ' + error };

	// Return a false value.
	return false;
}