/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       12 Mar 2018     denny
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type){
	nlapiLogExecution('DEBUG','type',type);
	if(type=='create' || type=='edit'){

		var ifId = nlapiGetRecordId();
		// I load the IF because by using nlapiGetLineItemCount('item') the result is wrong, so i use getLineItemCount instead
		var ifObj = nlapiLoadRecord('itemfulfillment',ifId);
		try{
			var soId = ifObj.getFieldValue('createdfrom');
			nlapiLogExecution('DEBUG','soId',soId);
			var createdFromType = nlapiLookupField('transaction',soId,'recordtype');
			if(createdFromType=='salesorder'){
				var so = nlapiLoadRecord('salesorder',soId);
				nlapiLogExecution('DEBUG','soForm',so.getFieldValue('customform'));
				if(so.getFieldValue('customform')==104 || so.getFieldValue('customform')==108 ){  // form = PSC Excess Stock Request
					var location = nlapiLookupField('job',so.getFieldValue('job'),'custentity_eym_proj_loc_whouse');
					var ia = nlapiCreateRecord('inventoryadjustment', {recordmode: 'dynamic'});
					var ifObj = nlapiLoadRecord('itemfulfillment',ifId);
					ia.setFieldValue('customform','109');
					ia.setFieldValue('customer',so.getFieldValue('job'));
					ia.setFieldValue('account',1018);
					ia.setFieldValue('custbody_eym_pr_projcode',so.getFieldValue('job'));
					ia.setFieldValue('memo',ifObj.getFieldValue('memo'));
					ia.setFieldValue('adjlocation',location);
					nlapiLogExecution('DEBUG','location',location);
					var soItem = [];
					for(var i=1; i<=so.getLineItemCount('item'); i++){
						soItem.push({
							'item' : so.getLineItemValue('item','item',i), 
							'qty' : so.getLineItemValue('item','quantity',i),
							'line' : so.getLineItemValue('item','line',i), 
							'requisition_rate' : so.getLineItemValue('item','custcol_eym_item_rate_srst',i)
						})
					}

					for(var i=1; i<=ifObj.getLineItemCount('item'); i++){
						var item = ifObj.getLineItemValue('item', 'item', i);
						var quantity = ifObj.getLineItemValue('item', 'quantity', i);

						ia.selectNewLineItem('inventory');
						ia.setCurrentLineItemValue('inventory', 'item', item);
						ia.setCurrentLineItemValue('inventory', 'adjustqtyby', quantity);
						ia.setCurrentLineItemValue('inventory', 'location', location);
						var useBin = nlapiLookupField('item',item,'usebins');
						if(useBin=='T'){
							var subRec = ia.viewCurrentLineItemSubrecord('inventory','inventorydetail');
					    		if(subRec)
					    			subRec = ia.createCurrentLineItemSubrecord('inventory', 'inventorydetail');
					    		else
					    			subRec = ia.createCurrentLineItemSubrecord('inventory', 'inventorydetail');
							subRec.selectLineItem('inventoryassignment', 1);
							//subRec.setCurrentLineItemValue('inventoryassignment', 'binnumber', binSearch[0].getId()); // set to RB.IB.HOLD	
							subRec.setCurrentLineItemValue('inventoryassignment', 'quantity', quantity);
							subRec.commitLineItem('inventoryassignment');
							subRec.commit();
						}
						ia.commitLineItem('inventory');
					}
					var newIa = nlapiSubmitRecord(ia);
					if(newIa){
						nlapiLogExecution('DEBUG','newIa',newIa);
						nlapiSubmitField('itemfulfillment',ifId,['custbody_eym_link_invadj_auto'],[newIa]);
						if(so.getFieldValue('custbody_eym_req_type')==2){
							var journalRec = nlapiCreateRecord('journalentry');
							journalRec.setFieldValue('subsidiary', so.getFieldValue('subsidiary'));
							journalRec.setFieldValue('currency', 1);

                          	// This code was added by ROY SELIM for PNID-407.
							journalRec.setFieldValue( 'custbody_eym_downpayment_created_from', ifId );
							// code ends here.

				            for(var i=1; i<=ifObj.getLineItemCount('item'); i++){
				            		var item = ifObj.getLineItemValue('item', 'item', i);
								var orderLine = ifObj.getLineItemValue('item', 'orderline', i);
								var lineLoc = ifObj.getLineItemValue('item', 'location', i);
								//var projLoc = nlapiSearchRecord('job',null,new nlobjSearchFilter('custentity_eym_proj_loc_whouse',null,'is',lineLoc));
								var projLoc = nlapiLookupField('location',lineLoc,'custrecord_eym_projectcode');
								if(!projLoc){
									throw nlapiCreateError('MISSING_DATA', 'No project found for location id = '+lineLoc, true);
								}
								var reqRate = 0;
								var reqQty = 1;
				            		for(var j=0; j<soItem.length; j++){
									if(soItem[j].line==orderLine && soItem[j].item==item){
										reqRate = soItem[j].requisition_rate;
										reqQty = soItem[j].qty;
										break;
									}
								}
				            		nlapiLogExecution('DEBUG','credit',projLoc);
				            		nlapiLogExecution('DEBUG','debit',so.getFieldValue('job'));
								var itemCogsAcc = nlapiLookupField('inventoryitem',item,'expenseaccount');

                              	// This code is added by ROY B SELIM for PNID 387. 
								// First get the destination BQI.
								var desBQI = ifObj.getLineItemValue( 'item', 'custcol_eym_pur_bqi', i );

								// Initialize a filter array and source bqi variables.
								var ssFilter 	= [];
								var srcBQI 		= null;

								// Add a filter for the item.
								ssFilter.push( new nlobjSearchFilter( 'item' , null, 'is', item ) );
								ssFilter.push( new nlobjSearchFilter( 'name' , null, 'is', projLoc ) );

								// Load the saved search.
								var BQISearchResults = nlapiSearchRecord( null, 'customsearch_eym_cogs_gl_if', ssFilter, null );

								// Check if there is a result.
								if( BQISearchResults ) {

									// Get the src bqi.
									srcBQI = BQISearchResults[0].getValue( 'custcol_eym_pur_bqi' );
								}
								// Code edit ends here.


								journalRec.selectNewLineItem('line');
								journalRec.setCurrentLineItemValue('line', 'account', itemCogsAcc);
					            journalRec.setCurrentLineItemValue('line', 'credit', reqRate * reqQty);
					            journalRec.setCurrentLineItemValue('line', 'entity', projLoc);

                         		// Code edit starts here. Added by ROY SELIM
					            if( srcBQI )
					            	journalRec.setCurrentLineItemValue( 'line', 'custcol_eym_pur_bqi', srcBQI );
					            // Code edit ends here.

					            journalRec.commitLineItem('line');

					            journalRec.selectNewLineItem('line');
					            journalRec.setCurrentLineItemValue('line', 'account', itemCogsAcc);
					            journalRec.setCurrentLineItemValue('line', 'debit', reqRate * reqQty);
					            journalRec.setCurrentLineItemValue('line', 'entity', so.getFieldValue('job'));

                              	// Code edit starts here. Added by ROY SELIM
				            	journalRec.setCurrentLineItemValue( 'line', 'custcol_eym_pur_bqi', desBQI );
					            // Code edit ends here.

					            journalRec.commitLineItem('line');
							}
				            var jeId = nlapiSubmitRecord(journalRec, true);

				            // This code was added by ROY SELIM for PNID-407
				            if( jeId && ifId ) {
				            	excessLedgerEntry( ifId, jeId );
				            }
				            // Code edit ends here.

				            nlapiLogExecution('DEBUG','jeId',jeId);
			                nlapiSubmitField('itemfulfillment', ifId, 'custbody_eym_link_journentry_auto', jeId);
						}
					}
				}
			}
		}catch(e){
			var errMsg = '';
			if(e instanceof nlobjError)
				errMsg = e.getCode()+': '+e.getDetails();
	        else
	        		errMsg = 'UNEXPECTED ERROR: '+e.toString();	
			nlapiLogExecution('DEBUG','Error', 'IF/'+ifId+' '+errMsg);
		}
	}
}
