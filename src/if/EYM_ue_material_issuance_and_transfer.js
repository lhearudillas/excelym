/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       21 Feb 2018     Lhea
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request){
	if (type == 'create' || type == 'edit') {
		var createdFrom = nlapiGetFieldValue('createdfrom');
		var customForm = nlapiLookupField('salesorder', createdFrom, 'customform');
		var itemCount = nlapiGetLineItemCount('item');
		
		if (customForm == 104) {
			if (itemCount > 0) {
				for (var i = 1; i <= itemCount; i++) {
					var onBudget = nlapiGetLineItemValue('item', 'custcol_eym_so_on_budget', i);
					
					if (onBudget == 'F') {
						nlapiSetLineItemValue('item', 'itemreceive', i, 'F');
					}
				}
			}
		}
		
		if (customForm == 108) {
          var empId = request ? request.getParameter('emp_id') : null;
			
			if (empId != null) {
				nlapiSetFieldValue('custbody_eym_pr_pic', empId);
				var inCharge = nlapiGetFieldValue('custbody_eym_pr_pic');
				
				if (itemCount > 0) {
					for (var i = 1; i <= itemCount; i++) {
						var location = nlapiGetLineItemValue('item', 'location', i);
						var onBudget = nlapiGetLineItemValue('item', 'custcol_eym_so_on_budget', i);
						var locInCharge = nlapiLookupField('location', location, 'custrecord_eym_loc_pic');
						
						if (onBudget == 'F' || inCharge != locInCharge) {
							nlapiSetLineItemValue('item', 'itemreceive', i, 'F');
						}
					}
				}
			} else {
				if (itemCount > 0) {
					for (var i = 1; i <= itemCount; i++) {
						nlapiSetLineItemValue('item', 'itemreceive', i, 'F');
					}
				}
			}
		} 
	}
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type){
	if (type != 'xedit') {
		var createdFrom = nlapiGetFieldValue('createdfrom');
		var createdFromType = nlapiLookupField('transaction', createdFrom, 'recordtype');
		
		if (createdFromType == 'salesorder') {
			var salesOrder = nlapiLoadRecord('salesorder', createdFrom);
			var status = salesOrder.getFieldValue('status');
			
			if (status == 'Pending Billing') {
				var itemCount = salesOrder.getLineItemCount('item');
				
				if (itemCount > 0) {
					for(var i =1 ; i <= itemCount; i++) {
						salesOrder.setLineItemValue('item', 'isclosed', i, 'T');
					}
					nlapiSubmitRecord(salesOrder);
				}
			}
		}
		nlapiLogExecution('DEBUG', 'R-USAGE', nlapiGetContext().getRemainingUsage());
	}
}