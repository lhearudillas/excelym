/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       01 Jun 2018     Roy Selim
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 *
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit( type )
{
	// Run this script on a delete operation.
	if( type == 'delete' ) {

		// Get the record ID.
		var ifId = nlapiGetRecordId();

		// Create a columns search return.
		var columns = [ 'internalid', 'custrecord_eym_ple_bqi' ];

		// Try to get the project ledger entries using the ID.
		var tryPles = getProjectLedgerEntries( ifId, null, columns );

		// Initialize a bqi array.
		var bqiArray = [];

		// Delete all the project ledger entries. Start with looping thru all PLEs
		if( !tryPles.error && tryPles.value ) {

			// Assign the current item to a variable.
			var ples = tryPles.value;

			// Loop thru all the ples
			for( var i = 0; i < ples.length; i++ ) {

				// Get the bqi.
				var bqi = ples[i].getValue( 'custrecord_eym_ple_bqi' );

				// Check if there is a value.
				if( bqi && ( bqiArray.indexOf( bqi ) == -1 ) ) bqiArray.push( bqi );
			}

			// Delete the ples
			deletePles( ples );

			// Update all the affected bqis.
			for( var j = 0; j < bqiArray.length; j++ ) {

				// Update.
				updateBQIUsage( bqiArray[j] );
			}
		}
	}
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only)
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type)
{
	// Run this script on edit operation only.
	if( type == 'edit' ) {

      	// Initialize scope variables.
      	var transRec 	= null;
      	var transType 	= null;

		// Get the IF ID.
		var ifId = nlapiGetRecordId();

		// Additional validations. Get the transaction id.
		var transId = nlapiLookupField( 'itemfulfillment', ifId, 'createdfrom' );

		// Check if this id is from SO.
		var tryTransRec = tryLoadRecord( 'salesorder', transId );

		// If there is a returned value.
		if( !tryTransRec.error && tryTransRec.value ) transRec = tryTransRec.value;

		// Check the type of transaction.
		if( transRec ) transType = transRec.getFieldValue( 'custbody_eym_req_type' );

      	// Run this edit code if transaction is from SO and type is CW and PR.
      	if( transType == 1 || transType == 4 ) {

            // Declare scope variables.
            var gl 		= null;
            var ples	= null;

            // Get the GL impact from the item fulfillment.
            var tryGl = getGLImpact( 'itemfulfillment', null, ifId );

            // Check if there are errors.
            if( !tryGl.error && tryGl.value ) {

                // Assign the gl array to a new variable.
                gl = tryGl.value;

                // Add BQI to the gl.
                gl = addBQIToGL( gl, ifId );
            }

            // Try to get all the PLEs related to this record.
            var tryPles = getProjectLedgerEntries( ifId, null, [ 'internalid', 'custrecord_eym_ple_bqi' ] );

            // Check if there are search results.
            if( !tryPles.error && tryPles.value ) {

                // Assign the project ledger entries to a variable.
                ples = tryPles.value;

                // Initialize argument variables.
                var transId = ifId;
                var projId	= nlapiLookupField( 'itemfulfillment', ifId, 'entity' );

                // Initialize a bqi array.
                var bqiArray = [];

                // Loop thru all the gl impact entries.
                for( var i = 0; i < gl.length; i++ ) {

                    // Initialize a container variable.
                    var glBqi = gl[i].bqi;

                    // If there is a bqi value, push it to the array.
                    if( glBqi && ( bqiArray.indexOf( glBqi ) == -1 ) ) bqiArray.push( glBqi );

                    // Assign a project ledger item to a variable.
                    var plItem = ( ples[i] != undefined ) ? ples[i] : null;

                    // Initialize a ple id variable.
                    var pleId 		= null;
                    var pleRecord 	= null;

                    // Get the id of the project ledger search result.
                    if( plItem ) {

                        // Get the ID.
                        pleId = plItem.getValue( 'internalid' );

                        // Load the project ledger entry record.
                        var trypleRecord = tryLoadRecord( 'customrecord_eym_proj_ledger_entry', pleId );

                        // Check if the project ledger entry was loaded.
                        if( !trypleRecord.error && trypleRecord.value ); {

                            // Assign the record to the variable.
                            pleRecord = trypleRecord.value;
                        }
                    }
                    
                    // Get the transaction date.
                    var transDate = nlapiLookupField( 'itemfulfillment', ifId, 'trandate' );

                    // Enter data to the ledger entry record.
                    ledgerEntry( gl[i], pleRecord, transId, projId, transDate );
                }

                // If there are excess ples, just delete them after IF edit.
                if( ples.length > gl.length ) {

                    // Loop thru all the excess ples and delete them.
                    var delPles = ples.slice( gl.length );

                    // Delete the excess ples
                    deletePles( delPles );

                    // Loop thru all the deleted ples.
                    for( var k = 0; k < delPles.length; k++ ) {

                        // Get the bqi.
                        var delBqi = delPles[k].getValue( 'custrecord_eym_ple_bqi' );

                        // Check if the bqi is already on the array.
                        if ( delBqi && ( bqiArray.indexOf( delBqi ) == -1 ) ) bqiArray.push( delBqi );
                    }
                }

                // Update all bqi usage.
                for( var j = 0; j < bqiArray.length; j++ ) {

                    // Update the bqi usage.
                    updateBQIUsage( bqiArray[j] );
                }

                // If there are no ples, meaning there is no project ledger created yet.
            } else {

                // Create a project ledger and project ledger entries.
                projectLedgerEntry( ifId, null );
            }
        }
	}
}