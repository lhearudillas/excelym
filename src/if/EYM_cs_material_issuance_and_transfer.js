/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       21 Feb 2018     Lhea
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType 
 * 
 * @param {String} type Access mode: create, copy, edit
 * @returns {Void}
 */
function clientPageInit(type, form)
{
	var createdFrom = nlapiGetFieldValue('createdfrom');
	var customForm = nlapiLookupField('salesorder', createdFrom, 'customform');
	
	if (customForm == 108) {
		var recId = '&rec_id=' + createdFrom;
		
		if (window.location.href.indexOf("emp_id") == -1) {
			pinCodePopup('customscript_sl_eym_if_pincode_popup', 'customdeploy_sl_eym_if_pincode_popup', recId);
		}
	}
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Sublist internal id
 * @param {String} name Field internal id
 * @param {Number} linenum Optional line item number, starts from 1
 * @returns {Void}
 */
function clientFieldChanged(type, name, linenum)
{
	if (type == 'item') {
		var createdFrom = nlapiGetFieldValue('createdfrom');
		var customForm = nlapiLookupField('salesorder', createdFrom, 'customform');
		var itemCount = nlapiGetLineItemCount('item');
		var inCharge = nlapiGetFieldValue('custbody_eym_pr_pic');
		
		if (itemCount > 0) {
			if (customForm == 104) {
				for (linenum = 1; linenum <= itemCount; linenum++) {
					var onBudget = nlapiGetLineItemValue('item', 'custcol_eym_so_on_budget', linenum);
					var itemreceive = nlapiGetLineItemValue('item', 'itemreceive', linenum);
					
					if (onBudget == 'F' && itemreceive == 'T') {
						alert('Notice: Fulfilling this item will result to an over budget on the BQI. Please create a Change Order to fulfill this item.');
						nlapiSetCurrentLineItemValue('item', 'itemreceive', 'F');
					}
				}
			}
			
			if (customForm == 108) {
				for (linenum = 1; linenum <= itemCount; linenum++) {
					var onBudget = nlapiGetLineItemValue('item', 'custcol_eym_so_on_budget', linenum);
					var location = nlapiGetLineItemValue('item', 'location', linenum);
					var itemreceive = nlapiGetLineItemValue('item', 'itemreceive', linenum);
					var locInCharge = nlapiLookupField('location', location, 'custrecord_eym_loc_pic');
					
					if (onBudget == 'F' && inCharge == locInCharge && itemreceive == 'T') {
						alert('Notice: Fulfilling this item will result to an over budget on the BQI. Please create a Change Order to fulfill this item.');
						nlapiSetCurrentLineItemValue('item', 'itemreceive', 'F');
					}
					
					if (onBudget == 'F' && inCharge != locInCharge && itemreceive == 'T') {
						alert('Error: You must be the assigned Project In-Charge of the Project / Source Location to fulfill this item.');
						nlapiSetCurrentLineItemValue('item', 'itemreceive', 'F');
					}
					
					if (onBudget == 'T' && inCharge != locInCharge && itemreceive == 'T') {
						alert('Error: You must be the assigned Project In-Charge of the Project / Source Location to fulfill this item.');
						nlapiSetCurrentLineItemValue('item', 'itemreceive', 'F');
					}
					
					if (onBudget == 'T' && isNullOrEmpty(locInCharge) && itemreceive == 'T') {
						alert('Error: You must be the assigned Project In-Charge of the Project / Source Location to fulfill this item.');
						nlapiSetCurrentLineItemValue('item', 'itemreceive', 'F');
					}
				}
			}
		}
	}
}