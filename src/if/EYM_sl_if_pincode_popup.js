/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       23 Feb 2018     Lhea
 *
 */

function verifyPin(request, response)
{
	if (request.getMethod() == 'GET') {		
		doGet(request, response);
	} else if (request.getMethod() == 'POST') {
		doPost(request, response);
	}
}

function doGet(request, response)
{	
	var soId = request.getParameter('rec_id');
	var epRess = getPreferences();
	var mainCss = getPreference(epRess, 'main-css');
	
	var hiddenFields = {};
	hiddenFields['custpage_so_rec_id'] = soId;
	
	var html = pinCodeUI(mainCss, hiddenFields);
	
	response.write(html);
}

function doPost(request, response)
{
	var soId = request.getParameter('custpage_so_rec_id');
	var pin = nlapiEncrypt(request.getParameter('custpage_employee_pin'), 'sha1');
	
	var columns = new Array();
	columns.push(new nlobjSearchColumn('custentity_eym_approver_pincode'));
	
	var filters = new Array();	
	filters.push(new nlobjSearchFilter('custentity_eym_approver_pincode', null, 'is', pin));	
	
	var html = ''; 
	var record = nlapiSearchRecord('employee', null, filters, columns);
	var redirectSO = nlapiResolveURL('RECORD', 'salesorder', soId);
	
	if(record != null) {
		var empId = record[0].getId();
		var appType = nlapiLookupField('employee', empId, 'custentity_eym_approver_type');
		var appExist = appType.indexOf('7');
		
		if (appExist != -1) {
			var redirectIF = nlapiResolveURL('RECORD', 'itemfulfillment', soId);
			redirectIF += '&e=T&transform=salesord&memdoc=0&whence=&emp_id=' + empId;
			
			html = '<html>';
			html += '<head>';
			html += '<script language="JavaScript">';
			html += 'window.close();';
			html += 'window.opener.location.href = "' + redirectIF + '";';
			html += '</script>';
			html += '</head>';
			html += '<body>';
			html += '</body>';
			html += '</html>';
			
		} else {
			html = '<html>';
			html += '<head>';
			html += '<script language="JavaScript">';		
			html += 'alert("Error: PIN Code entered does not match any Project In-Charge.");';
			html += 'window.close();';
			html += 'window.opener.location.href = "' + redirectSO + '";';
			html += '</script>';
			html += '</head>';
			html += '<body>';
			html += '</body>';
			html += '</html>';
		}
	} else {	
		html = '<html>';
		html += '<head>';
		html += '<script language="JavaScript">';		
		html += 'alert("Error: PIN Code entered does not match any Project In-Charge.");';
		html += 'window.close();';
		html += 'window.opener.location.href = "' + redirectSO + '";';
		html += '</script>';
		html += '</head>';
		html += '<body>';
		html += '</body>';
		html += '</html>';
	}
	response.write(html);
}
