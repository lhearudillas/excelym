/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       05 Jun 2018     Roy Selim
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 *
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit( type )
{
	// Run this script on a delete operation.
	if( type == 'delete' ) {

		// Get the record ID.
		var jeId = nlapiGetRecordId();

		// Get the IF ID from the journal entry record.
		// var ifId = getIfId( jeId );

		// Create a try search column variable.
		var columns = [ 'internalid', 'custrecord_eym_ple_bqi' ];

		// Try to get the project ledger entries using the ID. Edited for PNID-464
		// var tryPles = getProjectLedgerEntries( ifId, null, columns );
		var tryPles = getProjectLedgerEntries( jeId, null, columns );

		// Initialize a bqi array.
		var bqiArray = [];

		// Delete all the project ledger entries. Start with looping thru all PLEs
		if( !tryPles.error && tryPles.value ) {

			// Assign the current item to a variable.
			var ples = tryPles.value;

			// Loop thru all the ples
			for( var i = 0; i < ples.length; i++ ) {

				// Get the bqi.
				var bqi = ples[i].getValue( 'custrecord_eym_ple_bqi' );

				// Check if there is a value.
				if( bqi && ( bqiArray.indexOf( bqi ) == -1 ) ) bqiArray.push( bqi );
			}

			// Delete the ples
			deletePles( ples );

			// Update all the affected bqis.
			for( var j = 0; j < bqiArray.length; j++ ) {

				// Update.
				updateBQIUsage( bqiArray[j] );
			}
		}
	}
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only)
 *                      paybills (vendor payments)
 * @returns {Void}
 */

function userEventAfterSubmit(type)
{
	// Run this script on edit operation only.
	if( type == 'edit' ) {

		// Initialize scope variables.
		var ples 		= [];
		var transId		= null;
		var transRec	= null;
		var transType	= null;

		// Get the JE ID.
		var jeId = nlapiGetRecordId();

		// Load the record.
		var jeRec = nlapiLoadRecord( 'journalentry', jeId );

		// Get the number of items in the journal entry.
		var jeRecLines = jeRec.getLineItemCount( 'line' );

		// Get the IF ID of the JE to be used in getting the ples.
		var ifId = jeRec.getFieldValue( 'custbody_eym_downpayment_created_from' );

		// Additional validations. Get the transaction id.
		transId = nlapiLookupField( 'itemfulfillment', ifId, 'createdfrom' );

		// Check if this id is from 'Transfer from excess'.
		if( transId ) var tryTransRec = tryLoadRecord( 'salesorder', transId );

		// If there is a returned value.
		if( !tryTransRec.error && tryTransRec.value ) transRec = tryTransRec.value;

		// Check the type of transaction.
		if( transRec ) transType = transRec.getFieldValue( 'custbody_eym_req_type' );

		// Run the edit code only for Stock transfer from excess.
		if( transType == 2 ) {

			// Get the project ledger entries related to the journal entry.
			var tryPles = getProjectLedgerEntries( jeId, null, [ 'internalid', 'custrecord_eym_ple_bqi' ] );

			// Check if there are values.
			if( !tryPles.error && tryPles.value ) ples = tryPles.value;

			// Initialize a bqi array.
			var bqiArray = [];

			// Loop thru all the je items.
			for( var i = 0; i < jeRecLines; i++ ) {

				// Initialize scope variables.
				var pleId	= null;
				var pleRec	= null;
				var projId	= null;

				// Initialize a line num variable.
				var lineNum = i + 1;

				// Get the bqi.
				var bqi = jeRec.getLineItemValue( 'line', 'custcol_eym_pur_bqi', lineNum );

                // If there is a bqi value, push it to the array.
                if( bqi && ( bqiArray.indexOf( bqi ) == -1 ) ) bqiArray.push( bqi );

				// Create a je item.
				var geItem = {
					accountId:	jeRec.getLineItemValue( 'line', 'account', lineNum ),
					bqi:		bqi,
					credit: 	jeRec.getLineItemValue( 'line', 'credit', lineNum ),
					debit: 		jeRec.getLineItemValue( 'line', 'debit', lineNum )
				};

				// Get the project id.
				projId = jeRec.getLineItemValue( 'line', 'entity', lineNum );

				// Check if there is a corresponding ples.
				if( ples[i] != undefined ) {

					// Get the ple item.
					var pleItem = ples[i];

					// Get the ple id.
					pleId = pleItem.getValue( 'internalid' );

					// Load the project ledger record.
					if( pleId ) pleRec = nlapiLoadRecord( 'customrecord_eym_proj_ledger_entry', pleId );
				}

                // Get the transaction date.
                var transDate = nlapiLookupField( 'journalentry', jeId, 'trandate' );

              	// Enter project ledger entry.
				ledgerEntry( geItem, pleRec, jeId, projId, transDate );
			}

			// If there are excess ples, just delete them after IF edit.
			if( ples.length > jeRecLines ) {

				// Loop thru all the excess ples and delete them.
				var delPles = ples.slice( jeRecLines );

				// Delete the excess ples
				deletePles( delPles );

                // Loop thru all the deleted ples.
                for( var k = 0; k < delPles.length; k++ ) {

                    // Get the bqi.
                    var delBqi = delPles[k].getValue( 'custrecord_eym_ple_bqi' );

                    // Check if the bqi is already on the array.
                    if ( delBqi && ( bqiArray.indexOf( delBqi ) == -1 ) ) bqiArray.push( delBqi );
                }
			}

    		// Update all bqi usage.
    		for( var j = 0; j < bqiArray.length; j++ ) {

    			// Update the bqi usage.
    			updateBQIUsage( bqiArray[j] );
    		}
		}
	}
}

function getIfId( jeId )
{
	return nlapiLookupField( 'journalentry', jeId, 'custbody_eym_downpayment_created_from' );
}