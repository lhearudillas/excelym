/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       09 Nov 2017     EYM-013
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */

function userEventBeforeSubmit(type){
	if(nlapiGetContext().getExecutionContext()!='suitelet'){
		if(type == 'xedit'){
		  
			var recId = nlapiGetRecordId();
			var recType = nlapiGetRecordType();
			
			var oldRec = nlapiGetOldRecord();
			var oldProjStatus = oldRec.getFieldValue('entitystatus');
			
			var record = nlapiLoadRecord(recType,recId);
			var customForm = record.getFieldValue('customform');	
			
			var formStage = nlapiGetFieldValue('entitystatus');
				nlapiLogExecution('DEBUG', 'if', formStage);
          
			if(customForm == 7){
              	if(!IsNullOrEmpty(formStage)){
				if(formStage != oldProjStatus){
					var stat = record.setFieldValue('entitystatus', oldProjStatus);
					nlapiSubmitRecord(record);
					throw 'This field can only be changed by opening the record.';
               		 }
				}
			}		
		} 
	}
}
  