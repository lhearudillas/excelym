/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       07 Feb 2018     EYM-013
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 *
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */

function userEventBeforeLoad(type, form, request){

    // Set form script for popup.
    form.setScript('customscript_eym_cs_popup_window');

    // Create a validation variable.
    var passedValidation = {};

    // Initialize function variables.
    var recordId = null;
    var bqiSearchResults = null;
    var prpSearchResults = null;
    var errorsArray = [];
    var ppsrLen = 0;

    // Initialize function constants.
    const IN_PROGRESS = 'In Progress';

    // Run the script in view mode.
    if( type == 'view' ) {

        // Get the record id.
        recordId = nlapiGetRecordId();

        // Test the record id.
        if( recordId && recordId != -1 ) {

            // Get the customer.
            var customer = nlapiGetFieldValue( 'parent' );

            // Get the project status.
            var projectStatus = nlapiGetFieldText('entitystatus');

			// Get the bqis in the project.
			var bqiFilterObject = { custrecord_eym_boq_projid: { operator: 'is', value: recordId }};
            var bqiColumnsArray = [ 'internalid' ];
			bqiSearchResults = trySearchRecord( 'customrecord_eym_boq', null, bqiFilterObject, bqiColumnsArray );

          	// Get the prps in the project.
			var prpFilterObject = { custrecord_eym_procplan_projcode: { operator: 'is', value: recordId }};
			prpSearchResults = trySearchRecord( 'customrecord_eym_procplan_item', null, prpFilterObject );

            // Test if there are procurement plans.
            if( !prpSearchResults.error && prpSearchResults.value ) {

                // Check for other validatoins.
                if( projectStatus == IN_PROGRESS && !bqiSearchResults.error && bqiSearchResults.value ) {

                    // Test for validation.
                    passedValidation = ValidateFulfillment( bqiSearchResults.value );

                    var buttonScriptExcess, buttonScriptItems;
                    var redirectLinkExcess, redirectLinkItems;

                    // If there are requested items that needs to be fulfilled?
                    if(passedValidation.mrf) {

                        // Define the redirect link for Request Items button.
                        redirectLinkItems = nlapiResolveURL('RECORD', 'customrecord_eym_material_req') + '&projID=' + recordId;

                        // Check if there are excess items that can be requested.
                        if(passedValidation.valid) {

                            // Define the redirect link.
                            redirectLinkExcess = nlapiResolveURL('RECORD', 'salesorder') + '&cf=108&projID=' + recordId + '&parent=' + customer;

                            // Redirect to the request items forms
                            buttonScriptExcess = "showPopupFull('customscript_sl_eym_transfer_from_excess', 'customdeploy_sl_eym_transfer_from_excess', '&projID=" + recordId + "&parent=" + customer + "', 800, 600)";

                            // Show a confirmation message before redirecting to Request items.
                            buttonScriptItems = "if(confirm('Items on the PRP are available on other project\\'s excess location. Click OK to proceed with MRF or Cancel to go back to the project record.')){window.location.assign('"+ redirectLinkItems +"')}";
                        } else {

                            // Show a message that Excess is not available.
                            buttonScriptExcess = "alert('Items on the Procurement Plan are not available on any of the other projects. Please click on the Request Items button to request the items from the Central Warehouse and/or Purchasing Department.')";

                            // Redirect to request items without confirmation.
                            buttonScriptItems = "window.location.assign('"+ redirectLinkItems +"')";
                        }
                    } else {

                        // Show a message that there is no need for requisition.
                        buttonScriptExcess = "alert('Notice: There are no items on the Procurement Plan that needs requisition.')";
                        buttonScriptItems = "alert('Notice: There are no items on the Procurement Plan that needs requisition.')";
                    }

                    // Show the Request from Excess button.
                    form.addButton('custpage_request_transfer_btn', 'Request Transfer from Excess', buttonScriptExcess);

                    // Show the Request Items button.
                    form.addButton('custpage_request_items_btn', 'Request Items', buttonScriptItems);
                }
            }
        }
    }

    // Test ends here
    var tillHere = 'Till here';
}

// Custom helper function for validation.
function ValidateFulfillment( bqiItems ) {

    // Initialize function variables.
    var compareObject = {};
    var itemsArray = [];
    var bqiValuesArray = [];
    var bqis = [];
    var validationStatus = false;
    var validationForMrf = false;

    // Initialize function constants.
    const PROCUREMENT_QUANTITY = 'procurement_quantity';
    const EXCESS_AVAILABLE = 'excess_available';
    const STOCK_TRANSFER_FORM = 108;
    const STOCK_REQUEST_FORM = 104;

    // Loop thru all the items.
	for( var i = 0; i < bqiItems.length; i++ ) {

        // Get the value of the bqi.
        var bqiValue = bqiItems[i].getValue( 'internalid' );

      	// Push the value to the array.
      	bqiValuesArray.push( bqiValue );
    }

    // Search for procurement items.
    var searchColumn = 'custrecord_eym_procplan_bqi';
    var searchColumnsArray = [
        'custrecord_eym_procplan_qty',
        'custrecord_eym_procplan_bqi',
        'custrecord_eym_procplan_item',
        'name'
    ];
    var searchRecordType = 'customrecord_eym_procplan_item';
    var procItemResults = performSearch( searchColumn, bqiValuesArray, searchColumnsArray, searchRecordType );

    // Check if there are results.
    if( procItemResults ) {

        // Loop thru all the results.
        for( var j = 0; j < procItemResults.length; j++ ) {

            // Initialize loop variables and values.
			var bqiResult = procItemResults[j].getText( 'custrecord_eym_procplan_bqi' );
			var bqiQuantity = Number( procItemResults[j].getValue( 'custrecord_eym_procplan_qty' ) );
          	var prpItem = procItemResults[j].getValue( 'name' );
            var prpItemStr = prpItem ? prpItem : 'procurement_item_' + j;
            var procItem = procItemResults[j].getValue( 'custrecord_eym_procplan_item' );
            var procItemStr = procItem ? 'ITEM_' + procItem : 'ITEM_' + j;

            // Add the procurement item to the ITEMS array.
            itemsArray.push(procItem);

            // Create the compareObject schema.
            compareObject = checkAndAddChild( compareObject, bqiResult );
            compareObject[bqiResult] = checkAndAddChild( compareObject[bqiResult], prpItemStr );
            compareObject[bqiResult][prpItemStr] = checkAndAddChild( compareObject[bqiResult][prpItemStr], procItemStr );
            compareObject[bqiResult][prpItemStr][procItemStr][PROCUREMENT_QUANTITY] = bqiQuantity;
        }
    }

    // Search for purchase requisitions.
    var prSearchObj = {
        custbody_eym_pr_projcode: nlapiGetRecordId(),
        approvalstatus: 2
    };
    var prColumnArray = [
        'custcol_eym_pur_bqi',
        'quantity',
        'item',
        'custcol_eym_sales_prpid'
    ];
    var prRecordType = 'purchaserequisition';

    // Search the purchase requisitions acquisitions.
    var prItemResults = performSearch2( prSearchObj, prColumnArray, prRecordType );

    // Test if there are returned values.
    if(prItemResults) {

        // Edit the compareObject schema with the pr results.
        compareObject = entryToObject( prItemResults, compareObject, 'custcol_eym_pur_bqi', 'quantity', 'custcol_eym_sales_prpid', 'item', 'purchase_requisition_quantity' );
    }

    // Loop thru all the compareObject keys.
    for(var n in compareObject) {

        // Fill the bqis array.
        bqis.push( n.slice( 3, n.length ) );
    }

    // Create a search filter object.
    var stSearchObj = {
        custcol_eym_pur_bqi: bqis,
        customform: STOCK_TRANSFER_FORM
    };
    var stSearchColumnArray = [
        'custcol_eym_pur_bqi', 
        'quantity', 
        'custcol_eym_sales_prpid',
        'item', 
        'customform'
    ];
    var stRecordType = 'salesorder';

    // Search the sales order for stock transfer records.
    var stSearchResults = performSearch2( stSearchObj, stSearchColumnArray, stRecordType );

    // Check if there are results 
    if( stSearchResults ) {

        // Edit the compareObject schema with the st results.
        compareObject = entryToObject( stSearchResults, compareObject, 'custcol_eym_pur_bqi', 'quantity', 'custcol_eym_sales_prpid', 'item', 'stock_transfer_quantity' );
    }

    // Create a search filter object.
    var soSearchObj = {
        custcol_eym_pur_bqi: bqis,
        customform: STOCK_REQUEST_FORM
    };
    var soSearchColumnArray = stSearchColumnArray;
    var soRecordType = stRecordType;

    // Search the sales order for stock transfer records.
    var soSearchResults = performSearch2( soSearchObj, soSearchColumnArray, soRecordType );

    // Check if there are results 
    if( soSearchResults ) {

        // Edit the compareObject schema with the st results.
        compareObject = entryToObject( soSearchResults, compareObject, 'custcol_eym_pur_bqi', 'quantity', 'custcol_eym_sales_prpid', 'item', 'sales_order_quantity' );
    }

    // Validate if excess are available
    var inventoryItems = getInventoryItems(itemsArray);

    // Create an Excess items object.
    var excessItemsObject = {};

    // Iterate thru the excess and arrange then according to items.
  	if( inventoryItems ) {

        // Loop thru all the results.
        for( var k = 0; k < inventoryItems.length; k++ ) {

            // Get the item value.
            var item = inventoryItems[k].getValue( 'internalid' );

            // Get the available quantity.
            var avail = Number(inventoryItems[k].getValue( 'locationquantityavailable' ));

            // Build up the schema.
            excessItemsObject = checkAndAddChild( excessItemsObject, 'ITEM_' + item );

            // Check if child object is undefined
            if(excessItemsObject['ITEM_' + item][EXCESS_AVAILABLE] == undefined) {
                excessItemsObject['ITEM_' + item][EXCESS_AVAILABLE] = 0;
            }
            excessItemsObject['ITEM_' + item][EXCESS_AVAILABLE] += avail;
        }
    }

    // Iterate thru the compareObject top level properties
  	for(var a in compareObject) {

        // Iterate thru the compareObject second level properties
        for(var b in compareObject[a]){

            // Iterate thru the compareObject third level properties
            for(var c in compareObject[a][b]){

                // Setup and fill up variables.
                var piq = compareObject[a][b][c][PROCUREMENT_QUANTITY];
                var prq = (compareObject[a][b][c]['purchase_requisition_quantity'] == undefined) ?
                  0 : compareObject[a][b][c]['purchase_requisition_quantity'];
                var stq = (compareObject[a][b][c]['stock_transfer_quantity'] == undefined) ?
                  0 : compareObject[a][b][c]['stock_transfer_quantity'];
                var soq = (compareObject[a][b][c]['sales_order_quantity'] == undefined) ?
                  0 : compareObject[a][b][c]['sales_order_quantity'];

                // Get the total fulfillment and subtract from procurement plan quantity.
                var totalFulfillment = prq + stq + soq;
                var requiredQuantity = piq - totalFulfillment;

                // Set validation for MRF.
                if(requiredQuantity > 0) {

                    // Set mrf validation status.
                    validationForMrf = true;
                }

                if(requiredQuantity >= 0) {

                    // Record the required quantity.
                    compareObject[a][b][c]['required_quantity'] = requiredQuantity;
                }

                // Check if there are excess items from other projects.
                if(requiredQuantity > 0 && excessItemsObject[c] && excessItemsObject[c]['excess_available']) {

                    // Set available qty.
                    var avail = excessItemsObject[c]['excess_available'];

                    if(avail >= requiredQuantity) {

                        compareObject[a][b][c]['fulfillment_from_excess'] = requiredQuantity;
                        excessItemsObject[c]['excess_available'] = avail - requiredQuantity;

                    } else {

                        if(compareObject[a][b][c]['fulfillment_from_excess'] == undefined) {

                            compareObject[a][b][c]['fulfillment_from_excess'] = 0;
                        }
                        compareObject[a][b][c]['fulfillment_from_excess'] += avail;
                        excessItemsObject[c]['excess_available'] = 0;
                    }
                }

                // Test if there are items that needs to request from Excess
                if(compareObject[a][b][c]['required_quantity'] && compareObject[a][b][c]['fulfillment_from_excess']) {

                    // Set validation status.
                    validationStatus = true;
                }
            }
        }
    }

    // Set additional object properties to the return value.
    compareObject['valid'] = validationStatus;
    compareObject['mrf'] = validationForMrf;

    // Return a value.
    return compareObject;
}

// Helper function to shorten search syntax.
function performSearch( searchColumn, searchCriteria, columnArray, recordType ) {

    // Initialize the return variable.
    var searchResults = null;

    // Define the search filters.
    var filters = [new nlobjSearchFilter(searchColumn, null, 'anyof', searchCriteria)];

    // Initialize column array.
    var columns = [];

    // Fill the column array with values.
    for(var i = 0; i < columnArray.length; i++) {
        columns.push(new nlobjSearchColumn(columnArray[i]));
    }

    // Attempt to search the record.
    try{
        searchResults = nlapiSearchRecord(recordType, null, filters, columns);
    } catch(e) {
        searchResults = null;
    }

    // Return the search results or null.
    return searchResults
}

// Helper function to perform a search on a record.
function performSearch2( searchObject, columnArray, recordType ) {

    // Initialize function variables.
    var filters = new Array();
    var searchResults = null;

    // Loop thru the search array.
    for(var n in searchObject) {
        filters.push( new nlobjSearchFilter( n, null, 'anyof', searchObject[n] ) )
    }

    // Create the return columns and fill up the array.
    var columns = [];
    for( var i = 0; i < columnArray.length; i++ ) {
        columns.push( new nlobjSearchColumn( columnArray[i] ) );
    }

    // Attempt to search the records.
    try{
        searchResults = nlapiSearchRecord( recordType, null, filters, columns );
    } catch (e) {
        searchResults = null;
    }

    // Return a value.
    return searchResults;
}

// Helper function for checking and generating child objects.
function checkAndAddChild(parentObj, childObject) {

    // Check if the child object exists.
	if(parentObj[childObject] === undefined) {
      	parentObj[childObject] = {};
    }

    // Return the parent object.
  	return parentObj;
}

// Helper function to enter values to an existing schema.
function entryToObject( searchResult, parentObject, bqi, qty, prp, item, propName ) {

    // Check if the search results is not null.
    if( searchResult != null ) {

        // Loop thru the results object.
        for( var i = 0; i < searchResult.length; i++ ) {

            // Prepare variables.
            var b = searchResult[i].getText( bqi );
            var q = Number( searchResult[i].getValue( qty ) );
            var p = searchResult[i].getText( prp );
            var ps = p ? p : 'procurement_item_' + i;
            var itm = searchResult[i].getValue( item );
            var is = itm ? 'ITEM_' + itm : 'ITEM_' + i;

            // Check the schema.
            if( parentObject[b] && parentObject[b][ps] && parentObject[b][ps][is] && ( q !== null || q !== undefined ) ) {

                if( parentObject[b][ps][is][propName] === undefined ) {
                  parentObject[b][ps][is][propName] = 0;
                }

                parentObject[b][ps][is][propName] += q;
            }
        }
    }

    // Return a value.
    return parentObject;
}

// Helper function to check if there are excess.
function getInventoryItems( itemIds, columnFields )
{
    // Initialize the return value.
    var searchResults = null;

    // Setup search filter.
	var filter = [ 
        new nlobjSearchFilter( 'internalid', null, 'anyof', itemIds )
    ];

    // Specify return columns.
	var columns = [
	    new nlobjSearchColumn( 'internalid' ),
        new nlobjSearchColumn( 'locationquantityavailable' )
    ];

    // Setup return value sorting.
	columns[0].setSort();
    columns[1].setSort( true );

    // Attempt to load the saved search.
    try{
        searchResults = nlapiSearchRecord( null,'customsearch_eym_item_with_exs_locs', filter, columns );
    } catch( e ) {
        searchResults = null;
    }

	return searchResults;
}