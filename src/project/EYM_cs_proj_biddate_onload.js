/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       28 Nov 2017     EYM-013
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType 
 * 
 * @param {String} type Access mode: create, copy, edit
 * @returns {Void}
 */
function clientPageInitOnLoad(type){
   if(type == 'create' || type == 'edit'){
	   
	   var projStatusOnLoad = nlapiGetFieldValue('entitystatus');
	   
	   if(projStatusOnLoad == 4){
		   nlapiSetFieldMandatory('custentity_eym_proj_biddate', true);
	   }
	   
	   if(projStatusOnLoad == 5){
		   nlapiSetFieldMandatory('custentity_eym_proj_awrddate', true);
	   }
	   
   }
}
