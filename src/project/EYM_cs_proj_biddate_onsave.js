/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       28 Nov 2017     EYM-013
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @returns {Boolean} True to continue save, false to abort save
 */
function clientSaveRecordOnSave(){
	var projStatusOnSave = nlapiGetFieldValue('entitystatus');
	var bidDateValue = nlapiGetFieldValue('custentity_eym_proj_biddate');
	var bidAwardDateValue = nlapiGetFieldValue('custentity_eym_proj_awrddate');

	var bidDateValue2 = nlapiStringToDate(bidDateValue);
	var bidAwardDateValue2 = nlapiStringToDate(bidAwardDateValue);

	if(projStatusOnSave == 4 && IsNullOrEmpty(bidDateValue)){
		alert('The field Bid Date cannot be empty. Please enter a value to proceed.');
		return false;
	}
	
	if(projStatusOnSave == 5 && IsNullOrEmpty(bidAwardDateValue)){
		alert('The field Bid Award Date cannot be empty. Please enter a value to proceed.');
		return false;
	}
	
	if(!IsNullOrEmpty(bidDateValue) && !IsNullOrEmpty(bidAwardDateValue) &&  bidDateValue2 > bidAwardDateValue2){
		alert('Bid Award Date could not be earlier than Bid Date.');
		return false;
	}
	
    return true;
}
