/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       07 Nov 2017     EYM-013
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Sublist internal id
 * @param {String} name Field internal id
 * @param {Number} linenum Optional line item number, starts from 1
 * @returns {Void}
 */
function clientValidateField(type, name, linenum){
	if(name === 'entitystatus'){
		
		var projStatus = nlapiGetFieldValue('entitystatus');
		var projStatusValue = nlapiGetFieldValue('custentity_eym_projstat_value');
		
		if(projStatusValue == '' || projStatusValue == null){		//Empty upon creation
			if(projStatus == 2){
				alert('The project status cannot be set to "In Progress" because this project does not have any approved BOQ.');
				return false;
			}
		}
		
		if(projStatusValue == 3 || projStatusValue == 4 || projStatusValue == 5){		//Lost=3, Bidding=4, Awarded=5
			if(projStatus == 2){
				alert('The project status cannot be set to "In Progress" because this project does not have any approved BOQ.');
				return false;
			}
		}
		
		if(projStatusValue == 17){		//Under Warranty
			if(projStatus == 2){
				alert('The Project Status cannot be reverted back to "In Progress"');
				return false;
			}
			else if(projStatus == 5 || projStatus == 4 || projStatus == 3 || projStatus == 2){
				alert('Current status cannot be changed.');
				return false;
			}
		}
		
		if(projStatusValue == 1){		//Closed
			if(projStatus == 2){
				alert('The Project Status cannot be reverted back to "In Progress"');
				return false;
			}
			else if(projStatus == 5 || projStatus == 4 || projStatus == 3 || projStatus == 2 || projStatus == 17){
				alert('Current status cannot be changed.');
				return false;
			}
		}
		
		if(projStatusValue == 2){		//In Progress
			if(projStatus == 5 || projStatus == 4 || projStatus == 3){
				alert('Current status cannot be changed.');
				return false;
			}
		}

	}
	return true; 
}
