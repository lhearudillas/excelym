/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       09 Nov 2017     EYM-013
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType 
 * 
 * @param {String} type Access mode: create, copy, edit
 * @returns {Void}
 */
function clientPageInit(type){
	if(type == 'create' || type == 'edit' || type == 'view'){
		var projStatusVal = nlapiGetFieldValue('entitystatus');
		var setProjStatVal = nlapiSetFieldValue('custentity_eym_projstat_value', projStatusVal);
	}
}
