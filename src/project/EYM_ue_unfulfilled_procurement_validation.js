/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       24 Jan 2018     EYM-013
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 *
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request){

  	// Run this script only when customer is viewing a project
  	if(type == 'view') {

      	// Create a compare object.
      	var compareObject = {};

  		// Get the total number of sublit items.
      	var bqiSublistItem = nlapiGetLineItemCount('custpage_boq_sublist');

      	// Create a variable to hold the bqi values.
      	var bqiValuesArray = new Array();

      	// Loop thru all the sublist items.
      	for(var i = 1; i < bqiSublistItem + 1; i++) {
			var stringValue = nlapiGetLineItemValue('custpage_boq_sublist', 'custpage_boq_projid', i);
          	var strIndex = stringValue.indexOf('&id=');
			bqiValuesArray.push(stringValue.substr(strIndex + 4, 3));
        }

      	// Create a search that will return all bqi's from the array.
      	var bqiArrayFilter = [new nlobjSearchFilter('custrecord_eym_procplan_bqi', null, 'anyof', bqiValuesArray)];
      	var bqiArrayColumns = [new nlobjSearchColumn('custrecord_eym_procplan_qty'), new nlobjSearchColumn('custrecord_eym_procplan_bqi')];
      	// Search the procurement items list.
		var procItemResults = nlapiSearchRecord('customrecord_eym_procplan_item', null, bqiArrayFilter, bqiArrayColumns);

      	// Compare fullfillment from the procurement items with Purchase Requisition items.
		var prArrayFilter = [new nlobjSearchFilter('custbody_eym_pr_projcode', null, 'anyof', nlapiGetRecordId())];
      	var prArrayColumns = [new nlobjSearchColumn('custcol_eym_pur_bqi'), new nlobjSearchColumn('quantity')];
      	// Search the purchase requisition list.
      	var prItemResults = nlapiSearchRecord('purchaserequisition', null, prArrayFilter, prArrayColumns);

      	// Iterate thru the procurement items
      	if(procItemResults !== null) {
      		for(var i = 0; i < procItemResults.length; i++) {
				var bqiResult = procItemResults[i].getText('custrecord_eym_procplan_bqi');
          		var bqiQuantity = Number(procItemResults[i].getValue('custrecord_eym_procplan_qty'));

          		// Check if the property has been set.
          		if(compareObject[bqiResult] === undefined) {
              	compareObject[bqiResult] = {procurement_quantity: 0};
            	}
          		compareObject[bqiResult]['procurement_quantity'] += bqiQuantity;
        	}
        }

      	// Iterate thru the procurement items.
      	if(prItemResults !== null) {
      		for(var i = 0; i < prItemResults.length; i++) {
				var bqiResult = prItemResults[i].getText('custcol_eym_pur_bqi');
          		var bqiRequestedQuantity = Number(prItemResults[i].getValue('quantity'));

          		if(compareObject[bqiResult]) {
          			// Check the value.
          			if(bqiRequestedQuantity !== null || bqiRequestedQuantity !== undefined) {
                  		if(compareObject[bqiResult]['requested_quantity'] === undefined) {
                      		compareObject[bqiResult]['requested_quantity'] = 0;
                   	 	}
                  		compareObject[bqiResult]['requested_quantity'] += bqiRequestedQuantity;
            		}
            	}
        	}
        }

      	// Till Here
      	var tillHere = 'Till Here';
    }
}