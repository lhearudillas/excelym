/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       26 Mar 2018     Roy Selim
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 *
 * @returns {Boolean} True to continue save, false to abort save
 */

// Global variables.
var EYM_cs_to_if_and_ir_pin_saveReturn 	= false;
var EYM_cs_to_if_and_ir_pin_fromTO		= false;
var EYM_cs_to_if_and_ir_pin_tryObjTO	= null;

function clientPageInit(type)
{
    if( type == 'copy' || type == 'edit' ) {

    	// Declare function constants.
      	const TOV_CREATED_FRM 		= 'createdfrom';
      	const TOV_CREATED_FRM_REC 	= 'transferorder';
      	const TOV_TO_INTERNAL_ID	= 19887;

    	// Get the created from value.
      	var createdFrom = nlapiGetFieldValue( TOV_CREATED_FRM );

      	// Run this script only for TOs.
      	if( createdFrom == TOV_TO_INTERNAL_ID ) {

	    	// Try to load the TO.
	    	EYM_cs_to_if_and_ir_pin_tryObjTO = tryLoadRecord( TOV_CREATED_FRM_REC, createdFrom );

	    	// Check the try object
	      	if( !EYM_cs_to_if_and_ir_pin_tryObjTO.error && EYM_cs_to_if_and_ir_pin_tryObjTO.value ) {

	      		// Return a true value;
	      		EYM_cs_to_if_and_ir_pin_fromTO = true;

	    		// Load the needed libraries.
	          	insertJQUI();
	      	}
      	}
    }
}

function clientSaveRecord()
{
	if( EYM_cs_to_if_and_ir_pin_saveReturn ) {
	       return true;
	}

	// Check if the form creates from TO.
  	if( EYM_cs_to_if_and_ir_pin_fromTO ) {

  		// Declare scope constants.
  	  	const TOV_ITM_FLFLMT		= 'itemfulfillment';
  	  	const TOV_ITM_RCPT			= 'itemreceipt';
  	  	const TOV_DES_LOC_PIC 		= 'employee';
  	  	const TOV_GEN_ERROR			= 'ERROR: The PIN Code does not match the PIN Code of the Project In-Charge of the ';

  	  	// Declare scope variables
  	  	var submitButton			= null;
  	  	var pinErrorStr				= null;

  		// Get the record type.
  		var recType = nlapiGetRecordType();

  		// Check the record type.
  		if( recType == TOV_ITM_RCPT ) {

  			// Fill up the variables for item receipt.
  			submitButton = 'submitter';
  			pinErrorStr  = TOV_GEN_ERROR + 'destination location.';

  		} else if( recType == TOV_ITM_FLFLMT ) {

  			// Fill up the variables for item receipt.
  			submitButton = 'btn_multibutton_submitter';
  			pinErrorStr  = TOV_GEN_ERROR + 'source location.';

  		}

      	// Get the project in charge to validate.
      	var pic = EYM_cs_to_if_and_ir_pin_tryObjTO.value.getFieldValue( TOV_DES_LOC_PIC );

      	// Verify if there is a src location project-in-charge.
      	if( pic ) {

            // Call the validate PIN function.
            validatePIN( pic, submitButton, pinErrorStr, 'EYM_cs_to_if_and_ir_pin_saveReturn' );

          	return EYM_cs_to_if_and_ir_pin_saveReturn;
        } else {

          	// Return false here to not fulfill the TO.
          	return false;
        }
    }

  	// Always return true at this point to allow save of other IFs.
  	return true;
}