/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       20 Apr 2018     Roy Selim
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 *
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type)
{
	// Run this script only in IR creation.
	if( type == 'create' || type == 'delete' ) {

		// Declare local scope constants.
		const TRQ_CREATED_FROM 		= 'createdfrom';
		const TRQ_TRANSFER_ORDER	= 'transferorder';
		const TRQ_ORIGINAL_TO		= 'custbody_eym_downpayment_created_from';
		const TRQ_ITEM_FLD			= 'item';
		const TRQ_QTY_FLD			= 'quantity';
		const TRQ_TO_RETURNED		= 'custcol_eym_to_returned';

		// Declare local scope variables.
		var createdFromRecord 		= null;
		var originalTORecord 		= null;
		var itemPaymentObject 		= {};

		// Get the transaction that created the IR.
		var createdFrom = nlapiGetFieldValue( TRQ_CREATED_FROM );

		/* Continue the script only if the transaction is from TO. */
		if( createdFrom ) {

			// Try to load a TO from the createdFrom value 
			var tryRecordObj = tryLoadRecord( TRQ_TRANSFER_ORDER, createdFrom );

			// Continue the script if there is a record loaded.
			if( !tryRecordObj.error && tryRecordObj.value ) {

				// Assign the value to a variable.
				createdFromRecord = tryRecordObj.value;

				// Get the value of the original TO.
				var originalTO = createdFromRecord.getFieldValue( TRQ_ORIGINAL_TO );

				// Continue this script only if there is a value.
				if( originalTO ) {

					// Try to load the original TO.
					var tryOrigRec = tryLoadRecord( TRQ_TRANSFER_ORDER, originalTO );

					// Check if there is a loaded record.
					if( !tryOrigRec.error && tryOrigRec.value ) {

						// Assign the value to a variable.
						originalTORecord = tryOrigRec.value;

						// Get the number of items in the IR.
						var IRItemCount = nlapiGetLineItemCount( TRQ_ITEM_FLD );

						// Loop thru all the items.
						for( var i = 1; i <= IRItemCount; i++ ) {

							// Get the item ID.
							var itemID = nlapiGetLineItemValue( TRQ_ITEM_FLD, TRQ_ITEM_FLD, i );

							// Get the quantity of the item in the IR.
							var itemQty = Number( nlapiGetLineItemValue( TRQ_ITEM_FLD, TRQ_QTY_FLD, i ) );

							// Check if this item is already in the itemPaymentObject.
							if( itemPaymentObject[itemID] == undefined ) {

								// Initialize to zero.
								itemPaymentObject[itemID] = 0;
							}

							// Add the number to the object.
							itemPaymentObject[itemID] += !isNaN( itemQty ) ? itemQty: 0;
						}

						// Get the number of items in the original TO.
						var originalTOItemCount = originalTORecord.getLineItemCount( TRQ_ITEM_FLD );

						// Loop thru all the the items in the original TO.
						for( var i = 1; i <= originalTOItemCount; i++ ) {

							// Get the item ID.
							var itemID = originalTORecord.getLineItemValue( TRQ_ITEM_FLD, TRQ_ITEM_FLD, i );

							// Get the quantity of the item.
							var itemQty = Number( originalTORecord.getLineItemValue( TRQ_ITEM_FLD, TRQ_QTY_FLD, i ) );

							// Get the number of replaced items.
							var TOReturned = Number( originalTORecord.getLineItemValue( TRQ_ITEM_FLD, TRQ_TO_RETURNED, i ) );

							// Get the required number to fulfill the replacement.
							var qtyRequired = ( type == 'create' ) ? itemQty - TOReturned: TOReturned;

							// Check if replacement required is greater than zero.
							if( qtyRequired > 0 ) {

								// Set up a variable for actual replacement.
								var actualReplacement = 0;

								// Check the payment object for available replacement.
								if( itemPaymentObject[itemID] != undefined ) {

									if( qtyRequired <= itemPaymentObject[itemID] ) {

										// Set the value of the actual replacement.
										actualReplacement = qtyRequired;

										// Subtract the replacement required from the payment object.
										itemPaymentObject[itemID] -= qtyRequired;

									} else {

										// Set the actual replacement to what is available.
										actualReplacement = itemPaymentObject[itemID];

										// Set the payment object item property to zero.
										itemPaymentObject[itemID] = 0;

									}

									// Set the TO returned column to a new value.
									if ( type == 'create' ) {

										// Add actual replacement to the returned qty.
										TOReturned += actualReplacement;

									// Else if IR is to be deleted.
									} else {

										// Subtract actual replacement to the returned qty.
										TOReturned -= actualReplacement;
									}

									// Verify that TO returned is not greater than qty.
									if( TOReturned <= itemQty ) {

										// Set the TO returned field.
										originalTORecord.setLineItemValue( TRQ_ITEM_FLD, TRQ_TO_RETURNED, i, TOReturned );
									}
								}
							}
						}

						// Submit the record.
						nlapiSubmitRecord( originalTORecord );
					}
				}
			}
		}
	}
}