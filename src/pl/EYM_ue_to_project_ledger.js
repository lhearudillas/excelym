/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define([ 'N/record', 'N/search' ],

function( record, search ) {

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */

	function beforeSubmit(scriptContext)
    {
    	// Initialize scope variables.
    	var TORec 		= null;
    	var bqiArray	= [];

    	// Assign values to variables.
    	var cntx 		= scriptContext;
    	var typ 		= cntx.UserEventType;
    	var rec			= cntx.newRecord;
       	var recId		= rec.id;

       	// Run only if the customer is deleting the JE.
       	if( cntx.type === typ.DELETE ) {

       		// Verify if the JE was created from TO.
       		TORec = verifyFromTO( rec );

       		// If there was a TO loaded, continue.
       		if( TORec ) {

	    		// Get the bqis associated with the record id.
	    		bqiArray = getBqis( recId );

	    		// Delete the ples
	    		deletePles( {
	    			fieldId: 'custrecord_eym_ple_tran_id',
	    			fieldValues: [ recId ]
	    		});
       		}

       		// Update all the bqi record's usage.
    		for( var i = 0; i < bqiArray.length; i++ ) {
    			updateBqi( bqiArray[i] );
    		}
       	}
    }

    // Function to verify if the IF comes from a TO.
    function verifyFromTO( rec ) 
    {
    	// Initialize scope variables.
    	var TORec 		= null;

    	// Get the created from field value.
    	var crtdFrm = rec.getValue( {fieldId: 'custbody_eym_downpayment_created_from'} );

    	if( crtdFrm ) {

    		// Get the created from of the transaction.
    		var transFrom = search.lookupFields({
    			type: search.Type.ITEM_FULFILLMENT,
    			id: crtdFrm,
    			columns: 'createdfrom'
    		});

    		// Assign the object to a variable.
    		var trnsFrmRes = transFrom.createdfrom;

    		// Check if it has a value.
    		if( trnsFrmRes ) {

    			// Try to load the estimate record.
    			try{
	    			TORec = record.load({
	    				  type: record.Type.TRANSFER_ORDER,
	    				  id: trnsFrmRes[0].value
	    			});
    			} catch(e) {
    				// Nothing to be done yet.
    			}
    		}
    	}

    	// Return a value.
    	return TORec;
    }

    function afterSubmit(scriptContext)
    {
    	// Initialize scope variables.
    	var TORec 		= null;
    	var bqiArray	= [];

    	// Assign values to variables.
    	var cntx 	= scriptContext;
    	var typ 	= cntx.UserEventType;
    	var rec		= cntx.newRecord;
    	var recId	= rec.id;

    	// Get the created from field value.
    	var crtdFrm = rec.getValue( {fieldId: 'custbody_eym_downpayment_created_from'} );

    	if( crtdFrm ) {

    		// Get the created from of the transaction.
    		var transFrom = search.lookupFields({
    			type: search.Type.ITEM_FULFILLMENT,
    			id: crtdFrm,
    			columns: 'createdfrom'
    		});

    		// Assign the object to a variable.
    		var trnsFrmRes = transFrom.createdfrom;

    		// Check if it has a value.
    		if( trnsFrmRes ) {

    			// Try to load the estimate record.
    			try{
	    			TORec = record.load({
	    				  type: record.Type.TRANSFER_ORDER,
	    				  id: trnsFrmRes[0].value
	    			});
    			} catch(e) {
    				// Nothing to be done yet.
    			}
    		}
    	}

    	// Make sure that this invoice is coming from a Quote.
    	if( TORec ) {

	    	// If the type is edit, delete the ples first.
	    	if( cntx.type === typ.EDIT ) {

	    		// Get the bqis associated with the record id.
	    		bqiArray = getBqis( recId );

	    		// Delete the ples
	    		deletePles( {
	    			fieldId: 'custrecord_eym_ple_tran_id',
	    			fieldValues: [ recId ]
	    		});
	    	}

	    	// Check if the type is edit or save.
	    	if( cntx.type === typ.EDIT || cntx.type === typ.CREATE ) {

	    		// Get the number of lines in the journal entry.
	    		var jeLines = rec.getLineCount( {sublistId: 'line'} );

	    		// Get the journal entry transacction date. This is added for PNID-494
	    		var transDate = rec.getValue( {fieldId: 'trandate'} );

	    		// Check if there is a transaction date. Bug cought by Jacq.
	    		if( !transDate ) {

		    		// Load the record.
		    		var jeRec = record.load({
		    			type: record.Type.JOURNAL_ENTRY,
		    			id: recId
		    		});

		    		// Get the transaction date.
		    		transDate = jeRec.getValue( {fieldId: 'trandate'} );
	    		}

	    		// Loop thru all the journal entry lines.
	    		for( var i = 0; i < jeLines; i++ ) {

	    			// Get field values from the journal entry.
	    			var accountId 	= getJESublistValue( rec, 'account', i );
	    			var debit		= getJESublistValue( rec, 'debit', i );
	    			var credit		= getJESublistValue( rec, 'credit', i );
	    			var bqi			= getJESublistValue( rec, 'custcol_eym_pur_bqi', i );
	    			var project		= getJESublistValue( rec, 'entity', i );

	    			// Add the BQI to the array.
	    			if( bqi && bqiArray.indexOf( bqi ) == -1 ) bqiArray.push( bqi );

					// Create a project ledger custom record.
					var plRec = record.create({
						type: 'customrecord_eym_proj_ledger_entry',
					});

					// Set the values.
					plRec.setValue( {fieldId: 'custrecord_eym_ple_tran_id',		value: recId} );
					plRec.setValue( {fieldId: 'custrecord_eym_ple_acc_num',		value: accountId} );
					plRec.setValue( {fieldId: 'custrecord_eym_ple_debit',		value: debit} );
					plRec.setValue( {fieldId: 'custrecord_eym_ple_credit',		value: credit} );
					plRec.setValue( {fieldId: 'custrecord_eym_ple_bqi',			value: bqi} );
					plRec.setValue( {fieldId: 'custrecord_eym_ple_proj_code',	value: project} );
					// This line is added for PNID-494.
					plRec.setValue( {fieldId: 'custrecord_eym_ple_date',		value: transDate} );

					// Save the record.
					var plId = plRec.save({ignoreMandatoryFields: true});
	    		}

	    		// Update all the bqi record's usage.
	    		for( var i = 0; i < bqiArray.length; i++ ) {
	    			updateBqi( bqiArray[i] );
	    		}
	    	}
    	}
    }

	// Function to delete project ledger entries
	function deletePles( options )
	{
		// Initialize scope constants.
		const DEL_TYP_STR = 'customrecord_eym_proj_ledger_entry';
		const DEL_FLD_STR = 'internalid';

		// Get the transaction id.
		var fieldId		= options.fieldId;
		var fldValArr	= options.fieldValues

		// Create a search object.
		var plesSearch = search.create({
			type: DEL_TYP_STR,
			columns: [
				{name: DEL_FLD_STR}
			],
			filters: [
				{
					name: fieldId,
					operator: 'anyof',
					values: fldValArr
				}
			]
		});

		plesSearch.run().each( function( ple ){

			// Get the value of the ple id.
			var pleId = ple.getValue( {name: DEL_FLD_STR} );

			// Delete the record.
			record.delete({
				type: DEL_TYP_STR,
				id: pleId
			});

			// Return a true value to continue.
			return true;
		});
	}

	// Function to get the sublist field value.
	function getJESublistValue( jeRec, fieldId, line )
	{
		// Initialize a return value.
		var retVal = null;

		// Check if argument is not empty or null
		if( jeRec && fieldId ) {

			retVal = jeRec.getSublistValue({
				sublistId: 'line',
				fieldId: fieldId,
				line: line
			})
		}

		// Return the value.
		return retVal;
	}

	// Function to get all bqis related with a journal entry.
	function getBqis( jeId )
	{
		// Initialize a return array.
		var bqiArray = [];

		// Create a search.
		try{
			var bqiSearch = search.create({
				type: 'customrecord_eym_proj_ledger_entry',
				columns: [
				    {name: 'custrecord_eym_ple_bqi'}
				],
				filters: [
				    {name: 'custrecord_eym_ple_tran_id', operator: 'is', values: [ jeId ] }
				]
			});

			// Loop thru all the results.
			bqiSearch.run().each( function( res ){

				// Get the bqi value.
				var bqi = res.getValue( {name: 'custrecord_eym_ple_bqi'} );

				// Check if the bqi is already on the array.
				if( bqi && bqiArray.indexOf( bqi ) == -1 ) bqiArray.push( bqi );

				// Return a true value to continue the loop.
				return true;
			});
		} catch(e){
			// Log an error.
		}

		// Return the array.
		return bqiArray;
	}

	// Function to update the bqi usage field.
	function updateBqi( bqi )
	{
		// Initialize the usage value.
		var usage 	= 0;

		try{

			// Try to load the bqi record.
			var bqiRecord = record.load( {type: 'customrecord_eym_boq', id: bqi} );

			// Get the usage of all project ledgers with BQI value. Start by creating a search.
			var plUsageSearch = search.create({
				type: 'customrecord_eym_proj_ledger_entry',
				columns: [
					{name: 'custrecord_eym_ple_bqi', join: null, summary: 'group' },
					{name: 'custrecord_eym_ple_debit', join: null, summary: 'sum' },
					{name: 'custrecord_eym_ple_credit', join: null, summary: 'sum' },
				],
				filters: [ {name: 'custrecord_eym_ple_bqi', operator: 'anyof', values: [ bqi ] } ]
			});

			// Run the search, and get the result
			var plUsage = plUsageSearch.run().getRange( {start: 0, end: 1} )[0];

			// Get the total debit and credit.
			var totalDebit 	= plUsage.getValue( {name: 'custrecord_eym_ple_debit', join: null, summary: 'sum' } );
			var totalCredit	= plUsage.getValue( {name: 'custrecord_eym_ple_credit', join: null, summary: 'sum' } );

			// Compute the total usage.
			usage = totalDebit - totalCredit;

			// Update the bqi usage.
			bqiRecord.setValue( {fieldId: 'custrecord_eym_bqi_usage', value: usage } );

			// Attempt to save the record.
			if( bqiRecord.save({}) ) {
				return true;
			} else {
				return false;
			}

		} catch(e){

			// Log an error and return a false value.
			return false;
		}
	}

    return {
        afterSubmit: afterSubmit,
        beforeSubmit: beforeSubmit
    };

});
