/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 * @NAmdConfig  /SuiteScripts/excelym/config/configuration.json
 */
define([ 
	'N/ui/serverWidget',
	'N/search',
	'N/record',
	'N/redirect',
	'N/runtime',
	'EYM/preferences'
],

function( widget, search, record, redirect, runtime, prex ) {

    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context)
    {
    	// Get the request method, on this case should be 'GET'
    	var requestMethod = context.request.method;

    	// Branch the logic according to the request method.
    	if ( requestMethod == 'GET' ) {

    		// Load the saved search
    		var ss = search.load( {id: 'customsearch_script_eym_bqi_mass_update'} );

    		// Create a native NetSuite form.
    		var form = widget.createForm( {title: 'EUR List'} );

    		// Add a client script to this suitelet.
    		form.clientScriptModulePath = 'SuiteScripts/excelym/src/pl/EYM_cs_bqi_mass_posting_filter.js';
    		
    		// Get the preferences.
    		var prefs = prex.getPreferences( {} );
    		
    		// Get the access roles for this page.
    		var roles = prex.getPreference( {preferences: prefs.value, name: 'eur-post-button'} );
    		
    		// Get the role of the logged in user.
    		var role = runtime.getCurrentUser().role;
    		
    		// If the user is allowed, continue with the script.
    		if( roles.indexOf( role.toString() ) != -1 ) {
	
	            // Create a filter dropdown.
	    		var projFilter = form.addField({
	    			id: 'custpage_project_filter',
	    			label: 'FILTER BY PROJECT',
	    			type: 'SELECT'
	    		});
	
				// Set the filter value.
				projFilter.addSelectOption({
					value	: '',
					text	: ''
				});
	
				// Get the filter value from the parameters.
				var filterVal = context.request.parameters.filtered_by;
	
	    		// Create a sublist for the EURs
	    		var sublist = form.addSublist({
	    		    id : 'custpage_eur_sublist',
	    		    type : widget.SublistType.LIST,
	    		    label : 'EUR List'
	    		});
	
	    		// Create the fields array.
	    		var fieldsArray = [
	    			{ id: 'checkbox', 	type: 'CHECKBOX', 	label: 'Post' },
	    			{ id: 'id',			type: 'TEXT',		label: 'Internal ID' },
	    			{ id: 'eur',		type: 'TEXT',		label: 'EUR' },
	    			{ id: 'series',		type: 'TEXT',		label: 'Series Number' },
	    			{ id: 'project', 	type: 'TEXT', 		label: 'Project' },
	    			{ id: 'status',		type: 'TEXT',		label: 'Status' },
	    			{ id: 'bqi',		type: 'TEXT',		label: 'BQI' }
	    		];
	
	    		// Add the fields to the form.
	    		sublist = addFields( sublist, widget, fieldsArray );
	
	    		// Create a check all button.
	    		sublist.addMarkAllButtons();
	
	    		// Initialize a line counter.
	    		var lc = 0;
	
	    		// Initialize a filter value array.
	    		var filterTxtArr = [];
	    		var filterValArr = [];
	
	    		// Loop thru all the results of the saved search.
	    		ss.run().each( function( result ){
	
	    			// If the project field is filtered, include only the filtered value. Get the project value and text.
	    			var projVal = result.getValue( {name: 'custrecord_eym_deur_h_proj'} );
	    			var projTxt	= result.getText( {name: 'custrecord_eym_deur_h_proj'} );
	
	    			// Push the values to the array container.
	    			if(filterTxtArr.indexOf( projTxt ) == -1 ){
	
	    				// Push the values.
	    				filterTxtArr.push( projTxt );
	    				filterValArr.push( {value: projVal, text: projTxt} );
	    			}
	
	    			if( !filterVal || ( filterVal && ( projVal == filterVal) ) ) {
	
		    			// Get the internal id.
		    			var id = result.getValue( {name: 'internalid'} );
	
		    			// Set the eur id field.
		    			sublist.setSublistValue({
		    			    id 		: 'custpage_id',
		    			    line 	: lc,
		    			    value 	: id
		    			});
	
		    			// Set the DEUR field.
		    			sublist.setSublistValue({
		    			    id 		: 'custpage_eur',
		    			    line 	: lc,
		    			    value 	: result.getValue( {name: 'name'} )
		    			});
	
		    			// Set the DEUR field.
		    			sublist.setSublistValue({
		    			    id 		: 'custpage_series',
		    			    line 	: lc,
		    			    value 	: result.getValue( {name: 'custrecord_eym_deur_eur_no'} )
		    			});
	
		    			// Set the project field.
		    			sublist.setSublistValue({
		    			    id 		: 'custpage_project',
		    			    line 	: lc,
		    			    value 	: projTxt
		    			});
	
		    			// Set the status field.
		    			sublist.setSublistValue({
		    			    id 		: 'custpage_status',
		    			    line 	: lc,
		    			    value 	: result.getText( {name: 'custrecord_eym_eur_h_status'} )
		    			});
	
		    			// Set the bqi field.
		    			sublist.setSublistValue({
		    			    id 		: 'custpage_bqi',
		    			    line 	: lc,
		    			    value 	: result.getText( {name: 'custrecord_eym_eur_h_bqi'} )
		    			});
	
		    			// Increment the line counter.
		    			lc++;
	    			}
	
	    			// Continue the loop.
	    			return true;
	    		});
	
				// Loop thru all the filter values.
				filterValArr.forEach(function(filter) {
	
					//	Add a filter value.
	    			projFilter.addSelectOption({
	    				value	: filter.value,
	    				text	: filter.text
	    			});
				});
	
	    		projFilter.defaultValue = filterVal;	    		
	    		
	    		// Add a submit button.
	    		form.addSubmitButton( {label: 'Post EURs'} );
    		} 

    		// Send the form as a response page.
    		context.response.writePage( form );

    	} else if( requestMethod == 'POST' ) {

    		// Get the number of lines.
    		var lc = context.request.getLineCount( {group: 'custpage_eur_sublist'} );

    		// Get the value of the filter.
    		var filterVal = context.request.parameters.custpage_project_filter;

    		// Loop thru all the items in the sublist.
    		for( var i = 0; i < lc; i++ ) {

    			// Get the value of the checkbox
    			var mu = context.request.getSublistValue(
    				{
    					line	: i,
    					group	: 'custpage_eur_sublist',
    					name	: 'custpage_checkbox'
    				}
    			);

    			// Get the id of the checked item.
    			var id = context.request.getSublistValue(
    				{
    					line	: i,
    					group	: 'custpage_eur_sublist',
    					name	: 'custpage_id'
    				}
        		);

    			// Check if the ID is checked.
    			if( id && mu == 'T' ) {

    				// Post the eur lines.
    				postEur( { id: id } );

    				// Remove the filter.
    				filterVal = '';
    			}
    		}

    		// Redirect the user to the DEUR list.
    		redirect.toSuitelet({
    		    scriptId		: 'customscript_sl_eym_bqi_mass_posting',
    		    deploymentId	: 'customdeploy_sl_eym_bqi_mass_posting',
    		    parameters		: {
    		    	'filtered_by': filterVal
    		    }
    		});
    	}
    }

    function addFields ( sublist, widget, args )
    {
    	// Loop thru all the arguments.
    	args.forEach( function( arg ){

			// Add the field to the sublist.
    		sublist.addField({
    			id		: 'custpage_' + arg.id,
    			type	: arg.type,
    			label	: arg.label,
    			source	: arg.source ? arg.source : null
    		});
    	});

    	return sublist;
    }

    function postEur( options )
    {
    	// Initialize scope variables.
    	var postedAll = true;

    	// Load the record.
    	var rec = record.load({
    		type	: 'customrecord_eym_eur_header',
    		id		: options.id
    	});

    	// Get all the needed EUR details for the project ledger.
    	var equiptAsset = rec.getValue( {fieldId: 'custrecord_eym_deur_h_equip_no'} );
    	var transDate	= rec.getValue( {fieldId: 'custrecord_eym_eur_h_date_completed'} );

    	// Get preferences.
    	var prefs = prex.getPreferences( {} );

    	// Initialize a post counter.
    	var postCounter = 0;

    	// Search for all EUR details as child or this EUR.
    	var eurLogs = search.create({
    		type	: 'customrecord_eym_psc_eur',
    		columns	: [
    			'internalid',
    			'custrecord_eym_deur_ea_id',
    			'custrecord_eym_deur_total',
    			'custrecord_eym_eur_bqi'
    		],
    		filters: [
    			{ 
    				name	: 'custrecord_eym_deur_num',
    				operator: 'is',
    				values	: [ options.id ]
    			}
    		]
    	}).run().each( function( res ){

    		// Get the internal ID of the EUR detail and the total value.
    		var intId 		= res.getValue( {name: 'internalid'} );
			var deurTotal 	= res.getValue( {name: 'custrecord_eym_deur_total'} );

    		// Loop up to 2 times.
    		for( var j = 0; j < 2; j++ ) {

        		// Create a project ledger record.
        		var plRec = record.create({
        			type: 'customrecord_eym_proj_ledger_entry'
        		});

        		// Set project ledger values from the EUR header.
        		plRec.setValue({
        			fieldId	: 'custrecord_eym_ple_equip_asset_num',
        			value	: equiptAsset
        		});
        		plRec.setValue({
        			fieldId	: 'custrecord_eym_ple_deur_line_no',
        			value	: intId
        		});
        		plRec.setValue({
        			fieldId	: 'custrecord_eym_ple_date',
        			value	: transDate
        		});

    			// For odd number.
    			if( j == 0 ) {

    				// Get the debit account from EYM Preference
    				var dtAcct = prex.getPreference({
    					preferences: prefs.value,
    					name: 'logistics-equipment-rental'
    				});

    				// Get the project field from the EUR.
    		    	var project = rec.getValue( {fieldId: 'custrecord_eym_deur_h_proj'} );

    				// Get the EUR bqi value for the project.
    		    	var bqi	= rec.getValue( {fieldId: 'custrecord_eym_eur_h_bqi' } );

    				// Set project ledger values from the EUR details.
    				plRec.setValue({
    	    			fieldId	: 'custrecord_eym_ple_acc_num',
    	    			value	: dtAcct
    	    		});
    				plRec.setValue({
    	    			fieldId	: 'custrecord_eym_ple_debit',
    	    			value	: deurTotal
    	    		});
    				plRec.setValue({
    	    			fieldId	: 'custrecord_eym_ple_bqi',
    	    			value	: bqi
    	    		});
    	    		plRec.setValue({
    	    			fieldId	: 'custrecord_eym_ple_proj_code',
    	    			value	: project
    	    		});

    				// TODO: update bqi usage

    			} else if( j == 1 ) {

    				// Get preference values, credit account and equipment project.
    				var ctAcct = prex.getPreference({
    					preferences: prefs.value,
    					name: 'logistics-rental-income'
    				});
    				var eqtProj = prex.getPreference({
    					preferences: prefs.value, 
    					name: 'equipment-dept-project'
    				});

    				// Set values for the credit line.
    				plRec.setValue({
    	    			fieldId	: 'custrecord_eym_ple_acc_num',
    	    			value	: ctAcct
    	    		});
    				plRec.setValue({
    	    			fieldId	: 'custrecord_eym_ple_credit',
    	    			value	: deurTotal
    	    		});
    				plRec.setValue({
    	    			fieldId	: 'custrecord_eym_ple_proj_code',
    	    			value	: eqtProj
    	    		});
    			}

        		// Save the project ledger record.
        		var recPosted = plRec.save( {} );

        		// Check if the record was not posted.
        		if( !recPosted ) postedAll = false;
    		}

    		// Continue the loop.
    		return true;
    	});

    	// Update the status of the EUR record.
    	if( postedAll ) {

    		// Update the value of the EUR status to posted.
    		rec.setValue( {fieldId: 'custrecord_eym_eur_h_status', value: 3 } );

    		// Save the record.
    		rec.save( {} );
    	}

    	// Return a boolean value.
    	return postedAll;
    }

    return {
        onRequest: onRequest
    };

});
