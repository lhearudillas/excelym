function tryLoadRecord( internalId, recordId )
{

	// Return a value.
	return tryCatch( "nlapiLoadRecord(args[0], args[1])", [internalId, recordId] );
}

function trySearchRecord ( type, id, filtersObject, columnsArray )
{
	// Declare function variables.
	var filters = null;
	var columns = null;

	if( filtersObject ) {

		// Create an array to put the filters.
		filters = new Array();

		// Loop thru the searchObject parameter.
		for(var n in filtersObject) {

			// Push a new filter object based on the object values.
			filters.push(new nlobjSearchFilter(n, null, filtersObject[n].operator, filtersObject[n].value));
		}
	}

	if( columnsArray ) {

		// Create an array for search columns.
		var columns = new Array();

		// Fill the columns array based on the parameters.
		for(var i = 0; i < columnsArray.length; i++) {

			// Check if the member is also an array.
			if( Array.isArray( columnsArray[i] ) ) {

				// Assign to an item.
				var item = columnsArray[i];

				// Verify that all the index has values or has null and not undefined.
				if( item[0] != undefined ) {

					// Assign variables to function arguments.
					var arg1 = item[0];
					var arg2 = item[1] != undefined ? item[1] : null;
					var arg3 = item[2] != undefined ? item[2] : null;

					// Create a columns array with additional arguments for join or summary.
					columns.push( new nlobjSearchColumn( arg1, arg2, arg3 ) );
				}

			} else {

				// Create a colunms array without join and summary.
				columns.push( new nlobjSearchColumn( columnsArray[i] ) );
			}
		}
	}

	return tryCatch("nlapiSearchRecord(args[0], args[1], args[2], args[3]);", [type, id, filters, columns]);
}

function tryCatch( cmdStr, args )
{

    // Declare function variables.
    var returnValue = null;
    var error       = false;
    var errorObject = null;

    // Attempt to load the object.
    try{
    	returnValue = eval( cmdStr );
    } catch(e) {
    	error = true;
    	errorObject = e;
    }

    // Return a value.
    return {
        error: error,
        value: returnValue,
        error_object: errorObject
    };
}