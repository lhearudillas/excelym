/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       07 Jun 2018     Roy Selim
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 *
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only)
 *                      paybills (vendor payments)
 * @returns {Void}
 */

function userEventAfterSubmit(type)
{
	// Initialize scope variables.
	var record 	= null;
	var bqi		= null;

	// For create operation.
	if( type == 'create' ) record = nlapiGetNewRecord();

	// For edit operation.
	if( type == 'edit' ) {

		// Get the PLE ID.
		var pleId = nlapiGetRecordId();

		// Load the record.
		record = nlapiGetOldRecord( pleId );
	}

	// For delete operation.
	if( type == 'delete' ) record = nlapiGetOldRecord();

	// Check if a record was loaded.
	if( record ) {

		// Get the bqi.
		var bqi = record.getFieldValue( 'custrecord_eym_ple_bqi' );

		// Update the bqi usage.
		if( bqi ) updateBQIUsage( bqi );
	}
}