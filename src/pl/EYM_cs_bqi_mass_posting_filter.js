/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 * @NAmdConfig  /SuiteScripts/excelym/config/configuration.json
 */
define([ 'N/ui/dialog', 'N/runtime', 'EYM/preferences' ],

function( dialog, runtime, prex ) {

    /**
     * Function to be executed after page is initialized.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.mode - The mode in which the record is being accessed (create, copy, or edit)
     *
     * @since 2015.2
     */
    function pageInit(scriptContext) 
    {
		// Get the preferences.
		var prefs = prex.getPreferences( {} );

		// Get the access roles for this page.
		var roles = prex.getPreference( {preferences: prefs.value, name: 'eur-post-button'} );

		// Get the role of the logged in user.
		var role = runtime.getCurrentUser().role;

		if( roles.indexOf( role.toString() ) == -1 ) {

			// Show a dialog message that he does not have access to the page.
			dialog.alert({
				title: 'Access denied',
				message: 'Access denied! your role is not allowed to access this page. Please contact your administrator.'
			});
		}
    }

    /**
     * Function to be executed when field is changed.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     * @param {string} scriptContext.fieldId - Field name
     * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
     * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
     *
     * @since 2015.2
     */
    function fieldChanged(scriptContext) {

    	// If the filter field was changed, run the script.
		if( scriptContext.fieldId == 'custpage_project_filter') {

			// Get the form.
          	var form = jQuery( '#main_form' );

          	// Click the unmark all to avoid posting checked item/s.
          	jQuery( "#custpage_eur_sublistunmarkall" ).trigger( 'click' );

          	// Submit the form to reload the page with the chosen filter value.
          	form.submit();
        }
    }

    return {
    	pageInit: pageInit,
        fieldChanged: fieldChanged
    };

});
