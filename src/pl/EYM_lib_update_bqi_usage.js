function updateBQIUsage( bqi )
{
  	// Initialize scope variables.
	var usage = 0;

	// Try to load the bqi record.
	var tryBqi = tryLoadRecord( 'customrecord_eym_boq', bqi );

	// Check if the bqi record was loaded.
	if( !tryBqi.error && tryBqi.value ) {

		// Assign the record to a variable.
		bqiRecord = tryBqi.value;

		// Create a search filter.
		var filters = { custrecord_eym_ple_bqi: { operator: 'is', value: bqi } };

		// Create a columns return array.
		var columns = [
			[
				'custrecord_eym_ple_bqi',
				null,
				'group'
			],
			[
				'custrecord_eym_ple_debit',
				null,
				'sum'
			],
			[
				'custrecord_eym_ple_credit',
				null,
				'sum'
			]
		];

		// Try to get all usage from PLES.
		var tryUsage = trySearchRecord( 'customrecord_eym_proj_ledger_entry', null, filters, columns );

		// Check the return value.
		if( !tryUsage.error && tryUsage.value ) {

			// Assign the value to a variable.
			var usage = tryUsage.value[0];

			// Get the total debit
			var totalDebit = usage.getValue( 'custrecord_eym_ple_debit', null, 'sum' );

			// Get the total credit.
			var totalCredit = usage.getValue( 'custrecord_eym_ple_credit', null, 'sum' );

			// To comute BQI usage from all PLEs, subtract total credit from total debit.
			usage = totalDebit - totalCredit;
		}

	    // Update the record.
	    bqiRecord.setFieldValue( 'custrecord_eym_bqi_usage', usage );

	    // Update the record.
	    nlapiSubmitRecord( bqiRecord );
	}
}