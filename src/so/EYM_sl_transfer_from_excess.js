/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       24 Jan 2018     EYM-013
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */

function suitelet(request, response){

	if (request.getMethod() == 'GET') {
		doGet(request, response);
	}else if (request.getMethod() == 'POST') {
		doPost(request, response);
	}

}

function doGet(request, response){

	var projCode = request.getParameter('projID');
	var projCustomer = request.getParameter('parent');

	response.writePage(createSTRecord(null, projCode, projCustomer));

}

function doPost(request, response)
{
	var mode = request.getParameter('custpage_mode');
	if(mode == 'close') {
		doPostClose(response);
	}else{
		doPostCreateST(request,response);
	}
}

function createSTRecord(abRecs, projectCode, projectCustomer){
	var values = new Array();

	var form = nlapiCreateForm('Procurement Plan', true);
	form.setScript('customscript_cs_eym_st_creation');


	var fld = form.addField('custpage_mode', 'text', 'Customer');
	fld.setDisplayType('hidden');

	//hide this
	var backOrder = form.addField('custpage_sl_backorder','text','Value').setDisplayType('hidden');
  	var orderIdCreated = form.addField('custpage_sl_order','select','SO','transaction').setDisplayType('hidden');

	//end hide

	var sublistFld = form.addSubList('custpage_trans_excess_sublist', 'list', 'Transfer from Excess');

	sublistFld.addButton('custpage_mark_all','Mark All','markAll_PR()');
	sublistFld.addButton('custpage_unmark_all','Unmark All','unMarkAll_PR()');

	sublistFld.addField('custpage_trans_excess_select','checkbox','Select')
	sublistFld.addField('custpage_trans_excess_proj','select', 'Project', 'job').setDisplayType('inline').setDisplayType('hidden');
	sublistFld.addField('custpage_trans_excess_cust','select','Customer','customer').setDisplayType('inline').setDisplayType('hidden');
	sublistFld.addField('custpage_trans_excess_boq','select', 'Bill of Quantity', 'customrecord_eym_boq').setDisplayType('inline');
	sublistFld.addField('custpage_trans_excess_ppi','select', 'Procurement Plan Item', 'customrecord_eym_procplan_item').setDisplayType('inline');
	sublistFld.addField('custpage_trans_excess_item','select', 'Item', 'item').setDisplayType('inline');
	sublistFld.addField('custpage_trans_excess_qty','float', 'PRP Quantity').setDisplayType('inline');
    sublistFld.addField('custpage_trans_excess_fulfilled_qty','float', 'Reserved Quantity').setDisplayType('inline');
	sublistFld.addField('custpage_trans_excess_remaining_qty','float', 'Remaining Quantity').setDisplayType('inline');
    sublistFld.addField('custpage_trans_excess_availonhand','float', 'Available on Hand').setDisplayType('inline');
	sublistFld.addField('custpage_trans_excess_loc','select','Location', 'location').setDisplayType('inline');
	sublistFld.addField('custpage_trans_excess_req_qty','float', 'Quantity for Request').setDisplayType('inline');
	
	sublistFld.addField('custpage_trans_excess_rec_date','text', 'Receiving Date').setDisplayType('hidden');
	sublistFld.addField('custpage_trans_excess_rec_loc','text', 'Receiving Location').setDisplayType('hidden');
	sublistFld.addField('custpage_trans_excess_remarks','text', 'Remarks').setDisplayType('hidden');


	var filter = new Array();
	filter.push(nlobjSearchFilter('custrecord_eym_procplan_projcode', null, 'is', projectCode));

	var ppItems = nlapiSearchRecord(null,'customsearch_eym_prp_item', filter);
	if(ppItems != null) {
		var itemIds = new Array();
		for(var i = 0; i < ppItems.length; i++) {
		    var currentId = ppItems[i].getValue('custrecord_eym_procplan_item');
		    if( itemIds.indexOf(currentId) == -1 ) itemIds.push(currentId);
		}

		var acquiredItemsRess = getAcquiredItems(itemIds);
		var itemRess = getInventoryItems(itemIds);

		var objPrps =  new Array();
        var locationsInItems = {};

		for(var i = 0; i < ppItems.length; i++) {
			var ppItem = ppItems[i];

			objPrp = {};
			objPrp.id = ppItem.getId();
			objPrp.item = ppItem.getValue('custrecord_eym_procplan_item');
			objPrp.location_qtys = new Array();
			objPrp.display_boq = ppItems[i].getValue('custrecord_eym_procplan_bqi', null);
            objPrp.display_ppi = ppItems[i].getValue('internalid', null);
            objPrp.display_prp_qty = Number(ppItems[i].getValue('custrecord_eym_procplan_qty', null));
            objPrp.display_rec_date = ppItems[i].getValue('custrecord_eym_procplan_deldate', null);
            objPrp.display_rec_loc = ppItems[i].getValue('custrecord_eym_procplan_recloc', null);
            objPrp.display_remarks = ppItems[i].getValue('custrecord_eym_procplan_remarks', null);


			var itemRets = getAvailableItemsInExcessLocation(itemRess, objPrp.item);

            var itemRetsJson = JSON.parse(JSON.stringify(itemRets));
			if(locationsInItems['ITEM_' + objPrp.item] == undefined) {
				locationsInItems['ITEM_' + objPrp.item] = itemRetsJson;
			}

			if(itemRets.length > 0) {
				for(var j = 0; j < itemRets.length; j++){
					var locId = itemRets[j].getValue('inventorylocation', null);
					var locQty = itemRets[j].getValue('locationquantityavailable', null);
					var locNm = itemRets[j].getText('inventorylocation', null);

                    /*
					objPrp.location_qtys.push({
						"location_id":locId,
						"location_name":locNm,
						"on_hand":locQty
					});
                    */

				}
			}

			objPrp.acquired_qty = getAcquiredItemQty(acquiredItemsRess, objPrp.item, objPrp.id);

			objPrps.push(objPrp);
		}

        // Declare display variables.
        var displayBoq, displayPpi, displayItem, displayPrpQty = null;
        var displayTransQty, displayReqQty, displayLocation = null;
        var displayAvailableQty, displayFulfilledQty, displayRemainingQty = null;
        var displayRecDate, displayRecLoc, displayRemarks = null;


        // Set the values of the display variables.
        var lineItem = 0;
        for(var k = 0; k < objPrps.length; k++) {

          displayItem = objPrps[k].item;

          if(objPrps[k].acquired_qty < objPrps[k].display_prp_qty && locationsInItems['ITEM_' + displayItem].length > 0) {

            var runningQty = 0;
			var fulfilledQtyForDisplay = Number(objPrps[k].acquired_qty);
			displayPrpQty = Number(objPrps[k].display_prp_qty);
			var remainingQtyForDisplay = displayPrpQty - fulfilledQtyForDisplay;

            for(var l = 0; l < locationsInItems['ITEM_' + displayItem].length; l++){

              if(locationsInItems['ITEM_' + displayItem][l].columns.locationquantityavailable && Number(locationsInItems['ITEM_' + displayItem][l].columns.locationquantityavailable) > 0) {

                lineItem++;

                // Assign variable values.
                displayBoq = objPrps[k].display_boq;
                displayPpi = objPrps[k].display_ppi;
                displayFulfilledQty = Number(objPrps[k].acquired_qty) + runningQty;
                displayRemainingQty = displayPrpQty - displayFulfilledQty;
				displayAvailableQty = locationsInItems['ITEM_' + displayItem][l].columns.locationquantityavailable
				displayLocation = locationsInItems['ITEM_' + displayItem][l].columns.inventorylocation.internalid;
				displayRecDate = objPrps[k].display_rec_date;
				displayRecLoc = objPrps[k].display_rec_loc;
				displayRemarks = objPrps[k].display_remarks;


                // Check if remaining quantity is greater than available on hand.
                var toBreak = false;
                if(displayRemainingQty > displayAvailableQty) {
                  displayTransQty = displayAvailableQty;
                  runningQty += displayTransQty;
                } else {
                  displayTransQty = displayRemainingQty;
                  toBreak = true;
                }
				locationsInItems['ITEM_' + displayItem][l].columns.locationquantityavailable -= displayTransQty;

                // Put the display variables to the field.
                sublistFld.setLineItemValue('custpage_trans_excess_proj', lineItem, projectCode);
                sublistFld.setLineItemValue('custpage_trans_excess_cust', lineItem, projectCustomer);
                sublistFld.setLineItemValue('custpage_trans_excess_boq', lineItem, displayBoq);
                sublistFld.setLineItemValue('custpage_trans_excess_ppi', lineItem, displayPpi);
                sublistFld.setLineItemValue('custpage_trans_excess_item',lineItem, displayItem );
                sublistFld.setLineItemValue('custpage_trans_excess_qty', lineItem, displayPrpQty);
				sublistFld.setLineItemValue('custpage_trans_excess_fulfilled_qty', lineItem, fulfilledQtyForDisplay);
				sublistFld.setLineItemValue('custpage_trans_excess_remaining_qty', lineItem, remainingQtyForDisplay);
                sublistFld.setLineItemValue('custpage_trans_excess_availonhand', lineItem, displayAvailableQty);
                sublistFld.setLineItemValue('custpage_trans_excess_loc', lineItem, displayLocation);
                sublistFld.setLineItemValue('custpage_trans_excess_req_qty', lineItem, displayTransQty);
                
                sublistFld.setLineItemValue('custpage_trans_excess_rec_date', lineItem, displayRecDate);
                sublistFld.setLineItemValue('custpage_trans_excess_rec_loc', lineItem, displayRecLoc);
                sublistFld.setLineItemValue('custpage_trans_excess_remarks', lineItem, displayRemarks);


                // Check if item has to break.
                if(toBreak) {
                  break;
                }

              // Added 3/1/2018 4AM
              }
            }
          }

            /*
			for(var s in locationsInItems) {
				locationsInItems[s].sort(function(a,b){
					if(a.columns.locationquantityavailable != undefined) {
						return b.columns.locationquantityavailable - a.columns.locationquantityavailable;
					}
				});
			}
            */
          
        }
	 }

     // Code Till Here ==========================================================================

     form.addSubmitButton('Create Request');
     form.addButton('custpage_close', 'Close', 'submitClosePopup()');

     response.writePage(form);
}

function doPostClose(response)
{
	var html = '';
	var itemHtml = '';

	html = '<html>';
	html += '<head>';
	html += '<script language="JavaScript">';
	html += 'window.close();';
    html += '</script>';
    html += '</head>';
    html += '<body>';
    html += '</body>';
    html += '</html>';

    response.write(html);
}


function doPostCreateST(request,response)
{

	var soId = request.getParameter('custpage_sl_order');
	var backOrdervalue = request.getParameter('custpage_sl_backorder');

	if(backOrdervalue == 'true'){
		//load order
		var stId = nlapiLoadRecord('salesorder', soId);
		var itemCount = stId.getLineItemCount('item');

		for(var x=1; x <= itemCount; x++){

			var oldQty = stId.getLineItemValue('item','quantity',x);
			var backOrderedVal = stId.getLineItemValue('item','quantitybackordered',x);
			var committedVal = stId.getLineItemValue('item','quantitycommitted',x);
			var amt = stId.getLineItemValue('item','amount',x);

				if(oldQty > backOrderedVal) {
					//var newQty = oldQty - backOrderedVal;
					stId.setLineItemValue('item', 'quantity', x, committed);
					stId.setLineItemValue('item','amount',x, amt);

                    /*
					//code if no line item
					var newItemCount = stId.getLineItemCount('item');
					itemCount = newItemCount;
					x -= 1;
					
                    
					if(newItemCount == 0){
						nlapiDeleteRecord('salesorder', soId);
						alertforST();
					}
                    */

				} else if(oldQty <= backOrderedVal) {
					stId.removeLineItem('item', x);
					
                    var newItemCount = stId.getLineItemCount('item');
					itemCount = newItemCount;
					x -= 1;
                    
				}
		}

		nlapiSubmitRecord(stId);
		nlapiSetRedirectURL('RECORD','salesorder', soId);

	} else if(backOrdervalue == 'false') {

		nlapiDeleteRecord('salesorder', soId);

		var html = '';
		var itemHtml = '';

		html = '<html>';
		html += '<head>';
		html += '<script language="JavaScript">';
		html += 'window.close();';
	    html += '</script>';
	    html += '</head>';
	    html += '<body>';
	    html += '</body>';
	    html += '</html>';

	    response.write(html);

	} else {
		nlapiSetRedirectURL('RECORD', 'salesorder', soId);
	}

}

function getBinByLocation(searchResult, location){
  if(searchResult && location) {
    for(var i = 0; i<searchResult.length; i++) {
      binLoc = searchResult[i].getValue('binnumber');
      if(binLoc == location) {
        return searchResult[i].getValue('location');
      }
    }
  }
  return null;
}

function getAcquiredItems(itemIds)
{
	var filter = new Array();
	filter.push(nlobjSearchFilter('item', null, 'anyof', itemIds));

	return nlapiSearchRecord(null,'customsearch_eym_prp_acquired_items', filter);
}

function getAcquiredItemQty(acquiredItemRess, itemId, prpId) 
{
	var acquiredItemRet = 0;

	if(acquiredItemRess != null) {
		for(var i = 0; i < acquiredItemRess.length; i++) {
			var acquiredItemRes = acquiredItemRess[i];
			if(acquiredItemRes.getValue('item', null, 'GROUP') == itemId && acquiredItemRes.getValue('custcol_eym_sales_prpid', null, 'GROUP') == prpId) {
				// acquiredItemRet = acquiredItemRes[i].getValue('quantity', null, 'SUM');
                acquiredItemRet = acquiredItemRes.getValue('quantity', null, 'SUM');
				break;
			}
		}
	}

	return acquiredItemRet;
}

function getInventoryItems(itemIds, columnFields)
{
	var filter = new Array();
	filter.push(nlobjSearchFilter('internalid', null, 'anyof', itemIds));

	var columns = new Array();
	columns.push(new nlobjSearchColumn('internalid'));
	columns.push(new nlobjSearchColumn('locationquantityavailable'));
	columns[0].setSort();
	columns[1].setSort(true);

	return nlapiSearchRecord(null,'customsearch_eym_item_with_exs_locs', filter, columns);
}

function getInventoryItem(itemRess, itemId) 
{
	var itemRet = null;

	if(itemRess != null) {
		for(var i = 0; i < itemRess.length; i++) {
			var itemRes = itemRess[i];
			if(itemRes.getId() == itemId) {
				itemRet = itemRes;
				break;
			}
		}
	}

	return itemRet;
}

function getAvailableItemsInExcessLocation(itemRess, itemId)
{
	var itemRets = new Array();

	if(itemRess != null) {
		for(var i = 0; i < itemRess.length; i++) {
			var itemRes = itemRess[i];
			if(itemRes.getId() == itemId) {
				itemRets.push(itemRes);
			}
		}
	}

	return itemRets;
}
