/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       14 Feb 2018     EYM-013
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Sublist internal id
 * @param {String} name Field internal id
 * @param {Number} linenum Optional line item number, starts from 1
 * @returns {Void}
 */
function clientFieldChanged(type, name, linenum){

	if(name == 'item'){
      
    
		var itemId = nlapiGetFieldValue('item');
		
		var recItem = nlapiLoadRecord('inventoryitem', itemId);
		
		recItem.nlapiSelectNewLineItem('item');
		recItem.nlapiSetCurrentLineItemValue('item', 'item', itemvalue);
		recItem.nlapiCommitLineItem('item');
	}
   
 		if(name == 'origlocation'){
          		//alert('sample');
          var itemId = nlapiGetFieldValue('item');
          var origlocation = nlapiGetFieldValue('origlocation');
          	var recItem = nlapiLoadRecord('inventoryitem', itemId);
          
                var i = 1;
          		 var value1 = recItem.getLineItemValue('locations','location', i);
    			 var ppi_cw_value = recItem.getLineItemValue('locations','averagecostmli', i);
          		while(value1 != origlocation){
           		i++;
          		value1 = recItem.getLineItemValue('locations','location', i);
          		ppi_cw_value = recItem.getLineItemValue('locations','averagecostmli', i);
   			 }
          nlapiSetCurrentLineItemValue('item','custcol_eym_item_rate_srst', ppi_cw_value);
        }
}
