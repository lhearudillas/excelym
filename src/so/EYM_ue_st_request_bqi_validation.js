/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       29 Jan 2018     Roy Selim
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 *
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request) {

  	// Check if the type is view
  	if(type == 'view') {

      	// Get the popup parameter.
      	var showPopup = request.getParameter('showPopup');

      	// Get the bqis
      	var bqis = request.getParameter('bqis');

      	// Check if there is popup.
      	if(showPopup && bqis) {

          	var displayMessage = 'Notice: This transaction will result to an Over Budget on the following BQI: ';
          	displayMessage += bqis + '. Create a Project Change Order and return to this record to proceed with the request.'

          	// Add a field to the form.
          	form.addField('custpage_inlinehtml', 'inlinehtml').setDefaultValue("<script type='text/javascript'>alert('"+ displayMessage +"')</script>");
        }
    }
}