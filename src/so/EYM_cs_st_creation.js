function submitClosePopup()
{
	nlapiSetFieldValue('custpage_mode', 'close');
	window.ischanged = false;
	document.forms[0].submit();
	return true;
}


function submitSearchAsset()
{
	nlapiSetFieldValue('custpage_mode','search');
	window.ischanged = false;
	document.forms[0].submit();
	return true;
}


function submitAddToCase()
{
	var count = nlapiGetLineItemCount('custpage_asset_search_result');
	var hasSelected = false;
    
    for(var i = 1; i <= count; i++) {
        var isSelected = nlapiGetLineItemValue('custpage_asset_search_result','sublist_as_select', i);        
        if(isSelected == 'T') {                        
            hasSelected = true;
            break;
        }
    }

    if(!hasSelected) {
        alert("Please select an asset."); 
        return false;       
    }else {
    	nlapiSetFieldValue('custpage_mode', 'addtocase');
		window.ischanged = false;
		document.forms[0].submit();
		return true;
    }        
}


function markAll_PR(){
	var resultList = nlapiGetLineItemCount('custpage_trans_excess_sublist');
	if(resultList > 0){
		for(var i = 1;i<=resultList;i++){
			nlapiSetLineItemValue('custpage_trans_excess_sublist','custpage_trans_excess_select',i,'T');
		}
	}
}

function unMarkAll_PR(){
	var resultList = nlapiGetLineItemCount('custpage_trans_excess_sublist');
	if(resultList > 0){
		for(var i = 1;i<=resultList;i++){
			nlapiSetLineItemValue('custpage_trans_excess_sublist','custpage_trans_excess_select',i,'F');
		}
	}
}

/*
function clientFieldChanged(type, name, linenum){
	
	if(name == 'custpage_req_qty'){
		var index = nlapiGetCurrentLineItemIndex('custpage_proc_plan_sublist');

		var req_qty = nlapiGetCurrentLineItemValue('custpage_proc_plan_sublist', 'custpage_req_qty');
		var qty = Number(nlapiGetLineItemValue('custpage_proc_plan_sublist', 'custpage_qty', index));
				
		if(req_qty > qty){
			alert('Requested Quantity is greater than the Procurement Plan Quantity.');
			var req_qty2 = nlapiSetCurrentLineItemValue('custpage_proc_plan_sublist', 'custpage_req_qty', '');
		}
	}
}
*/


function clientSaveRecord1(){
	
	var resCount = nlapiGetLineItemCount('custpage_trans_excess_sublist');
	
    //loop
    var itemArray = new Array();
    
	if(resCount > 0){
		var count = 0;
		for(var a=1; a <= resCount; a++){
			var isSelected = nlapiGetLineItemValue('custpage_trans_excess_sublist','custpage_trans_excess_select',a); 
			
			if(isSelected == 'T'){
		
				var ppi_proj = nlapiGetLineItemValue('custpage_trans_excess_sublist','custpage_trans_excess_proj',a);
				var ppi_customer = nlapiGetLineItemValue('custpage_trans_excess_sublist','custpage_trans_excess_cust',a);
				var ppi_boq = nlapiGetLineItemValue('custpage_trans_excess_sublist','custpage_trans_excess_boq',a);
				var ppi_ppi = nlapiGetLineItemValue('custpage_trans_excess_sublist','custpage_trans_excess_ppi',a);
				var ppi_item = nlapiGetLineItemValue('custpage_trans_excess_sublist','custpage_trans_excess_item',a);
				var ppi_req_qty = nlapiGetLineItemValue('custpage_trans_excess_sublist','custpage_trans_excess_req_qty',a);
				var ppi_loc = nlapiGetLineItemValue('custpage_trans_excess_sublist','custpage_trans_excess_loc',a);
                var ppi_rec_date = nlapiGetLineItemValue('custpage_trans_excess_sublist','custpage_trans_excess_rec_date',a);
				var ppi_rec_loc = nlapiGetLineItemValue('custpage_trans_excess_sublist','custpage_trans_excess_rec_loc',a);
				var ppi_remarks = nlapiGetLineItemValue('custpage_trans_excess_sublist','custpage_trans_excess_remarks',a);

				itemArray.push({
					'project': ppi_proj,
					'customer': ppi_customer,
					'boq': ppi_boq,
					'ppi': ppi_ppi,
					'item_value': ppi_item,
					'qty': ppi_req_qty,
					'loc': ppi_loc,
                    'rec_date': ppi_rec_date,
					'rec_loc': ppi_rec_loc,
					'remarks': ppi_remarks,

				});
							 
			} else if(isSelected == 'F') {
				count++; 
				
			}
			
		}
      
		if (count == resCount) {
			alert('Notice: No items selected.');
			return false;
		}
		
		var soId = createST(ppi_proj,ppi_customer,itemArray);
		 
	} 
	
    return true;
	
}


function createST(ppi_proj,ppi_customer,itemArray){
	
	var st = nlapiCreateRecord('salesorder');
	
	st.setFieldValue('customform',108);
	st.setFieldValue('entity',ppi_customer);
	st.setFieldValue('job',ppi_proj);
    st.setFieldValue('custbody_eym_req_type',2);
	
	//loop array
	for(a = 0; a < itemArray.length; a++){
		st.setLineItemValue('item', 'custcol_eym_pur_bqi', a + 1, itemArray[a].boq);
		st.setLineItemValue('item', 'custcol_eym_sales_prpid', a + 1, itemArray[a].ppi);
		st.setLineItemValue('item', 'item', a + 1, itemArray[a].item_value);
		st.setLineItemValue('item', 'quantity', a + 1, itemArray[a].qty);
		st.setLineItemValue('item', 'location', a + 1, itemArray[a].loc);
        st.setLineItemValue('item', 'custcol_eym_pur_receiveby', a + 1, itemArray[a].rec_date);
		st.setLineItemValue('item', 'custcol_eym_pur_receiveloc', a + 1, itemArray[a].rec_loc);
		st.setLineItemValue('item', 'custcol_eym_pur_remarks', a + 1, itemArray[a].remarks);

	}
	
	var id = nlapiSubmitRecord(st,true,true);
	nlapiSetFieldValue('custpage_sl_order',id);
	
	if(id){
		
		var stRec = nlapiLoadRecord('salesorder', id);
		var itemCount = stRec.getLineItemCount('item');

		var backOrderedFlag = false;
		
			for(var x = 1; x < itemCount + 1; x++){
				var qty_value = stRec.getLineItemValue('item','quantity', x);
				var qty_backOrdered = stRec.getLineItemValue('item', 'quantitybackordered', x);
			
				if(qty_backOrdered > 0){
		
					backOrderedFlag = true;
					break;
				
				}	
			}
		
		if(backOrderedFlag == true){
			var v = confirm('Notice: The requested items are no longer available. Click OK to modify and proceed with the request.'); 
			if( v == true){
				nlapiSetFieldValue('custpage_sl_backorder','true');
			}else{
				nlapiSetFieldValue('custpage_sl_backorder','false');
			}
		}
		
	}
	return id;
}

function getRequestHeaders(){
  var headers = new Array();
  headers['Content-Type'] = 'application/json';
  headers['Accept'] = 'application/json';
  headers['User-Agent-x'] = 'SuiteScript Call';
  return headers;
}




