/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       07 Feb 2018     EYM-013
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request){
	if(type == 'create'){

		var custForm = nlapiGetFieldValue('customform');
         nlapiLogExecution('DEBUG', 'custForm', custForm);

      if(custForm == 108){

      	// Get the project ID from the query string.
      	var projID = request.getParameter('projID');

      	// Get the customer ID.
      	var customer = request.getParameter('parent');

      	// Pre-load the project ID to the form.
      	if(projID && customer) {
      		form.setFieldValues({job: projID, entity: customer});
        }

	  		//Search Procurement Plan Items association to the Project
	    	//var projectName = nlapiGetFieldValue('job');

	    	var projectName = nlapiGetFieldValue('job');

	  		var filters = [
	  			new nlobjSearchFilter('custrecord_eym_procplan_projcode', null, 'is', projectName),
	  			new nlobjSearchFilter('custrecord_eym_procplan_remaining_qty', null, 'isnotempty'),
	  			new nlobjSearchFilter('custrecord_eym_procplan_remaining_qty', null, 'greaterthan', 0)
	  		];

	  		var columns = [
	  			new nlobjSearchColumn('custrecord_eym_procplan_item'),
	  			new nlobjSearchColumn('custrecord_eym_procplan_remaining_qty'),
	  			new nlobjSearchColumn('internalid'),
	  			new nlobjSearchColumn('custrecord_eym_procplan_bqi'),
                new nlobjSearchColumn('custrecord_eym_procplan_qty')
	  		];

	  		var searchRes = nlapiSearchRecord('customrecord_eym_procplan_item',null,filters,columns);

	         var columns2 = [
	            new nlobjSearchColumn('binnumber'),
	            new nlobjSearchColumn('binonhandavail'),
	            new nlobjSearchColumn('saleunit'),
		  		new nlobjSearchColumn('purchasedescription')
	         ];

	         var invItemObj = {};
	         var itemsTracker = [];

	  		if(searchRes){
	  			for(var i = 0; i < searchRes.length; i++){
	  				var itemvalue = searchRes[i].getValue('custrecord_eym_procplan_item');

                  		var prpValue = searchRes[i].getValue('internalid');
		  				var bqiValue = searchRes[i].getValue('custrecord_eym_procplan_bqi');
		  				var prpQty = searchRes[i].getValue('custrecord_eym_procplan_qty');

	  				if(itemsTracker.indexOf(itemvalue) == -1) {
	  					itemsTracker.push(itemvalue);

  	  		  			var filters2 = [
		  		            new nlobjSearchFilter('binnumber', null, 'startswith', 'EXS'),
		  		            new nlobjSearchFilter('internalid', null, 'is', itemvalue)
		  		         ];

		  				var invItemRes = nlapiSearchRecord('inventoryitem',null,filters2,columns2);

		  				if(invItemRes != null) {
		  					invItemObj['ITEM_' + itemvalue] = JSON.parse(JSON.stringify(invItemRes));
		  				}
	  				}

	  				if(invItemObj['ITEM_' + itemvalue]){

	  				// var remainingQty = searchRes[i].getValue('custrecord_eym_procplan_remaining_qty');
                    var aggregateQty = getPRPAggregateQty(prpValue, itemvalue, true, true);
		  			var remainingQty = prpQty - aggregateQty;

		  				var currentHighest = getHighest(invItemObj['ITEM_' + itemvalue]);

		  			// Subtract the remaining quantity.
                        invItemObj['ITEM_' + itemvalue][currentHighest.index].columns.binonhandavail -= remainingQty;

		  				var locationWithExcess = [];

		  				while(remainingQty > 0 && currentHighest.location != null){
		  					if(remainingQty > currentHighest.available){
		  						var nxtQty = remainingQty - currentHighest.available;

			  				    	if(nxtQty > 0){

				  				      // Add the location to the display.
				  				      locationWithExcess.push(currentHighest);

				  				      // Remove the highest index in the array
				  				      invItemObj['ITEM_' + itemvalue] = invItemObj['ITEM_' + itemvalue].filter(function(item){
				  				        return item.columns.binonhandavail != (currentHighest.available - remainingQty)
				  				      })

				  				      // Get the next highest
				  				      currentHighest = getHighest(invItemObj['ITEM_' + itemvalue]);
			  				    	}

			  				    	remainingQty = nxtQty;

		  					} else{
			  				    remainingQty = 0;
			  				    locationWithExcess.push(currentHighest);
		  					}
		  				}

		  				for(var a = 0; a < locationWithExcess.length; a++){
		  					//Search for Location
		  					var binNumber = locationWithExcess[a].location;
			  					var filters3 = [
			  						new nlobjSearchFilter('binnumber', null, 'is', binNumber)
			  					];

				  		  		var columns3 = [
				  		  			new nlobjSearchColumn('binnumber'),
				  		  			new nlobjSearchColumn('location')
				  		  		];

				  		  		var binNumberRes = nlapiSearchRecord('bin',null,filters3,columns3);

				  		  			//Get the location
					  		  		if(binNumberRes){
					  		  			for(x = 0; x < binNumberRes.length; x++){
					  		  				var binNumberInt = binNumberRes[x].getValue('location');
					  		  			}
					  		  		}

				  		  		//Search for the info of the items in Inventory Item
						  		  	if(invItemRes){
					  		  			for(y = 0; y < invItemRes.length; y++){
					  		  				var saleUnit = invItemRes[y].getValue('saleunit');
							  		  		var itemDes = invItemRes[y].getValue('purchasedescription');
								  		  	//var qty = invItemRes[y].getLineItemValue('price1', 'price_1_', y);
								  			//var basePrice = invItemRes[y].getLineItemValue('price1', 'pricelevel', y);
					  		  			}
					  		  		}

						  		/*
						  		//Search for Price Level
						  		var priceLvl = nlapiLoadRecord('pricelevel', 1);
						  		var basePrice = priceLvl.getId();
						  		*/

						  		/*
						  		//Search for Tax Code
						  		var taxCode = nlapiLoadRecord('salestaxitem', 5);
						  		var taxCodeId = taxCode.getId();
						  		var taxRate = taxCode.getFieldValue('rate');

					  			//Search for Rate
					  			var invItemRate = nlapiLoadRecord('inventoryitem', itemvalue);
					  			var qty = invItemRate.getLineItemValue('price1', 'price_1_', 1);
					  			var basePrice = invItemRate.getLineItemValue('price1', 'pricelevel', 1);
					  			*/

		  					var binAvailable = locationWithExcess[a].available;
		  	                // remainingQty = searchRes[i].getValue('custrecord_eym_procplan_remaining_qty');
                            var remainingQty = prpQty - aggregateQty;

		  					var quantityToAdd = (remainingQty > binAvailable) ? binAvailable: remainingQty;

		  					//var amt =  quantityToAdd * qty;


                                 nlapiSelectNewLineItem('item');

                                 nlapiSetCurrentLineItemValue('item', 'item', itemvalue);

                                 nlapiSetCurrentLineItemValue('item', 'quantity', quantityToAdd);
                                 nlapiSetCurrentLineItemValue('item', 'location', binNumberInt);

                                 //nlapiSetCurrentLineItemValue('item', 'quantitycommitted', qtyCommitted);
                                 //nlapiSetCurrentLineItemValue('item', 'quantitybackordered', qtyBackOrdered);
                                 nlapiSetCurrentLineItemValue('item', 'units', saleUnit);
                                 nlapiSetCurrentLineItemValue('item', 'description', itemDes);
                                 //nlapiSetCurrentLineItemValue('item', 'pricelevels', basePrice);
                                 //nlapiSetCurrentLineItemValue('item', 'taxcode', taxCodeId);
                                 //nlapiSetCurrentLineItemValue('item', 'taxrate1', taxRate);
                                 //nlapiSetCurrentLineItemValue('item', 'rate', qty);
                                 //nlapiSetCurrentLineItemValue('item', 'amount', amt);


                                 nlapiSetCurrentLineItemValue('item', 'custcol_eym_sales_prpid', prpValue);
                                 nlapiSetCurrentLineItemValue('item', 'custcol_eym_pur_bqi', bqiValue);

                                 nlapiCommitLineItem('item');

		  				}

	  				}

	  			}
	  		}
		}
	}
}

function getHighest(thisObj){
  var location;
  var highest = 0;
  var index = 0;
  	for(var i = 0; i< thisObj.length; i++){
  		if(thisObj[i].columns.binnumber !== 'EXS-CW') {
  			var avail = Number(thisObj[i].columns.binonhandavail);
			if(avail > highest) {
				location = thisObj[i].columns.binnumber;
				highest = avail;
                                index = i;
			}
		}
  	}
	return {location: location, available: highest, index: index};
}


