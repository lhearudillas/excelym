/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       07 Feb 2018     EYM-013
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request){
	if(type == 'view'){
		var projID = nlapiGetRecordId();
		
		//The project has at least 1 Procurement plan
		var filters = new Array();
    	filters.push(new nlobjSearchFilter('custrecord_eym_procplan_projcode', null, 'anyof', projID));
    	var searchRes = nlapiSearchRecord('customrecord_eym_procplan_item', null, filters);
    	
    	if(searchRes > 0){
      		var redirectLink = nlapiResolveURL('RECORD', 'custform_eym_st_fr_excess');
  	  		redirectLink += '&projID=' + projID;

  	  		var buttonScript = "window.location.assign('"+ redirectLink +"')";

      		form.addButton('custpage_request_transfer_btn', 'Request Transfer from Excess', buttonScript);

    	}
	}
	
}
