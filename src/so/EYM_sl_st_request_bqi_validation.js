/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       13 Feb 2018     Roy Selim
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function displayMessage(request, response){

	// Check if request is GET or POST
	if(request.getMethod() == 'GET') {

		// Get the bqis parameter.
		var bqis = request.getParameter('bqis');

		// Get the record id.
		var recId = request.getParameter('recId');

		// Create a form
		var form = nlapiCreateForm('Over-budget notification');

      	// Add the error message.
      	var obMessage =  'Notice: This transaction will result to an Over Budget on the following BQI: ';
      	obMessage += bqis + '. Create a Project Change Order and return to this record to proceed with the request.”';

		// Add a text to the form.
		form.addField('bqi_message', 'label', obMessage);

      	// Create a redirect link.
      	var redirectLink = nlapiResolveURL('RECORD', 'salesorder', recId);

      	// Add a button.
      	var script = "window.location.assign('"+ redirectLink +"')";

      	// Add the button to the form.
      	form.addButton('redirect_button', 'Return to record', script);

		// Display the form.
		response.writePage( form );
	} else {

		// Dump response.
		dumpResponse(request,response);
	}
}