/*
 * @param {Number} prpId [required] - The internal ID of the procurement plan item.
 * @param {Number} itemId [required] - The internal ID of the item requested on the procurement plan.
 * @param {Boolean} st [optional] - If set to True, all items from the stock transfer records are deducted. Default TRUE.
 * @param {Boolean} pr [optional] - If set to True, all items from the purchase requisition records are deducted. Default TRUE.
 * @returns {Number|false} - Returns the aggregate quantity which is the sum of all fulfillment. If there are errors, it will return false.
 * @since 1.0
 */
function getPRPAggregateQty(prpId, itemId, so, pr)
{
	// Declare function variables.
	var aggregateQuantity = 0;
	var sor, prr = null;
	var resultsArray = [];
	var columns = ['quantity'];

    // Check the prpId & itemId.
    var id = Number(prpId);
    var item = Number(itemId);

    if(!isNaN(id) && id != 0 && typeof(prpId) != 'boolean' &&
       !isNaN(item)	&& item != 0 && typeof(itemId) != 'boolean') {

        // Get orders thru sales order
        if(so) {
        	var soFilter = {
        		custcol_eym_sales_prpid: {
        			operator: 'is',
        			value: id
        		},
        		customform: {
        			operator: 'is',
        			value: [104, 108]
        		},
        		item: {
        			operator: 'is',
        			value: item
        		}
	       };

        	// Search the sales order.
        	sor = performSearch(soFilter, columns, 'salesorder');
        }

        // Get usage thru purchase requisition.
        if(pr) {

        	// Search for all purchase requisition.
        	var filters = {
        		custcol_eym_sales_prpid: {
        			operator: 'is',
        			value: id
        		},
        		//approvalstatus: {
        			//operator: 'is',
        			//value: 2
        		//},
    			item: {
    				operator: 'is',
    				value: item
    			}
        	};

        	prr = performSearch(filters, columns, 'purchaserequisition');
        }

        // Aggregate all the results.
        resultsArray = [sor, prr];

        // Loop thru all the results.
        for(var i = 0; i < resultsArray.length; i++) {

        	// Check if the element is not null.
        	if(resultsArray[i]) {

        		// Loop thru all the results
        		for(var j = 0; j < resultsArray[i].length; j++) {

        			// Add the remaining quantity.
        			var qtyToAdd = resultsArray[i][j].getValue('quantity');

        			aggregateQuantity += Number(qtyToAdd);
        		}
        	}
        }

        return aggregateQuantity;
    } else {
    	return false;
    }
}

/*
 * @param {Object} searchObject [required]
 * @param {Array} columnArray [required]
 * @param {nlobjRecord} recordType [required]
 * @returns {nlobjSearchResult}
 * @since 1.0
 */
function performSearch(searchObject, columnArray, recordType) {

  // If there are errors, put them on an array.
  var errors = [];

  // Create a results array.
  var searchResult = [];

  // Create an array to put the filters.
  var filters = new Array();

  // Loop thru the searchObject parameter.
  for(var n in searchObject) {

    // Push a new filter object based on the object values.
    filters.push(new nlobjSearchFilter(n, null, searchObject[n].operator, searchObject[n].value));
  }

  // Create an array for search columns.
  var columns = new Array();

  // Fill the columns array based on the parameters.
  for(var i = 0; i < columnArray.length; i++) {
    columns.push(new nlobjSearchColumn(columnArray[i]));
  }

  // Perform the search against NetSuite and return the result.
  try{
	  searchResult = nlapiSearchRecord(recordType, null, filters, columns);
  } catch (e) {
	  errors.push(e);
  }

  // Check if there are errors, and return false.
  if(errors.length > 0) {
	  return false;
  } else {
	  return searchResult;
  }
}