/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       03 Sep 2017     denny
 *
 */

function validatePin(request, response){
	var currentUserId = nlapiGetUser();
	var pin = request.getParameter('pin');
	var recordtype = request.getParameter('type');
	var recordid = request.getParameter('id');
	nlapiLogExecution('DEBUG','currentUserId',currentUserId);
	nlapiLogExecution('DEBUG','type - id',recordtype+' - '+recordid);
	switch(recordtype){
		case 'project' : t = 'job'; field = 'custentity_boq_approver'; break;
		case 'pco' : t = 'customrecord_eym_change_order'; field = 'custrecord_eym_pco_approver'; break;
	}
	var approver = nlapiLookupField(t,recordid,field);
    var error = [];
    var encryptPin = nlapiEncrypt(pin,'sha1');
    var filter = [
	    new nlobjSearchFilter('custentity_eym_approver_type',null,'anyof',1),
	    new nlobjSearchFilter('custentity_eym_approver_pincode',null,'is',encryptPin),
	    new nlobjSearchFilter('internalid',null,'is',approver)
	];
	var employee = nlapiSearchRecord('employee',null,filter);
    
    if(!employee){
    		error.push('Invalid Pin'); 
    }
	var ret = {'error':error};
	response.write(JSON.stringify(ret));
}
