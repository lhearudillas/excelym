function userEventBeforeLoad(type, form, request){
	if(type == 'view' || type == 'edit'){
		var id = nlapiGetRecordId();
		var projStatus = nlapiGetFieldValue('entitystatus');
		var context = nlapiGetContext();
	    var role = context.getRole();	
	    
		var filters = new Array();
			filters[0] = new nlobjSearchFilter('custrecord_eym_boq_projid', null, 'is', id);	
		
		var columns = new Array();
			columns[0] = new nlobjSearchColumn('name');
			columns[1] = new nlobjSearchColumn('custrecord_eym_proj_subsystem');
			columns[2] = new nlobjSearchColumn('custrecord_eym_boq_categories');
			columns[3] = new nlobjSearchColumn('custrecord_eym_act_desc');
			columns[4] = new nlobjSearchColumn('custrecord_eym_boq_base_cost');
			columns[5] = new nlobjSearchColumn('custrecord_eym_boq_class');
			columns[6] = new nlobjSearchColumn('custrecord_eym_changeorder_no');

 		var searchRes = nlapiSearchRecord('customrecord_eym_boq',null,filters,columns);
		
 		if((projStatus == 2) && (searchRes != null)){
 			if(role == 1011 || role == 1012 || role == 1013){		//sandbox(Cost Approver=1011, Cost Engineer=1012, Operations Engineer = 1013)    prod(Cost Approver=1011, Cost Engineer=1012, Operations Engineer = 1013)
 				//sandbox
 				//var script = "window.location.href='https://system.sandbox.netsuite.com/app/common/search/searchresults.nl?searchtype=Job&Entity_INTERNALID=" + id +"&style=NORMAL&report=&grid=&searchid=88&dle=&sortcol=ADM_Custom_NAME_raw&sortdir=ASC&csv=HTML&OfficeXML=F&pdf=&size=50&twbx=F'";
 				
 				//prod
 				var script = "window.location.href='https://system.na3.netsuite.com/app/common/search/searchresults.nl?searchtype=Job&Entity_INTERNALID=" + id +"&style=NORMAL&report=&grid=&searchid=64&dle=&sortcol=ADM_Custom_NAME_raw&sortdir=ASC&csv=HTML&OfficeXML=F&pdf=&size=50&twbx=F'";
 				
 				form.addButton('custpage_exportboq_btn', 'Export BOQ', script);
 			}
 		}
    }
}