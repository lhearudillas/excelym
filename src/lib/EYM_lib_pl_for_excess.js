/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       04 Jun 2018     Roy Selim
 *
 */

function excessLedgerEntry( ifId, jeId )
{
	// Copy all the items from the journal entry. First load the journal entry record.
	var tryJeRec = tryLoadRecord( 'journalentry', jeId );

	// Check if record was loaded.
	if( !tryJeRec.error && tryJeRec.value ) {

		// Assign the value to a variable.
		var jeRec = tryJeRec.value;

		// Get the total number of je items.
		var jeItems = jeRec.getLineItemCount( 'line' );

		// Initialize a bqi array.
		var bqiArray = [];

		// Loop thru all the items.
		for( var i = 1; i <= jeItems; i++ ) {

			// Initialize scope variables.
			var bqi 			= null;
			var projCode 		= null;

			// Get the project code from the journal line item.
			projCode = jeRec.getLineItemValue( 'line', 'entity', i );

			// Get the bqi for the item.
			var bqi = jeRec.getLineItemValue( 'line', 'custcol_eym_pur_bqi', i );

	        // If there is a bqi value, push it to the array.
            if( bqi && ( bqiArray.indexOf( bqi ) == -1 ) ) bqiArray.push( bqi );

			// If the project code is not on the journal, try to get from the bqi.
			if( !projCode && bqi ) {

				// If the project code is not on the line item of the journal, get it from the bqi instead.
				projCode = nlapiLookupField( 'customrecord_eym_boq', bqi, 'custrecord_eym_boq_projid' );
			}

			// Check if the project code is present.
			if( projCode ) {

                // Create a je item.
                var geItem = {
                    accountId:	jeRec.getLineItemValue( 'line', 'account', i ),
                    bqi:		bqi,
                    credit: 	jeRec.getLineItemValue( 'line', 'credit', i ),
                    debit: 		jeRec.getLineItemValue( 'line', 'debit', i )
                };

                // Get the transaction date.
                var transDate = nlapiLookupField( 'journalentry', jeId, 'trandate' );

                // Add to excess project ledger entries.
                ledgerEntry( geItem, null, jeId, projCode, transDate );
			}
		}

		// Update all bqi usage.
		for( var j = 0; j < bqiArray.length; j++ ) {

			// Update the bqi usage.
			updateBQIUsage( bqiArray[j] );
		}
	}
}