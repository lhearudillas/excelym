/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       05 Jun 2018     Roy Selim
 *
 */
function deletePles( ples )
{
	// Loop thru all the PLEs
	for( var i = 0; i < ples.length; i++ ) {

		// Get the ID of the record.
		var pleId = ples[i].getValue( 'internalid' );

		// Use the ID to delete the record.
		nlapiDeleteRecord( 'customrecord_eym_proj_ledger_entry', pleId );
	}
}

// function ledgerEntry( glItem, plItem, plId, transId, projId  )
function ledgerEntry( glItem, plItem, transId, projId, transDate )
{
	// Initialize scope variables.
	var plItemId = null;

	// Assign the values of debit and credit to variables.
	var debit 	= glItem.debit;
	var credit 	= glItem.credit;
	var account = glItem.accountId;
	var bqi 	= glItem.bqi;
	var projid	= projId ? projId: glItem.projid;

	// Verify if the two are not empty.
	if( ( debit || credit ) &&  projid ) {

		// Get the pl id.
		// var plid = plId ? plId: getCreateProjectLedgerId( projid );

		// Create a project ledger item record if there is no plitem on the arguments.
		var projectLedgerItem = plItem ? plItem : nlapiCreateRecord( 'customrecord_eym_proj_ledger_entry' );

		// Set record values from the glItem variable.
		projectLedgerItem.setFieldValue( 'custrecord_eym_ple_debit', debit );
		projectLedgerItem.setFieldValue( 'custrecord_eym_ple_credit', credit );
		projectLedgerItem.setFieldValue( 'custrecord_eym_ple_acc_num', account );
		projectLedgerItem.setFieldValue( 'custrecord_eym_ple_bqi', bqi );

		// All the values are from the function arguments.
		// projectLedgerItem.setFieldValue( 'custrecord_eym_ple_pl_no', plid );
		projectLedgerItem.setFieldValue( 'custrecord_eym_ple_tran_id', transId );
		projectLedgerItem.setFieldValue( 'custrecord_eym_ple_proj_code', projid );
		
		// Check if the transaction date was included in the arguments.
		if( transDate ) projectLedgerItem.setFieldValue( 'custrecord_eym_ple_date', transDate );

		// Submit the project ledger entry.
		plItemId = nlapiSubmitRecord( projectLedgerItem );
	}
}

function getProjectLedgerEntries( id, filters, columns )
{
	// Use argument ID to search for project ledger entries. Start with creating a search.
	var searchFilter = filters ? filters : {
		custrecord_eym_ple_tran_id: { operator: 'is', value: id }
	};

	// Create columns array.
	var columsArray = columns ? columns : [ 'internalid' ];

	// Try to search for the the project ledger entries.
	return trySearchRecord( 'customrecord_eym_proj_ledger_entry', null, searchFilter, columsArray );
}

function getCreateProjectLedgerId( projCode )
{
	// Initialize scope variables.
	var projectLedgerId = null;

	// Check if there is a project code returned.
	if( projCode ) {

		// Search all project ledgers with this project code. We start with creating a search filter.
		var searchFilter = {
			custrecord_eym_pl_project: { operator: 'is', value: projCode }
		};

		// Create a search return column.
		var searchColumn = [ 'internalid' ];

		// Search netsuite for project ledger with this project code.
		var tryPls = trySearchRecord( 'customrecord_eym_project_ledger', null, searchFilter, searchColumn );

		// Check for errors and if there is a value returned.
		if( !tryPls.error && tryPls.value ) {

			// Get the ID of the first result.
			projectLedgerId = tryPls.value[0].getId();
		}

		// Check if there is a project ledger ID.
		if( !projectLedgerId ) {

			// Create the project ledger.
			var projectLedger = nlapiCreateRecord( 'customrecord_eym_project_ledger' );

			// Set the values of fields.
			projectLedger.setFieldValue( 'custrecord_eym_pl_project', projCode );

			// Submit the record to the backend.
			projectLedgerId = nlapiSubmitRecord( projectLedger );
		}
	}

	// Return the create project ledger id.
	return projectLedgerId;
}

function getProjectCode( recType, recId, fieldName )
{
	return nlapiLookupField( recType, recId, fieldName );
}

function projectLedgerEntry( id )
{
	// Initialize scope variables.
	var projCode 		= null;
	var projectLedgerId	= null;

	// Get the project code.
	projCode = getProjectCode( 'vendorbill', id, 'custbody_eym_pr_projcode' );

	// Get the project ledger id.
	// if( projCode ) projectLedgerId = getCreateProjectLedgerId( projCode );

	// Get the GL impact from the item fulfillment.
	var tryGl = getGLImpact( 'vendorbill', null, id );

	// Check if there are errors.
	if( !tryGl.error && tryGl.value ) {

		// Assign the gl array to a new variable.
		var gl = tryGl.value;

		// Add BQI to the gl.
		gl = addBQIToBillGL( gl, id, 'vendorbill' );

		// Create a bqi array.
		var bqiArray = [];

		// Loop thru all the gl impact array.
		for( var i = 0; i < gl.length; i++ ) {

			// Initialize a gl bqi container variable.
			var glBqi = gl[i].bqi;

			// If there is a bqi value, push it to the array.
			if( glBqi && ( bqiArray.indexOf( glBqi ) == -1 ) ) bqiArray.push( glBqi );
			
			// Get the transaction date.
			var transDate = nlapiLookupField( 'vendorbill', id, 'trandate' );

			// Add info to the ledger entry.
          	ledgerEntry( gl[i], null, id, projCode, transDate );
		}

		// Update all bqi usage.
		for( var j = 0; j < bqiArray.length; j++ ) {

			// Update the bqi usage.
			updateBQIUsage( bqiArray[j] );
		}
	}
}

function addBQIToBillGL( gl, id, recType, itemGroup )
{
	// Load the IF record.
	if( recType ) {

		// Load the record.
		var record = tryLoadRecord( recType, id );

		// Check if the record loaded properly.
		if( !record.error && record.value ) {

			// Assign the result to a variable.
			var rec = record.value;

			// Loop thru the gl impact.
			for( var i = 0; i < gl.length; i++ ) {

				// Initialize scope variables.
				var debit 	= gl[i].debit;
				var credit 	= gl[i].credit;
				var bqi 	= null;
				var projId	= null;

				// Check the debit and credit.
				if( debit ) {

					// Get the bqi
					bqi = getFieldByCompare( debit, rec, 'item', 'amount', 'custcol_eym_pur_bqi', '==' );

					// Get the project ID.
					projId = getFieldByCompare( debit, rec, 'item', 'amount', 'customer', '==' );
				}

				// Add the bqi to the gl object.
				gl[i].bqi = bqi;

				// Add the project id to the gl object.
				gl[i].projid = projId;
			}
		}
	}

	// Return the new GL
	return gl;
}

function getFieldByCompare( compareValue, record, groupName, compareField, returnField, operator )
{
	// Initialize the bqi variable.
	var returnValue = null;

	// Get the record number of lines.
	var recNumLines = record.getLineItemCount( groupName );

	// Loop thru all the items.
	for( var i = 1; i <= recNumLines; i++ ) {

		// Get the compareField value.
		var fldValue = record.getLineItemValue( groupName, compareField, i );

		// Compare the compareField value to the amount.
		eval( 'if( Number( compareValue ) ' + operator + ' Number( fldValue ) ) returnValue = record.getLineItemValue( groupName, returnField, i );' );
	}

	// Return the value.
	return returnValue;
}

function getGLImpact( type, id, recordId )
{
	// Create a search filter.
	var filtersObject = {
		internalid: { operator: 'anyof', value: recordId }
	};

	// Create a columns array
	var columnsArray = [
		['name', 'account'],
		'account',
		'debitamount',
		'creditamount'
	];

	// Attempt to load the record.
	var recAttempt = trySearchRecord( type, id, filtersObject, columnsArray );

	// Check if there is an error
	if( !recAttempt.error && recAttempt.value ) {

		// Assign the value to a variable.
		var results = recAttempt.value;

		// Create a different array from the results array.
		var glArray = ( results || [] ).map( function( line ) {

			// Create a new array.
	        return {
	            account: line.getValue( 'name', 'account' ),
	            accountId: line.getValue( 'account' ),
	            debit: line.getValue( 'debitamount' ),
	            credit: line.getValue( 'creditamount' )
	         };
	    });

		// Sanitize the array and remove the blank values.
		glArray = glArray.filter( function(item){

			// Return only if either debit or credit has value.
			return item.debit || item.credit
		});

		// Replace the result with the gl array.
		recAttempt.value = glArray;
	}

	// Return the rec attempt.
	return recAttempt;
}

function tryLoadRecord( internalId, recordId )
{

	// Return a value.
	return tryCatch( "nlapiLoadRecord(args[0], args[1])", [internalId, recordId] );
}

function trySearchRecord ( type, id, filtersObject, columnsArray )
{
	// Declare function variables.
	var filters = null;
	var columns = null;

	if( filtersObject ) {

		// Create an array to put the filters.
		filters = new Array();

		// Loop thru the searchObject parameter.
		for(var n in filtersObject) {

			// Push a new filter object based on the object values.
			filters.push(new nlobjSearchFilter(n, null, filtersObject[n].operator, filtersObject[n].value));
		}
	}

	if( columnsArray ) {

		// Create an array for search columns.
		var columns = new Array();

		// Fill the columns array based on the parameters.
		for(var i = 0; i < columnsArray.length; i++) {

			// Check if the member is also an array.
			if( Array.isArray( columnsArray[i] ) ) {

				// Assign to an item.
				var item = columnsArray[i];

				// Verify that all the index has values or has null and not undefined.
				if( item[0] != undefined ) {

					// Assign variables to function arguments.
					var arg1 = item[0];
					var arg2 = item[1] != undefined ? item[1] : null;
					var arg3 = item[2] != undefined ? item[2] : null;

					// Create a columns array with additional arguments for join or summary.
					columns.push( new nlobjSearchColumn( arg1, arg2, arg3 ) );
				}

			} else {

				// Create a colunms array without join and summary.
				columns.push( new nlobjSearchColumn( columnsArray[i] ) );
			}
		}
	}

	return tryCatch("nlapiSearchRecord(args[0], args[1], args[2], args[3]);", [type, id, filters, columns]);
}

function tryCatch( cmdStr, args )
{

    // Declare function variables.
    var returnValue = null;
    var error       = false;
    var errorObject = null;

    // Attempt to load the object.
    try{
    	returnValue = eval( cmdStr );
    } catch(e) {
    	error = true;
    	errorObject = e;
    }

    // Return a value.
    return {
        error: error,
        value: returnValue,
        error_object: errorObject
    };
}