/* String Manipulation */
function leftPad(number, length) {   
  var str = '' + number;
  while (str.length < length) {
      str = '0' + str;
  }
  return str;
}

function IsNullOrEmpty(value) {
	var isNullOrEmpty = true;
	
	if (value != undefined && value != null) {
		if (typeof (value) == 'string') {
			if (value.length > 0)
				isNullOrEmpty = false;
		}else {
			isNullOrEmpty = false;
		}
	}		
	
	return isNullOrEmpty;
}

function sugarJSDateFormat(nsDateFormat)
{
	var dateFormat = null;
	
	if(!IsNullOrEmpty(nsDateFormat)) {
		if (nsDateFormat.toUpperCase() == 'MM/DD/YYYY') {
			dateFormat = '{MM}/{dd}/{yyyy}';
		}else if (nsDateFormat.toUpperCase() == 'DD/MM/YYYY') {
			dateFormat = '{dd}/{MM}/{yyyy}';	
		}else if (nsDateFormat.toUpperCase() == 'DD-Mon-YYYY') {
			dateFormat = '{dd}-{Mon}-{yyyy}';
		}else if (nsDateFormat.toUpperCase() == 'DD.MM.YYYY') {
			dateFormat = '{dd}.{MM}.{yyyy}';
		}else if (nsDateFormat.toUpperCase() == 'DD-MONTH-YYYY') {
			dateFormat = '{dd}-{MONTH}-{yyyy}';
		}else if (nsDateFormat.toUpperCase() == 'DD MONTH, YYYY') {
			dateFormat = '{dd} {MONTH}, {yyyy}';
		}else if (nsDateFormat.toUpperCase() == 'YYYY/MM/DD') {
			dateFormat = '{yyyy}/{MM}/{dd}';
		}else if (nsDateFormat.toUpperCase() == 'YYYY-MM-DD') {
			dateFormat = '{yyyy}-{MM}-{dd}';
		}
	}
	
	return dateFormat;
}

function generateGUID() 
{
	function s4() 
	{
		return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
	}
	
	return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}
