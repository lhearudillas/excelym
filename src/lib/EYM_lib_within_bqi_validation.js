/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       12 Mar 2018     Roy Selim
 *
 */

function createCompareObject( recordObject, paramsObject )
{
	// Declare function variables.
	var compareObject = {};
	var lineNumbers   = {};
	var bqiNumbers 	  = [];
	var item_name 	  = 'item';
	var bqi_column	  = 'custcol_eym_pur_bqi';
	var amount_column = null;
	var prop_name 	  = 'proposed_amount'

	// Assign params to local variables.
	for( var o in paramsObject ) {
		eval( o + ' = \'' + paramsObject[o] + '\'' );
	}

	if( amount_column ) {

		// Get the number of items.
		var items = recordObject.getLineItemCount( item_name );

		// Check if there are items.
		if( items ) {

			// Loop thru all the items.
			for( var i = 1; i <= items; i++ ) {

				// Get the bqi value, number and gross amout.
				var bqiValue  	   = recordObject.getLineItemText( item_name, bqi_column, i );
				var bqiNumber 	   = recordObject.getLineItemValue( item_name, bqi_column, i );
				var proposedAmount = recordObject.getLineItemValue( item_name, amount_column, i );

                // Test the bqi value.
              	if( bqiValue && proposedAmount ) {

                  	// Test the compareObject for child properties.
                  	if( compareObject[bqiValue] === undefined ) {

                      	// Add the child object.
                        compareObject[bqiValue] = {};

                        // Add a 'Proposed' property.
                        compareObject[bqiValue][prop_name] = 0;

                      	// Add a line number.
                        lineNumbers[bqiValue] = [];

                        // Add to bqiNumbers.
                        bqiNumbers.push( bqiNumber );
                    }

                    // Add the proposed amount to the compareObject.
                    compareObject[bqiValue][prop_name] += Number( proposedAmount );

                    // Track the line number of the bqi.
                    lineNumbers[bqiValue].push( i );
              	}
			}

			// Return an object value.
			return {
				compare_object: compareObject,
				line_numbers: lineNumbers,
				bqi_numbers: bqiNumbers
			};
		}
	}

	// Return a false value.
	return false;
}

function appendCompareObject( compareObject, searchResultsArray, paramsObject )
{
	// Declare function variables.
	var member         = null;
	var get_type       = null;
	var prop_name      = null;
	var member_name    = null;
	var column_name    = null;
	var member_join    = null;
	var member_summary = null;
	var column_join    = null;
	var column_summary = null;

	// Assign params to variables.
	for( var p in paramsObject ) {
		eval( p + ' = \'' + paramsObject[p] + '\'' );
	}

	// Check for required parameters
	if( get_type && member_name && column_name && prop_name ) {

		// Loop thru all the results.
		for( var i = 0; i < searchResultsArray.length; i++ ) {

			// Check if we want to get the value or text property.
			if( get_type == 'value' ) {
				member = searchResultsArray[i].getValue( member_name, member_join, member_summary );
			} else if( get_type == 'text' ) {
				member = searchResultsArray[i].getText( member_name, member_join, member_summary );
			}

			// Check if the member property exists
			if( member && compareObject[member] ) {

				// Append the member property with value
				compareObject[member][prop_name] = Number( searchResultsArray[i].getValue( column_name, column_join, column_summary ) );
			}
		}
	}

	// Return a false value.
	return compareObject;
}

function tryLoadRecord( internalId, recordId ) {

  // Return a value.
  return tryCatch( "nlapiLoadRecord(args[0], args[1])", [internalId, recordId] );
}

function trySearchRecord ( type, id, filtersObject, columnsArray ) {

  // Declare function variables.
  var filters = null;
  var columns = null;

  if( filtersObject ) {

    // Create an array to put the filters.
    filters = new Array();

    // Loop thru the searchObject parameter.
    for(var n in filtersObject) {

      // Push a new filter object based on the object values.
      filters.push(new nlobjSearchFilter(n, null, filtersObject[n].operator, filtersObject[n].value));
    }
  }

  if( columnsArray ) {

    // Create an array for search columns.
    var columns = new Array();

    // Fill the columns array based on the parameters.
    for(var i = 0; i < columnsArray.length; i++) {

      columns.push(new nlobjSearchColumn(columnsArray[i]));
    }
  }

  return tryCatch("nlapiSearchRecord(args[0], args[1], args[2], args[3]);", [type, id, filters, columns])
}

function tryCatch( cmdStr, args ) {

    // Declare function variables.
    var returnValue = null;
    var error       = false;
    var errorObject = null;

    // Attempt to load the object.
    try{
      returnValue = eval( cmdStr );
    } catch(e) {
      error = true;
      errorObject = e;
    }

    // Return a value.
    return {
        error: error,
        value: returnValue,
        error_object: errorObject
    };
}