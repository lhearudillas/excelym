/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       31 May 2018     Roy Selim
 *
 */

function deletePles( ples )
{
	// Loop thru all the PLEs
	for( var i = 0; i < ples.length; i++ ) {

		// Get the ID of the record.
		var pleId = ples[i].getValue( 'internalid' );

		// Use the ID to delete the record.
		nlapiDeleteRecord( 'customrecord_eym_proj_ledger_entry', pleId );
	}
}

function ledgerEntry( glItem, plItem, transId, projId, transDate  )
{
  	// Initialize scope variables.
	var plItemId = null;

	// Assign the values of debit and credit to variables.
	var debit 	= glItem.debit;
	var credit 	= glItem.credit;
	var account = glItem.accountId;
	var bqi 	= glItem.bqi;

	// Verify if the two are not empty.
	if( debit || credit ) {

		// Create a project ledger item record if there is no plitem on the arguments.
		var projectLedgerItem = plItem ? plItem : nlapiCreateRecord( 'customrecord_eym_proj_ledger_entry' );

		// Set record values from the glItem variable.
		projectLedgerItem.setFieldValue( 'custrecord_eym_ple_debit', debit );
		projectLedgerItem.setFieldValue( 'custrecord_eym_ple_credit', credit );
		projectLedgerItem.setFieldValue( 'custrecord_eym_ple_acc_num', account );
		projectLedgerItem.setFieldValue( 'custrecord_eym_ple_bqi', bqi );

		// All the values are from the function arguments.
		projectLedgerItem.setFieldValue( 'custrecord_eym_ple_tran_id', transId );
		projectLedgerItem.setFieldValue( 'custrecord_eym_ple_proj_code', projId );
		
		// Check if transaction date is included in the argument.
		if( transDate ) projectLedgerItem.setFieldValue( 'custrecord_eym_ple_date', transDate );

		// Submit the project ledger entry.
		plItemId = nlapiSubmitRecord( projectLedgerItem );
	}
}

function getProjectLedgerEntries( ifId, filters, columns )
{
	// Use argument ID to search for project ledger entries. Start with creating a search.
	var searchFilter = filters ? filters : {
		custrecord_eym_ple_tran_id: { operator: 'is', value: ifId }
	};

	// Create columns array.
	var columsArray = columns ? columns : [ 'internalid' ];

	// Try to search for the the project ledger entries.
	return trySearchRecord( 'customrecord_eym_proj_ledger_entry', null, searchFilter, columsArray );
}

function addBQIToGL( gl, ifId )
{
	// Load the IF record.
	var ifRecord = tryLoadRecord( 'itemfulfillment', ifId );

	// Check if the record loaded properly.
	if( !ifRecord.error && ifRecord.value ) {

		// Assign the result to a variable.
		var ifRec = ifRecord.value;

		// Initialize variables.
		var debitBalance = 0;
		var creditBalance = 0;
		var lineNum = 1;

		// Loop thru the gl impact.
		for( var i = 0; i < gl.length; i++ ) {

			// Initialize scope variables.
			var debit = gl[i].debit;
			var credit = gl[i].credit;

			// Increment the balances.
			debitBalance += Number( debit );
			creditBalance += Number( credit );

			// Add the BQI property to the GL array.
			gl[i].bqi = ifRec.getLineItemValue( 'item', 'custcol_eym_pur_bqi', lineNum );

			// Check if they have already been incremented.
			if( (debitBalance || creditBalance) &&  ( debitBalance == creditBalance ) ) {

				// Reset the balances and increment the lineNum
				debitBalance = 0;
				creditBalance = 0;
				lineNum += 1;
			}
		}
	}

	// Return the new GL
	return gl;
}

function getProjectCode( ifId, irId )
{
	// Initialize scope variables
	var projCode 		= null;

	// If there is no IR id provided in the arguments, use the IF to find the proj code.
	if( irId == null ) {

		// Use the if to get the project id value.
		projCode = nlapiLookupField( 'itemfulfillment', ifId, 'entity' );

	} else {

		// Use the ir id to get the project code.
		projCode = nlapiLookupField( 'itemreceipt', irId, 'custbody_eym_pr_projcode' );
	}

	// Return the project id value.
	return projCode;
}

function projectLedgerEntry( ifId, irId )
{
	// Initialize scope variables.
	var projCode = null;

	// Get the project code.
	projCode = getProjectCode( ifId, irId );

	// Check if there is project code from the if.
	if( projCode ) {

        // Get the GL impact from the item fulfillment.
        var tryGl = getGLImpact( 'itemfulfillment', null, ifId );

        // Check if there are errors.
        if( !tryGl.error && tryGl.value ) {

            // Assign the gl array to a new variable.
            var gl = tryGl.value;

            // Add BQI to the gl.
            gl = addBQIToGL( gl, ifId );

          	// Create a bqi array.
			var bqiArray = [];

            // Loop thru all the gl impact array.
            for( var i = 0; i < gl.length; i++ ) {

              // Initialize a gl bqi container variable.
              var glBqi = gl[i].bqi;

              // If there is a bqi value, push it to the array.
              if( glBqi && ( bqiArray.indexOf( glBqi ) == -1 ) ) bqiArray.push( glBqi );
              
              // Get the transaction date.
              var transDate = nlapiLookupField( 'itemfulfillment', ifId, 'trandate' );

              // Add info to the ledger entry.
              ledgerEntry( gl[i], null, ifId, projCode, transDate );
            }

			// Update all bqi usage.
			for( var j = 0; j < bqiArray.length; j++ ) {

				// Update the bqi usage.
				updateBQIUsage( bqiArray[j] );
			}
        }
	}
}

function getGLImpact( type, id, recordId )
{
	// Create a search filter.
	var filtersObject = {
		internalid: { operator: 'anyof', value: recordId }
	};

	// Create a columns array
	var columnsArray = [
		['name', 'account'],
		'account',
		'debitamount',
		'creditamount'
	];

	// Attempt to load the record.
	var recAttempt = trySearchRecord( type, id, filtersObject, columnsArray );

	// Check if there is an error
	if( !recAttempt.error && recAttempt.value ) {

		// Assign the value to a variable.
		var results = recAttempt.value;

		// Create a different array from the results array.
		var glArray = ( results || [] ).map( function( line ) {

			// Create a new array.
	        return {
	            account: line.getValue( 'name', 'account' ),
	            accountId: line.getValue( 'account' ),
	            debit: line.getValue( 'debitamount' ),
	            credit: line.getValue( 'creditamount' )
	         };
	    });

		// Sanitize the array and remove the blank values.
		glArray = glArray.filter( function(item){

			// Return only if either debit or credit has value.
			return item.debit || item.credit
		});

		// Replace the result with the gl array.
		recAttempt.value = glArray;
	}

	// Return the rec attempt.
	return recAttempt;
}

function tryLoadRecord( internalId, recordId )
{

	// Return a value.
	return tryCatch( "nlapiLoadRecord(args[0], args[1])", [internalId, recordId] );
}

function trySearchRecord ( type, id, filtersObject, columnsArray )
{
	// Declare function variables.
	var filters = null;
	var columns = null;

	if( filtersObject ) {

		// Create an array to put the filters.
		filters = new Array();

		// Loop thru the searchObject parameter.
		for(var n in filtersObject) {

			// Push a new filter object based on the object values.
			filters.push(new nlobjSearchFilter(n, null, filtersObject[n].operator, filtersObject[n].value));
		}
	}

	if( columnsArray ) {

		// Create an array for search columns.
		var columns = new Array();

		// Fill the columns array based on the parameters.
		for(var i = 0; i < columnsArray.length; i++) {

			// Check if the member is also an array.
			if( Array.isArray( columnsArray[i] ) ) {

				// Assign to an item.
				var item = columnsArray[i];

				// Verify that all the index has values or has null and not undefined.
				if( item[0] != undefined ) {

					// Assign variables to function arguments.
					var arg1 = item[0];
					var arg2 = item[1] != undefined ? item[1] : null;
					var arg3 = item[2] != undefined ? item[2] : null;

					// Create a columns array with additional arguments for join or summary.
					columns.push( new nlobjSearchColumn( arg1, arg2, arg3 ) );
				}

			} else {

				// Create a colunms array without join and summary.
				columns.push( new nlobjSearchColumn( columnsArray[i] ) );
			}
		}
	}

	return tryCatch("nlapiSearchRecord(args[0], args[1], args[2], args[3]);", [type, id, filters, columns]);
}

function tryCatch( cmdStr, args )
{

    // Declare function variables.
    var returnValue = null;
    var error       = false;
    var errorObject = null;

    // Attempt to load the object.
    try{
    	returnValue = eval( cmdStr );
    } catch(e) {
    	error = true;
    	errorObject = e;
    }

    // Return a value.
    return {
        error: error,
        value: returnValue,
        error_object: errorObject
    };
}