/**
 *
 */

// This global client script will monitor the Enter if pressed.
jQuery( 'body' ).keypress( function( e ){

  	// Check if the enter key is pressed.
    if(e.keyCode == 13) {

        // Check if the PIN input element has focus.
        if( document.activeElement.id == 'if_save_pin' ) {

			// Get the button.
          	var submitButton = jQuery( '.ui-dialog-buttonset' ).children()[0];
          	jQuery( submitButton ).trigger( 'click' );
        }
    }
});

function insertJQUI()
{
    // Define function constants.
    const TOV_JQUERY_UI	= 'https://code.jquery.com/ui/1.12.1/jquery-ui.js';
    const TOV_JQUERY_CSS1	= '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css';
    const TOV_JQUERY_CSS2 = 'https://system.netsuite.com/core/media/media.nl?id=2070&c=4317943_SB1&h=cd245c977c25e6dce96f&_xt=.css';

    // insertScriptToHead( TOV_JQUERY_SRC );
    insertScriptToHead( TOV_JQUERY_UI );

    // Add the css to the head.
    insertLinkToHead( TOV_JQUERY_CSS1 );
    // insertLinkToHead( TOV_JQUERY_CSS2 );
}

function validatePIN( pic, btn, pinErrorStr, varToChange )
{
	(function(jq){

      	// Encode the the form html.
        var formHtml = '<div id="enter-pin-form" title="Enter PIN">';
        formHtml 	+=		'<input type="password" name="pin" id="if_save_pin"  class="text ui-widget-content ui-corner-all">';
        formHtml 	+=		'<div id="pin_result_notification" style="padding: 10px 0px 5px 5px"></div>';
        formHtml 	+= '</div>';

      	// Clear the notification.
        jq( '#pin_result_notification' ).html( '' );

      	// Check the DOM.
        var domPresent = jq( '#enter-pin-form' );

		// Check if the form has already been added.
      	if( domPresent[0] == undefined ) {

          	// Add to the form.
        	jq('body').append(formHtml);
        }

        // Validate the PIN
        dialog = jq( "#enter-pin-form" ).dialog({
              autoOpen: false,
              height: 220,
              width: 375,
              modal: true,
              position: { my: "center top", at: "center top", of: '#div__body' },
              buttons: {
                  "Submit": function() {

                      // Get the PIN entered by user.
                      var pinEntered = jq( '#if_save_pin' )[ 0 ].value;

                      // Verify if the user entered the correct pincode.
                      if( verifyPIN( pic, pinEntered ) ) {

                          // Close the modal.
                          dialog.dialog( "close" );

                          // Change the value of the variable.
                          eval( varToChange + ' = ' + pinEntered );

                          // Simulate a button click.
                          jq( '#' + btn ).trigger('click');

                      } else {

                          // Clear the pin text field.
						  jq( '#if_save_pin' )[0].value = '';

                          // Show invalid PIN message.
                          jq( '#pin_result_notification' ).html( pinErrorStr );
                      }
                  },
                  Cancel: function() {

                      // Close the modal dialog.
                      dialog.dialog( "close" );
                  }
              }
        });

        // Open the dialog
        return dialog.dialog("open");

	})(jQuery);
}

function insertScriptToHead ( src )
{
	// Define the element.
	var js 	= document.createElement( 'script' );
	js.type = 'text/javascript';
	js.src 	= src;

  	// Insert the script to the head element of the DOM.
  	jQuery( 'head' ).append( js );
}

function insertLinkToHead ( src )
{
	// Define the element.
	var link 	= document.createElement( 'link' );
	link.rel 	= 'stylesheet';
	link.href 	= src;

	// Insert the script to the head element of the DOM.
  	jQuery( 'head' ).append( link );
}

function verifyPIN( pic, pin )
{
  	// If pin is empty, return false;
  	if( !pin ) return false;

  	// Encrypt the pin.
  	pin = nlapiEncrypt( pin, 'sha1' );

    // Generate the columns.
    var columns = [ 'custentity_eym_approver_pincode' ];

    // Generate the filters.
    var filters = {
        internalid: { operator: 'is', value: pic },
        custentity_eym_approver_pincode: { operator: 'is', value: pin }
    };

    // Try to load the pins.
    var tryPinCodes = trySearchRecord( 'employee', null, filters, columns );

    // Verify if the user entered the correct pincode.
    if( !tryPinCodes.error && tryPinCodes.value ) {

      	// Return true
      	return true;
    }

  	// Retun a false value.
  	return false;
}