/**
 * EYM_lib_rentals_pl.js
 * @NapiVersion 2.x
 * @NModuleScope Public
 * @returns
 */

define([ 'N/search', 'N/record' ],

function( search, record ) {

	// Function to get the GL Impact.
	function getGLImpact( context )
	{
		// Create a search object
		var mySearch = search.create({
		  type: context.type,
		  columns: [
		    {name: 'name', join: 'account' },
		    {name: 'account'},
		    {name: 'debitamount'},
		    {name: 'creditamount'},
		    {name: 'memo'}
		  ],
		  filters: [
		    {
		      name: 'internalid',
		      operator: 'anyof',
		      values: [ context.recordId ]
		    }
		  ]
		});

		// Create an entity array.
		var entity = [];

		// Try to search for the record.
		try{
			// Run the search and run each result.
			mySearch.run().each(function(result){

				// Get the debit and credit values.
				var debit = result.getValue({name: 'debitamount'});
				var credit = result.getValue({name: 'creditamount'});

				// Check if debit or credit is not empty.
				if( debit || credit ) {

					// Push the value into the array.
					entity.push({
						debit		: debit,
						credit		: credit,
						memo		: result.getValue({name: 'memo'}),
						accountId	: result.getValue({name: 'account'}),
						account		: result.getValue({name: 'name', join: 'account' })
					});
				}

				// Continue with all the rest of the results.
				return true;
			});

			// Return the results.
			return {
				error: false,
				value: entity
			};

		// Catch any errors.
		} catch(e){

			return {
				error: true,
				msg: e.message
			}
		}
	}

	function getTransactionLine( options )
	{
		// Get the record.
		var rec = options.transactionRecord;

		// Get the line count.
		var count = rec.getLineCount({
			sublistId: options.sublistId
		});

		// Traverse thru all the items and compare.
		for( var i = 0; i < count; i++ ) {

			var compResult = false;

			// Check if the index is not exluded.
			if( options.exluded.indexOf( i ) == -1 ) {

				// Loop thru all the compare options.
				for( var n in options.compare ){

					// Assign the current item to a variable.
					var crntItm = options.compare[n];

					// Get the value.
					var fldVal = rec.getSublistValue({
						line		: i,
						fieldId		: crntItm.fieldId,
						sublistId	: 'item'
					});

					if( fldVal != crntItm.value ) {
						compResult = false;
						break;
					} else {
						compResult = true;
					}
				}

				// Test if sameVal is false.
				if( compResult ) {
					return i;
				}
			}
		}

		// Return a null value.
		return null;
	}

	// Function to delete project ledger entries
	function deletePles( options )
	{
		// Initialize scope constants.
		const DEL_TYP_STR = 'customrecord_eym_proj_ledger_entry';
		const DEL_FLD_STR = 'internalid';

		// Get the transaction id.
		var fieldId		= options.fieldId;
		var fldValArr	= options.fieldValues

		// Create a search object.
		var plesSearch = search.create({
			type: DEL_TYP_STR,
			columns: [
				{name: DEL_FLD_STR}
			],
			filters: [
				{
					name: fieldId,
					operator: 'anyof',
					values: fldValArr
				}
			]
		});

		plesSearch.run().each( function( ple ){

			// Get the value of the ple id.
			var pleId = ple.getValue( {name: DEL_FLD_STR} );

			// Delete the record.
			record.delete({
				type: DEL_TYP_STR,
				id: pleId
			});

			// Return a true value to continue.
			return true;
		});
	}

	// Try catch function.
	function tryCatch( cmdStr, args )
	{
	    // Declare function variables.
	    var error       = false;
	    var returnValue = null;
	    var errorObject = null;

	    // Attempt to load the object.
	    try{
	    	returnValue = eval( cmdStr );
	    } catch(e) {
	    	error = true;
	    	errorObject = e;
	    }

	    // Return a value.
	    return {
	        error: error,
	        value: returnValue,
	        error_object: errorObject
	    };
	}

	// Get preference function.
	function getPreference( options )
	{
		// Initialize return value.
		var returnValue = null;

		// Attempt to get the preference
		try{
			returnValue = search.create({
				type: 'customrecord_eym_preferences',
				columns: [ {name:'custrecord_eym_pref_value'} ],
				filters: [ {name: 'name', operator: 'is', values: [ options.name ] } ]
			}).run().getRange( {start:0, end:1} )[0].getValue( {name:'custrecord_eym_pref_value'} );
		} catch(e){
			// Log some errors here.
		}

		// Return a value.
		return returnValue;
	}

    return {
    	tryCatch: tryCatch,
    	getGLImpact: getGLImpact,
    	deletePles: deletePles,
    	getTransactionLine: getTransactionLine,
    	getPreference: getPreference
    };
});
