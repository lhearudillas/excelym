/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/search', 'N/record', './EYM_lib_rentals_pl.js'],

function( search, record, plLib ) {

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {string} scriptContext.type - Trigger type
     * @param {Form} scriptContext.form - Current form
     * @Since 2015.2
     */
    function beforeLoad( scriptContext ) {

    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function beforeSubmit( scriptContext )
    {
        // Assign values to variables.
    	var cntx 	= scriptContext;
    	var typ 	= cntx.UserEventType;

        // If the invoice is to be deleted, run this script.
    	if( cntx.type === typ.DELETE ) {

    		// Delete the ples
    		plLib.deletePles( {
    			fieldId: 'custrecord_eym_ple_tran_id',
    			fieldValues: [ cntx.newRecord.id ]
    		});
    	}
    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function afterSubmit( scriptContext )
    {
    	// Initialize scope variables.
    	var estRec = null;

    	// Assign values to variables.
    	var cntx 	= scriptContext;
    	var typ 	= cntx.UserEventType;
    	var rec		= cntx.newRecord;
    	var recId	= rec.id;

    	// Get the created from field value.
    	var crtdFrm = rec.getValue({
    		fieldId: 'createdfrom'
    	});

    	// Test the invoice if it was coming from an Quote
    	if( crtdFrm ) {

    		// Get the created from of the transaction.
    		var transFrom = search.lookupFields({
    			type: search.Type.SALES_ORDER,
    			id: crtdFrm,
    			columns: 'createdfrom'
    		});

    		// Assign the object to a variable.
    		var trnsFrmRes = transFrom.createdfrom;

    		// Check if it has a value.
    		if( trnsFrmRes ) {

    			// Try to load the estimate record.
    			try{
	    			var estRec = record.load({
	    				  type: record.Type.ESTIMATE,
	    				  id: trnsFrmRes[0].value
	    			});
    			} catch(e) {
    				// Log some errors here.
    			}
    		}

    	// If not from the Quote, this could possibly be an Affiliate invoice, check the equipment id.
    	} else {

    		// Get the customform preference.
    		var edCustomForm = plLib.getPreference( {name: 'psc-ed-invoice-customform'} );

    		// Get the rec custom form.
    		var recCustomForm = rec.getValue( {fieldId: 'customform'} );

	    	// If the field is not blank, set the estRec to true.
	    	if( edCustomForm == recCustomForm ) estRec = true;
    	}

    	// Make sure that this invoice is coming from a Quote.
    	if( estRec ) {

	    	// If the type is edit, delete the ples first.
	    	if( cntx.type === typ.EDIT ) {

	    		// Delete the ples
	    		plLib.deletePles( {
	    			fieldId: 'custrecord_eym_ple_tran_id',
	    			fieldValues: [ recId ]
	    		});
	    	}

	    	// Check if the type is edit or save.
	    	if( cntx.type === typ.EDIT || cntx.type === typ.CREATE ) {

	    		// Get the GL Impact.
	    		var tryGL = plLib.getGLImpact({
	    			type: search.Type.INVOICE,
	    			recordId: recId
	    		});

	    		// Check if there are no errors.
	    		if( !tryGL.error ) {

	    			// Assign the value to a variable.
	    			var gls = tryGL.value;

	    			// Load the invoice.
	    			var invRec = record.load({
	    				type: record.Type.INVOICE,
	    				id: recId
	    			});

	    			// Get the transaction date. Added for PNID-494
	    			var tranDate = invRec.getValue( {fieldId: 'trandate'} );

	    			// Create a tracker for all the trans line.
	    			var tltArr = [];

	    			// Run a function for each of the array values.
	    			gls.forEach( function( gl ){

	    				// Get only the transaction line, if it is credit or income.
	    				if( gl.credit && !gl.debit ) {

		    				// Get the transaction line for this GL line.
		    				var transLine = plLib.getTransactionLine({
		    					transactionRecord	: invRec,
		    					sublistId			: 'item',
		    					compare				: [
		    						{ fieldId: 'amount', value: Number( gl.debit + gl.credit ) },
		    						{ fieldId: 'description', value: gl.memo }
		    					],
		    					exluded 			: tltArr
		    				});
	    				}

	    				// Initialize an equipment variable.
	    				var eqpmnt = null;

	    				// Get the equipment asset number.
	    				if( transLine != null ) {

	    					// Add the transaction line to the tracker.
	    					tltArr.push( transLine );

	    					// Get the equipment asset Id.
		    				eqpmnt = invRec.getSublistValue({
		    					sublistId	: 'item',
		    					fieldId		: 'custcol_eym_fuel_so_equip_asset',
		    					line		: transLine
		    				});
	    				}

	    				// Get the project entry from the EYM Preference.
	    				var projectId = plLib.getPreference( {name: 'equipment-dept-project'} );

	    				// Create a project ledger custom record.
	    				var plRec = record.create({
	    					type: 'customrecord_eym_proj_ledger_entry',
	    				});

	    				// Set the values.
	    				plRec.setValue( {fieldId: 'custrecord_eym_ple_date',	value: tranDate} );
	    				plRec.setValue( {fieldId: 'custrecord_eym_ple_tran_id',	value: recId} );
	    				plRec.setValue( {fieldId: 'custrecord_eym_ple_acc_num',	value: gl.accountId} );
	    				plRec.setValue( {fieldId: 'custrecord_eym_ple_debit',	value: gl.debit} );
	    				plRec.setValue( {fieldId: 'custrecord_eym_ple_credit',	value: gl.credit} );
	    				plRec.setValue( {fieldId: 'custrecord_eym_ple_proj_code',value: projectId} );
	    				plRec.setValue( {fieldId: 'custrecord_eym_ple_equip_asset_num',value: eqpmnt} );

	    				// Save the record.
	    				var plId = plRec.save({ignoreMandatoryFields: true});
	    			});
	    		}
	    	}
    	}
    }

    return {
        afterSubmit: afterSubmit,
      	beforeSubmit: beforeSubmit
    };
});