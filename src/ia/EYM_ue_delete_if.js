/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       13 Mar 2018     denny
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type){
	nlapiLogExecution('DEBUG','type',type);
	if(type=='delete'){
		var recId = nlapiGetRecordId();
		var recType = nlapiGetRecordType();
		nlapiLogExecution('DEBUG','recId',recId);
		nlapiLogExecution('DEBUG','recType',recType);
		try{
			var field = '';
			switch(recType){
				case 'inventoryadjustment' : field = 'custbody_eym_link_invadj_auto'; break;
				case 'itemfulfillment' : field = 'custbody_eym_link_if_auto'; break;
				case 'salesorder' : field = 'custbody_eym_link_sr_auto'; break;
				case 'journalentry' : field = 'custbody_eym_link_journentry_auto'; break;
			}
			
//			var filters = [
//				new nlobjSearchFilter('type', null, 'anyOf', ['ItemRcpt', 'ItemShip']),
//				new nlobjSearchFilter('mainline', null, 'is', 'T'),
//				new nlobjSearchFilter(field, null, 'is', recId)
//			];
			var filters = [
				['type', 'anyOf', ['ItemRcpt', 'ItemShip']], 'AND',
				['mainline', 'is', 'F'], 'AND',
				[[field, 'is', recId], 'OR', ['internalid', 'is', recId]]
			];
			var columns = [
				new nlobjSearchColumn('custbody_eym_link_sr_auto'),
				new nlobjSearchColumn('custbody_eym_link_if_auto'),
				new nlobjSearchColumn('custbody_eym_link_invadj_auto'),
				new nlobjSearchColumn('custbody_eym_link_journentry_auto'),
				new nlobjSearchColumn('type'),
				new nlobjSearchColumn('createdfrom')
			]
			var transSearch = nlapiSearchRecord('transaction',null,filters, columns);
			if(transSearch){
				var transId = transSearch[0].getId();
				var transType = transSearch[0].getValue('type');
				nlapiLogExecution('DEBUG','transId',transId);
				nlapiLogExecution('DEBUG','transType',transType);
				var srId = transSearch[0].getValue('custbody_eym_link_sr_auto') ? transSearch[0].getValue('custbody_eym_link_sr_auto') : '';
				var ifId = transSearch[0].getValue('custbody_eym_link_if_auto') ? transSearch[0].getValue('custbody_eym_link_if_auto') : '';
				var iaId = transSearch[0].getValue('custbody_eym_link_invadj_auto') ? transSearch[0].getValue('custbody_eym_link_invadj_auto') : '';
				var jeId = transSearch[0].getValue('custbody_eym_link_journentry_auto') ? transSearch[0].getValue('custbody_eym_link_journentry_auto') : '';
				var createdFrom = transSearch[0].getValue('createdfrom') ? transSearch[0].getValue('createdfrom') : '';
				
				if(recType=='inventoryadjustment'){
					if(jeId){
						nlapiDeleteRecord('journalentry',jeId);
						nlapiLogExecution('DEBUG','JE deleted',jeId);
					}
					if(ifId){
						nlapiDeleteRecord('itemfulfillment',ifId);
						nlapiLogExecution('DEBUG','IF deleted',ifId);
					}
					if(srId){
						nlapiDeleteRecord('salesorder',srId);
						nlapiLogExecution('DEBUG','SR deleted',srId);
					}
					if(transType=='ItemRcpt'){
						nlapiSubmitField('itemreceipt',transId,'custbody_eym_confirm_status','1');
					}else if(transType=='ItemShip'){
						nlapiLogExecution('DEBUG','createdFrom',createdFrom);
						if(createdFrom){
							var soObj = nlapiLoadRecord('salesorder',createdFrom);
							var status = soObj.getFieldValue('status');
							nlapiLogExecution('DEBUG','SO status',status);
							if(status=='Closed'){
								var soLineCount = soObj.getLineItemCount('item');
								for(var i=1; i<=soLineCount; i++){
									soObj.setLineItemValue('item','isclosed',i,'F');
								}
								var updatedSoId = nlapiSubmitRecord(soObj);
								nlapiLogExecution('DEBUG','updatedSoId',updatedSoId);
							}
						}
						nlapiDeleteRecord('itemfulfillment',transId);
						nlapiLogExecution('DEBUG','Manual IF deleted',transId);
						
					}
				}else if(recType=='itemfulfillment'){
					if(iaId){
						nlapiDeleteRecord('inventoryadjustment',iaId);
						nlapiLogExecution('DEBUG','IA deleted',iaId);
					}
					if(jeId){
						
						// This code section was added by ROY SELIM for PNID-420	
						// Initialize a bqiArray.
						var bqiArray = [];
					
						// Get tall the project ledger associated with this JE record.
						var tryPles = getProjectLedgerEntries( jeId, null, [ 'internalid', 'custrecord_eym_ple_bqi' ] );
						
						// Check if there is a returned object.
						if( !tryPles.error && tryPles.value ) {

							// Assign the ples to a variable.
							var ples = tryPles.value;

							// Loop thru all the ples returned.
							for( var i = 0; i < ples.length; i++ ) {

								// Get all the BQIs from the ples.
								var bqi = ples[i].getValue( 'custrecord_eym_ple_bqi' );

								// Check if there is a bqi value.
								if( bqi && bqiArray.indexOf( bqi ) == -1 ) bqiArray.push( bqi );
							}

							// Delete all the project ledger entries.
							deletePles( ples );

							// Update the BQI usage.
							for( var j = 0; j < bqiArray.length; j++ ) {

								// Update the bqi usage.
								updateBQIUsage( bqiArray[j] );
							}
						}
						// Code edit ends here.
						
						nlapiDeleteRecord('journalentry',jeId);
						nlapiLogExecution('DEBUG','JE deleted',jeId);
					}
					if(srId){
						//nlapiDeleteRecord('salesorder',srId);
						nlapiSubmitField('salesorder',srId,'custbody_to_be_deleted','T');
						nlapiLogExecution('DEBUG','SR to be deleted',srId);
					}
					nlapiLogExecution('DEBUG','createdFrom',createdFrom);
					if(createdFrom){
						var soObj = nlapiLoadRecord('salesorder',createdFrom);
						var status = soObj.getFieldValue('status');
						nlapiLogExecution('DEBUG','SO status',status);
						if(status=='Closed'){
							var soLineCount = soObj.getLineItemCount('item');
							for(var i=1; i<=soLineCount; i++){
								soObj.setLineItemValue('item','isclosed',i,'F');
							}
							var updatedSoId = nlapiSubmitRecord(soObj);
							nlapiLogExecution('DEBUG','updatedSoId',updatedSoId);
						}
					}
				}else if(recType=='salesorder'){
					if(ifId){
						nlapiDeleteRecord('itemfulfillment',ifId);
						nlapiLogExecution('DEBUG','IF deleted',ifId);
					}
					if(iaId){
						nlapiDeleteRecord('inventoryadjustment',iaId);
						nlapiLogExecution('DEBUG','IA deleted',iaId);
					}
					if(transType=='ItemRcpt'){
						nlapiSubmitField('itemreceipt',transId,'custbody_eym_confirm_status','1');
					}
				}else if(recType=='journalentry'){

					// This code was added by ROY SELIM for PNID-407/PNID-464
					// Create a columns folder.
					var columns = [ 'internalid', 'custrecord_eym_ple_bqi' ];

					// Try to get the project ledger entries using the ID.
					var tryPles = getProjectLedgerEntries( recId, null, columns );

					// Initialize a bqiArray.
					var bqiArray = [];

					// Delete all the project ledger entries. Start with looping thru all PLEs
					if( !tryPles.error && tryPles.value ) {

						// Assign the current item to a variable.
						var ples = tryPles.value;

						// Loop thru all the ples.
						for( var k = 0; k < ples.length; k++ ) {

							// Get the bqi value.
							var bqi = ples[k].getValue( 'custrecord_eym_ple_bqi' );

							// Push the value to the array.
							if( bqi && ( bqiArray.indexOf( bqi) == -1 ) ) bqiArray.push( bqi );
						}

						// Delete the ples
						deletePles( ples );

						// Update the bqi record.
						for( var l = 0; l < bqiArray.length; l++ ) {

							// Update the record.
							updateBQIUsage( bqiArray[l] );
						}
					}
					// Code ends here.

					if(iaId){
						nlapiDeleteRecord('inventoryadjustment',iaId);
						nlapiLogExecution('DEBUG','IA deleted',iaId);
					}
					
					if(transType=='ItemShip'){
						nlapiLogExecution('DEBUG','createdFrom',createdFrom);
						if(createdFrom){
							var soObj = nlapiLoadRecord('salesorder',createdFrom);
							var status = soObj.getFieldValue('status');
							nlapiLogExecution('DEBUG','SO status',status);
							if(status=='Closed'){
								var soLineCount = soObj.getLineItemCount('item');
								for(var i=1; i<=soLineCount; i++){
									soObj.setLineItemValue('item','isclosed',i,'F');
								}
								var updatedSoId = nlapiSubmitRecord(soObj);
								nlapiLogExecution('DEBUG','updatedSoId',updatedSoId);
							}
						}
						nlapiDeleteRecord('itemfulfillment',transId);
						nlapiLogExecution('DEBUG','Manual IF deleted',transId);
					}
				}
			}
		}catch(e){
			var errMsg = '';
			if(e instanceof nlobjError)
				errMsg = e.getCode()+': '+e.getDetails();
	        else
	        		errMsg = 'UNEXPECTED ERROR: '+e.toString();	
			nlapiLogExecution('DEBUG','Error', recType+'/'+recId+' '+errMsg);
		}	
	}
}

function userEventAfterSubmit(type){
	nlapiLogExecution('DEBUG','After Submit type',type);
	if(type=='delete'){
		if(nlapiGetRecordType()=='itemfulfillment'){
			var filters = [
				new nlobjSearchFilter('mainline', null, 'is', 'T'),
				new nlobjSearchFilter('custbody_to_be_deleted', null, 'is', 'T')
			];
			var soSearch = nlapiSearchRecord('salesorder',null,filters);
			if(soSearch){
				var soId = soSearch[0].getId();
				var filters = [
					new nlobjSearchFilter('mainline', null, 'is', 'T'),
					new nlobjSearchFilter('custbody_eym_link_sr_auto', null, 'is', soId)
				];
				var irSearch = nlapiSearchRecord('itemreceipt',null,filters);
				
				nlapiDeleteRecord('salesorder', soId);
				nlapiLogExecution('DEBUG','SO deleted', soId);
				
				if(irSearch){
					nlapiSubmitField('itemreceipt',irSearch[0].getId(),'custbody_eym_confirm_status','1');
				}
			}
		}
		
		
	}
}	
