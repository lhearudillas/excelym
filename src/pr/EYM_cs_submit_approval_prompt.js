/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       04 Apr 2018     Roy Selim
 *
 */

/**
 * 
 */
(function(jq){
	jq( window ).on( 'load', function(){

      	// Change the onclick function.
      	jq( '#custpageworkflow137' ).on( 'click', function(){

          	// Alert a message.
          	alert( 'Budget status is \'Over Budget\', cannot proceed with approval.' );
        });
    })
})(jQuery);