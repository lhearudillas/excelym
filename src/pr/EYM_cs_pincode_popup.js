/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       05 Jan 2015     dashikoy
 *
 */

function customModal() {
  
  var jquery_modal = document.createElement("script");
  jquery_modal.type = "text/javascript";
  jquery_modal.src = "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js";
  
  document.getElementsByTagName('head')[0].insertBefore(script, jquery_modal);
  
  //loading css & javascript
  var jquery_modal = document.createElement("script");
  jquery_modal.type = "text/javascript";
  jquery_modal.src = "https://system.na3.netsuite.com/core/media/media.nl?id=536&c=4317943&h=c1a0fa134372b1c1ccd9&_xt=.js";
  // Use any selector
  $("head").append(jquery_modal);
  
  var s = document.createElement("script");
  s.type = "text/javascript";
  s.src = "https://system.na3.netsuite.com/core/media/media.nl?id=536&c=4317943&h=c1a0fa134372b1c1ccd9&_xt=.js";
  // Use any selector
  $("head").append(s);
  
  $('head').append('<link rel="stylesheet" href="https://system.na3.netsuite.com/core/media/media.nl?id=537&c=4317943&h=46754b3f758bf64e52a6&_xt=.css" type="text/css" />');
  
  //custom modal html
  var custom_modal_html;
  custom_modal_html += '<form name="frmPin" id="customFrmPin" style="display:none" method="POST">\n';
  custom_modal_html += '<p><label>PIN:</label></p><p><input type="password" name="pin" class="form-control" id="pin"></p>\n';
  custom_modal_html += '<p><input type="submit" id="btnOkPin" class="btn btn-primary" value="OK" /></p>\n';
  custom_modal_html += '</form>\n';
  
  $("body").append(custom_modal_html);
  
  $("#customFrmPin").modal({ modalClass:"jModal", escapeClose: false, clickClose: false});
  
}
function showPopup(scriptId, deploymentId, args, w, h)
{
    
  	
  
	var url = nlapiResolveURL('SUITELET', scriptId, deploymentId);
	if(!isEmpty(args)) {
		url += '&';
		url += args;
	}
	
	windowOpener(url, w, h);
}

function windowOpener(url, a, b)
{
	newWindow = window.open(url, "PopUp", "resizable=1,width=" + a + ",height=" + b + ",left=" + (window.screen.width - a) / 2 + ",top=" + (window.screen.height - b) / 2 + ",scrollbars=no");
	newWindow.focus();
	return newWindow.name
};

function isEmpty(fldValue)
{
	return fldValue == '' || fldValue == null || fldValue == undefined;
}