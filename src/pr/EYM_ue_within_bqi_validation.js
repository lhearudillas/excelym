/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       29 Jan 2018     Roy Selim
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 *
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */

function userEventAfterSubmit(type)
{
  	// Declare function variables.
	  var bqiNumbers    = [];
	  var budgetStatus  = 1;
	  var compareObject = {};
	  var lineNumbers   = {};
	  var validRecord   = false;
	  var recordType    = null;
	  var amountColumn  = null;
	  var logString     = null;
      var onBudgetCol   = null;
	  var overBudgets   = '';

	  // Get the id of the record.
	  var recordId = nlapiGetRecordId();

	  // Get the record type.
	  var recordType = nlapiGetRecordType();

	  // Check the record ID.
	  if( recordId && recordType && recordId != -1 ) {

			// Try to load the record.
		  var tryCatchObj = tryLoadRecord( recordType, recordId );

			// Check if there is a record loaded.
		  if( !tryCatchObj.error && tryCatchObj.value ) {

				// Assign a current record.
			  var currentRecord = tryCatchObj.value;

              // Assign the name of the within bqi column.
			  onBudgetCol = COM_ON_BGT_FLD;

		      // Get the form used on the record.
			  var formUsed = Number( currentRecord.getFieldValue( COM_CUST_FORM ) );

			  // Check the type of record loaded.
			  if( recordType == PRV_PR ) {

				  // Validate the record.
				  validRecord = ( !isNaN(formUsed) && formUsed != null && formUsed != 103 );

				  // Assign the correct amount column.
				  amountColumn = PRV_EST_AMOUNT;

				  // Assign the correct log string.
				  logString = PRV_LOG_ERROR_1;

			  // Check if record type is PO.
			  } else if ( recordType == POV_PO ) {

				  // Initialize a project code variable.
				  var projectCode = currentRecord.getFieldValue( POV_PROJECT_CODE );

				  // Validate the record.
				  validRecord = projectCode != null;

				  // Assign the correct amount column.
				  amountColumn = POV_GROSS_AMOUNT;

				  // Assign the correct log string.
				  logString = POV_LOG_ERROR_1;

			  // Check if record type is sales order.
			  } else if ( recordType == SOV_SO ) {

				  // Validate the record.
				  validRecord = ( formUsed && ( formUsed == 108 || formUsed == 104 ) );

				  // Assign the correct amount column.
				  amountColumn = SOV_RATE_REQ;

				  // Assign the correct log string.
				  logString = SOV_LOG_ERROR_1;

                  // Change the on budget column.
			  	  onBudgetCol = SOV_ON_BGT_FLD;
			  }

			  // Check if validation has passed for each record type.
			  if( validRecord ) {

				  // Get the total number of sublist items.
				  var itemsCount = currentRecord.getLineItemCount( COM_ITEM_NAME );

				  // Check if there are items.
				  if( itemsCount ) {

					  // Create a compareObject.
					  var coObj = createCompareObject( currentRecord, { amount_column: amountColumn } );

					  // Test the compareObject result.
					  if( coObj ) {

						  // Get the variables.
						  compareObject = coObj.compare_object;
						  lineNumbers   = coObj.line_numbers;
						  bqiNumbers    = coObj.bqi_numbers;

						  // Create a filter object.
						  var bqiFilterObj = { internalid: { operator: 'is', value: bqiNumbers } };

						  // Attempt to load a saved search.
						  var tryCatchBqiObj = trySearchRecord( null, COM_BQI_BUD_SS, bqiFilterObj );

						  // Check if there is a bqi object result.
						  if( !tryCatchBqiObj.error && tryCatchBqiObj.value ) {

							  // Create a params object.
							  var budgetParams = {
								  member_name: COM_NAME_VALUE,
								  get_type: COM_BGT_GET_TYPE,
								  prop_name: COM_EST_BGT,
								  column_name: COM_FORM_NUM
							  };

							  // Append the compareOBject.
							  compareObject = appendCompareObject( compareObject, tryCatchBqiObj.value, budgetParams );

							  // Create a saved search filter.
							  var ssFilterObj = { CUSTCOL_EYM_PUR_BQI: {operator: 'anyof', value: bqiNumbers} };

							  // Attempt to load the saved search usage amount.
							  var tryCatchSavedSearchObj = trySearchRecord( null, COM_SAVE_SEARCH, ssFilterObj );

							  if( !tryCatchSavedSearchObj.error && tryCatchSavedSearchObj.value ) {

								  // Create a usage params.
								  var usageParams = {
									  member_name: COM_BQI_VALUE,
									  get_type: COM_USG_GET_TYPE,
									  prop_name: COM_AMT_DDCTD,
									  column_name: COM_FORM_NUM,
									  member_summary: COM_SUMM_GRP,
									  column_summary: COM_SUMM_SUM
								  };

								  // Append the createObject with usage.
								  compareObject = appendCompareObject( compareObject, tryCatchSavedSearchObj.value, usageParams );
							  }

							  // Evaluate the compareObject if there are lines in Over budget.
							  for( var n in compareObject ) {

								  // Create a placeholder variable.
								  var testAmount;

								  // Check if there is a usage amount.
								  if(compareObject[n][COM_AMT_DDCTD]) {

									  // For sales order, double subtract the proposed amount from usage.
									  if( recordType == SOV_SO ) {
										  compareObject[n][COM_AMT_DDCTD] -= compareObject[n][COM_PRPSD_AMT];
									  }

									  // For all other record types, subtract the usage from the estimated budget.
									  var remainingAmount = compareObject[n][COM_EST_BGT] - compareObject[n][COM_AMT_DDCTD];
									  testAmount = remainingAmount - compareObject[n][COM_PRPSD_AMT];

								  } else {

									  // Subtract from the estimated amount then proposed amount.
									  testAmount = compareObject[n][COM_EST_BGT] - compareObject[n][COM_PRPSD_AMT];
								  }

								  // Set the budget status.
								  if(testAmount < 0) {

									  // Set the budget status field to 'Over Budget'.
									  budgetStatus = 2;

									  // For sales order, note the bqis that went overbudget.
									  overBudgets += n + ', ';
								  }

								  // Loop thru all the line items.
								  for(var j = 0; j < lineNumbers[n].length; j++) {

									  if( testAmount >= 0 ) {
										  currentRecord.setLineItemValue( COM_ITEM_NAME, onBudgetCol, lineNumbers[n][j], 'T');
									  } else {
										  currentRecord.setLineItemValue( COM_ITEM_NAME, onBudgetCol, lineNumbers[n][j], 'F');
									  }
								  }
							  }

							  if( recordType == PRV_PR || recordType == POV_PO ) {

								  // Set the budget status to.
								  currentRecord.setFieldValue( COM_STAT_FIELD, budgetStatus );
							  }

							  // Submit the record.
							  nlapiSubmitRecord( currentRecord );

							  // For sales order, add a query string to the redirect URL.
							  if( recordType == SOV_SO ) {

								  // Check if there are over budgets.
								  if( overBudgets.length > 0 ) {

									  // Remove the last comma.
									  overBudgets = overBudgets.replace( /,\s$/, '' );

									  // Redirect to record with addititonal query string parameters.
									  nlapiSetRedirectURL( 'RECORD', SOV_SO, recordId, false, {showPopup: 'T', bqis: overBudgets} );
								  }
							  }

						  // Log errors.
						  } else {
							  nlapiLogExecution( 'DEBUG', logString + ' Check if there is a bqi object result.', 'There is an error searching for the bqi record.' );
						  }
					  } else {
						  nlapiLogExecution( 'DEBUG', logString + ' Check the compareObject', 'Compare object was not successfully created.' );
					  }
				  } else {
					  nlapiLogExecution( 'DEBUG', logString + ' Check if there are items.', 'The record has no line items' );
				  }
			  } else {
				  nlapiLogExecution( 'DEBUG', logString + ' Check the form used.', 'formUsed value is either isNaN, null or not 103' );
			  }
		  } else {
			  nlapiLogExecution( 'DEBUG', logString + ' Check if there is a record loaded.', 'Error loading the record id: ' +  recordId + ' for record type: ' + recordType );
		  }
	  } else {
		  nlapiLogExecution( 'DEBUG', logString + ' Check the record ID.', 'Record is null or -1' );
	  }
}