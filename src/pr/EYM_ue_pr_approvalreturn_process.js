/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       30 Jan 2018     EYM-013
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request)
{
	
	try{
		if(type == 'edit' || type == 'view') {
			
			form.setScript('customscript_eym_cs_popup_window'); 
			
			//var rec = nlapiGetNewRecord();
			var recId = nlapiGetRecordId(); 
			var recType = nlapiGetRecordType();
			var rec = nlapiLoadRecord(recType, recId);

			var budgetStatus = rec.getFieldValue('custbody_eym_pur_budget_stat');	
			var appStatus = rec.getFieldValue('custbody_eym_wf_approval_status');	
			var custForm = rec.getFieldValue('customform');

          
			if (custForm == 100) {
				var isInternal = rec.getFieldValue('custbody_eym_is_internal');
				
				if(budgetStatus == 1 && appStatus == 2 && (isInternal == 'F' || isInternal == null)){
					var empId = rec.getFieldValue('nextapprover');
					var empNm = rec.getFieldText('nextapprover');
					var args = '&rec_id=' + recId + '&rec_type=' + recType + '&employee_id=' + empId + '&employee_name=' + empNm + '&status=' + appStatus;
					var script =  "showPopup('customscript_sl_eym_pr_pincode_popup', 'customdeploy_sl_eym_pr_pincode_popup', '" + args + "', 800, 600);";
					form.addButton('custpage_approve', 'Approve', script);				
				}		
			}
			else if (custForm == 103) {
				if(appStatus == 2) {
					var empId = rec.getFieldValue('nextapprover');
					var empNm = rec.getFieldText('nextapprover');
					var args = '&rec_id=' + recId + '&rec_type=' + recType + '&employee_id=' + empId + '&employee_name=' + empNm + '&status=' + appStatus;
					var script =  "showPopup('customscript_sl_eym_pr_pincode_popup', 'customdeploy_sl_eym_pr_pincode_popup', '" + args + "', 800, 600);";
					form.addButton('custpage_approve', 'Approve', script);				
				}	
			}	
			
			// PSC ED SubCon
			if (custForm == 142) {
				if(appStatus == 2) {
					var empId = rec.getFieldValue('nextapprover');
					var empNm = rec.getFieldText('nextapprover');
					var args = '&rec_id=' + recId + '&rec_type=' + recType + '&employee_id=' + empId + '&employee_name=' + empNm + '&status=' + appStatus;
					var script =  "showPopup('customscript_sl_eym_pr_pincode_popup', 'customdeploy_sl_eym_pr_pincode_popup', '" + args + "', 800, 600);";
					form.addButton('custpage_approve', 'Approve', script);		
				}
			}
		}
	} catch(ex){
		if(ex.getDetails != undefined){
			nlapiLogExecution('DEBUG', 'Exception', ex.message == '' ? ex : ex.message);
			throw ex;
		} else {
            nlapiLogExecution('ERROR', 'Unexpected Error', ex.toString());
            throw nlapiCreateError('99999', ex.toString());
        }
	}
 
}


