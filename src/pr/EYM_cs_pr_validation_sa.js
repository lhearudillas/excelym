/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       31 Jan 2018     excelym_202
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *
 * @returns {Boolean} True to continue save, false to abort save
 */
function clientSaveRecord(){

  	// Set the initial value of budget status to On-Budget.
  	nlapiSetFieldValue('custbody_eym_pur_budget_stat','1');

  	// Load the saved search results for BQI usage.
 	var savedSearchResult = nlapiSearchRecord(null, 'customsearch_eym_usage_per_bqi');

  	// Get the number of sublist items.
  	var sublistCount = nlapiGetLineItemCount('item');

  	// Create an object for comparing the amounts.
  	var compareObject = {};

  	// Loop thru the subitems.
  	for(var i = 1; i < sublistCount + 1; i++) {
		var bqiValue = nlapiGetLineItemValue('item', 'custcol_eym_pur_bqi_display', i);

      	if(compareObject[bqiValue] == undefined) {
          	compareObject[bqiValue] = 0;
        }

      	var eAmount = Number(nlapiGetLineItemValue('item', 'estimatedamount', i));
      	compareObject[bqiValue] += eAmount;
    }

  	for(var i = 0; i < savedSearchResult.length; i++) {
		var bqiName = savedSearchResult[i].rawValues[2].text;

        if(compareObject[bqiName] !== undefined) {
      		var bqiUsage = Number(savedSearchResult[i].rawValues[4].value);

          	var bqiAdjustedAmount = Number(savedSearchResult[i].rawValues[3].value);

          	// Subtract usage from adjusted amount.
          	var amountDifference = bqiAdjustedAmount - bqiUsage;

          	// Perform the validation on the amount difference.
          	var validationAmount = amountDifference - compareObject[bqiName];

          	// If amount is less then zero, set status to overbudget.
          	if(validationAmount < 0) {

              	// Set the budget status to overbudget.
              	nlapiSetFieldValue('custbody_eym_pur_budget_stat','2');
            }
        }
    }

    return true;
}