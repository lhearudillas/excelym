/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       11 Aug 2017     EYM-013
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */


function userEventBeforeLoad(type, form, request){
	if(type=='view'){
		var id = nlapiGetRecordId();
		try{
			var status = nlapiGetFieldValue('custrecord_eym_mrf_status');
			var mrfObj = nlapiLoadRecord('customrecord_eym_material_req',id);
			var mrfCount = mrfObj.getLineItemCount('recmachcustrecord_eym_mrfi_mrfid');
			
			nlapiLogExecution('DEBUG','status',status);
			nlapiLogExecution('DEBUG','mrfCount',mrfCount);
			if(status=='2' && mrfCount>0){ // Submitted
				var hasQty = false;
				for(var i=1; i<=mrfCount; i++){
					var mPRQty = mrfObj.getLineItemValue('recmachcustrecord_eym_mrfi_mrfid', 'custrecord_eym_mrfi_prqty', i);
					var mSRQty = mrfObj.getLineItemValue('recmachcustrecord_eym_mrfi_mrfid', 'custrecord_eym_mrfi_srqty', i);
					
					var linkedPr = mrfObj.getLineItemValue('recmachcustrecord_eym_mrfi_mrfid', 'custrecord_eym_mrfi_linkpr', i);
					var linkedSr = mrfObj.getLineItemValue('recmachcustrecord_eym_mrfi_mrfid', 'custrecord_eym_mrfi_linksr', i);
					
					nlapiLogExecution('DEBUG','mPRQty-mSRQty-linkedPr-linkedSr',mPRQty+'-'+mSRQty+'-'+linkedPr+'-'+linkedSr);
					if((mPRQty>0 && !linkedPr) || (mSRQty>0 && !linkedSr)){
						hasQty = true;
						break;
					}
				}
				if(hasQty){
					var url = nlapiResolveURL('SUITELET', 'customscript_sl_eym_create_request', 'customdeploy_sl_eym_create_request')+'&mrf_id='+id;
					form.addButton('custpage_createrequestbutton','Create Request', "document.location='"+url +"'");
				}
			}
		}catch(e){
			var errMsg = '';
			if(e instanceof nlobjError)
				errMsg = e.getCode()+': '+e.getDetails();
	        else
	        		errMsg = 'UNEXPECTED ERROR: '+e.toString();	
			nlapiLogExecution('DEBUG','Error', 'MRF '+id+' '+errMsg);
		}	
	}
}
