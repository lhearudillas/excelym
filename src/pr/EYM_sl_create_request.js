/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       05 Sep 2017     denny
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function create(request, response){
	nlapiLogExecution('DEBUG', 'Start', 'Starting..');
	var mrf_id = request.getParameter('mrf_id');
	var params = {};
	nlapiLogExecution('DEBUG', 'mrf_id', mrf_id);
	try{
		var mrfObj = nlapiLoadRecord('customrecord_eym_material_req',mrf_id);
		var mrfCount = mrfObj.getLineItemCount('recmachcustrecord_eym_mrfi_mrfid');
		var mrfItems = {};
		
		var createPr = false;
		var createSr = false;
		for(var i=1; i<=mrfCount; i++){
			var prQty = mrfObj.getLineItemValue('recmachcustrecord_eym_mrfi_mrfid', 'custrecord_eym_mrfi_prqty', i);
			var srQty = mrfObj.getLineItemValue('recmachcustrecord_eym_mrfi_mrfid', 'custrecord_eym_mrfi_srqty', i);
			
			var linkedPr = mrfObj.getLineItemValue('recmachcustrecord_eym_mrfi_mrfid', 'custrecord_eym_mrfi_linkpr', i);
			var linkedSr = mrfObj.getLineItemValue('recmachcustrecord_eym_mrfi_mrfid', 'custrecord_eym_mrfi_linksr', i);
			
			if(prQty>0 && !linkedPr){
				createPr = true;
			}
			if(srQty>0 && !linkedSr){
				createSr = true;
			}
		}	
		if(createPr){
			try{
				var pr = nlapiCreateRecord('purchaserequisition', {recordmode: 'dynamic'});
				pr.setFieldValue('customform','100');
				pr.setFieldValue('entity',nlapiGetUser());
				pr.setFieldValue('custbody_eym_pr_mrf',mrf_id);
				nlapiLogExecution('DEBUG','project code',mrfObj.getFieldValue('custrecord_eym_mr_projcode'));
				pr.setFieldValue('custbody_eym_pr_projcode',mrfObj.getFieldValue('custrecord_eym_mr_projcode'));
				pr.setFieldValue('custbody_eym_pr_pic',mrfObj.getFieldValue('custrecord_eym_mr_pic'));
				for(var i=1; i<=mrfCount; i++){
					var lineId = mrfObj.getLineItemValue('recmachcustrecord_eym_mrfi_mrfid', 'id', i);
					var mBoq = mrfObj.getLineItemValue('recmachcustrecord_eym_mrfi_mrfid', 'custrecord_eym_mrfi_boq', i);
					var mItem = mrfObj.getLineItemValue('recmachcustrecord_eym_mrfi_mrfid', 'custrecord_eym_mrfi_item', i);
					var mUnit = mrfObj.getLineItemValue('recmachcustrecord_eym_mrfi_mrfid', 'custrecord_eym_mrfi_uom', i);
					var mUnitPrice = mrfObj.getLineItemValue('recmachcustrecord_eym_mrfi_mrfid', 'custrecord_eym_mrfi_unitprice', i);
					var mPRQty = mrfObj.getLineItemValue('recmachcustrecord_eym_mrfi_mrfid', 'custrecord_eym_mrfi_prqty', i);
					var mRecDate = mrfObj.getLineItemValue('recmachcustrecord_eym_mrfi_mrfid', 'custrecord_eym_mrfi_recdate', i);
					var mLoc = mrfObj.getLineItemValue('recmachcustrecord_eym_mrfi_mrfid', 'custrecord_eym_mrfi_loc', i);
					var mRemark = mrfObj.getLineItemValue('recmachcustrecord_eym_mrfi_mrfid', 'custrecord_eym_mrfi_remarks', i);
					var pItem = mrfObj.getLineItemValue('recmachcustrecord_eym_mrfi_mrfid', 'custrecord_eym_mrfi_prpitem', i);
					nlapiLogExecution('DEBUG', 'i', 'i =' + i+' -- '+lineId);
						
					if(mPRQty){
						pr.selectNewLineItem('item');
						pr.setCurrentLineItemValue('item', 'custcol_eym_pur_bqi', mBoq);
						pr.setCurrentLineItemValue('item', 'item', mItem);
						pr.setCurrentLineItemValue('item', 'custcol_eym_sales_prpid', pItem);
						pr.setCurrentLineItemValue('item', 'quantity', mPRQty);
						pr.setCurrentLineItemValue('item', 'estimatedrate', mUnitPrice);
						//pr.setLineItemValue('item', 'rate', j, mUnitPrice);
						//pr.setCurrentLineItemValue('item', 'units', j, mUnit);
						
						
						pr.setCurrentLineItemValue('item', 'custcol_eym_pur_receiveby', mRecDate);
						pr.setCurrentLineItemValue('item', 'custcol_eym_pur_receiveloc', mLoc);
						pr.setCurrentLineItemValue('item', 'custcol_eym_pur_remarks', mRemark);
						pr.commitLineItem('item');
						
						mrfItems[lineId] = {};
					}
				}
				nlapiLogExecution('DEBUG', 'newPr', mrfItems);
				var newPr = nlapiSubmitRecord(pr);
				if(newPr){
					params["pr_id"] = newPr;
				}
				for(var x in mrfItems){
					mrfItems[x]["linkedPr"] = newPr;
				}
				
			}catch(e){
				if(e instanceof nlobjError)
					errMsg = e.getCode()+': '+e.getDetails();
		        else
		        		errMsg = 'UNEXPECTED ERROR: '+e.toString();
				nlapiLogExecution('DEBUG','errMsg',errMsg);
				params["error_pr"] = errMsg;
			}
		}
		
		if(createSr){
			try{
				var sr = nlapiCreateRecord('salesorder', {recordmode: 'dynamic'});
				sr.setFieldValue('customform','104');
				sr.setFieldValue('entity',mrfObj.getFieldValue('custrecord_eym_cust_name'));
				sr.setFieldValue('custbody_eym_req_type',1);
				sr.setFieldValue('orderstatus','B');
				sr.setFieldValue('custbody_eym_pr_mrf',mrf_id);
				sr.setFieldValue('job',mrfObj.getFieldValue('custrecord_eym_mr_projcode'));
				sr.setFieldValue('custbody_eym_pr_pic',mrfObj.getFieldValue('custrecord_eym_mr_pic'));
				sr.setFieldValue('location',1);
				for(var i=1; i<=mrfCount; i++){
					var lineId = mrfObj.getLineItemValue('recmachcustrecord_eym_mrfi_mrfid', 'id', i);
					var mBoq = mrfObj.getLineItemValue('recmachcustrecord_eym_mrfi_mrfid', 'custrecord_eym_mrfi_boq', i);
					var mItem = mrfObj.getLineItemValue('recmachcustrecord_eym_mrfi_mrfid', 'custrecord_eym_mrfi_item', i);
					var mUnit = mrfObj.getLineItemValue('recmachcustrecord_eym_mrfi_mrfid', 'custrecord_eym_mrfi_uom', i);
					var mUnitPrice = mrfObj.getLineItemValue('recmachcustrecord_eym_mrfi_mrfid', 'custrecord_eym_mrfi_unitprice', i);
					var mSRQty = mrfObj.getLineItemValue('recmachcustrecord_eym_mrfi_mrfid', 'custrecord_eym_mrfi_srqty', i);
					var mRecDate = mrfObj.getLineItemValue('recmachcustrecord_eym_mrfi_mrfid', 'custrecord_eym_mrfi_recdate', i);
					var mLoc = mrfObj.getLineItemValue('recmachcustrecord_eym_mrfi_mrfid', 'custrecord_eym_mrfi_loc', i);
					var mRemark = mrfObj.getLineItemValue('recmachcustrecord_eym_mrfi_mrfid', 'custrecord_eym_mrfi_remarks', i);
					var pItem = mrfObj.getLineItemValue('recmachcustrecord_eym_mrfi_mrfid', 'custrecord_eym_mrfi_prpitem', i);
					nlapiLogExecution('DEBUG', 'i', 'i =' + i+' -- '+mSRQty);
					if(mSRQty){
						sr.selectNewLineItem('item');
						sr.setCurrentLineItemValue('item', 'custcol_eym_pur_bqi', mBoq);
						sr.setCurrentLineItemValue('item', 'item', mItem);
						sr.setCurrentLineItemValue('item', 'quantity', mSRQty);
						sr.setCurrentLineItemValue('item', 'custcol_eym_sales_prpid', pItem);
						//sr.setCurrentLineItemValue('item', 'units', mUnit);
						
						//sr.setCurrentLineItemValue('item', 'taxcode', 5);

						sr.setCurrentLineItemValue('item', 'custcol_eym_pur_receiveby', mRecDate);
						sr.setCurrentLineItemValue('item', 'custcol_eym_pur_receiveloc', mLoc);
						sr.setCurrentLineItemValue('item', 'custcol_eym_pur_remarks', mRemark);
						sr.commitLineItem('item');
						
						if(!(lineId in mrfItems)){
							mrfItems[lineId] = {'linkedSr':''};
						}else{
							mrfItems[lineId]["linkedSr"] = '';
						}
					}
				}
				var newSr = nlapiSubmitRecord(sr);
				if(newSr){
					params["sr_id"] = newSr;
				}
				for(var x in mrfItems){
					if(mrfItems[x].hasOwnProperty('linkedSr')){
						mrfItems[x].linkedSr = newSr;
					}
				}
				nlapiLogExecution('DEBUG', 'newSr', 'newSr =' + newSr);
			}catch(e){
				if(e instanceof nlobjError)
					errMsg = e.getCode()+': '+e.getDetails();
		        else
		        		errMsg = 'UNEXPECTED ERROR: '+e.toString();
				nlapiLogExecution('DEBUG','errMsg',errMsg);
				params["error_sr"] = errMsg;
			}	
		}
		
		for(var x in mrfItems){
			var fields = [];
			var values = [];
			if(mrfItems[x].linkedPr){
				fields.push('custrecord_eym_mrfi_linkpr');
				values.push(mrfItems[x].linkedPr);
			}
			if(mrfItems[x].linkedSr){
				fields.push('custrecord_eym_mrfi_linksr');
				values.push(mrfItems[x].linkedSr);
			}
			nlapiLogExecution('DEBUG','values',values);
			if(fields.length>0){
				nlapiSubmitField('customrecord_eym_material_req_item',x,fields,values);
			}
		}
		
	}catch(e){
		if(e instanceof nlobjError)
			errMsg = e.getCode()+': '+e.getDetails();
        else
        		errMsg = 'UNEXPECTED ERROR: '+e.toString();
		nlapiLogExecution('DEBUG','errMsg',errMsg);
		params["error_all"] = errMsg;
	}
	nlapiSetRedirectURL('SUITELET','customscript_sl_eym_create_request_sts','customdeploy_sl_eym_create_request_sts',null,params);
	//nlapiSetRedirectURL('RECORD','purchaserequisition',newPr);
}
