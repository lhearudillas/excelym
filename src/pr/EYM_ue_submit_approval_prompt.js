/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       04 Apr 2018     Roy Selim
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 *
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request)
{
  	// Run the script only in view mode.
  	if( type == 'view' ) {

        // Declare function constants.
        const PNID_339_CLIENT_SCRIPT 	= 'customscript_cs_eym_submit_approval_prom';
      	const PNID_339_BGT_STATUS_FLD	= 'custbody_eym_pur_budget_stat';

      	// Get the on-budget status.
      	var onBudgetStatus = nlapiGetFieldValue( PNID_339_BGT_STATUS_FLD );

        // Add prompt script if the status is overbudget.
      	if( onBudgetStatus && onBudgetStatus == 2 ) {

          	// Set an automatic running script.
        	form.setScript( PNID_339_CLIENT_SCRIPT );
        }
    }
}