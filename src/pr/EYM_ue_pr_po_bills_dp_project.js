/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       17 May 2018     Roy Selim
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit( type )
{
	// Run this script when record is created or edited.
	if( type == 'create' || type == 'edit' ) {

		// Declare local scope constants.
		const PHL_PROJ_FLD_STR		= 'custbody_eym_pr_projcode';
		const PHL_PROJ_LINE_STR		= 'customer';
		const PHL_ITEM_FLD_STR		= 'item';
		const PHL_EXPENSE_FLD_STR	= 'expense';
		const PHL_CHECK_REC_STR		= 'check';
      	const PHL_VENDOR_REC_STR	= 'vendorbill';

      	// Declare scope variables.
      	var sublistID = PHL_ITEM_FLD_STR;

		// Get the type of record.
		var recordType = nlapiGetRecordType();

      	// Check what kind of record is returned.
      	if( recordType == 'vendorbill' || recordType == 'check' ) {

          	// Edit the sublist ID with expense.
          	sublistID = PHL_EXPENSE_FLD_STR;
        }

		// Get the number of items on the record.
		var numberOfItems = nlapiGetLineItemCount( sublistID );

		// Get the project code from the header.
		var projectCode = nlapiGetFieldValue( PHL_PROJ_FLD_STR );

		// For checks (direct payments), exclude the edit operations.
		if( recordType != 'check' ) {

			// Loop thru all the items.
			for( var i = 1; i <= numberOfItems; i++ ) {

				// Set the project field to the project code.
				nlapiSetLineItemValue( sublistID, PHL_PROJ_LINE_STR, i, projectCode );
			}

		// For all other record types.
		} else {

			// Run the code only if the type is create.
			if( type == 'create' ) {

				// Loop thru all the items.
				for( var i = 1; i <= numberOfItems; i++ ) {

					// Set the project field to the project code.
					nlapiSetLineItemValue( sublistID, PHL_PROJ_LINE_STR, i, projectCode );
				}
			}
		}
	}
}
