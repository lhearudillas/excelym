/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       05 Sep 2017     denny
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function show(request, response){
	nlapiLogExecution('DEBUG', 'Start', 'Starting..');
	var list = nlapiCreateList('Create Request Status');
	 
    list.setStyle(request.getParameter('style'));
    list.addColumn('request_number', 'text', 'Number', 'left');
	list.addColumn('request_type', 'text', 'Type', 'left');
	list.addColumn('request_status', 'text', 'Status', 'left');
	if(request.getParameter("error_all")){
		list.addRow({"request_number": "", "request_type": "", "request_status": "Error: "+request.getParameter("error_all")});
	}else{
		if(request.getParameter("pr_id")){
			var prData = nlapiLookupField('purchaserequisition',request.getParameter("pr_id"),['tranid','total']);
	    		var prUrl = nlapiResolveURL('RECORD', 'purchaserequisition', request.getParameter("pr_id"));
			list.addRow({"request_number": "<a href="+prUrl+">"+prData.tranid+"</a>", "request_type": "Purchase Requisition", "request_status": "Success"});
	    }else if(request.getParameter("error_pr")){
	    		list.addRow({"request_number": "", "request_type": "Purchase Requisition", "request_status": "Error: "+request.getParameter("error_pr")});
	    }
		
		if(request.getParameter("sr_id")){
			var srData = nlapiLookupField('salesorder',request.getParameter("sr_id"),['tranid','total']);
	    		var srUrl = nlapiResolveURL('RECORD', 'salesorder', request.getParameter("sr_id"));
			list.addRow({"request_number": "<a href="+srUrl+">"+srData.tranid+"</a>", "request_type": "Stock Requisition", "request_status": "Success"});
	    }else if(request.getParameter("error_sr")){
	    		list.addRow({"request_number": "", "request_type": "Stock Requisition", "request_status": "Error: "+request.getParameter("error_sr")});
	    }
	}

    response.writePage( list );
}
