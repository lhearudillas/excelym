/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       11 Aug 2017     dennysutanto
 *
 */
function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function truncateString(str, length) {
   return str.length > length ? str.substring(0, length - 3) + '...' : str
}

function boqForm(request, response)
{
  var jqueryModalJs = 'https://system.netsuite.com/core/media/media.nl?id=536&c=4317943_SB1&h=d715f48c82e22e51d3a5&_xt=.js';
  var mainCss = 'https://system.netsuite.com/core/media/media.nl?id=626&c=4317943_SB1&h=80e5044fe17a116c6e29&_xt=.css';
  var boqFolder = '108';
  if(nlapiGetContext().getEnvironment()=='PRODUCTION'){
	  jqueryModalJs = 'https://system.na3.netsuite.com/core/media/media.nl?id=536&c=4317943&h=c1a0fa134372b1c1ccd9&_xt=.js';
	  mainCss = 'https://system.na3.netsuite.com/core/media/media.nl?id=626&c=4317943&h=6ae2a43efd859b421040&_xt=.css';
	  boqFolder = '108';
  }
  	
  var columns = [
    new nlobjSearchColumn('name')
  ];
  var subsystemList = nlapiSearchRecord('customlist_eym_proj_subsystem',null,null,columns);

  var projectId = request.getParameter('projectid');
  var boqFileName = 'boq_template_'+projectId;
  var createBoqRestletUrl = nlapiResolveURL('SUITELET', 'customscript_sl_eym_create_boq', 'customdeploy_sl_eym_create_boq')+'&projectid='+projectId;
  var approvalRestletUrl = nlapiResolveURL('SUITELET', 'customscript_sl_eym_submit_approval', 'customdeploy_sl_eym_submit_approval')+'&projectid='+projectId;
  var returnBoqRestletUrl = nlapiResolveURL('SUITELET', 'customscript_sl_eym_return_boq', 'customdeploy_sl_eym_return_boq')+'&projectid='+projectId;
  var pinValidateUrl = nlapiResolveURL('SUITELET', 'customscript_sl_eym_validate_pin', 'customdeploy_sl_eym_validate_pin')+'&type=project&id='+projectId;
  var projectUrl = nlapiResolveURL('RECORD', 'job', projectId);
  var classFilter = [
    new nlobjSearchFilter('custrecord_eym_use_in_psc',null,'is','T')
  ];
  var classColumn = [
    new nlobjSearchColumn('name'),
    new nlobjSearchColumn('custrecord_eym_psc_tempsequence')
  ]
  classColumn[1].setSort();
  var classes = nlapiSearchRecord('classification',null,classFilter,classColumn);
  var currentUserId = nlapiGetUser();
  var currentUserRole = nlapiGetRole();
  nlapiLogExecution('DEBUG','currentUserRole',currentUserRole);
  var extraHtml = "";
  var projectObj = nlapiLoadRecord('job',projectId);//nlapiLookupField('job',projectId,['entityid','companyname','parent','entitystatus']);
  if ( request.getMethod() == 'POST' ){

    var subsystemData = {};
    var classTotal = {};
    var grandTotal = 0;
    for(var i=0;i<subsystemList.length;i++){
      var subSystemId = subsystemList[i].getId();
      var filters = [
        new nlobjSearchFilter('custrecord_eym_projsub_main',null,'is',subSystemId)
      ];
      var columns = [
        new nlobjSearchColumn('custrecord_eym_projsub_main'),
        new nlobjSearchColumn('name'),
        new nlobjSearchColumn('custrecord_boq_cat_descr')
      ];
      var activities = nlapiSearchRecord('customrecord_eym_boq_activity',null,filters,columns);
      for(var j=0;j<activities.length;j++){
        var total = 0;
        var activity = activities[j];
        var suffix = "-"+subSystemId+"-"+activity.getId();
        for(var c=0;c<classes.length;c++){
        	  var amount = (request.getParameter("class-"+classes[c].getId()+suffix).replace(/,/g, '') * 1);
        	  subsystemData["class-"+classes[c].getId()+suffix] = amount;
          total += amount;
          if(!("class-"+classes[c].getId()+'-'+subSystemId in classTotal)){
        	  	classTotal["class-"+classes[c].getId()+'-'+subSystemId] = [];
          }
          classTotal["class-"+classes[c].getId()+'-'+subSystemId].push(amount);
        }
        subsystemData["total"+suffix] = total;
        grandTotal += total;
        //subsystemData.push(tempObj);
      }
    }
    
    for(var i=0;i<subsystemList.length;i++){
    		var subSystemId = subsystemList[i].getId();
    		var classGrandTotal = 0;	
    		for(var key in classTotal){
    			var aKey = key.split('-');
    			if(aKey[2]==subSystemId){
    				var classTotalVal = 0;
    				for (var ss = 0; ss < classTotal[key].length; ss++ ){
	    	    			classTotalVal += (classTotal[key][ss] * 1 );
	    	    			classGrandTotal += (classTotal[key][ss] * 1 );
	    	    		}
    				subsystemData["classTotal-"+key] = classTotalVal;
    			}
	    }
    		subsystemData["classGrandTotal-"+subSystemId] = classGrandTotal;
    		
    }
    
    var jsonFile = {
      'projectId' : projectId,
      'grandTotal' : grandTotal,
      'subsystemData' : subsystemData
    }
    var tempFile = nlapiCreateFile(boqFileName, 'PLAINTEXT', JSON.stringify(jsonFile));
    tempFile.setFolder(boqFolder);
  	var newfile = nlapiSubmitFile(tempFile);
  	var fields = ['custentity_eym_boq_status','custentity_boq_approver'];
  	var values = [4,request.getParameter("selBoqApprover")];
  	if(!projectObj.getFieldValue('custentity_eym_boq_creator')){
  		fields.push('custentity_eym_boq_creator');
  		values.push(currentUserId);
  	}
  	nlapiSubmitField('job',projectId,fields,values);
  	if(request.getParameter("btnClick")!="2"){
  		extraHtml += '<script type="text/javascript">\n';
  	  	extraHtml += ' alert("BOQ Template Saved")\n';
  	    extraHtml += '</script>\n';
  	}else{
  		var approver = request.getParameter("selBoqApprover");
  		extraHtml += '<script type="text/javascript">\n';
  	  	extraHtml += ' window.location = "'+approvalRestletUrl+'&approver='+approver+'";\n';
  	    extraHtml += '</script>\n';
  	    response.write(extraHtml);
  	}
  }
  var filters = [ new nlobjSearchFilter('name',null,'is',boqFileName)];
  var fileSearch = nlapiSearchRecord('file',null,filters);
  var subsystemJson = null;
  var json = null;
  if(fileSearch){
    tempFile = nlapiLoadFile(fileSearch[0].getId());
    json = JSON.parse(tempFile.getValue());
    subsystemJson = json.subsystemData;
  }
  var projectObj = nlapiLoadRecord('job',projectId);//nlapiLookupField('job',projectId,['entityid','companyname','parent','entitystatus']);
  var boqApprover = nlapiSearchRecord(null,'customsearch_eym_boq_approver_list',null);
  
  var correctApprover = false;
  var costApprover = nlapiSearchRecord(null,'customsearch_eym_cost_approver_list',null);
  if(costApprover){
	  for(var b=0;b<costApprover.length;b++){
		  nlapiLogExecution('DEBUG','costApprover[b].getId()==currentUserId',costApprover[b].getId()+'=='+currentUserId);
		  if(currentUserId==costApprover[b].getId()){
			  correctApprover = true;
			  break;
	      }
	  }
  }

  var html = '<html>\n';
  html += '<head>\n';
  html += '<link rel="stylesheet" href="'+mainCss+'" type="text/css" media="screen" />\n';
  html += '<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" type="text/css" media="screen" />\n';
  html += '<title>PSC BOQ Template</title>\n';
  html += '</head>\n';
  html += '<body>\n';
  html += '<form name="frmReturn" id="frmReturn" style="display:none" method="POST" action="'+returnBoqRestletUrl+'">\n';
  html += '<p><label>PIN:</label></p><p><input type="password" name="pin" class="form-control" id="pin"></p>\n';
  html += '<p><label>Reason for Return:</label></p><p><textarea name="returnMsg" class="form-control" id="returnMsg"></textarea></p>\n';
  html += '<p><input type="submit" id="btnOkReturn" class="btn btn-primary" value="OK" /></p>\n';
  html += '</form>\n';
  html += '<form name="frmPin" id="frmPin" style="display:none" method="POST">\n';
  html += '<p><label>PIN:</label></p><p><input type="password" name="pin" class="form-control" id="pin"></p>\n';
  html += '<p><input type="submit" id="btnOkPin" class="btn btn-primary" value="OK" /></p>\n';
  html += '</form>\n';
  html += '<form name="frmBoq" id="frmBoq" method="POST" action="">\n';
  html += '<input type="hidden" name="projectid" value="'+projectId+'">\n';
  html += '<input type="hidden" name="btnClick" id="btnClick" value="">\n';
  html += '<header class="actions stick-to-top">\n';
  html += '	<div class="card">\n';
  html += '		<div class="card-body"><div class="row"><div class="col">\n';
  var disabled = '';
  if(projectObj.getFieldValue('custentity_eym_boq_status')=='4' || projectObj.getFieldValue('custentity_eym_boq_status')=='3' || projectObj.getFieldValue('custentity_eym_boq_status')==null){
	  if(currentUserRole=='3' || projectObj.getFieldValue('custentity_eym_boq_creator')==currentUserId || projectObj.getFieldValue('custentity_eym_boq_creator')=='' || projectObj.getFieldValue('custentity_eym_boq_creator')==null){
		  html += '    <input type="submit" class="btn btn-primary" value="Save as Draft" name="btnSaveDraft" id="btnSaveDraft">\n';
		  if(json){
			  if(Number(json.grandTotal) > 0){
				  html += '    <input type="submit" class="btn btn-primary" value="Submit for Approval" name="btnApproval" id="btnApproval">\n';
			  }
		  }
	  }else{
		  disabled = 'disabled';
	  }
	  
  }else if(projectObj.getFieldValue('custentity_eym_boq_status')=='1'){
	  if(correctApprover || currentUserRole=='3'){
		  html += '    <input type="button" class="btn btn-primary" value="Approve BOQ" name="btnApprove" id="btnApprove">\n';
		  html += '    <input type="button" class="btn btn-primary" value="Return BOQ" name="btnReturn" id="btnReturn">\n';
	  }
	  disabled = 'disabled';
  }
  html += '    <input type="button" class="btn btn-default" value="Back" name="btnBack" id="btnBack"></div>\n';
  html += '		<div class="col-3"><dl class="row mb-0"><dt class="col-md-5">Project Code</dt><dd class="col-md-7">'+projectObj.getFieldValue('entityid')+'</dd></dl></div>\n';
  html += '		<div class="col-3"><dl class="row mb-0"><dt class="col-md-5">Total BOQ Baseline</dt><dd class="col-md-7">'+(json ? addCommas(Number(json.grandTotal).toFixed(2)) : "0.00")+'</dd></dl></div>\n';
  html += '		</div></div></div></header>\n';
  html += '<main class="main-content">\n';
  html += ' <div class="container-fluid">\n';
  html += '	 <div class="row">\n';
  html += '		<div class="col">\n';
  html += '		<div class="card mb-3">\n';
  html += '		<div class="card-header"><h3 class="float-left mt-2 mb-1">Project Summary</h3></div>\n';
  html += '		<div class="card-body"><div class="row">\n';
  html += '			<div class="col-md-4">\n';
  html += '				<dl class="row">\n';
  //html += '					<dt class="col-md-4">Project Code</dt>\n';
  //html += '					<dd class="col-md-8">'+projectObj.getFieldValue('entityid')+'</dt>\n';
  html += '					<dt class="col-md-4">Project Name</dt>\n';
  html += '					<dd class="col-md-8">'+projectObj.getFieldValue('companyname')+'</dt>\n';
  html += '					<dt class="col-md-4">Customer</dt>\n';
  html += '					<dd class="col-md-8">'+projectObj.getFieldText('parent')+'</dt>\n';
  html += '					<dt class="col-md-4">Project Status</dt>\n';
  html += '					<dd class="col-md-8">'+projectObj.getFieldText('entitystatus')+'</dt>\n';
  html += '				</dl>\n';
  html += '			</div>\n';
  html += '			<div class="col-md-6">\n';
  html += '				<dl class="row">\n';
  //html += '					<dt class="col-md-4">Total BOQ Baseline</dt>\n';
  //html += '					<dd class="col-md-8"><span id="totalBoq">'+(json ? addCommas(Number(json.grandTotal).toFixed(2)) : "0.00")+'</span></dt>\n';
  html += '					<dt class="col-md-4">BOQ Approver</dt>\n';
  html += '					<dd class="col-md-8"><select name="selBoqApprover" id="selBoqApprover" '+disabled+'>\n';
  html += '						<option value=""></option>\n';
  if(boqApprover){
	  for(var b=0;b<boqApprover.length;b++){
	      var selected = "";
		  if(projectObj.getFieldValue('custentity_boq_approver')==boqApprover[b].getId()){
	    	  	selected = "selected";
	      }
		  html += '<option value="'+boqApprover[b].getId()+'" '+selected+'>'+boqApprover[b].getValue('entityid')+'</option>\n';
	  }
  }
  html += '         				</select></dd>\n';
  html += '					<dt class="col-md-4">BOQ Status</dt>\n';
  html += '					<dd class="col-md-8">'+projectObj.getFieldText('custentity_eym_boq_status')+'</dt>\n';
  html += '				</dl>\n';
  html += '			</div>\n';
  html += '		</div></div>\n';
  html += '		</div>\n';
  html += '		</div>\n';
  html += '	 </div>\n';
  
  html += '	 <div class="row">\n';
  html += '		<div class="col">\n';
  html += '		<div class="card mb-3">\n';
  html += '		<div class="card-header">\n';
  html += '			<h3 class="float-left mt-2 mb-1">Project Subsystems and Activities</h3>\n';
  html += '			<input type="button" class="btn btn-primary float-right ml-1" id="btnCollapse" value="Collapse All">\n';
  html += '			<input type="button" class="btn btn-primary float-right" id="btnExpand" value="Expand All">\n';
  html += '		</div>\n';
  html += '		<div class="table-responsive-wrapper">\n';
  html += '			<div class="card-header p-0 clearfix">\n';
  html += '			<table class="structured-table" style="width:100%;"><thead><tr>\n';
  html += '				<th style="width: 33.5%;"><h5 class="mt-2 mb-1">Project Subsystems</h5></th>\n';
  for(var c=0;c<classes.length;c++){
      html += '			<th class="text-center" style="width:9.5%"><h5 class="mt-2 mb-1">'+classes[c].getValue('name').replace(' ','<br>')+'</h5></th>\n';
  }
  html += '				<th class="text-center" style="width:9.5%"><h5 class="mt-2 mb-1">Total</h5></th>\n';
  html += '			</tr></thead></table>\n';
  html += '			</div>\n';
  html += '			<div class="card-body p-0">\n';
  html += '			<div class="accordion_content">\n';
  if(subsystemList){
	  for(var i=0;i<subsystemList.length;i++){
	      var subSystemId = subsystemList[i].getId();
	      var filters = [
	        new nlobjSearchFilter('custrecord_eym_projsub_main',null,'is',subSystemId)
	      ];
	      var columns = [
	        new nlobjSearchColumn('custrecord_eym_projsub_main'),
	        new nlobjSearchColumn('name'),
	        new nlobjSearchColumn('custrecord_boq_cat_descr')
	      ];
	      var activities = nlapiSearchRecord('customrecord_eym_boq_activity',null,filters,columns);
	      html += '<div class="card">\n';
	      html += '	<div class="card-header" role="tab" id="heading'+subSystemId+'">\n';
	      html += '	<table class="structured-table header" style="width: 100%;"><thead><tr>\n';
	      html += '		<th class="btn-toggle-col" style="width: 33.5%;"><h5 class="float-left mb-0"><div class="btn-desc">\n';
	      html += '			<div class="button">\n';
	      html += '				<button type="button" data-toggle="collapse" href="#collapse'+subSystemId+'" aria-expanded="true" aria-controls="collapse'+subSystemId+'" class="btn btn-default btn-sm btn-icon collapse-toggler-btn"><i class="mdi mdi-plus"></i></button>\n';
	      html += '			</div>\n';
	      html += '			<div class="content">'+subsystemList[i].getValue('name')+'</div>\n';
	      html += '		</div></h5></th>\n';
	      for(var c=0;c<classes.length;c++){
	    	  	html += '       <th class="text-center" style="width:9.5%"><h5 class="mt-2 mb-1">'+(subsystemJson ? addCommas((subsystemJson["classTotal-class-"+classes[c].getId()+'-'+subSystemId] *1).toFixed(2)) : "0.00")+'</h5></th>';
		  }
	      html += '       <th class="text-center" style="width:9.5%"><h5 class="mt-2 mb-1">'+(subsystemJson ? addCommas((subsystemJson["classGrandTotal-"+subSystemId] *1).toFixed(2)) : "0.00")+'</h5></th>';
	      html += '	</tr></thead></table>\n';
	      html += '	</div>\n';
	      
	      html += '	<div id="collapse'+subSystemId+'" class="collapse" role="tabpanel" aria-labelledby="heading'+subSystemId+'" data-parent="#accordion">\n';
	      html += '	<div class="card-body"><table class="structured-table" style="width:100%;"><thead>\n';
	      html += '		<table class="structured-table" style="width:100%;"><thead>\n';
	      html += '			<tr>\n';
	      html += '				<th style="width:16.75%"><h5 class="mt-2 mb-1">Activity</h5></th>\n';
	      html += '				<th style="width:16.75%"><h5 class="mt-2 mb-1">Description</h5></th>\n';
	      for(var c=0;c<classes.length;c++){
	          html += '			<th class="text-center" style="width:9.5%"><h5 class="mt-2 mb-1">'+classes[c].getValue('name').replace(' ','<br>')+'</h5></th>\n';
	      }
	      html += '				<th class="text-center" style="width:9.5%"><h5 class="mt-2 mb-1">Total</h5></th>\n';
	      html += '			</tr>\n';
	      
	      if(activities){
	    	  html += '		<tbody>\n';	
	    	  	for(var j=0;j<activities.length;j++){
	          var activity = activities[j];
	          var suffix = "-"+subSystemId+"-"+activity.getId();
	          html += '     <tr class="actClass-'+subSystemId+'">\n';
	          html += '       <td style="width:16.75%">'+activity.getValue('name')+'</td>';
	          html += '       <td style="width:16.75%"><span title="'+activity.getValue('custrecord_boq_cat_descr')+'">'+truncateString(activity.getValue('custrecord_boq_cat_descr'),25)+'</span></td>';
	          for(var c=0;c<classes.length;c++){
	            var amt = (subsystemJson ? (subsystemJson["class-"+classes[c].getId()+suffix] * 1).toFixed(2) : "0.00");
	        	  	html += '       <td style="width:9.5%" class="text-center"><input '+disabled+' type="text" data-cid="'+classes[c].getId()+'" data-i="'+suffix+'" class="form-control form-control-sm numOnly amt'+suffix+'" name="class-'+classes[c].getId()+suffix+'" id="class-'+classes[c].getId()+suffix+'" value="'+addCommas(amt)+'"></td>';
	          }
	          html += '       <td style="width:9.5%" class="text-center"><input disabled type="text" class="subTotalBoq form-control form-control-sm" name="total'+suffix+'" id="total'+suffix+'" value="'+(subsystemJson ? addCommas((subsystemJson["total"+suffix] *1).toFixed(2)) : "0.00")+'"></td>';
	          html += '     </tr>\n';
	        }
	    	  html += '		</tbody>\n';
	      }
	      html += '		</thead></table>\n';
	      html += '	</div>\n';
	      html += '	</div>\n';
	      
	      html += '</div>\n';
	  }
  }
  
  html += '			</div>\n';
  html += '			</div>\n';
  html += '		</div>\n';
  
  
  
  
  
  html += '		</div>\n';
  html += '		</div>\n';
  html += '	 </div>\n';
  
  
  html += '	</div>\n';
  html += '</main>\n';
  html += '</form>\n';
  html += '</body>\n';
  html += extraHtml;
  html += '<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>\n';
  html += '<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>\n';
  html += '<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>\n';
  html += '<script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>\n';
  html += '<script type="text/javascript" src="'+jqueryModalJs+'"></script>\n';  
  html += '<script type="text/javascript">\n';
  html += ' $(document).ready(function(){\n';
  html += '   $(document).tooltip();\n';
  html += '   function getTotal(){\n';
  html += '     var total = 0\n';
  html += '     $(".subTotalBoq").each(function(row, val){\n';
  html += '     		total += ($(this).val().replace(/,/g, "")*1);\n';
  html += '     });\n';
  html += '     return total\n';
  html += '   }\n';
  html += '   function setTotal(total){\n';
  html += '     $("#totalBoq").html(total.toFixed(2));\n';
  html += '   }\n';
  //html += '   var t = getTotal();\n';
  //html += '   setTotal(t);\n';
  html += '   $("#btnBack").click(function(){\n';
  html += '     window.location = "'+projectUrl+'"; return false; \n';
  html += '   })\n';
  /*
  html += '   var classTotal = {};\n';  
  html += '   $(".subClass").each(function(){\n';
  html += '     var i = $(this).data("i");\n';
  html += '     $(".actClass-"+i+" .numOnly").each(function(){\n';
  html += '     		var key = i+"-"+$(this).data("cid");\n';
  html += '     		if(!(key in classTotal))\n';
  html += '     			classTotal[key] = []\n';
  html += '     		classTotal[key].push($(this).val());\n';
  html += '     });\n';
  html += '   });\n';

  html += '  for(var k in classTotal){\n';
  html += '     var cTotal = 0;\n';  
  html += '     for (var ss = 0; ss < classTotal[k].length; ss++){\n';
  html += '     		var cAmt = classTotal[k][ss];\n';
  html += '     		cTotal += cAmt*1;\n';
  html += '     }\n';
  html += '     $("#"+k).val(cTotal.toFixed(2));\n';
  html += '   }\n';
  */
  html += '   $(".numOnly").keydown(function (e) {\n';
  html += '     if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1){ return; }\n';
  html += '     if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) { e.preventDefault(); }\n';
  html += '   })\n';
  
  html += '   $(".numOnly").click(function (e) {\n';
  html += '     if($(this).val()=="0.00"){ $(this).val("")}\n';
  html += '   })\n';
  
  html += '   $(".numOnly").blur(function (e) {\n';
  html += '     if($(this).val()==""){ $(this).val("0.00")}\n';
  html += '   })\n';
  
  html += '   $(".numOnly").keyup(function (e) {\n';
  html += '     var cGrandTotal = 0;\n';
  html += '     var i = $(this).data("i");\n';
  html += '     if($(this).val()==""){ $(this).val("0.00")}\n';
  html += '     var subtot = 0\n';
  html += '     $(".amt"+i).each(function(row, val){\n';
  html += '     		var amt = $(this);\n';
  html += '     		subtot += (amt.val().replace(/,/g, "")*1);\n';
  html += '     });\n';
  html += '     if(!isNaN(subtot.toFixed(2))){\n';
  html += '     $("#total"+i).val(subtot.toFixed(2));\n';
  
  html += '     var classTotal = {};\n';  
  html += '     var subId = i.split("-");\n';
  html += '     $(".actClass-"+subId[1]+" .numOnly").each(function(){\n';
  html += '     		var key = subId[1]+"-"+$(this).data("cid");\n';
  html += '     		if(!(key in classTotal))\n';
  html += '     			classTotal[key] = []\n';
  html += '     		classTotal[key].push($(this).val());\n';
  html += '     });\n';

  html += '  	for(var k in classTotal){\n';
  html += '     		var cTotal = 0;\n';  
  html += '     		for (var ss = 0; ss < classTotal[k].length; ss++){\n';
  html += '     			var cAmt = classTotal[k][ss];\n';
  html += '     			cTotal += cAmt.replace(/,/g, "")*1;\n';
  html += '     		}\n';
  html += '     		$("#"+k).val(cTotal.toFixed(2));\n';
  html += '     		cGrandTotal += cTotal;\n';
  html += '   	}\n';
  html += '     	$("#sub-"+subId[1]).val(cGrandTotal.toFixed(2));\n';
  html += '     var t = getTotal();\n';
  html += '     setTotal(t);\n';
  html += '   	}\n';
  html += '   })\n';
  
  html += '   $("#frmBoq").on("submit",function(){\n';
  html += '     var t = getTotal()\n';
  html += '     var submit = true;\n';
  html += '     if(t==0){ alert("Total BOQ Baseline cannot be empty"); submit = false; }\n';
  html += '     $(".numOnly").each(function(){\n';
  html += '     		if(isNaN($(this).val().replace(/,/g, ""))){\n';
  html += '     			alert("Please input valid number for amount"); submit = false; return false;\n';
  html += '     		}\n';
  html += '     });\n';
  html += '   	if(!submit){ return false; }\n';
  html += '   })\n';
  
  html += '   $("#btnApprove").click(function(){\n';
  html += '     $("#frmPin").modal({ modalClass:"jModal", escapeClose: false, clickClose: false});\n';
  html += '   })\n';
  
  html += '   $("#btnOkPin").click(function(e){\n';
  html += '     	e.preventDefault();\n';
  html += '     	var btn = $(this); btn.attr("disabled", "disabled");\n';
  html += '     	var ajaxCheck = $.post("'+pinValidateUrl+'", $("#frmPin").serialize());\n';
  html += '     	ajaxCheck.done(function(resp){\n';
  html += '     		console.log(resp);var jsonRes = JSON.parse(resp);\n';
  //html += '     		$(".loading").hide();\n';
  html += '     		if(jsonRes.error.length>0){\n';
  html += '     			alert("ERROR: Incorrect PIN Code. Please enter the assigned approver\'s PIN Code."); btn.removeAttr("disabled")\n';
  html += '     		}else{ window.location = "'+createBoqRestletUrl+'";}\n';
  html += '     	});\n';
  html += '   })\n';
  
  html += '   $("#btnOkReturn").click(function(e){\n';
  html += '     	e.preventDefault();\n';
  html += '     	if($("#returnMsg").val()==""){ alert("Please input reason for return"); }else{\n';
  html += '     	var btn = $(this); btn.attr("disabled", "disabled");\n';
  html += '     	var ajaxCheck = $.post("'+pinValidateUrl+'", $("#frmReturn").serialize());\n';
  html += '     	ajaxCheck.done(function(resp){\n';
  html += '     		var jsonRes = JSON.parse(resp);\n';
  //html += '     		$(".loading").hide();\n';
  html += '     		if(jsonRes.error.length>0){\n';
  html += '     			alert("ERROR: Incorrect PIN Code. Please enter the assigned approver\'s PIN Code."); btn.removeAttr("disabled")\n';
  html += '     		}else{ $("#frmReturn").submit();}\n';
  html += '     	});}\n';
  html += '   })\n';
  
  
  html += '   $("#btnReturn").click(function(){\n';
  html += '     $("#frmReturn").modal({ modalClass:"jModal", escapeClose: false, clickClose: false});\n';
  //html += '     window.location = "'+returnBoqRestletUrl+'"; return false; \n';
  html += '   })\n';
  html += '   $("#btnSaveDraft").click(function(){\n';
  html += '     $("#btnClick").val(1);\n';
  html += '   })\n';
  html += '   $("#btnApproval").click(function(){\n';
  html += '     $("#btnClick").val(2);\n';
  html += '     var approver = $("#selBoqApprover").val();\n';
  html += '     if(approver==""){ alert("Please choose BOQ Approver"); return false; }\n';
  //html += '     window.location = "'+approvalRestletUrl+'&approver="+approver; return false;\n';
  html += '   })\n';
  html += '   $(".collapse-toggler-btn").click(function(){\n';
  html += '     var $elem = $(this);\n';
  html += '     if($elem.find("i").hasClass("mdi-plus")){\n';
  html += '       $elem.find("i").removeClass("mdi-plus");\n';
  html += '       $elem.find("i").addClass("mdi-minus");';
  html += '     }else{\n';
  html += '       $elem.find("i").removeClass("mdi-minus");\n';
  html += '       $elem.find("i").addClass("mdi-plus");\n';
  html += '     }\n';
  html += '   })\n';
  
  html += '   $("#btnExpand").click(function(){\n';
  html += '     var triggerBtnId;\n';
  html += '     $.each($(".accordion_content .collapse"), function(i, elem){\n';
  html += '     		if(!$(this).hasClass("show")){\n';
  html += '     			triggerBtnId = $(this).attr("id");\n';
  html += '     			$("button[href=\'#"+triggerBtnId+"\']").trigger("click");\n';
  html += '     		}\n';
  html += '     	});\n';
  html += '   })\n';
  html += '   $("#btnCollapse").click(function(){\n';
  html += '     var triggerBtnId;\n';
  html += '     $.each($(".accordion_content .collapse"), function(i, elem){\n';
  html += '     		if($(this).hasClass("show")){\n';
  html += '     			triggerBtnId = $(this).attr("id");\n';
  html += '     			$("button[href=\'#"+triggerBtnId+"\']").trigger("click");\n';
  html += '     		}\n';
  html += '     	});\n';
  html += '   })\n';
  html += ' })\n';
  html += '</script>\n';
  html += '</html>\n';
  response.write(html);
}
