/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       11 Aug 2017     EYM-013
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */


function userEventBeforeLoad(type, form, request){
	var projStatus = nlapiGetFieldValue('entitystatus');
	var recID = nlapiGetRecordId();
	var context = nlapiGetContext();
    var role = context.getRole();	
  
	var boqFileName = 'boq_template_'+recID;
	var filters = [ new nlobjSearchFilter('name',null,'is',boqFileName)];
	var fileSearch = nlapiSearchRecord('file',null,filters);

	var boqFreeFormFileName = 'boq_freeform_template_'+recID;
	var filters = [ new nlobjSearchFilter('name',null,'is',boqFreeFormFileName)];
	var fileSearchFreeForm = nlapiSearchRecord('file',null,filters);
	
	if(type == 'view' || type == 'edit'){
        if(role != 1013 && role != 1014){     	//sandbox(Operations Engineer = 1025, PSC CEO = 1026)    prod(Operations Engineer = 1013, PSC CEO = 1014)
			if((fileSearch == null && fileSearchFreeForm == null ) && projStatus == 5){
				var loadFileRec = nlapiLoadRecord('job', recID);
				var projChar1 = loadFileRec.getFieldValue('custentity_eym_proj_charter');
	
				var script = "var projChar = '"+projChar1+"'; "; 
				script += "if(projChar == '' || projChar == 'null'){ alert('Please upload a Project Charter before creating a BOQ.'); } ";         
				script += "else{ window.location.replace('" + nlapiResolveURL('SUITELET', 'customscript_sl_eym_boq_template', 'customdeploy_sl_eym_boq_template') + "&projectid=" + recID + "') } ";
				
				form.addButton('custpage_createboq_btn', 'Create BOQ', script);
			}
        }
      
      
      	if(role != 1013 && role != 1014){		//sandbox(Operations Engineer = 1025, PSC CEO = 1026)    prod(Operations Engineer = 1013, PSC CEO = 1014)
			if((fileSearch == null && fileSearchFreeForm == null ) && projStatus == 5){
				var loadFileRec = nlapiLoadRecord('job', recID);
				var projChar1 = loadFileRec.getFieldValue('custentity_eym_proj_charter');
	
				var script = "var projChar = '"+projChar1+"'; "; 
				script += "if(projChar == '' || projChar == 'null'){ alert('Please upload a Project Charter before creating a BOQ.'); } ";         
				script += "else{ window.location.replace('" + nlapiResolveURL('SUITELET', 'customscript_sl_eym_boq_free_template', 'customdeploy_sl_eym_boq_free_template') + "&projectid=" + recID + "') } ";
				
				form.addButton('custpage_createfreeform_btn', 'Create Free-form BOQ', script);
			}
		}	 
      
      
		if(role != 1013 && role != 1014){		//sandbox(Operations Engineer = 1025, PSC CEO = 1026)    prod(Operations Engineer = 1013, PSC CEO = 1014)
			if(fileSearch != null && projStatus == 5){
				var manageBOQlink = "window.location.replace('" + nlapiResolveURL('SUITELET', 'customscript_sl_eym_boq_template', 'customdeploy_sl_eym_boq_template') + "&projectid=" + recID + "')";
				form.addButton('custpage_manageboq_btn', 'Manage BOQ', manageBOQlink);
			}
          
			if(fileSearchFreeForm != null && projStatus == 5){
				var manageFreeFormlink = "window.location.replace('" + nlapiResolveURL('SUITELET', 'customscript_sl_eym_boq_free_template', 'customdeploy_sl_eym_boq_free_template') + "&projectid=" + recID + "')";
				form.addButton('custpage_manageboq_btn', 'Manage BOQ', manageFreeFormlink);	
			}
		}
    
 
    }

	try{

	var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_eym_boq_projid', null, 'is', recID);	
		
	var columns = new Array();
		columns[0] = new nlobjSearchColumn('name');
 		columns[1] = new nlobjSearchColumn('custrecord_eym_proj_subsystem');
 		columns[2] = new nlobjSearchColumn('custrecord_eym_boq_categories');
 		columns[3] = new nlobjSearchColumn('custrecord_eym_act_desc');
 		columns[4] = new nlobjSearchColumn('custrecord_eym_boq_base_cost');
 		columns[5] = new nlobjSearchColumn('custrecord_eym_boq_class');
 		columns[6] = new nlobjSearchColumn('custrecord_eym_changeorder_no');


	var searchRes = nlapiSearchRecord('customrecord_eym_boq',null,filters,columns);
	
	boqSubtab(searchRes);
	}
	catch(ex){
		nlapiLogExecution('DEBUG', 'Exception', ex.message == '' ? ex : ex.message);
	}
}


function boqSubtab(searchRes){
	if (type == 'create' || type == 'edit' || type == 'view'){
		var boqTab = form.addTab('custpage_boq_tab', 'Bill of Quantity');
	    var boqSubTab = form.addSubTab('custpage_boq_subtab', 'Bill of Quantity','custpage_boq_tab');
		var boqSubList = form.addSubList('custpage_boq_sublist', 'list', 'Bill of Quantity', 'custpage_boq_subtab');

		boqSubList.addField('custpage_boq_projid', 'text', 'ID').setDisplayType('inline'); 
		//boqSubList.addField('custpage_boq_projsubsystem', 'select', 'Project Subsystem', 'customlist_eym_proj_subsystem').setDisplayType('inline');
		boqSubList.addField('custpage_boq_projsubsystem', 'text', 'Project Subsystem').setDisplayType('inline');
		//boqSubList.addField('custpage_boq_actgroup', 'select', 'Activity Group', 'customrecord_eym_boq_activity').setDisplayType('inline');
		boqSubList.addField('custpage_boq_actgroup', 'text', 'Activity Group').setDisplayType('inline');
		boqSubList.addField('custpage_boq_description', 'text', 'Description').setDisplayType('inline'); 
		boqSubList.addField('custpage_boq_totalcost', 'currency', 'Total Cost').setDisplayType('inline'); 
		boqSubList.addField('custpage_boq_costtype', 'select', 'Cost Type', 'classification').setDisplayType('inline'); 
		boqSubList.addField('custpage_boq_pconum', 'select', 'Change Order No.', 'customrecord_eym_change_order').setDisplayType('inline'); 


		if(searchRes){		
			for(var i = 0; i < searchRes.length; i++) {
				
				var outputResultBOQ =  searchRes[i];
				//nlapiLogExecution('DEBUG','outputResultBOQ', outputResultBOQ);

				var listBOQ = outputResultBOQ.getId();
					    	
				var projSubsystem = outputResultBOQ.getValue('custrecord_eym_proj_subsystem');
				var actGroup = outputResultBOQ.getValue('custrecord_eym_boq_categories');
				var desc = outputResultBOQ.getValue('custrecord_eym_act_desc');
				var baseCost = outputResultBOQ.getValue('custrecord_eym_boq_base_cost');
				nlapiLogExecution('DEBUG', 'base cost', baseCost);
				var actClass = outputResultBOQ.getValue('custrecord_eym_boq_class');
				var coNum = outputResultBOQ.getValue('custrecord_eym_changeorder_no');
				
				var boqLink = nlapiResolveURL('RECORD', 'customrecord_eym_boq', listBOQ);
		    	
				boqSubList.setLineItemValue('custpage_boq_projid', i + 1, "<a href='"+boqLink+"'>"+ outputResultBOQ.getValue('name')  +"</a>");
				boqSubList.setLineItemValue('custpage_boq_projsubsystem', i + 1, projSubsystem);
				boqSubList.setLineItemValue('custpage_boq_actgroup', i + 1, actGroup);
				boqSubList.setLineItemValue('custpage_boq_description', i + 1, desc);
				boqSubList.setLineItemValue('custpage_boq_totalcost', i + 1, baseCost);
				boqSubList.setLineItemValue('custpage_boq_costtype', i + 1, actClass);
				boqSubList.setLineItemValue('custpage_boq_pconum', i + 1, coNum);

			}
		}
	}
}

