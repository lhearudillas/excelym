/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       11 Aug 2017     dennysutanto
 *
 */

function isEmpty(fldValue){
	return fldValue == '' || fldValue == null || fldValue == undefined || fldValue == 'null';
}

function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function boqFreeForm(request, response)
{
  var jqueryModalJs = 'https://system.netsuite.com/core/media/media.nl?id=536&c=4317943_SB1&h=d715f48c82e22e51d3a5&_xt=.js';
  var mainCss = 'https://system.netsuite.com/core/media/media.nl?id=626&c=4317943_SB1&h=80e5044fe17a116c6e29&_xt=.css';
  var boqFolder = '108';
  if(nlapiGetContext().getEnvironment()=='PRODUCTION'){
	  jqueryModalJs = 'https://system.na3.netsuite.com/core/media/media.nl?id=536&c=4317943&h=c1a0fa134372b1c1ccd9&_xt=.js';
	  mainCss = 'https://system.na3.netsuite.com/core/media/media.nl?id=626&c=4317943&h=6ae2a43efd859b421040&_xt=.css';
	  boqFolder = '108';
  }	
  var columns = [
    new nlobjSearchColumn('name')
  ];

  var projectId = request.getParameter('projectid');
  var boqFileName = 'boq_freeform_template_'+projectId;
  var createBoqRestletUrl = nlapiResolveURL('SUITELET', 'customscript_sl_eym_create_boq', 'customdeploy_sl_eym_create_boq')+'&projectid='+projectId+'&f=freeform';
  var approvalRestletUrl = nlapiResolveURL('SUITELET', 'customscript_sl_eym_submit_approval', 'customdeploy_sl_eym_submit_approval')+'&projectid='+projectId;
  var returnBoqRestletUrl = nlapiResolveURL('SUITELET', 'customscript_sl_eym_return_boq', 'customdeploy_sl_eym_return_boq')+'&projectid='+projectId;
  var pinValidateUrl = nlapiResolveURL('SUITELET', 'customscript_sl_eym_validate_pin', 'customdeploy_sl_eym_validate_pin')+'&type=project&id='+projectId;
  var projectUrl = nlapiResolveURL('RECORD', 'job', projectId);
  var classFilter = [
    new nlobjSearchFilter('custrecord_eym_use_in_psc',null,'is','T')
  ];
  var classColumn = [
    new nlobjSearchColumn('name'),
    new nlobjSearchColumn('custrecord_eym_psc_tempsequence')
  ]
  classColumn[1].setSort();
  var classes = nlapiSearchRecord('classification',null,classFilter,classColumn);
  var currentUserId = nlapiGetUser();
  var currentUserRole = nlapiGetRole();
  nlapiLogExecution('DEBUG','currentUserRole',currentUserRole);
  var extraHtml = "";
  var subCount = 0;
  var subsystemJson = [];
  var projectObj = nlapiLoadRecord('job',projectId);//nlapiLookupField('job',projectId,['entityid','companyname','parent','entitystatus']);
  if ( request.getMethod() == 'POST' ){
	/*  
	var j = 
	{
		grandTotal : "300.00",
		subsystemData :[
		{
			"id" : 1,
			"name" : "Subsystem1",
			"total" : "200.00",
			"classTotal" :{
				"subTotal" : [
					{"1":"50.00"},
					{"2":"30.00"}
				],
				"grandTotal" : "80.00"
			},

			"activities" : [{
				"id" : 1,
				"name" : "Activity 1",
				"description" : "Description 1",
				"total" : "100.00",
				"classes":[
					{"id":1, "name": "Material", "amount":10},
					{"id":2, "name": "Labor Admin", "amount":11},
					{"id":3, "name": "Subcon", "amount":12}
				]
			]}
		}
		]
		 
	}	
	*/
	subCount = request.getParameter('subCount');
	nlapiLogExecution('DEBUG','subCount ',subCount);
    var boqTotal = 0;
    var classTotal = {};
    for(var i=0;i<subCount;i++){
    	  if(!isEmpty(request.getParameter("txtSubsystem-"+i))){
	    	  var subsystemData = {};	
	    	  var subsystemTotal = 0;
	    	  var subSystemId = i;
	      subsystemData.id=i;
	      subsystemData.name=request.getParameter("txtSubsystem-"+i).replace(/"/g, '\"');
	      subsystemData.classTotal = {};
	      subsystemData.activities = [];
	      var actCount = request.getParameter("actCount-"+i);
	      nlapiLogExecution('DEBUG','actCount-'+i,actCount);
	      for(var j=0;j<actCount;j++){
	    	  	nlapiLogExecution('DEBUG','act-'+i,request.getParameter("txtActName-"+i+"-"+j));
	    	  	if(!isEmpty(request.getParameter("txtActName-"+i+"-"+j))){
		    	    var actData = {};
		        actData.id = j;
		        actData.name = request.getParameter("txtActName-"+i+"-"+j).replace(/"/g, '\"');
		        actData.description = request.getParameter("txtActDesc-"+i+"-"+j).replace(/"/g, '\"');
		    	  	actData.classes = [];
		        var actTotal = 0;
		        var activityId = j;
		        var prefix = subSystemId+"-"+activityId;
		        for(var c=0;c<classes.length;c++){
		        	  var classData = {};
		          classData.id = classes[c].getId();
		          classData.name = classes[c].getValue("name");
		          var amount = request.getParameter("class-"+prefix+"-"+classes[c].getId()).replace(/,/g, '') * 1;
		          //nlapiLogExecution('DEBUG','amount-'+classes[c].getId(),amount);
		          classData.amount = amount
		          actTotal += amount;
		          actData.classes.push(classData);
		          
		          if(!("class-"+subSystemId+'-'+classes[c].getId() in classTotal)){
		      	  	classTotal["class-"+subSystemId+'-'+classes[c].getId()] = [];
			      }
			      classTotal["class-"+subSystemId+'-'+classes[c].getId()].push(amount);
		        }
		        actData.total = actTotal;
		        subsystemTotal += actTotal;
		        subsystemData.activities.push(actData);
	    	  	}
	      }
	      subsystemData.total = subsystemTotal;
	      boqTotal += subsystemTotal;
	      
	      var classGrandTotal = 0;	
	      subsystemData.classTotal.subTotal = {};
	      for(var key in classTotal){
			var aKey = key.split('-');
			if(aKey[1]==subSystemId){
				var classTotalVal = 0;
				for (var ss = 0; ss < classTotal[key].length; ss++ ){
		    			classTotalVal += (classTotal[key][ss] * 1 );
		    			classGrandTotal += (classTotal[key][ss] * 1 );
	    			}
				subsystemData.classTotal.subTotal[aKey[2]] = classTotalVal;
			}
		  }
	      subsystemData.classTotal.grandTotal = classGrandTotal;
		  //subsystemData["classGrandTotal-"+subSystemId] = classGrandTotal;
			
	      subsystemJson.push(subsystemData);
      }
    }
    
    var jsonFile = {
      'projectId' : projectId,
      'grandTotal' : boqTotal,
      'subsystemData' : subsystemJson
    }
    var tempFile = nlapiCreateFile(boqFileName, 'PLAINTEXT', JSON.stringify(jsonFile));
    tempFile.setFolder(boqFolder);
  	var newfile = nlapiSubmitFile(tempFile);
  	var fields = ['custentity_eym_boq_status','custentity_boq_approver'];
  	var values = [4,request.getParameter("selBoqApprover")];
  	if(!projectObj.getFieldValue('custentity_eym_boq_creator')){
  		fields.push('custentity_eym_boq_creator');
  		values.push(currentUserId);
  	}
  	nlapiSubmitField('job',projectId,fields,values);
  	if(request.getParameter("btnClick")!="2"){
  		extraHtml += '<script type="text/javascript">\n';
  	  	extraHtml += ' alert("BOQ Template Saved")\n';
  	    extraHtml += '</script>\n';
  	}else{
  		var approver = request.getParameter("selBoqApprover");
  		extraHtml += '<script type="text/javascript">\n';
  	  	extraHtml += ' window.location = "'+approvalRestletUrl+'&approver='+approver+'";\n';
  	    extraHtml += '</script>\n';
  	    response.write(extraHtml);
  	}
  }
  
  var filters = [ new nlobjSearchFilter('name',null,'is',boqFileName)];
  var fileSearch = nlapiSearchRecord('file',null,filters);
  var subsystemJson = null;
  var json = null;
  if(fileSearch){
    tempFile = nlapiLoadFile(fileSearch[0].getId());
    json = JSON.parse(tempFile.getValue());
    subsystemJson = json.subsystemData;
    subCount = subsystemJson.length;
  }
  var boqApprover = nlapiSearchRecord(null,'customsearch_eym_boq_approver_list',null);
  
  var correctApprover = false;
  var costApprover = nlapiSearchRecord(null,'customsearch_eym_cost_approver_list',null);
  if(costApprover){
	  for(var b=0;b<costApprover.length;b++){
		  nlapiLogExecution('DEBUG','costApprover[b].getId()',costApprover[b].getId());
		  if(currentUserId==costApprover[b].getId()){
			  correctApprover = true;
			  break;
	      }
	  }
  }
  
  var html = '<html>\n';
  html += '<head>\n';
  html += '<link rel="stylesheet" href="'+mainCss+'" type="text/css" media="screen" />\n';
  html += '<title>PSC Free Form BOQ Template</title>\n';
  html += '</head>\n';
  html += '<body>\n';
  //html += '<form name="frmReturn" id="frmReturn" class="jModal" style="display:none" method="POST" action="'+returnBoqRestletUrl+'">\n';
  //html += '<p><label>Reason for Return:</label></p><p><textarea class="form-control" name="returnMsg" id="returnMsg"></textarea></p>\n';
  //html += '<p><input type="submit" id="btnOkReturn" value="Ok" class="btn btn-primary"></p>\n';
  //html += '</form>\n';
  html += '<form name="frmReturn" id="frmReturn" style="display:none" method="POST" action="'+returnBoqRestletUrl+'">\n';
  html += '<p><label>PIN:</label></p><p><input type="password" name="pin" class="form-control" id="pin"></p>\n';
  html += '<p><label>Reason for Return:</label></p><p><textarea name="returnMsg" class="form-control" id="returnMsg"></textarea></p>\n';
  html += '<p><input type="submit" id="btnOkReturn" class="btn btn-primary" value="OK" /></p>\n';
  html += '</form>\n';
  html += '<form name="frmPin" id="frmPin" style="display:none" method="POST">\n';
  html += '<p><label>PIN:</label></p><p><input type="password" name="pin" class="form-control" id="pin"></p>\n';
  html += '<p><input type="submit" id="btnOkPin" class="btn btn-primary" value="OK" /></p>\n';
  html += '</form>\n';
  html += '<form name="frmBoq" id="frmBoq" method="POST" action="">\n';
  var disabled = '';
  var showAddBtn = false;
  html += '<input type="hidden" name="projectid" value="'+projectId+'">\n';
  html += '<input type="hidden" name="subCount" value="'+subCount+'" id="subCount">\n';
  html += '<input type="hidden" name="btnClick" id="btnClick" value="">\n';
  html += '<header class="actions stick-to-top">\n';
  html += '	<div class="card">\n';
  html += '		<div class="card-body"><div class="row"><div class="col">\n';
  if(projectObj.getFieldValue('custentity_eym_boq_status')=='4' || projectObj.getFieldValue('custentity_eym_boq_status')=='3' || projectObj.getFieldValue('custentity_eym_boq_status')==null){
	  //if(true){
	  if(currentUserRole=='3' || projectObj.getFieldValue('custentity_eym_boq_creator')==currentUserId || projectObj.getFieldValue('custentity_eym_boq_creator')=='' || projectObj.getFieldValue('custentity_eym_boq_creator')==null){
		  showAddBtn = true;
		  html += '    <input type="submit" class="btn btn-primary" value="Save as Draft" name="btnSaveDraft" id="btnSaveDraft">\n';
		  if(json){
			  if(Number(json.grandTotal) > 0){
				  html += '    <input type="submit" class="btn btn-primary" value="Submit for Approval" name="btnApproval" id="btnApproval">\n';
			  }
		  }
	  }else{
		  disabled = 'disabled';
	  }
	  
  }else if(projectObj.getFieldValue('custentity_eym_boq_status')=='1'){
	  if(correctApprover || currentUserRole=='3'){
		  html += '    <input type="button" class="btn btn-primary" value="Approve BOQ" name="btnApprove" id="btnApprove">\n';
		  html += '    <input type="button" class="btn btn-primary" value="Return BOQ" name="btnReturn" id="btnReturn">\n';
	  }
	  disabled = 'disabled';
  }
  html += '    <input type="button" class="btn btn-default" value="Back" name="btnBack" id="btnBack"></div>\n';
  html += '		<div class="col-3"><dl class="row mb-0"><dt class="col-md-5">Project Code</dt><dd class="col-md-7">'+projectObj.getFieldValue('entityid')+'</dd></dl></div>\n';
  html += '		<div class="col-3"><dl class="row mb-0"><dt class="col-md-5">Total BOQ Baseline</dt><dd class="col-md-7"><span id="totalBoq">'+(json ? addCommas(Number(json.grandTotal).toFixed(2)) : "0.00")+'</span></dd></dl></div>\n';
  html += '		</div></div></div></header>\n';
  
  html += '<main class="main-content">\n';
  html += ' <div class="container-fluid">\n';
  html += '	 <div class="row">\n';
  html += '		<div class="col">\n';
  html += '		<div class="card mb-3">\n';
  html += '		<div class="card-header"><h3 class="float-left mt-2 mb-1">Project Summary</h3></div>\n';
  html += '		<div class="card-body"><div class="row">\n';
  html += '			<div class="col-md-4">\n';
  html += '				<dl class="row">\n';
  //html += '					<dt class="col-md-4">Project Code</dt>\n';
  //html += '					<dd class="col-md-8">'+projectObj.getFieldValue('entityid')+'</dt>\n';
  html += '					<dt class="col-md-4">Project Name</dt>\n';
  html += '					<dd class="col-md-8">'+projectObj.getFieldValue('companyname')+'</dt>\n';
  html += '					<dt class="col-md-4">Customer</dt>\n';
  html += '					<dd class="col-md-8">'+projectObj.getFieldText('parent')+'</dt>\n';
  html += '					<dt class="col-md-4">Project Status</dt>\n';
  html += '					<dd class="col-md-8">'+projectObj.getFieldText('entitystatus')+'</dt>\n';
  html += '				</dl>\n';
  html += '			</div>\n';
  html += '			<div class="col-md-6">\n';
  html += '				<dl class="row">\n';
  //html += '					<dt class="col-md-4">Total BOQ Baseline</dt>\n';
  //html += '					<dd class="col-md-8"><span id="totalBoq">'+(json ? addCommas(Number(json.grandTotal).toFixed(2)) : "0.00")+'</span></dt>\n';
  html += '					<dt class="col-md-4">BOQ Approver</dt>\n';
  html += '					<dd class="col-md-8"><select name="selBoqApprover" id="selBoqApprover" '+disabled+'>\n';
  html += '						<option value=""></option>\n';
  if(boqApprover){
	  for(var b=0;b<boqApprover.length;b++){
	      var selected = "";
		  if(projectObj.getFieldValue('custentity_boq_approver')==boqApprover[b].getId()){
	    	  	selected = "selected";
	      }
		  html += '<option value="'+boqApprover[b].getId()+'" '+selected+'>'+boqApprover[b].getValue('entityid')+'</option>\n';
	  }
  }
  html += '         				</select></dd>\n';
  html += '					<dt class="col-md-4">BOQ Status</dt>\n';
  html += '					<dd class="col-md-8">'+projectObj.getFieldText('custentity_eym_boq_status')+'</dt>\n';
  html += '				</dl>\n';
  html += '			</div>\n';
  html += '		</div></div>\n';
  html += '		</div>\n';
  html += '		</div>\n';
  html += '	 </div>\n';
  
  html += '	 <div class="row">\n';
  html += '		<div class="col">\n';
  html += '		<div class="card mb-3">\n';
  html += '		<div class="card-header">\n';
  html += '			<h3 class="float-left mt-2 mb-1">Project Subsystems and Activities</h3>\n';
  html += '			<input type="button" class="btn btn-primary float-right ml-1" id="btnCollapse" value="Collapse All">\n';
  html += '			<input type="button" class="btn btn-primary float-right" id="btnExpand" value="Expand All">\n';
  html += '		</div>\n';
  
  html += '		<div class="table-responsive-wrapper">\n';
  html += '			<div class="card-header p-0 clearfix">\n';
  html += '			<table class="structured-table" style="width:100%;"><thead><tr>\n';
  html += '				<th style="width: 24%;"><h5 class="mt-2 mb-1">Project Subsystems</h5></th>\n';
  for(var c=0;c<(classes.length);c++){
	  html += '			<th class="text-center" style="width:9.5%"><h5 class="mt-2 mb-1">'+classes[c].getValue('name').replace(' ','<br>')+'</h5></th>\n';
  }
  html += '				<th class="text-center" style="width:9.5%"><h5 class="mt-2 mb-1">Total</h5></th>\n';
  html += '				<th class="text-center" style="width:9.5%"><h5 class="mt-2 mb-1"><!-- filler: do not remove --></h5></th>\n';
  html += '			</tr></thead></table>\n';
  html += '			</div>\n';
  html += '		</div>\n';
  
  html += '		<div class="card-body p-0"><div class="accordion_content">\n';
  if(subsystemJson){
	for(var i=0;i<subsystemJson.length;i++){
      var subsystem = subsystemJson[i];
      html += '		<div class="card">\n';
      html += '		<div class="card-header" role="tab" id="heading'+i+'">\n';
      html += '			<table class="structured-table header" style="width: 100%;"><thead><tr>\n';
      html += '				<th class="btn-toggle-col" style="width: 24%;">\n';
      html += '					<input type="hidden" value="'+(request.getParameter('actCount-'+i) ? request.getParameter('actCount-'+i) : subsystem.activities.length)+'" name="actCount-'+i+'" id="actCount-'+i+'"><h5 class="float-left mb-0"><div class="btn-desc">\n';
      html += '					<div class="button"><button type="button" data-toggle="collapse" href="#collapse'+i+'" aria-expanded="true" aria-controls="collapse'+i+'" class="btn btn-default btn-sm btn-icon collapse-toggler-btn" ><i class="mdi mdi-plus"></i></button></div>\n';
      html += '					<div class="content"><input type="text" class="subClass form-control form-control-sm" '+disabled+' id="txtSubsystem-'+i+'" data-i="'+i+'" name="txtSubsystem-'+i+'" value="'+subsystem.name.replace(/"/g, "&quot;")+'"></div>\n';
      html += '					</div></h5>\n';	
      if(showAddBtn){
    	  	html += '				<button type="button" class="btnRemoveSub btn btn-danger btn-sm ml-1"><i class="mdi mdi-delete"></i></button>\n';
      }
      html += '				</th>\n';
      for(var c=0;c<(classes.length);c++){
    	  	html += '			<th class="text-center" style="width:9.5%"><h5 class="mt-2 mb-1"><input disabled type="text" class="form-control form-control-sm numOnly" value="'+addCommas((subsystem.classTotal.subTotal[classes[c].getId()] *1).toFixed(2))+'"></h5></th>\n';
      }
      html += '				<th class="text-center" style="width:9.5%"><h5 class="mt-2 mb-1"><input disabled type="text" class="form-control form-control-sm numOnly" value="'+addCommas((subsystem.classTotal.grandTotal *1).toFixed(2))+'"></h5></th>\n';
      html += '				<th class="text-center" style="width:9.5%"><h5 class="mt-2 mb-1"><!-- filler: do not remove --></h5></th>\n';
      html += '			</tr></thead></table>\n';
      html += '		</div>\n';
      
      html += '		<div id="collapse'+i+'" class="collapse" role="tabpanel" aria-labelledby="heading'+i+'" data-parent="#accordion">\n';
      html += '			<div class="card-body"><table class="structured-table" style="width:100%;">\n';
      html += '			<thead><tr>\n';
      html += '				<th style="width:12%"><h5 class="mt-2 mb-1">Activity</h5></th>\n';
      html += '				<th style="width:12%"><h5 class="mt-2 mb-1">Description</h5></th>\n';
      for(var c=0;c<classes.length;c++){
          html += '			<th class="text-center" style="width:9.5%"><h5 class="mt-2 mb-1">'+classes[c].getValue('name').replace(' ','<br>')+'</h5></th>\n';
      }
      html += '				<th class="text-center" style="width:9.5%"><h5 class="mt-2 mb-1">Total</h5></th>\n';
      html += '				<th class="text-center" style="width:9.5%"><h5 class="mt-2 mb-1">Action</h5></th>\n';
      html += '			</tr></thead>\n';
      html += '			<tbody>\n';
	  for(var j=0;j<subsystem.activities.length;j++){
	      var activity = subsystem.activities[j];
	      nlapiLogExecution('DEBUG','activity',activity);
	      nlapiLogExecution('DEBUG','activity.name',activity.name);
	      var prefix = i+"-"+j;
	      html += '     <tr>\n';
	      html += '       <td style="width:12%"><input type="text" '+disabled+' class="form-control form-control-sm actClass-'+i+'" style="text-align:left" id="txtActName-'+prefix+'" name="txtActName-'+prefix+'" value="'+activity.name.replace(/"/g, "&quot;")+'"></td>';
	      html += '       <td style="width:12%"><input type="text" '+disabled+' class="form-control form-control-sm" style="text-align:left" name="txtActDesc-'+prefix+'" value="'+activity.description.replace(/"/g, "&quot;")+'"></td>';
	      for(var c=0;c<classes.length;c++){
	        var boqClass = activity.classes[c];
	    	  	var amt = (boqClass.amount * 1).toFixed(2);
	    	  	html += '       <td class="text-center" style="width:9.5%"><input '+disabled+' type="text" class="form-control form-control-sm numOnly amt-'+prefix+'" data-i="'+prefix+'" name="class-'+prefix+'-'+classes[c].getId()+'" value="'+addCommas(amt)+'"></td>';
	      }
	      html += '       <td class="text-center" style="width:9.5%"><input disabled type="text" id="total-'+prefix+'" class="subTotalBoq form-control form-control-sm numOnly" name="total-'+prefix+'" value="'+ addCommas((activity.total *1).toFixed(2))+'"></td>';
	      html += '       <td class="text-center" style="width:9.5%"><button title="Remove" type="button" '+(showAddBtn ? "": "disabled")+' class="btnRemoveAct btn btn-danger btn-sm btn-icon"><i class="mdi mdi-delete"></i></button></td>';
	      html += '     </tr>\n';
	  }
	  if(showAddBtn){
		  html += '     <tr class="no-data"><td colspan="'+(classes.length + 4)+'"><button type="button" class="btnAddActivity btn btn-primary btn-sm" data-i="'+i+'" id="btnAddActivity-'+i+'"><i class="mdi mdi-plus"></i> Add Activity</button></td></tr>';
	  }
      html += '			</tbody>\n';
      html += '			</table></div>\n';
      html += '		</div></div>\n';
    }
  }
  
  if(showAddBtn){
	  html += '		<div id="trAdd" class="card no-data"><div class="card-header" role="tab" id="headingOne">\n';
	  html += '  		<button id="btnAddSubsystem" type="button" class="btn btn-primary btn-sm"><i class="mdi mdi-plus"></i> Add Subsystem</button>\n';
	  html += '		</div></div>\n';
  }
  
  
  html += '		</div></div></div></div>\n';
  
  
  html += '		</div>\n';
  html += '	 </div>\n';
	
  html += '	</div>\n';
  html += '</main>\n';
  html += '</form>\n';
  html += extraHtml;
  html += '<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>\n';
  html += '<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>\n';
  html += '<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>\n';
  html += '<script type="text/javascript" src="'+jqueryModalJs+'"></script>\n';  
  html += '<script type="text/javascript">\n';
  
  html += ' $(document).ready(function(){\n';
  html += '   function getTotal(){\n';
  html += '     var total = 0\n';
  html += '     $(".subTotalBoq").each(function(row, val){\n';
  html += '     		total += ($(this).val().replace(/,/g, "")*1);\n';
  html += '     });\n';
  html += '     return total\n';
  html += '   }\n';
  html += '   function setTotal(total){\n';
  html += '     $("#totalBoq").html(total.toFixed(2));\n';
  html += '   }\n';
  html += '   $("#btnBack").click(function(){\n';
  html += '     window.location = "'+projectUrl+'"; return false; \n';
  html += '   })\n';
  html += '   $(document).on("keydown",".numOnly", function (e) {\n';
  html += '     if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1){ return; }\n';
  html += '     if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) { e.preventDefault(); }\n';
  html += '   })\n';
  
  html += '   $(document).on("click",".numOnly", function (e) {\n';
  html += '     if($(this).val()=="0.00"){ $(this).val("")}\n';
  html += '   })\n';
  
  html += '   $(document).on("blur",".numOnly", function (e) {\n';
  html += '     if($(this).val()==""){ $(this).val("0.00")}\n';
  html += '   })\n';
  
  html += '   $(document).on("keyup",".numOnly", function (e) {\n';
  //html += '   $(".numOnly").keyup(function (e) {\n';
  html += '     var cGrandTotal = 0;\n';
  html += '     var i = $(this).data("i");\n';
  html += '     if($(this).val()==""){ $(this).val("0.00")}\n';
  html += '     var subtot = 0\n';
  html += '     $(".amt-"+i).each(function(row, val){\n';
  html += '     		var amt = $(this);\n';
  html += '     		subtot += (amt.val().replace(/,/g, "")*1);\n';
  html += '     });\n';
  html += '     if(!isNaN(subtot.toFixed(2))){\n';
  html += '     $("#total-"+i).val(subtot.toFixed(2));\n';
  
  html += '     var t = getTotal();\n';
  html += '     setTotal(t);\n';
  html += '     }\n';
  html += '   })\n';

  html += '   $("#btnApprove").click(function(){\n';
  html += '     $("#frmPin").modal({ modalClass:"jModal", escapeClose: false, clickClose: false});\n';
  html += '   })\n';
  
  html += '   $("#btnOkPin").click(function(e){\n';
  html += '     	e.preventDefault();\n';
  html += '     	var btn = $(this); btn.attr("disabled", "disabled");\n';
  html += '     	var ajaxCheck = $.post("'+pinValidateUrl+'", $("#frmPin").serialize());\n';
  html += '     	ajaxCheck.done(function(resp){\n';
  html += '     		var jsonRes = JSON.parse(resp);\n';
  //html += '     		$(".loading").hide();\n';
  html += '     		if(jsonRes.error.length>0){\n';
  html += '     			alert("ERROR: Incorrect PIN Code. Please enter the assigned approver\'s PIN Code."); btn.removeAttr("disabled")\n';
  html += '     		}else{ window.location = "'+createBoqRestletUrl+'";}\n';
  html += '     	});\n';
  html += '   })\n';
  
  html += '   $("#btnOkReturn").click(function(e){\n';
  html += '     	e.preventDefault();\n';
  html += '     	if($("#returnMsg").val()==""){ alert("Please input reason for return"); }else{\n';
  html += '     	var btn = $(this); btn.attr("disabled", "disabled");\n';
  html += '     	var ajaxCheck = $.post("'+pinValidateUrl+'", $("#frmReturn").serialize());\n';
  html += '     	ajaxCheck.done(function(resp){\n';
  html += '     		var jsonRes = JSON.parse(resp);\n';
  //html += '     		$(".loading").hide();\n';
  html += '     		if(jsonRes.error.length>0){\n';
  html += '     			alert("ERROR: Incorrect PIN Code. Please enter the assigned approver\'s PIN Code."); btn.removeAttr("disabled")\n';
  html += '     		}else{ $("#frmReturn").submit();}\n';
  html += '     	});}\n';
  html += '   })\n';

  html += '   $("#btnReturn").click(function(){\n';
  html += '     $("#frmReturn").modal({ modalClass:"jModal", escapeClose: false, clickClose: false});\n';
  //html += '     window.location = "'+returnBoqRestletUrl+'"; return false; \n';
  html += '   })\n';
  
  html += '   $("#btnSaveDraft").click(function(){\n';
  html += '     $("#btnClick").val(1);\n';
  html += '   })\n';
  
  html += '   $("#btnApproval").click(function(){\n';
  html += '     $("#btnClick").val(2);\n';
  html += '     var approver = $("#selBoqApprover").val();\n';
  html += '     if(approver==""){ alert("Please choose BOQ Approver"); return false; }\n';
  //html += '     window.location = "'+approvalRestletUrl+'&approver="+approver; return false;\n';
  html += '   })\n';
  
  html += '   $("#btnAddSubsystem").click(function(){\n';
  html += '     var r = $("#subCount").val();\n';
  html += '     $("#subCount").val(parseInt(r)+1);\n';
  html += '     var htmlTable1 = \'<div class="card"><div class="card-header" role="tab" id="heading\'+r+\'"><table class="structured-table header" style="width: 100%;"><thead><tr>\'\n';
  html += '     htmlTable1 += \'<th class="btn-toggle-col" style="width: 24%;">\'\n';
  html += '     htmlTable1 += 	\'<input type="hidden" value="0" name="actCount-\'+r+\'" id="actCount-\'+r+\'"><h5 class="float-left mb-0"><div class="btn-desc">\'\n';
  html += '     htmlTable1 += 	\'<div class="button"><button type="button" data-toggle="collapse" href="#collapse\'+r+\'" aria-expanded="true" aria-controls="collapse\'+r+\'" class="btn btn-default btn-sm btn-icon collapse-toggler-btn" ><i class="mdi mdi-plus"></i></button></div>\'\n';
  html += '     htmlTable1 += 	\'<div class="content"><input type="text" class="subClass form-control form-control-sm" data-i="\'+r+\'" name="txtSubsystem-\'+r+\'" id="txtSubsystem-\'+r+\'"></div>\'\n';
  html += '     htmlTable1 += 	\'</div></h5>\'\n';
  html += '     htmlTable1 += 	\'<button type="button" class="btnRemoveSub btn btn-danger btn-sm ml-1"><i class="mdi mdi-delete"></i></button>\'\n';
  html += '     htmlTable1 += \'</th>\'\n';
  
  for(var c=0;c<(classes.length);c++){
	  html += '     htmlTable1 += \'<th class="text-center" style="width:9.5%"><h5 class="mt-2 mb-1"><input disabled type="text" class="form-control form-control-sm numOnly" value="0.00"></h5></th>\'\n';
  }
  html += '     htmlTable1 += \'<th class="text-center" style="width:9.5%"><h5 class="mt-2 mb-1"><input disabled type="text" class="form-control form-control-sm numOnly" value="0.00"></h5></th>\'\n';
  html += '     htmlTable1 += \'<th class="text-center" style="width:9.5%"><h5 class="mt-2 mb-1"><!-- filler: do not remove --></h5></th>\'\n';
  html += '     htmlTable1 += \'</tr></thead></table></div>\'\n';

  html += '     htmlTable1 += \'<div id="collapse\'+r+\'" class="collapse" role="tabpanel" aria-labelledby="heading\'+r+\'" data-parent="#accordion">\'\n';
  html += '     htmlTable1 += \'	<div class="card-body"><table class="structured-table" style="width:100%;">\'\n';
  html += '     htmlTable1 += \'	<thead><tr>\'\n';
  html += '     htmlTable1 += \'		<th style="width:12%"><h5 class="mt-2 mb-1">Activity</h5></th>\'\n';
  html += '     htmlTable1 += \'		<th style="width:12%"><h5 class="mt-2 mb-1">Description</h5></th>\'\n';
  for(var c=0;c<classes.length;c++){
	  html += ' htmlTable1 += \'		<th class="text-center" style="width:9.5%"><h5 class="mt-2 mb-1">'+classes[c].getValue('name').replace(' ','<br>')+'</h5></th>\'\n';
  }
  html += '     htmlTable1 += \'		<th class="text-center" style="width:9.5%"><h5 class="mt-2 mb-1">Total</h5></th>\'\n';
  html += '     htmlTable1 += \'		<th class="text-center" style="width:9.5%"><h5 class="mt-2 mb-1">Action</h5></th>\'\n';
  html += '     htmlTable1 += \'	</tr></thead>\'\n';
  html += '     htmlTable1 += \'	<tbody>\'\n';
  html += '     htmlTable1 += \'	<tr class="no-data"><td colspan="'+(classes.length + 4)+'"><button type="button" class="btnAddActivity btn btn-primary btn-sm" data-i="\'+r+\'" id="btnAddActivity-\'+r+\'"><i class="mdi mdi-plus"></i> Add Activity</button></td></tr>\'\n';
  
  html += '     htmlTable1 += \'	</tbody>\'\n';
  html += '     htmlTable1 += \'	</div></table>\'\n';
  html += '     htmlTable1 += \'</div>\'\n';
  
  html += '     $("#trAdd").before(htmlTable1)\n';
  html += '   })\n';
  
  html += '   $(document).on("click",".btnAddActivity",function(){\n';
  
  html += '     var r = $(this).data("i");\n';
  html += '     var ar = $("#actCount-"+r).val();\n';
  html += '     $("#actCount-"+r).val(parseInt(ar)+1);\n';
  html += '     var htmlTable2 = \'<tr><td style="width:12%"><input type="text" style="text-align:left" class="form-control form-control-sm actClass-\'+r+\'" id="txtActName-\'+r+\'-\'+ar+\'" name="txtActName-\'+r+\'-\'+ar+\'"></td>\'\n';
  html += '     htmlTable2 += \'<td style="width:12%"><input type="text" style="text-align:left" class="form-control form-control-sm" name="txtActDesc-\'+r+\'-\'+ar+\'"></td>\'\n';
  for(var c=0;c<classes.length;c++){
	  //var amt = (subsystemJson ? (subsystemJson["class-"+classes[c].getId()+suffix] * 1).toFixed(2) : "0.00");
	  html += '     htmlTable2 += \'<td class="text-center" style="width:9.5%"><input type="text" class="form-control form-control-sm numOnly amt-\'+r+\'-\'+ar+\'" data-i="\'+r+\'-\'+ar+\'" value="0.00" name="class-\'+r+\'-\'+ar+\'-'+classes[c].getId()+'"></td>\'\n';
  }
  html += '     htmlTable2 += \'<td class="text-center" style="width:9.5%"><input disabled type="text" class="subTotalBoq form-control form-control-sm numOnly" value="0.00" id="total-\'+r+\'-\'+ar+\'" name="total-\'+r+\'-\'+ar+\'"></td>\'\n';
  html += '     htmlTable2 += \'<td class="text-center" style="width:9.5%"><button type="button" title="Remove" class="btnRemoveAct btn btn-danger btn-sm btn-icon"><i class="mdi mdi-delete"></i></button></td>\'\n';
  html += '     htmlTable2 += \'</tr>\'\n';
  html += '     $(this).parent().parent().before(\'\'+htmlTable2+\'\')\n';
  html += '   })\n';
  
  html += '   $(document).on("click",".btnRemoveSub",function(){\n';
  html += '     if(confirm("Are you sure you want to remove this Project Subsystem?")){\n';
  html += '     $(this).closest("div.card").remove()\n';
  html += '     }\n';
  html += '   })\n';
  
  html += '   $(document).on("click",".btnRemoveAct",function(){\n';
  html += '     $(this).closest("tr").remove()\n';
  html += '   })\n';
  
  html += '   $(document).on("click",".collapse-toggler-btn",function(){\n';
  html += '     var $elem = $(this);\n';
  html += '     if($elem.find("i").hasClass("mdi-plus")){\n';
  html += '       $elem.find("i").removeClass("mdi-plus");\n';
  html += '       $elem.find("i").addClass("mdi-minus");';
  html += '     }else{\n';
  html += '       $elem.find("i").removeClass("mdi-minus");\n';
  html += '       $elem.find("i").addClass("mdi-plus");\n';
  html += '     }\n';
  html += '   })\n';
  
  html += '   $("#btnExpand").click(function(){\n';
  html += '     var triggerBtnId;\n';
  html += '     $.each($(".accordion_content .collapse"), function(i, elem){\n';
  html += '     		if(!$(this).hasClass("show")){\n';
  html += '     			triggerBtnId = $(this).attr("id");\n';
  html += '     			$("button[href=\'#"+triggerBtnId+"\']").trigger("click");\n';
  html += '     		}\n';
  html += '     	});\n';
  html += '   })\n';
  html += '   $("#btnCollapse").click(function(){\n';
  html += '     var triggerBtnId;\n';
  html += '     $.each($(".accordion_content .collapse"), function(i, elem){\n';
  html += '     		if($(this).hasClass("show")){\n';
  html += '     			triggerBtnId = $(this).attr("id");\n';
  html += '     			$("button[href=\'#"+triggerBtnId+"\']").trigger("click");\n';
  html += '     		}\n';
  html += '     	});\n';
  html += '   })\n';
  
  html += '   $("#frmBoq").on("submit",function(){\n';
  html += '     var submit = true;\n';
  html += '     var t = getTotal()\n';
  html += '     if(t==0){ alert("Total BOQ Baseline cannot be empty"); submit = false; }\n';
  html += '     $(".subClass").each(function(row, val){\n';
  html += '     		var subsystem = $(this);\n';
  html += '     		var i = $(this).data("i");\n';
  html += '     		if(subsystem.val()==""){\n';
  html += '     			alert("Please input subsystem "+(row+1)); submit = false; return false;\n';
  html += '     		}\n';
  html += '     		var ar = $(".actClass-"+i).length;\n';
  html += '     		if(ar<=0){\n';
  html += '     			alert("Please add Activity for Subsystem "+(row+1)); submit=false; return false;\n';
  html += '     		}\n';
  html += '     		$(".actClass-"+i).each(function(actRow, actVal){\n';
  html += '     			if($(this).val()==""){\n';
  html += '     				alert("Please input Activity "+(actRow+1)+" for Subsystem "+(row+1)); submit=false; return false;\n';
  html += '     			}\n';
  html += '     		});\n';
  html += '     });\n';
  html += '     $(".numOnly").each(function(){\n';
  html += '     		if(isNaN($(this).val().replace(/,/g, ""))){\n';
  html += '     			alert("Please input valid number for amount"); submit = false; return false;\n';
  html += '     		}\n';
  html += '     });\n';

  html += '   	if(!submit){ return false; } \n';
  html += '   })\n';
  html += ' })\n';
  html += '</script>\n';
  html += '</html>\n';
  response.write(html);

}
