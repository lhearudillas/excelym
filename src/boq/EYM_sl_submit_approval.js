/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       03 Sep 2017     denny
 *
 */

function approveBoq(request, response){
	var projectId = request.getParameter('projectid');
	nlapiSubmitField('job',projectId,['custentity_eym_boq_status','custentity_boq_approver'],[1,request.getParameter('approver')]);
	nlapiLogExecution('Debug','Project ID',projectId);
	nlapiSetRedirectURL('RECORD','job',projectId);
	//return '1';
}
