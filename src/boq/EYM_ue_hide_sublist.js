/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       03 Sep 2017     denny
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request){
	//nlapiLogExecution('DEBUG','getExecutionContext',nlapiGetContext().getExecutionContext());
	if(nlapiGetContext().getExecutionContext()!='suitelet'){
		var frm = form.getSubList('recmachcustrecord_eym_boq_projid');
		if(frm){
			frm.setDisplayType('hidden');
		}
		
		nlapiLogExecution('DEBUG','Type',type);
		nlapiLogExecution('Debug','nlapiGetUser',nlapiGetUser());
		nlapiLogExecution('DEBUG','getExecutionContext',nlapiGetContext().getExecutionContext());
	}
}
