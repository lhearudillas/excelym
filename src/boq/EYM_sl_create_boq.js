/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       05 Sep 2017     denny
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function create(request, response){
	nlapiLogExecution('DEBUG', 'Start', 'Starting..');
	var projectId = request.getParameter('projectid');
	var pcoId = request.getParameter('pcoid');
	var boqFileName = 'boq_template_'+projectId;
	if(request.getParameter('f')=="freeform"){
		boqFileName = 'boq_freeform_template_'+projectId;
	}else if(request.getParameter('f')=="pco"){
		boqFileName = 'change_order_template_'+projectId+'_'+pcoId;
	}
	
	
	nlapiLogExecution('DEBUG', 'boqFileName', boqFileName);
	var filters = [ new nlobjSearchFilter('name',null,'is',boqFileName)];
	var fileSearch = nlapiSearchRecord('file',null,filters);
	var subsystemJson = null;
	var json = null;
	if(fileSearch){
		tempFile = nlapiLoadFile(fileSearch[0].getId());
	    json = JSON.parse(tempFile.getValue());
	    if(request.getParameter('f')=="pco"){
	    		subsystemJson = json.pcoData;
	    }else{
	    		subsystemJson = json.subsystemData;
	    }	
	}
	nlapiLogExecution('DEBUG', 'subsystemJson', subsystemJson);
	try{
		var projectObj = nlapiLoadRecord('job',projectId);
		projectObj.setFieldValue("entitystatus",2);
		projectObj.setFieldValue("custentity_eym_boq_status",2);
		
		var classFilter = [
			new nlobjSearchFilter('custrecord_eym_use_in_psc',null,'is','T')
		];
		var classColumn = [
			new nlobjSearchColumn('name'),
			new nlobjSearchColumn('custrecord_eym_psc_tempsequence')
		]
		classColumn[1].setSort();
		var classes = nlapiSearchRecord('classification',null,classFilter,classColumn);
		var boqObj = {};
		var arrBoqId = [];
		if(request.getParameter("f")=="freeform"){
			var no = 1;
			for(var i=0;i<subsystemJson.length;i++){
		      var subsystem = subsystemJson[i];
		      nlapiLogExecution('DEBUG', 'subsystem.activities.length', subsystem.activities.length);
		      	for(var j=0;j<subsystem.activities.length;j++){
			      var activity = subsystem.activities[j];
			      var prefix = subsystem.id+"-"+activity.id;
			      nlapiLogExecution('DEBUG', 'classes.length', classes.length);
			      for(var c=0;c<classes.length;c++){
			        var boqClass = activity.classes[c];
			    	  	var amt = (boqClass.amount * 1);
			    	  	nlapiLogExecution('DEBUG', 'amt', amt);
			    	  	if(amt>0){
			    	  		projectObj.setLineItemValue('recmachcustrecord_eym_boq_projid', 'custrecord_eym_boq_amount', no, amt);
				    	  	projectObj.setLineItemValue('recmachcustrecord_eym_boq_projid', 'custrecord_eym_boq_totadj', no, 0);
			    	  		projectObj.setLineItemValue('recmachcustrecord_eym_boq_projid', 'custrecord_eym_boq_base_cost', no, amt);
						projectObj.setLineItemValue('recmachcustrecord_eym_boq_projid', 'custrecord_eym_boq_class', no, classes[c].getId());
						projectObj.setLineItemValue('recmachcustrecord_eym_boq_projid', 'custrecord_eym_proj_subsystem', no, subsystem.name);
						projectObj.setLineItemValue('recmachcustrecord_eym_boq_projid', 'custrecord_eym_boq_categories', no, activity.name);
						projectObj.setLineItemValue('recmachcustrecord_eym_boq_projid', 'custrecord_eym_act_desc', no, activity.description);
						projectObj.setLineItemValue('recmachcustrecord_eym_boq_projid', 'custrecord_eym_boq_original', no, 'T');
						no++;
						var actClass = classes[c].getValue("name").toLowerCase();
						if(!(actClass in boqObj)){
				    			boqObj[actClass] = [];
				    		}
				    		boqObj[actClass].push(amt);
			    	  	}
			      }
			    }
		    }
		}else if(request.getParameter("f")=="pco"){
			var filters = new Array();
			filters[0] = new nlobjSearchFilter('custrecord_eym_boq_projid', null, 'is', projectId);
			filters[1] = new nlobjSearchFilter('custrecord_eym_changeorder_no', null, 'noneof', '@NONE@');
			
			var columns = new Array();
	 		columns[0] = new nlobjSearchColumn('custrecord_eym_boq_base_cost');
	 		columns[1] = new nlobjSearchColumn('custrecord_eym_boq_class');
	
			var boqList = nlapiSearchRecord('customrecord_eym_boq',null,filters,columns);
			if(boqList){
				for(var i=0;i<boqList.length;i++){
					var actClass = boqList[i].getText("custrecord_eym_boq_class").toLowerCase();
					if(!(actClass in boqObj)){
			    			boqObj[actClass] = [];
			    		}
			    		boqObj[actClass].push(boqList[i].getValue("custrecord_eym_boq_base_cost"));
				}
			}
			for(var i=0;i<subsystemJson.length;i++){
				var subsystem = subsystemJson[i];
		    	  	var amt = (subsystem.amount * 1);
		    	  	nlapiLogExecution('DEBUG', 'amt', amt);
		    	  	nlapiLogExecution('DEBUG', 'pcoId', pcoId);
		    	  	projectObj.selectNewLineItem('recmachcustrecord_eym_boq_projid');
		    	  	var className = nlapiLookupField('classification',subsystem.sClass,'name');
		    	  	nlapiLogExecution('DEBUG','className',className);
		    	  	className = className.toLowerCase();
		    	  	
		    	  	projectObj.setCurrentLineItemValue('recmachcustrecord_eym_boq_projid', 'custrecord_eym_boq_amount', amt);
		    	  	projectObj.setCurrentLineItemValue('recmachcustrecord_eym_boq_projid', 'custrecord_eym_boq_totadj', 0);
		    	  	projectObj.setCurrentLineItemValue('recmachcustrecord_eym_boq_projid', 'custrecord_eym_boq_base_cost', amt);
				projectObj.setCurrentLineItemValue('recmachcustrecord_eym_boq_projid', 'custrecord_eym_boq_class', subsystem.sClass);
				projectObj.setCurrentLineItemValue('recmachcustrecord_eym_boq_projid', 'custrecord_eym_proj_subsystem', subsystem.name);
				projectObj.setCurrentLineItemValue('recmachcustrecord_eym_boq_projid', 'custrecord_eym_boq_categories', subsystem.activity);
				projectObj.setCurrentLineItemValue('recmachcustrecord_eym_boq_projid', 'custrecord_eym_act_desc', subsystem.description);
				projectObj.setCurrentLineItemValue('recmachcustrecord_eym_boq_projid', 'custrecord_eym_boq_original', 'F');
				projectObj.setCurrentLineItemValue('recmachcustrecord_eym_boq_projid', 'custrecord_eym_adjto_itemid', subsystem.boqId);
				projectObj.setCurrentLineItemValue('recmachcustrecord_eym_boq_projid', 'custrecord_eym_changeorder_no', pcoId);
				projectObj.commitLineItem('recmachcustrecord_eym_boq_projid');
				if(!(className in boqObj)){
		    			boqObj[className] = [];
		    		}
		    		boqObj[className].push(amt);
		    		if(subsystem.boqId){
		    			if(arrBoqId.indexOf(subsystem.boqId)===-1){
			    			arrBoqId.push(subsystem.boqId);
			    		}
		    		}
		    }
		}else{
			var columns = [
		    new nlobjSearchColumn('name')
		    ];
			var subsystemList = nlapiSearchRecord('customlist_eym_proj_subsystem',null,null,columns);

			var no = 1;
			for(var i=0;i<subsystemList.length;i++){
		      var subSystemId = subsystemList[i].getId();
		      var filters = [
		        new nlobjSearchFilter('custrecord_eym_projsub_main',null,'is',subSystemId)
		      ];
		      var columns = [
		        new nlobjSearchColumn('custrecord_eym_projsub_main'),
		        new nlobjSearchColumn('name'),
		        new nlobjSearchColumn('custrecord_boq_cat_descr')
		      ];
		      var activities = nlapiSearchRecord('customrecord_eym_boq_activity',null,filters,columns);

		      for(var j=0;j<activities.length;j++){
		        var total = 0;
		        var activity = activities[j];
		        var suffix = "-"+subSystemId+"-"+activity.getId();
		        for(var c=0;c<classes.length;c++){
					if(subsystemJson["class-"+classes[c].getId()+suffix]>0){
						projectObj.setLineItemValue('recmachcustrecord_eym_boq_projid', 'custrecord_eym_boq_amount', no, subsystemJson["class-"+classes[c].getId()+suffix]);
			    	  		projectObj.setLineItemValue('recmachcustrecord_eym_boq_projid', 'custrecord_eym_boq_totadj', no, 0);
						projectObj.setLineItemValue('recmachcustrecord_eym_boq_projid', 'custrecord_eym_boq_base_cost', no, subsystemJson["class-"+classes[c].getId()+suffix]);
						projectObj.setLineItemValue('recmachcustrecord_eym_boq_projid', 'custrecord_eym_boq_class', no, classes[c].getId());
						projectObj.setLineItemValue('recmachcustrecord_eym_boq_projid', 'custrecord_eym_proj_subsystem', no, subsystemList[i].getValue('name'));
						projectObj.setLineItemValue('recmachcustrecord_eym_boq_projid', 'custrecord_eym_boq_categories', no, activity.getValue('name'));
						projectObj.setLineItemValue('recmachcustrecord_eym_boq_projid', 'custrecord_eym_act_desc', no, activity.getValue('custrecord_boq_cat_descr'));
						no++;
						var actClass = classes[c].getValue("name").toLowerCase();
						if(!(actClass in boqObj)){
				    			boqObj[actClass] = [];
				    		}
				    		boqObj[actClass].push(subsystemJson["class-"+classes[c].getId()+suffix]);
					}
		        }
		      }
			}
		}

		var grandTotal = 0;
		for(var key in boqObj){
			var fieldName = '';
			switch(key){
				case 'material' : fieldName = request.getParameter("f")=="pco" ? "custentity_eym_pcototalmat": "custentity_eym_materialbaseline"; break;
				case 'labor admin' : fieldName = request.getParameter("f")=="pco" ? "custentity_eym_pcototallab" : "custentity_eym_laborbaseline"; break;
				case 'subcon' : fieldName = request.getParameter("f")=="pco" ? "custentity_eym_pcototalsub" : "custentity_eym_subconbaseline"; break;
				case 'equipment' : fieldName = request.getParameter("f")=="pco" ? "custentity_eym_pcototalequip" : "custentity_eym_equipmentbaseline"; break;
				case 'material with labor' : fieldName = request.getParameter("f")=="pco" ? "custentity_eym_pcototalmatlab" : "custentity_eym_matlaborbaseline"; break;
				case 'general requirements' : fieldName = request.getParameter("f")=="pco" ? "custentity_eym_pcototalgenreq" : "custentity_eym_genreqbaseline"; break;
			}
			
			var totalClass = 0;
			for (var ss = 0; ss < boqObj[key].length; ss++ ){
				nlapiLogExecution('Debug','boqObj[key][ss]',boqObj[key][ss]);
				totalClass += boqObj[key][ss]*1;
			}
			if(fieldName!=''){
				nlapiLogExecution('Debug','fieldName',fieldName+'--'+totalClass);
				projectObj.setFieldValue(fieldName,totalClass);
				//nlapiLogExecution('Debug','fieldName',fieldName+'--'+totalClass);
			}
			grandTotal += totalClass; 
		}
		var totalField = request.getParameter("f")=="pco" ? "custentity_eym_pcototal" : "custentity_eym_totalbaseline";
		projectObj.setFieldValue(totalField,grandTotal);
		
		
		var internalId = nlapiSubmitRecord(projectObj);
		if(internalId && request.getParameter("f")=="pco"){
			for(var x=0;x<arrBoqId.length;x++){
				var filters = new Array();
				filters[0] = new nlobjSearchFilter('custrecord_eym_adjto_itemid', null, 'is', arrBoqId[x]);
				
				var columns = new Array();
		 		columns[0] = new nlobjSearchColumn('custrecord_eym_boq_base_cost');
		
				var boqList = nlapiSearchRecord('customrecord_eym_boq',null,filters,columns);
				var totalAdj = 0;
				if(boqList){
					for(var i=0;i<boqList.length;i++){
						totalAdj += boqList[i].getValue("custrecord_eym_boq_base_cost")*1;
					}
				}
				var boqOriEst = nlapiLookupField('customrecord_eym_boq',arrBoqId[x],'custrecord_eym_boq_base_cost');
		    	  	var adjEst = (boqOriEst * 1) + totalAdj;
		    	  	nlapiSubmitField('customrecord_eym_boq',arrBoqId[x],['custrecord_eym_boq_totadj','custrecord_eym_boq_amount'],[totalAdj,adjEst]);
		    }
			
			nlapiSubmitField('customrecord_eym_change_order',pcoId,['custrecord_eym_pco_status'],[3]);
			nlapiLogExecution('DEBUG', 'Result..', 'PCO Updated =' + pcoId);
		}
		nlapiLogExecution('DEBUG', 'Result..', 'Project Updated =' + internalId);
		nlapiLogExecution('DEBUG', 'End', 'End ...');

	}catch(e){
		if(e instanceof nlobjError)
			errMsg = e.getCode()+': '+e.getDetails();
        else
        		errMsg = 'UNEXPECTED ERROR: '+e.toString();
		nlapiLogExecution('DEBUG','errMsg',errMsg);
	}
	nlapiSetRedirectURL('RECORD','job',projectId);
}
