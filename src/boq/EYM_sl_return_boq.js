/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       03 Sep 2017     denny
 *
 */

function returnBoq(request, response){
	var projectId = request.getParameter('projectid');
	if(request.getParameter('f')=='pco'){
		var pcoId = request.getParameter('pcoid');
		nlapiSubmitField('customrecord_eym_change_order',pcoId,['custrecord_eym_pco_status','custrecord_eym_poc_return_reason'],[4,request.getParameter('returnMsg')]);
	}else{
		nlapiSubmitField('job',projectId,['custentity_eym_boq_status','custentity_eym_return_reason'],[3,request.getParameter('returnMsg')]);
	}
	//nlapiSubmitField('job',projectId,['custentity_eym_boq_status','custentity_eym_return_reason'],[3,request.getParameter('returnMsg')]);
	nlapiLogExecution('Debug','Project ID',projectId);
	nlapiSetRedirectURL('RECORD','job',projectId);
	//return '1';
}
