define(['N/runtime'],

function(runtime) {
	// Check current environment
	function isProductionEnvironment()
	{
		if (runtime.envType == 'PRODUCTION') {	
			return true;
		}
		return false;
	}

    return {
    	isProductionEnvironment: isProductionEnvironment
    };
});