define(['N/search'],

function( search ) {

	/*
	 * options.type: The record type.
	 * options.columns: The desired return columns.
	 */
	function getPreferences( options )
	{
		// Initialize return variables.
		var error 		= false;
		error_object	= null;
		value			= null;

		// Create the search
		try{
			// Define a default columns array of objects.
			var columns = [{name:'custrecord_eym_pref_value'},{name:'name'}];

			// Search the 
			value = search.create({
				type	: options.type 		? options.type 		: 'customrecord_eym_preferences',
				columns	: options.columns 	? options.columns	: columns
			}).run().getRange({start:0, end: 1000});
		}
		catch(e){

			// Assign a true value to the error.
	    	error = true;

	    	// Pass the error object to the return object.
	    	errorObject = e;
		}

		// Return an object.
		return {
			error: error,
			value: value,
			error_object: error_object
		};
	}

	/*
	 * options.preferences: An array of search results.
	 * options.fieldId: The search criteria
	 * options.returnField: The field name where value is required.
	 */

	function getPreference( options )
	{
		// Assign to a shorter variable.
		var prefs = options.preferences;

		// Assign default values.
		var fieldId 		= options.fieldId 		? options.fieldId 		: 'name';
		var returnFieldId 	= options.returnFieldId ? options.returnFieldId : 'custrecord_eym_pref_value';

		if( prefs ) {

			// Loop thru all the preferences.
			for( var i = 0; i < prefs.length; i++ ) {

				// Get the value of the name.
				var name = prefs[i].getValue( {name: fieldId} );

				// Check if the name is matching.
				if( name == options.name) {

					// Return the value.
					var values =  prefs[i].getValue( {name:returnFieldId} ).split(',');

					// If it is comma separated, return an array.
					return ( values.length > 1 ) ? values: values[0];
				}
			}
		}

		return null;
	}

    return {
    	getPreference: getPreference,
    	getPreferences: getPreferences
    };

});
