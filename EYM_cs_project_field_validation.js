/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       13 Apr 2018     EYM-013
 *
 */


function clientPageInit(type)
{
		
	var projStatusVal = nlapiGetFieldValue('entitystatus');
	
	//Project Status Validation
	if(type == 'create' || type == 'edit' || type == 'view') {
		var setProjStatVal = nlapiSetFieldValue('custentity_eym_projstat_value', projStatusVal);
	}
	
	//Bid Date Validation	
	if(type == 'create' || type == 'edit') {
		if(projStatusVal == 4) {  //Bidding
		   nlapiSetFieldMandatory('custentity_eym_proj_biddate', true);
		}
	   
		if(projStatusVal == 5) {  //Awarded
		   nlapiSetFieldMandatory('custentity_eym_proj_awrddate', true);
		}
	}
	   
}

function projStatValidateField(type, name, linenum)
{
	if(name === 'entitystatus'){
		
		var projStatus = nlapiGetFieldValue('entitystatus');
		var projStatusValue = nlapiGetFieldValue('custentity_eym_projstat_value');
		
		if(projStatusValue == '' || projStatusValue == null) {		//Empty upon creation
			if(projStatus == 2){
				alert('The project status cannot be set to "In Progress" because this project does not have any approved BOQ.');
				return false;
			}
		}
		
		if(projStatusValue == 3 || projStatusValue == 4 || projStatusValue == 5) {		//Lost=3, Bidding=4, Awarded=5
			if(projStatus == 2) {
				alert('The project status cannot be set to "In Progress" because this project does not have any approved BOQ.');
				return false;
			}
		}
		
		if(projStatusValue == 17) {		//Under Warranty
			if(projStatus == 2) {
				alert('The Project Status cannot be reverted back to "In Progress"');
				return false;
			}
			else if(projStatus == 5 || projStatus == 4 || projStatus == 3 || projStatus == 2) {
				alert('Current status cannot be changed.');
				return false;
			}
		}
		
		if(projStatusValue == 1) {		//Closed
			if(projStatus == 2){
				alert('The Project Status cannot be reverted back to "In Progress"');
				return false;
			}
			else if(projStatus == 5 || projStatus == 4 || projStatus == 3 || projStatus == 2 || projStatus == 17) {
				alert('Current status cannot be changed.');
				return false;
			}
		}
		
		if(projStatusValue == 2) {		//In Progress
			if(projStatus == 5 || projStatus == 4 || projStatus == 3) {
				alert('Current status cannot be changed.');
				return false;
			}
		}

	}
    return true;
}

//function bidDateOnLoad(type)
//{
//   if(type == 'create' || type == 'edit') {
//	   
//	   var projStatusOnLoad = nlapiGetFieldValue('entitystatus');
//	   
//	   if(projStatusOnLoad == 4) {
//		   nlapiSetFieldMandatory('custentity_eym_proj_biddate', true);
//	   }
//	   
//	   if(projStatusOnLoad == 5) {
//		   nlapiSetFieldMandatory('custentity_eym_proj_awrddate', true);
//	   }
//	   
//   }
//}

function bidDateOnSave()
{
	var projStatusOnSave = nlapiGetFieldValue('entitystatus');
	var bidDateValue = nlapiGetFieldValue('custentity_eym_proj_biddate');
	var bidAwardDateValue = nlapiGetFieldValue('custentity_eym_proj_awrddate');

	var bidDateValue2 = nlapiStringToDate(bidDateValue);
	var bidAwardDateValue2 = nlapiStringToDate(bidAwardDateValue);

	if(projStatusOnSave == 4 && IsNullOrEmpty(bidDateValue)) {
		alert('The field Bid Date cannot be empty. Please enter a value to proceed.');
		return false;
	}
	
	if(projStatusOnSave == 5 && IsNullOrEmpty(bidAwardDateValue)) {
		alert('The field Bid Award Date cannot be empty. Please enter a value to proceed.');
		return false;
	}
	
	if(!IsNullOrEmpty(bidDateValue) && !IsNullOrEmpty(bidAwardDateValue) &&  bidDateValue2 > bidAwardDateValue2) {
		alert('Bid Award Date could not be earlier than Bid Date.');
		return false;
	}
    return true;
}
