/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(['N/record'],

function(record) {
    
    /**
     * Function to be executed after page is initialized.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.mode - The mode in which the record is being accessed (create, copy, or edit)
     *
     * @since 2015.2
     */
    function pageInit(scriptContext) {
    	
    	var REC;
    	var ITEM_FIELD_LIST = ["quantity","grossamt","amount","rate"];
    	//var CUSTOM_FORM = 101;
    	
    	REC = scriptContext.currentRecord;
    	var customForm = REC.getValue({fieldId: 'customform'});
    	if(customForm == 101 || customForm == 127){
    		
    		for (var f in ITEM_FIELD_LIST) {
        		
        		nsDisableField(scriptContext, "item", ITEM_FIELD_LIST[f]);
        	}
    		
    	}

    }
    
    function fieldChanged(scriptContext) {
    	
    	var currentRecord = scriptContext.currentRecord;
    	var sublistName = scriptContext.sublistId;
    	var sublistFieldName = scriptContext.fieldId;
    	var customForm = currentRecord.getValue({fieldId: 'customform'});
    	if(customForm == 101 || customForm == 127){
    		
    		var basePrice = Number(currentRecord.getCurrentSublistValue({ sublistId: 'item', fieldId: 'custcol_eym_pur_item_base_unit'}));
    		var baseAmount = Number(currentRecord.getCurrentSublistValue({ sublistId: 'item', fieldId: 'custcol_eym_pur_item_base_amount'}));
    		var currentRate = Number(currentRecord.getCurrentSublistValue({ sublistId: 'item', fieldId: 'rate'}));
    		var currentAmount = Number(currentRecord.getCurrentSublistValue({ sublistId: 'item', fieldId: 'amount'})).toFixed(2);
    		var quantity = Number(currentRecord.getCurrentSublistValue({ sublistId: 'item', fieldId: 'quantity'}));
    		//var taxAmnt = Number(currentRecord.getCurrentSublistValue({ sublistId: 'item', fieldId: 'tax1amt'}));
    		var discount = Number(currentRecord.getCurrentSublistValue({ sublistId: 'item', fieldId: 'custcol_eym_pur_itemdisc_amount'}));
    		var taxRate = Number(currentRecord.getCurrentSublistValue({ sublistId: 'item', fieldId: 'taxrate1'}) / 100);
    		
    		if(customForm == 127){
    			
    			if(sublistName == 'item' && (sublistFieldName == 'amount' || sublistFieldName == 'rate')){
    				if(basePrice > 0){
    				
    					discount = Number(currentRecord.getCurrentSublistValue({ sublistId: 'item', fieldId: 'custcol_eym_po_forex_discount'}));
		    			var poExRate = Number(currentRecord.getValue({fieldId: 'custbody_eym_po_exrate'}));
		    			var discountPerUnit = Number(discount / quantity).toFixed(2);
		    			
		    			var oldRate = Number( (basePrice - discountPerUnit ) *  poExRate );
		    			var oldAmount = Number(oldRate * quantity).toFixed(2);
		    			
		    			if(oldAmount != currentAmount){
		    				alert('You are not allowed to edit this field.');
	        		    	currentRecord.cancelLine({
	        		    	    sublistId: 'item'
	        		    	});
		    			}
	    			
    				}
    				
    			}
    			
    		}else{
    			
    			if(sublistName == 'item' && (sublistFieldName == 'amount' || sublistFieldName == 'rate')){
    				if(basePrice > 0){
    					
	    				var taxValue = Number( ( baseAmount - discount ) / 1+ (taxRate) );
	    	    		var taxAmnt = Number(taxValue * taxRate);
	    	    			
	    	    		var taxAmntPerUnit = Number(taxAmnt / quantity).toFixed(2);
	    	    		var discountPerUnit = Number(discount / quantity).toFixed(2);
	    	    		var toSum = Number(taxAmntPerUnit) + Number(discountPerUnit);
	    	    			
	    	    		var oldRate = Number(basePrice- toSum );
	    	    		var oldAmount = Number(oldRate * quantity).toFixed(2);
	    	    		
	    	    		if(oldAmount != currentAmount){
	    	    				
	    	    		alert('You are not allowed to edit this field.');
		    		    	currentRecord.cancelLine({
		    		    	    sublistId: 'item'
		    		    	});
	    	    				
	    	    		}
    	    		
    				}
    				
    			}
    			
    		}
    		
    		/*if(sublistName == 'item' && (sublistFieldName == 'amount' || sublistFieldName == 'rate')){
    			alert(customForm);
    	    	if(basePrice > 0){
    	    		
    	    		if(customForm == 127){
    	    				
    	    			discount = Number(currentRecord.getCurrentSublistValue({ sublistId: 'item', fieldId: 'custcol_eym_po_forex_discount'}));
    	    			var poExRate = Number(currentRecord.getValue({fieldId: 'custbody_eym_po_exrate'}));
    	    			var discountPerUnit = Number(discount / quantity).toFixed(2);
    	    			
    	    			var oldRate = Number( (basePrice - discount ) *  poExRate );
    	    			var oldAmount = Number(oldRate * quantity).toFixed(2);
    	    			alert(oldAmount +'-'+ currentAmount);
    	    			if(oldAmount != currentAmount){
    	    				alert('You are not allowed to edit this field.');
            		    	//currentRecord.cancelLine({
            		    	//    sublistId: 'item'
            		    	//});
    	    			}
    	    				
    	    		}else{
    	    				
    	    			var taxValue = Number( ( baseAmount - discount ) / 1+ (taxRate) );
        	    		var taxAmnt = Number(taxValue * taxRate);
        	    			
        	    		var taxAmntPerUnit = Number(taxAmnt / quantity).toFixed(2);
        	    		var discountPerUnit = Number(discount / quantity).toFixed(2);
        	    		var toSum = Number(taxAmntPerUnit) + Number(discountPerUnit);
        	    			
        	    		var oldRate = Number(basePrice- toSum );
        	    		var oldAmount = Number(oldRate * quantity).toFixed(2);
        	    		
        	    		if(oldAmount != currentAmount){
        	    				
        	    		alert('You are not allowed to edit this field.');
        		    	currentRecord.cancelLine({
        		    	    sublistId: 'item'
        		    	});
        	    				
        	    		}
    	    				
    	    		}
    	    	}
    	    		
    	    }*/
    	
    	}
    	
    }
    
    function validateField(scriptContext) {
    	
    	var currentRecord = scriptContext.currentRecord;
    	var sublistName = scriptContext.sublistId;
    	var sublistFieldName = scriptContext.fieldId;
    
    	var customForm = currentRecord.getValue({fieldId: 'customform'});
    	
    	if(customForm == 101){
    	
	    	if(sublistName == 'item' && (sublistFieldName == 'amount' || sublistFieldName == 'rate')){
	    		var currentRate = Number(currentRecord.getCurrentSublistValue({ sublistId: 'item', fieldId: 'rate'}));
	    		var currentAmount = Number(currentRecord.getCurrentSublistValue({ sublistId: 'item', fieldId: 'amount'}));
	    		var basePrice = Number(currentRecord.getCurrentSublistValue({ sublistId: 'item', fieldId: 'custcol_eym_pur_item_base_unit'}));
	    		if(basePrice > 0){
	    			alert('You are not allowed to edit this field.'+currentRate);
	    			//currentRecord.setCurrentSublistValue({sublistId:'item', fieldId:'rate', value:currentRate, ignoreFieldChange:true});
	    			//currentRecord.setCurrentSublistValue({sublistId:'item', fieldId:'amount', value:currentAmount, ignoreFieldChange:true});
	    			return false;
	    		}
	    	}
    	
    	}
    	return true;
    }

    function nsDisableField(context,formId,fieldId,lineNbr){
		try {
			
			var fld;
			
			//using DOM referencing and NetSuite supplied functions, find the field and disable it
			if (formId) {
				fld = getFormElement(document.forms[formId+"_form"],getFieldName(fieldId));
				if (fld == null)
					fld = getFormElement( document.forms[formId+'_form'], getFieldName(fieldId)+lineNbr);
			}
			else
				fld = getFormElement(document.forms["main_form"],getFieldName(fieldId));
			
			if (isSelect(fld)){
				disableSelect(fld,true);				
			} else {
			    disableField(fld,true);
			}

			fld = REC.getField(fieldId);
			if (fld) {
				fld.isDisabled = true;
			}
		} catch (e) {
			;
		}
		
	}
    
    return {
        pageInit: pageInit,
        //validateField: validateField
        fieldChanged: fieldChanged,
        
    };
    
});
