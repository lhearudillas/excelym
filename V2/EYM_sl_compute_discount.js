/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/ui/serverWidget', 'N/http', 'N/search', './lib/EYM_psc_lib.js'],

function(record, serverWidget, http, search, pscLib) {
   
    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context) {
    	
    	if (context.request.method === 'GET') {
    		doGet(context,record);
    	}else{
    		doPost(context,record);
    	}

    }
    
    function doGet(context,record){
    	
    	var grossAmount = context.request.parameters['gross'];
    	
    	
    	var epRess = pscLib.getPreferences();
    	var mainCss = pscLib.getPreference(epRess, 'main-css');
    	var jqueryModalJs = 'https://system.netsuite.com/core/media/media.nl?id=536&c=4317943_SB1&h=d715f48c82e22e51d3a5&_xt=.js';
    	var boqFolder = '108';
    	  /*if(nlapiGetContext().getEnvironment()=='PRODUCTION'){
    		  jqueryModalJs = 'https://system.na3.netsuite.com/core/media/media.nl?id=536&c=4317943&h=c1a0fa134372b1c1ccd9&_xt=.js';
    		  boqFolder = '108';
    	  }*/	
    	
    	var html = serverWidget.createForm({ title : 'Discounts' });
    	//html.clientScriptFileId = 2228;
    	
    	html += '<html>\n';
    	html += '<head>\n';
    	html += '<link rel="stylesheet" href="'+mainCss+'" type="text/css" media="screen" />\n';
    	html += '<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" type="text/css" media="screen" />\n';
    	html += '<title>PSC BOQ Template</title>\n';
    	html += '</head>\n';
    	
    	html += '<body>\n';
    	html += '<form name="frmBoq" id="frmBoq" method="POST" action="">\n';
    	html += '<header class="actions stick-to-top">\n';
    	html += '	<div class="card">\n';
    		 html += '		<div class="card-body"><div class="row"><div class="col">\n';
    		 html += '    <input type="submit" class="btn btn-primary" value="Save" name="btnSave" id="btnSave">\n';
    	html += '<main class="main-content">\n';
    	html += ' <div class="container-fluid">\n';
    	html += '	 <div class="row">\n';
    	html += '		<div class="col">\n';
    	html += '		<div class="card mb-3">\n';
    	html += '		<div class="card-header"><h3 class="float-left mt-2 mb-1">Discounts</h3></div>\n';
    	html += '		<div class="card-body"><div class="row">\n';
    	html += '           <input type="hidden" name="custpage_gross_amnt_ns" id="custpage_gross_amnt_ns" value="' + grossAmount + '">';
    	html += '           <input type="hidden" name="custpage_gross_amnt" id="custpage_gross_amnt" value="' + grossAmount + '">';
    	html += '			<div class="col-md-8">\n';
    	html += '				<dl class="row">\n';
    	html += '					<dt class="col-md-4">Discount 1 (%) </dt>\n';
    	html += '					<dt class="col-md-4"><input type="number" name="custpage_disc1_percent" id="custpage_disc1_percent"></dt>\n';
    	html += '					<dt class="col-md-4"><input type="number" name="custpage_disc1_total" id="custpage_disc1_total"></dt>\n';
    	html += '				</dl>\n';
    	html += '			</div>\n';
    	html += '			<div class="col-md-8">\n';
    	html += '				<dl class="row">\n';
    	html += '					<dt class="col-md-4">Discount 2 (%) </dt>\n';
    	html += '					<dt class="col-md-4"><input type="number" name="custpage_disc2_percent" id="custpage_disc2_percent"></dt>\n';
    	html += '					<dt class="col-md-4"><input type="number" name="custpage_disc2_total" id="custpage_disc2_total"></dt>\n';
    	html += '				</dl>\n';
    	html += '			</div>\n';
    	html += '			<div class="col-md-8">\n';
    	html += '				<dl class="row">\n';
    	html += '					<dt class="col-md-4">Discount 3 (%) </dt>\n';
    	html += '					<dt class="col-md-4"><input type="number" name="custpage_disc3_percent" id="custpage_disc3_percent"></dt>\n';
    	html += '					<dt class="col-md-4"><input type="number" name="custpage_disc3_total" id="custpage_disc3_total"></dt>\n';
    	html += '				</dl>\n';
    	html += '			</div>\n';
    	html += '			<div class="col-md-8">\n';
    	html += '				<dl class="row">\n';
    	html += '					<dt class="col-md-4">Discount 4 (%) </dt>\n';
    	html += '					<dt class="col-md-4"><input type="number" name="custpage_disc4_percent" id="custpage_disc4_percent"></dt>\n';
    	html += '					<dt class="col-md-4"><input type="number" name="custpage_disc4_total" id="custpage_disc4_total"></dt>\n';
    	html += '				</dl>\n';
    	html += '			</div>\n';
    	html += '			<div class="col-md-8">\n';
    	html += '				<dl class="row">\n';
    	html += '					<dt class="col-md-4">Discount 5 (%) </dt>\n';
    	html += '					<dt class="col-md-4"><input type="number" name="custpage_disc5_percent" id="custpage_disc5_percent"></dt>\n';
    	html += '					<dt class="col-md-4"><input type="number" name="custpage_disc5_total" id="custpage_disc5_total"></dt>\n';
    	html += '				</dl>\n';
    	html += '			</div>\n';
    	html += '			<div class="col-md-8">\n';
    	html += '				<dl class="row">\n';
    	html += '					<dt class="col-md-4">Discount Amount </dt>\n';
    	html += '					<dt class="col-md-4"></dt>\n';
    	html += '					<dt class="col-md-4"><input type="number" name="custpage_disc_amount" id="custpage_disc_amount"></dt>\n';
    	html += '				</dl>\n';
    	html += '			</div>\n';
    	html += '			<div class="col-md-8">\n';
    	html += '				<hr class="col-md-12" style="background:black">\n';	
    	html += '			</div>\n';
    	html += '			<div class="col-md-8">\n';
    	html += '				<dl class="row">\n';
    	html += '					<dt class="col-md-4">Total Discount </dt>\n';
    	html += '					<dt class="col-md-4"></dt>\n';
    	html += '					<dt class="col-md-4"><input type="number" name="custpage_total_discount" id="custpage_total_discount"></dt>\n';
    	html += '				</dl>\n';
    	html += '			</div>\n';
    	html += '		</div></div>\n';
    	
    	html += '		</div>\n';
    	html += '		</div>\n';
    	html += '	 </div>\n';
    	
    	html += '</form>\n';
    	
    	html += '<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>\n';
    	html += '<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>\n';
    	html += '<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>\n';
    	html += '<script type="text/javascript" src="'+jqueryModalJs+'"></script>\n';  
    	html += '<script type="text/javascript">\n';
    	
    	//Discount1 Percentage
    	html += '   $(document).on("change","#custpage_disc1_percent", function(){\n'; 
    	html += '  var disc1Percent = Number( $("#custpage_disc1_percent").val() / 100 );';
    	html += '	var grossAmount = Number( $("#custpage_gross_amnt").val() );';
    	html += ' var disc1Amnt = Number(grossAmount * disc1Percent).toFixed(2);';
    	html += ' var newGrossAmnt = Number(grossAmount - disc1Amnt);';
    	html += ' $("#custpage_disc1_total").val(disc1Amnt); ';
    	html += ' $("#custpage_gross_amnt").val(newGrossAmnt); ';
    	html += ' $("#custpage_total_discount").val(disc1Amnt); ';
    	html += 'alert(disc1Amnt)';
    	html += '   });\n';
    	
    	//Discount2 Percentage
    	html += '   $(document).on("change","#custpage_disc2_percent", function(){\n'; 
    	html += '  var disc2Percent = Number( $("#custpage_disc2_percent").val() / 100 );';
    	html += '	var grossAmount = Number( $("#custpage_gross_amnt").val() );';
    	html += '	var disc1Amnt = Number( $("#custpage_disc1_total").val() );';
    	html += ' var disc2Amnt = Number( (grossAmount) * disc2Percent).toFixed(2);';
    	html += ' var totalDiscount = Number( Number(disc1Amnt) + Number(disc2Amnt)).toFixed(2); ';
    	html += ' var newGrossAmnt = Number(grossAmount - disc2Amnt);';
    	html += ' $("#custpage_disc2_total").val(disc2Amnt); ';
    	html += ' $("#custpage_gross_amnt").val(newGrossAmnt); ';
    	html += ' $("#custpage_total_discount").val(totalDiscount); ';
    	html += 'alert(totalDiscount)';
    	html += '   });\n';
    	
    	//Discount3 Percentage
    	html += '   $(document).on("change","#custpage_disc3_percent", function(){\n'; 
    	html += '  var disc3Percent = Number( $("#custpage_disc3_percent").val() / 100 );';
    	html += '	var grossAmount = Number( $("#custpage_gross_amnt").val() );';
    	html += ' var disc3Amnt = Number( (grossAmount) * disc3Percent).toFixed(2);';
    	html += ' var newGrossAmnt = Number(grossAmount - disc3Amnt);';
    	html += '	var disc1Amnt = Number( $("#custpage_disc1_total").val() );';
    	html += '	var disc2Amnt = Number( $("#custpage_disc2_total").val() );';
    	html += ' var totalDiscount = Number (Number(disc1Amnt) + Number(disc2Amnt) + Number(disc3Amnt)).toFixed(2); ';
    	html += ' $("#custpage_disc3_total").val(disc3Amnt); ';
    	html += ' $("#custpage_gross_amnt").val(newGrossAmnt); ';
    	html += ' $("#custpage_total_discount").val(totalDiscount); ';
    	html += '   });\n';
    	
    	//Discount4 Percentage
    	html += '   $(document).on("change","#custpage_disc4_percent", function(){\n'; 
    	html += '  var disc4Percent = Number( $("#custpage_disc4_percent").val() / 100 );';
    	html += '	var grossAmount = Number( $("#custpage_gross_amnt").val() );';
    	html += ' var disc4Amnt = Number( (grossAmount) * disc4Percent).toFixed(2);';
    	html += ' var newGrossAmnt = Number(grossAmount - disc4Amnt);';
    	html += '	var disc1Amnt = Number( $("#custpage_disc1_total").val() );';
    	html += '	var disc2Amnt = Number( $("#custpage_disc2_total").val() );';
    	html += '	var disc3Amnt = Number( $("#custpage_disc3_total").val() );';
    	html += ' var totalDiscount = Number( Number(disc1Amnt) + Number(disc2Amnt) + Number(disc3Amnt) + Number(disc4Amnt)).toFixed(2); ';
    	html += ' $("#custpage_disc4_total").val(disc4Amnt); ';
    	html += ' $("#custpage_gross_amnt").val(newGrossAmnt); ';
    	html += ' $("#custpage_total_discount").val(totalDiscount); ';
    	html += '   });\n';
    	
    	//Discount5 Percentage
    	html += '   $(document).on("change","#custpage_disc5_percent", function(){\n'; 
    	html += '  var disc5Percent = Number( $("#custpage_disc5_percent").val() / 100 );';
    	html += '	var grossAmount = Number( $("#custpage_gross_amnt").val() );';
    	html += ' var disc5Amnt = Number( (grossAmount) * disc5Percent).toFixed(2);';
    	html += ' var newGrossAmnt = Number(grossAmount - disc5Amnt);';
    	html += '	var disc1Amnt = Number( $("#custpage_disc1_total").val() );';
    	html += '	var disc2Amnt = Number( $("#custpage_disc2_total").val() );';
    	html += '	var disc3Amnt = Number( $("#custpage_disc3_total").val() );';
    	html += '	var disc4Amnt = Number( $("#custpage_disc4_total").val() );';
    	html += ' var totalDiscount = Number (Number(disc1Amnt) + Number(disc2Amnt) + Number(disc3Amnt) + Number(disc4Amnt) + Number(disc5Amnt)).toFixed(2); ';
    	html += ' $("#custpage_disc5_total").val(disc5Amnt); ';
    	html += ' $("#custpage_gross_amnt").val(newGrossAmnt); ';
    	html += ' $("#custpage_total_discount").val(totalDiscount); ';
    	html += '   });\n';
    	
    	//Discount Amount
    	html += '   $(document).on("change","#custpage_disc_amount", function(){\n'; 
    	html += '  var discAmount = Number( $("#custpage_disc_amount").val() );';
    	html += '	var grossAmount = Number( $("#custpage_gross_amnt").val() );';
    	html += '	var disc1Amnt = Number( $("#custpage_disc1_total").val() );';
    	html += '	var disc2Amnt = Number( $("#custpage_disc2_total").val() );';
    	html += '	var disc3Amnt = Number( $("#custpage_disc3_total").val() );';
    	html += '	var disc4Amnt = Number( $("#custpage_disc4_total").val() );';
    	html += '	var disc5Amnt = Number( $("#custpage_disc5_total").val() );';
    	html += ' var totalDiscount = Number (Number(disc1Amnt) + Number(disc2Amnt) + Number(disc3Amnt) + Number(disc4Amnt) + Number(disc5Amnt) + Number(discAmount) ).toFixed(2); ';
    	html += ' $("#custpage_total_discount").val(totalDiscount); ';
    	html += '   });\n';
    	
    	
    	//html += '</body>\n';
    
    	html += '</script>\n';
    	html += '</html>\n';
    	
    	
    	context.response.write(html);
    }
    
    function doPost(context,record){
    	
    	var html = ''; 
    	
    	var nsGrossAmount = context.request.parameters['custpage_gross_amnt_ns'];
    	var newGrossAmount = context.request.parameters['custpage_gross_amnt'];
    	var disc1Percent = context.request.parameters['custpage_disc1_percent'];
    	var disc1Amount = context.request.parameters['custpage_disc1_total'];
    	var disc2Percent = context.request.parameters['custpage_disc2_percent'];
    	var disc2Amount = context.request.parameters['custpage_disc2_total'];
    	var disc3Percent = context.request.parameters['custpage_disc3_percent'];
    	var disc3Amount = context.request.parameters['custpage_disc3_total'];
    	var disc4Percent = context.request.parameters['custpage_disc4_percent'];
    	var disc4Amount = context.request.parameters['custpage_disc4_total'];
    	var disc5Percent = context.request.parameters['custpage_disc5_percent'];
    	var disc5Amount = context.request.parameters['custpage_disc5_total'];
    	var discountAmnt = context.request.parameters['custpage_disc_amount'];
    	var totalDiscount = context.request.parameters['custpage_total_discount'];
    	
    	var discount = {};
    	discount.ns_gross_amount = nsGrossAmount;
    	discount.gross_amount = newGrossAmount;
    	discount.percent1 = disc1Percent;
    	discount.amount1 = disc1Amount;
    	discount.percent2 = disc2Percent;
    	discount.amount2 = disc2Amount;
    	discount.percent3 = disc3Percent;
    	discount.amount3 = disc3Amount;
    	discount.percent4 = disc4Percent;
    	discount.amount4 = disc4Amount;
    	discount.percent5 = disc5Percent;
    	discount.amount5 = disc5Amount;
    	discount.discount_amount = discountAmnt;
    	discount.total_discount = totalDiscount;
    	
    	discount['gross_amnt_ns'] = nsGrossAmount;
    	discount['gross_amnt'] = nsGrossAmount;
    	discount['disc1_percent'] = disc1Percent;
    	discount['disc1_total'] = disc1Amount;
    	discount['disc2_percent'] = disc2Percent;
    	discount['disc2_total'] = disc2Amount;
    	discount['disc3_percent'] = disc3Percent;
    	discount['disc3_total'] = disc3Amount;
    	discount['disc4_percent'] = disc4Percent;
    	discount['disc4_total'] = disc4Amount;
    	discount['disc5_percent'] = disc5Percent;
    	discount['disc5_total'] = disc5Amount;
    	discount['disc_amount'] = discountAmnt;
    	discount['total_discount'] = totalDiscount;
    	var disc = JSON.stringify(discount);
    	
    	log.debug({
		    title: 'JSON', 
		    details: disc
		});
    	
    	//var fileObj = nlapiCreateFile('discount_file_' + saleFileId + '.json', 'PLAINTEXT', prop);		
		//fileObj.setFolder(252);
		//var fileId = nlapiSubmitFile(fileObj);
		
		html = '<html>';
		html += '<head>';
		html += '<script language="JavaScript">';
		html += 'if (window.opener)';
		html += '{'; 

		html += '  window.opener.isinited = true;';
		
		//html += "  window.opener.record.setSublistValue({sublistId: 'item',fieldId: 'custcol_eym_discount_json',line: 1,value: disc});";
		html += "  window.opener.record.setValue({fieldId: 'memo',value: disc});";
		//html += "  window.opener.nlapiSetFieldValue('custpage_part_no_2'," + assetPartNo + ");";
		//html += "  window.opener.nlapiSetFieldValue('custpage_part_desc_2','" + assetPartDesc + "');";
		
		html += '}';    
	    html += '';
	    html += '';
		
	    html += 'window.close();';
	    html += '</script>';
	    html += '</head>';
	    html += '<body>';
	    html += '</body>';
	    html += '</html>';

	    context.response.write(html);
    	
    	
    }

    return {
        onRequest: onRequest
    };
    
});
