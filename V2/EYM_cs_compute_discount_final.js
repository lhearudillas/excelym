/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/url', './lib/EYM_string_helper.js'],

function(record, url, stringHelperLib) {
	
	var sublistName = 'item';
	var isDiscountFld = 'custcol_eym_po_discounts';
	var grossAmntFld = 'grossamt';
	var totalGrossFld = 'custcol_eym_pur_item_base_amount';
    
    
	/**
     * Function to be executed when field is changed.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     * @param {string} scriptContext.fieldId - Field name
     * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
     * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
     *
     * @since 2015.2
     */
    function fieldChanged(scriptContext) {
    	
    	var sublistName = scriptContext.sublistId;
    	var sublistFieldName = scriptContext.fieldId;
    	var currentRecord = scriptContext.currentRecord;
    	var customForm = currentRecord.getValue({fieldId: 'customform'});
    	
    	if(sublistName == sublistName && sublistFieldName == isDiscountFld){
    		
    		var grossAmnt = Number( currentRecord.getCurrentSublistValue({ sublistId: sublistName, fieldId: grossAmntFld }) );
    		var rate = currentRecord.getCurrentSublistValue({ sublistId: sublistName, fieldId: 'rate' });
    		var totalGross = Number( currentRecord.getCurrentSublistValue({ sublistId: sublistName, fieldId: totalGrossFld }) );
    		if(totalGross == 0){
    			totalGross = grossAmnt;
    		}
    		var discountJson = currentRecord.getCurrentSublistValue({ sublistId: sublistName, fieldId: 'custcol_eym_discount_json' });
    		var vendor = currentRecord.getValue({fieldId: 'entity'});
    		
    		//if Custom Form = Foreign - - //custcol_eym_foreign_rate
    		var foreignAmnt = Number( currentRecord.getCurrentSublistValue({ sublistId: sublistName, fieldId: 'custcol_eym_foreign_amount' }) );
    		var foreignRate = Number( currentRecord.getCurrentSublistValue({ sublistId: sublistName, fieldId: 'custcol_eym_foreign_rate' }) );
    		if(customForm == 127){
    			totalGross = foreignAmnt;
    			rate = foreignRate;
    		}
    		
    		var item = currentRecord.getCurrentSublistValue({ sublistId: sublistName, fieldId: 'item' });
    		var lineNum = currentRecord.getCurrentSublistIndex({sublistId: 'item'});
    		var taxRate = currentRecord.getCurrentSublistValue({ sublistId: sublistName, fieldId: 'taxrate1' });
    		var qty = currentRecord.getCurrentSublistValue({ sublistId: sublistName, fieldId: 'quantity' });
    		var isChecked = currentRecord.getCurrentSublistValue({ sublistId: sublistName, fieldId: isDiscountFld });
    		if(isChecked == true){
    			//call suitelet
    			showPopup('customscript_sl_eym_discount_computation', 'customdeploy_sl_eym_discount_computation', 'gross= '+ totalGross+'&vendor='+vendor+'&file='+discountJson+'&form='+customForm+'&item='+item+'&row='+lineNum+'&tax='+taxRate+'&rate='+rate+'&qty='+qty , 1200, 600);
    		}else{
    			//call suitelet
    		}
    		
    	}
    	
    }

    
    function showPopup(scriptId, deploymentId, args, w, h)
    {	
    	var output = url.resolveScript({ scriptId: scriptId, deploymentId: deploymentId,returnExternalUrl: false});
    	if(args != null) {
    		output += '&';
    		output += args;
    	}
    	
    	windowOpener(output, w, h);
    }

    function windowOpener(url, a, b)
    {
    	newWindow = window.open(url, "PopUp", "resizable=1,width=" + a + ",height=" + b + ",left=" + (window.screen.width - a) / 2 + ",top=" + (window.screen.height - b) / 2 + ",scrollbars=yes");
    	newWindow.focus();
    	return newWindow.name
    }
    
    return {
        fieldChanged: fieldChanged,
    };
    
});
