/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(['N/record','./lib/EYM_string_helper.js'],
/**
 * @param {config} config
 * @param {record} record
 * @param {runtime} runtime
 * @param {transaction} transaction
 */
function(record, stringHelperLib) {
	
	var discountFld = 'custcol_eym_pur_itemdisc_amount';
	var baseUnitPrice = 'custcol_eym_pur_item_base_unit';
	var totalGrossFld = 'custcol_eym_pur_item_base_amount';
	var qtyFld = 'quantity';
	var amntFld = 'amount';
	var taxFld = 'taxrate1';
	var taxAmntFld = 'tax1amt';
	var grossFld = 'grossamt';
	var rateFld = 'rate';
	var itemFld = 'item';
	var sublistNm = 'item';
	var unitPriceIncVat = 'custcol_eym_po_gross_price';
	var foreignDiscFld = 'custcol_eym_po_forex_discount';
	var foreignXRate = 'custbody_eym_po_exrate';
	
	var changeDiscountFld = 'custcol_change_discount';
  	var grossRateFld = 'custcol_eym_po_gross_price';
	
    /**
     * Function to be executed after page is initialized.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.mode - The mode in which the record is being accessed (create, copy, or edit)
     *
     * @since 2015.2
     */
    function pageInit(scriptContext) {
    	
    }

    /**
     * Function to be executed when field is changed.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     * @param {string} scriptContext.fieldId - Field name
     * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
     * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
     *
     * @since 2015.2
     */
    function fieldChanged(scriptContext) {
    	
    	var currentRecord = scriptContext.currentRecord;
    	var sublistName = scriptContext.sublistId;
    	var sublistFieldName = scriptContext.fieldId;
    	var customForm = currentRecord.getValue({fieldId: 'customform'});
    	
    	if(customForm == 101){
    	
	    	if(sublistName == sublistNm) {
				if(sublistFieldName == itemFld || sublistFieldName == amntFld || sublistFieldName == qtyFld || sublistFieldName == rateFld){
					// Gross 
					var grossVal = currentRecord.getCurrentSublistValue({
		                sublistId: sublistName,
		                fieldId: grossFld
		            });
					
					if(stringHelperLib.IsNullOrEmpty(grossVal)) 
						grossVal = 0;
					
					// Discount
					var discountVal = currentRecord.getCurrentSublistValue({
		                sublistId: sublistName,
		                fieldId: discountFld
		            });
					
					if(stringHelperLib.IsNullOrEmpty(discountVal))
						discountVal = 0;
					
					var grossTotal = parseFloat(grossVal) + parseFloat(discountVal);
					currentRecord.setCurrentSublistValue({sublistId:sublistName ,fieldId: totalGrossFld, value:grossTotal, ignoreFieldChange: true});					
				}
				
				if(sublistFieldName == unitPriceIncVat) {
					
					//Unit Price Inc VAT (Gross price)
					var upIncVat = currentRecord.getCurrentSublistValue({
		                sublistId: sublistName,
		                fieldId: unitPriceIncVat
		            });
					
					//Quantity
					var qty = currentRecord.getCurrentSublistValue({
		                sublistId: sublistName,
		                fieldId: qtyFld
		            });
					
					var amountIncVat = Number(upIncVat * qty);
					currentRecord.setCurrentSublistValue({sublistId:sublistName ,fieldId: grossFld, value:amountIncVat, ignoreFieldChange: false});
					
				}
				
				if(sublistFieldName == discountFld) {
					
					// Gross 
					var grossVal = currentRecord.getCurrentSublistValue({
		                sublistId: sublistName,
		                fieldId: grossFld
		            });
					
					// Total Gross
					var totalGrossVal =  currentRecord.getCurrentSublistValue({
		                sublistId: sublistName,
		                fieldId: totalGrossFld
		            });
					
					if(stringHelperLib.IsNullOrEmpty(totalGrossVal)){
						totalGrossVal = grossVal;
						//Netsuite Add Mutliple functionality can't set the value for Total Gross
						currentRecord.setCurrentSublistValue({sublistId:sublistName, fieldId:totalGrossFld, value:grossVal, ignoreFieldChange:false});
					}
					
					
					// Discount
					var discountVal = currentRecord.getCurrentSublistValue({
		                sublistId: sublistName,
		                fieldId: discountFld
		            });
					
					if(stringHelperLib.IsNullOrEmpty(discountVal))
						discountVal = 0;
					
					var grossVal = parseFloat(totalGrossVal) - parseFloat(discountVal);
					
					changeDiscount = true;
					
					currentRecord.setCurrentSublistValue({sublistId:sublistName, fieldId:changeDiscountFld, value:true, ignoreFieldChange:true});
					currentRecord.setCurrentSublistValue({sublistId:sublistName, fieldId:grossFld, value:grossVal, ignoreFieldChange:false});
					
				}
				
				if(sublistFieldName == grossFld || sublistFieldName == taxFld) {
					// Gross 
					var grossVal = currentRecord.getCurrentSublistValue({
		                sublistId: sublistName,
		                fieldId: grossFld
		            });
					
					if(stringHelperLib.IsNullOrEmpty(grossVal)) 
						grossVal = 0;
					
					// Tax
					var taxVal = currentRecord.getCurrentSublistValue({
		                sublistId: sublistName,
		                fieldId: taxFld
		            });
					
					// Amount
					var amountVal = currentRecord.getCurrentSublistValue({
		                sublistId: sublistName,
		                fieldId: amntFld
		            });
											
					if(stringHelperLib.IsNullOrEmpty(amountVal))
						amountVal = 0;
					
					// Discount
					var discountVal = currentRecord.getCurrentSublistValue({
		                sublistId: sublistName,
		                fieldId: discountFld
		            });
					
					var changeDiscountVal = currentRecord.getCurrentSublistValue({
		                sublistId: sublistName,
		                fieldId: changeDiscountFld
		            });
												
					if(sublistFieldName == grossFld && changeDiscountVal == false) {
						discountVal = 0;
						currentRecord.setCurrentSublistValue({ sublistId:sublistName ,fieldId: discountFld, value:0, ignoreFieldChange: true});					
					}
					
					if(stringHelperLib.IsNullOrEmpty(discountVal))
						discountVal = 0;
											
					if(stringHelperLib.IsNullOrEmpty(amountVal))
						amountVal = 0;
					
					//Quantity
					var qty = currentRecord.getCurrentSublistValue({
		                sublistId: sublistName,
		                fieldId: qtyFld
		            });
					
					if(stringHelperLib.IsNullOrEmpty(qty))
						qty = 0;
					
					//var rateVal = getItemRate(amountVal, qty);
	              	var rateVal = getItemRate(amountVal, qty);
	              
					var grossTotal = parseFloat(grossVal) + parseFloat(discountVal);
	              	var grossRate = getItemRate(grossVal, qty);
	              
					
					currentRecord.setCurrentSublistValue({ sublistId:sublistName ,fieldId: rateFld, value:rateVal, ignoreFieldChange: true});
					currentRecord.setCurrentSublistValue({ sublistId:sublistName ,fieldId: totalGrossFld, value:grossTotal, ignoreFieldChange: true});
					currentRecord.setCurrentSublistValue({ sublistId:sublistName ,fieldId: changeDiscountFld, value:false, ignoreFieldChange: true});
	              
	              	currentRecord.setCurrentSublistValue({ sublistId:sublistName ,fieldId: grossRateFld, value:grossRate, ignoreFieldChange: true});
				}
				
				if(sublistFieldName == foreignDiscFld){
					
					// Foreign Discount
					var foreignDiscount = currentRecord.getCurrentSublistValue({
		                sublistId: sublistName,
		                fieldId: foreignDiscFld
		            });
											
					if(stringHelperLib.IsNullOrEmpty(foreignDiscount))
						foreignDiscount = 0;
					
					//PO Exchange Rate
					var poXRate = currentRecord.getValue({fieldId: foreignXRate});
					
					if(stringHelperLib.IsNullOrEmpty(poXRate))
						poXRate = 0;
					
					var pesoDiscount = parseFloat(foreignDiscount) * parseFloat(poXRate);
					currentRecord.setCurrentSublistValue({ sublistId:sublistName ,fieldId: discountFld, value:pesoDiscount, ignoreFieldChange: true});
					
				}
	    	}
    	
    	}
    }
    
    /**
     * Function to be executed after sublist is inserted, removed, or edited.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     *
     * @since 2015.2
     */
    function validateLine(scriptContext) {
    	
    	
    }

    
    function getItemAmount(gross, tax)
    {
	     var amount = 0.0;
	     
	     if(isNaN(tax)) {
	      tax = 0;
	     }
	     
	     if(!isNaN(gross) && parseFloat(gross) > 0) {
	      amount = parseFloat(gross) / (1 + (tax / 100));
	     }
	     
	     return amount;
    }
    
    function getItemRate(amount, quantity)
    {
	     var rate = 0.0;
	     
	    
	     if(!isNaN(amount) && parseFloat(amount)) {
			rate = parseFloat(amount) / quantity; 
	     }
	     
	     return rate;
    }

    return {
        //pageInit: pageInit,
        fieldChanged: fieldChanged,
        //validateLine: validateLine,
        //saveRecord: saveRecord
    };
    
});
