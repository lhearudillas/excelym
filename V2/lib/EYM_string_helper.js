define(function(){
    /**
     * Return true if the input variable is empty
     * @param stValue
     * @returns {boolean}
     */
   
    function IsNullOrEmpty(value) {
    	var isNullOrEmpty = true;
    	
    	if (value != undefined && value != null) {
    		if (typeof (value) == 'string') {
    			if (value.length > 0)
    				isNullOrEmpty = false;
    		}else {
    			isNullOrEmpty = false;
    		}
    	}		
    	
    	return isNullOrEmpty;
    }
    
    return {
    	IsNullOrEmpty: IsNullOrEmpty
    }
    
});