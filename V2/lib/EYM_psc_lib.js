define(['N/search'] , function(search){
    /**
     * Return true if the input variable is empty
     * @param stValue
     * @returns {boolean}
     */
   
    function IsNullOrEmpty(value) {
    	var isNullOrEmpty = true;
    	
    	if (value != undefined && value != null) {
    		if (typeof (value) == 'string') {
    			if (value.length > 0)
    				isNullOrEmpty = false;
    		}else {
    			isNullOrEmpty = false;
    		}
    	}		
    	
    	return isNullOrEmpty;
    }
    
    function getPreferences()
    {
    	//V2
    	var preferencesSrch = search.create({
		    type: 'customrecord_eym_preferences',
		    //id: 'customrecord_eym_preferences',
		    columns: [{	name: 'name'}, 
		    		  {name: 'custrecord_eym_pref_value'}
		    		 ],
    	});
		return preferencesSrch.run().getRange({start: 0,end: 1000});
		    
	}
    
    function getPreference(epRess, name, isArray, separator)
    {
    	var value = "";
    	
    	if(epRess == undefined)
    		epRess = null;
    	
    	if(isArray == undefined)
    		isArray = false;
    	
    	if(separator == undefined)
    		separator = ',';
    	
    	if(epRess != null){
    		for(var i = 0; i < epRess.length; i++) {	
    			if(epRess[i].getValue({name:'name'}) == name){
    				value = epRess[i].getValue({name:'custrecord_eym_pref_value'});
    				break;
    			}	
    		}
    	}
    	
    	if(isArray == true) {
    		if(isNullOrEmpty(value)) {
    			value = null;
    		}else {
    			value = value.split(separator);
    		}
    	}
    	
    	return value;
    }
    
    return {
    	IsNullOrEmpty: IsNullOrEmpty,
    	getPreferences: getPreferences,
    	getPreference: getPreference
    }
    
});